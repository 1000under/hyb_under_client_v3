webpackJsonp([13],{

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__set_others__ = __webpack_require__(402);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SetOthersPageModule", function() { return SetOthersPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SetOthersPageModule = (function () {
    function SetOthersPageModule() {
    }
    return SetOthersPageModule;
}());
SetOthersPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__set_others__["a" /* SetOthersPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__set_others__["a" /* SetOthersPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__set_others__["a" /* SetOthersPage */]
        ]
    })
], SetOthersPageModule);

//# sourceMappingURL=set-others.module.js.map

/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_device_device__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chats_count_chats_count__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_outer_link_outer_link__ = __webpack_require__(219);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SetOthersPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//-----> Providers





var SetOthersPage = (function () {
    function SetOthersPage(viewCtrl, navCtrl, navParams, modalCtrl, alertCtrl, actionSheetCtrl, http, events, deviceProvider, userInfoProvider, chatsCountProvider, outerLinkProvider, msgService, ga) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.http = http;
        this.events = events;
        this.deviceProvider = deviceProvider;
        this.userInfoProvider = userInfoProvider;
        this.chatsCountProvider = chatsCountProvider;
        this.outerLinkProvider = outerLinkProvider;
        this.msgService = msgService;
        this.ga = ga;
        this.memberURI = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";
        this.push_enable = ''; //1: YES, 2:NO
        //-------------------------------------------------> loginParams for userInfoProvider
        this.loginParams = [
            { loginApp: '' },
            { u_name: '' },
            { u_phone_num: '' },
            { u_email: '' },
            { u_pw: '' },
            { u_photo: '' },
            { u_type: '' },
            { cu_flag: false },
            { fb_flag: false },
            { kk_flag: false },
            { gg_flag: false },
            { login_flag: false },
            { isMember_flag: false },
        ];
        this.queryParams = 'email=' + this.userInfoProvider.userProfile.u_email + '&pw=' + this.userInfoProvider.userProfile.u_pw;
        var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.memberURI + "get_member.php";
        this.http.post(url, body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.version = data.member.version;
            if (data.member.push_enable == 'YES') {
                _this.isPush = true;
            }
            else {
                _this.isPush = false;
            }
        });
    }
    SetOthersPage.prototype.ionViewDidLoad = function () {
        this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
        console.log('ionViewDidLoad SetOthersPage');
    };
    SetOthersPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('환경설정'); //trackView for Google Analytics
    };
    SetOthersPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss(true);
    };
    SetOthersPage.prototype.checkPush = function () {
        var _this = this;
        console.log('this.isPush: ' + this.isPush);
        if (this.isPush == true) {
            this.push_enable = 'YES';
        }
        else {
            this.push_enable = 'NO';
        }
        this.queryParams = 'email=' + this.userInfoProvider.userProfile.u_email +
            '&pw=' + this.userInfoProvider.userProfile.u_pw +
            '&push_enable=' + this.push_enable;
        var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.memberURI + "set_member_modify.php";
        this.http.post(url, body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.result != 'ok') {
                var alert_1 = _this.alertCtrl.create({
                    title: '알림',
                    subTitle: _this.msgService.getMessage(data.result),
                    buttons: ['확인']
                });
                alert_1.present();
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: '알림',
                    subTitle: '정상 처리되었습니다.',
                    buttons: ['확인']
                });
                alert_2.present();
            } //====> (data.result == 'ok')
        });
        console.log('this.push_enable: ' + this.push_enable);
    };
    SetOthersPage.prototype.termsUse = function () {
        //data.policy_list.type == '이용';
        var termsUsePage_modal = this.modalCtrl.create('TermsUsePage', { termUseContent: this.outerLinkProvider.termUseContent });
        termsUsePage_modal.present();
    };
    SetOthersPage.prototype.logout = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: '로그아웃',
                    role: 'destructive',
                    handler: function () {
                        _this.queryParams = 'email=' + _this.userInfoProvider.userProfile.u_email +
                            '&pw=' + _this.userInfoProvider.userProfile.u_pw +
                            '&type=' + _this.userInfoProvider.userProfile.u_type;
                        var body = _this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = _this.memberURI + 'set_member_logout.php';
                        _this.http.post(url, body, options)
                            .map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            if (data.result != 'ok') {
                                var alert_3 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: _this.msgService.getMessage(data.result),
                                    buttons: ['확인']
                                });
                                alert_3.present();
                            }
                            else {
                                _this.loginParams.u_name = _this.userInfoProvider.userProfile.u_name;
                                _this.loginParams.u_phone_num = _this.userInfoProvider.userProfile.u_phone_num;
                                _this.loginParams.u_email = _this.userInfoProvider.userProfile.u_email;
                                _this.loginParams.u_pw = _this.userInfoProvider.userProfile.u_pw;
                                _this.loginParams.u_photo = _this.userInfoProvider.userProfile.u_photo;
                                _this.loginParams.u_type = _this.userInfoProvider.userProfile.u_type;
                                _this.loginParams.cu_flag = _this.userInfoProvider.userProfile.cu_flag;
                                _this.loginParams.fb_flag = _this.userInfoProvider.userProfile.fb_flag;
                                _this.loginParams.gg_flag = _this.userInfoProvider.userProfile.gg_flag;
                                _this.loginParams.kk_flag = _this.userInfoProvider.userProfile.kk_flag;
                                _this.loginParams.login_flag = false;
                                _this.loginParams.isMember_flag = true;
                                _this.userInfoProvider.setProfile(_this.loginParams);
                                _this.chatsCountProvider.logoutChatsCount(); //Reset 0 to chats count
                                var alert_4 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: '정상 처리되었습니다.',
                                    buttons: ['확인']
                                });
                                alert_4.present();
                                _this.viewCtrl.dismiss(false);
                            }
                        }); //====> (subscribe(data => {)
                    }
                }, {
                    text: '취소',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    SetOthersPage.prototype.memberLeave = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: '회원탈퇴',
                    role: 'destructive',
                    handler: function () {
                        _this.queryParams = 'email=' + _this.userInfoProvider.userProfile.u_email +
                            '&pw=' + _this.userInfoProvider.userProfile.u_pw;
                        var body = _this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = _this.memberURI + 'set_member_leave.php';
                        _this.http.post(url, body, options)
                            .map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            if (data.result != 'ok') {
                                var alert_5 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: _this.msgService.getMessage(data.result),
                                    buttons: ['확인']
                                });
                                alert_5.present();
                            }
                            else {
                                _this.userInfoProvider.clearProfile(); //Clear UserProfile
                                _this.chatsCountProvider.logoutChatsCount(); //Reset 0 to chats count
                                var alert_6 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: '정상 처리되었습니다.',
                                    buttons: ['확인']
                                });
                                alert_6.present();
                                _this.viewCtrl.dismiss(false);
                            }
                        }); //====> (subscribe(data => {)
                    }
                }, {
                    text: '취소',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    return SetOthersPage;
}());
SetOthersPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-set-others',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/set-others/set-others.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button icon-only color="underblack" (click)="closeModal()">\n        <ion-icon name="md-close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>환경설정</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class="action-sheets-page">\n  <ion-item no-lines>\n    <ion-label><span style="font-size: 13pt;">알림</span></ion-label>\n    <ion-toggle [(ngModel)]="isPush" color="1000Under" (ionChange)="checkPush()"></ion-toggle>\n  </ion-item>\n  <div class="spacer" style="height: 5px;"></div>\n  <div class="spacer" style="height: 1px; background-color:#eeeeee"></div>\n  <button class="btn-start" ion-item detail-none no-lines block clear (click)="termsUse()"><span style="font-size: 13pt;">이용약관</span>\n    <ion-icon name="ios-arrow-forward" item-end color="undergray"></ion-icon>\n  </button>\n  <div class="spacer" style="height: 5px;"></div>\n  <div class="spacer" style="height: 1px; background-color:#eeeeee"></div>\n  <ion-item no-lines>\n    <ion-label><span style="font-size: 13pt;">버전정보</span></ion-label>\n    <div item-content>\n      {{version}}\n    </div>\n  </ion-item>\n  <div class="spacer" style="height: 5px;"></div>\n  <div class="spacer" style="height: 1px; background-color:#eeeeee"></div>\n  <button class="btn-start" ion-item detail-none no-lines block clear (click)="logout()"><span style="font-size: 13pt;">로그아웃</span>\n    <ion-icon name="ios-arrow-forward" item-end color="undergray"></ion-icon>\n  </button>\n  <div class="spacer" style="height: 5px;"></div>\n  <div class="spacer" style="height: 1px; background-color:#eeeeee"></div>\n  <button class="btn-start" ion-item detail-none no-lines block clear (click)="memberLeave()"><span style="font-size: 13pt;">회원탈퇴</span>\n    <ion-icon name="ios-arrow-forward" item-end color="undergray"></ion-icon>\n  </button>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/set-others/set-others.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ActionSheetController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
        __WEBPACK_IMPORTED_MODULE_4__providers_device_device__["a" /* DeviceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_chats_count_chats_count__["a" /* ChatsCountProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_outer_link_outer_link__["a" /* OuterLinkProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], SetOthersPage);

//# sourceMappingURL=set-others.js.map

/***/ })

});
//# sourceMappingURL=13.main.js.map