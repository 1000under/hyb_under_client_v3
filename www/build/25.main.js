webpackJsonp([25],{

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__intro__ = __webpack_require__(387);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IntroPageModule", function() { return IntroPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var IntroPageModule = (function () {
    function IntroPageModule() {
    }
    return IntroPageModule;
}());
IntroPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__intro__["a" /* IntroPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__intro__["a" /* IntroPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__intro__["a" /* IntroPage */]
        ]
    })
], IntroPageModule);

//# sourceMappingURL=intro.module.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(53);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IntroPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var IntroPage = (function () {
    function IntroPage(navCtrl, viewCtrl, storage) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.storage = storage;
        this.tutorials = [
            { id: '01', imgPath: "img/intro/tutorial1.png" },
            { id: '02', imgPath: "img/intro/tutorial2.png" },
            { id: '03', imgPath: "img/intro/tutorial3.png" },
            { id: '04', imgPath: "img/intro/tutorial4.png" },
            { id: '05', imgPath: "img/intro/tutorial5.png" },
            { id: '06', imgPath: "img/intro/tutorial6.png" },
            { id: '07', imgPath: "img/intro/tutorial7.png" },
            { id: '08', imgPath: "img/intro/tutorial8.png" }
        ];
        this.options = {
            show: false
        };
    }
    IntroPage.prototype.doNotShow = function () {
        if (!this.options.show) {
            //Save flag if don't need to show intro slider on next app startup
            this.storage.set('showIntro', 0).then(function () {
                // we don't actually need this callbacks in such simple example,
                // but in real apps it will be useful
            });
        }
        this.viewCtrl.dismiss();
    };
    IntroPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return IntroPage;
}());
IntroPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-intro',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/intro/intro.html"*/'<ion-header>\n    <div class="head-space"></div>\n</ion-header>\n<ion-content>\n  <div>\n    <ion-slides pager class="top-slides">\n      <ion-slide *ngFor="let t of tutorials" class="top-slide top-image-wrapper">\n        <img [src]="t.imgPath" />\n      </ion-slide>\n    </ion-slides>\n    <ion-fab left top>\n      <button ion-fab (click)="dismiss()" icon-start style="height: 35px;\n              color: #000000; background-color: rgba( 255, 255, 255, 0.0 ); box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.0), 0 0 0 rgba(0, 0, 0, 0.0);">\n              <ion-icon name="md-close"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab right top>\n      <button ion-fab (click)="doNotShow()" icon-end style="height: 35px; width: 120px;\n              color: #000000; background-color: rgba( 255, 255, 255, 0.0 ); box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.0), 0 0 0 rgba(0, 0, 0, 0.0);">다시 보지 않기&nbsp;\n              <ion-icon name="ios-checkbox-outline"></ion-icon>\n      </button>\n    </ion-fab>\n  </div>\n</ion-content>\n\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/intro/intro.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], IntroPage);

//# sourceMappingURL=intro.js.map

/***/ })

});
//# sourceMappingURL=25.main.js.map