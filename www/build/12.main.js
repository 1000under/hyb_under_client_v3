webpackJsonp([12],{

/***/ 333:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__similar_car__ = __webpack_require__(404);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SimilarCarPageModule", function() { return SimilarCarPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SimilarCarPageModule = (function () {
    function SimilarCarPageModule() {
    }
    return SimilarCarPageModule;
}());
SimilarCarPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__similar_car__["a" /* SimilarCarPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__similar_car__["a" /* SimilarCarPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__similar_car__["a" /* SimilarCarPage */]
        ]
    })
], SimilarCarPageModule);

//# sourceMappingURL=similar-car.module.js.map

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_device_device__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_outer_link_outer_link__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_maker_model_maker_model__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_setanalytics_setanalytics__ = __webpack_require__(218);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SimilarCarPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


 //-------> Connect to server .php



//-----> Providers






var SimilarCarPage = (function () {
    function SimilarCarPage(platform, viewCtrl, navCtrl, navParams, alertCtrl, modalCtrl, http, events, msgService, deviceProvider, userInfoProvider, makerModelProvider, socialSharing, geolocation, ga, outerLinkProvider, setAnalytics) {
        this.platform = platform;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.events = events;
        this.msgService = msgService;
        this.deviceProvider = deviceProvider;
        this.userInfoProvider = userInfoProvider;
        this.makerModelProvider = makerModelProvider;
        this.socialSharing = socialSharing;
        this.geolocation = geolocation;
        this.ga = ga;
        this.outerLinkProvider = outerLinkProvider;
        this.setAnalytics = setAnalytics;
        this.marketURI = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";
        this.imageViewMoreTabFlag = false;
        this.contactUsFlag = false;
        this.reportFlag = false;
        this.navigationFlag = false;
        this.sunLoopFlag = false;
        this.blackBoxFlag = false;
        this.warrantyFlag = false;
        this.rearSensorFlag = false;
        this.startButtonFlag = false;
        this.rearCameraFlag = false;
        this.heatingSeatFlag = false;
        this.photos = []; // for Image more view
        this.photo_path = []; //*ngFor="let pp of photo_path"
        this.lookUpDoc = '조회가능';
        this.address_lat = 0;
        this.address_lon = 0;
        this.outerLink_loan = [];
        this.outerLink_ins = [];
        this.selectedCar = this.navParams.get("carParam");
    }
    SimilarCarPage.prototype.ionViewDidLoad = function () {
        this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
        console.log('ionViewDidLoad SimilarCarPage');
    };
    SimilarCarPage.prototype.ionViewDidEnter = function () {
        this.carsReflashFlag = false;
        this.topOrBottom = this.contentHandle._tabsPlacement;
        this.contentBox = document.querySelector(".scroll-content")['style'];
        if (this.topOrBottom == "top") {
            this.tabBarHeight = this.contentBox.marginTop;
        }
        else if (this.topOrBottom == "bottom") {
            this.tabBarHeight = this.contentBox.marginBottom;
        }
        var set_key = this.selectedCar.key;
        var set_type = '1'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
        this.setAnalytics.setAnalytics(set_key, set_type);
        for (var i = 0; i < this.selectedCar.photo_list.length; i++) {
            if (this.selectedCar.photo_list[i].type == '9') {
                this.registImgPath = this.selectedCar.photo_list[i].path; //photo_list.type: 9(차량원부)
            }
            else if (this.selectedCar.photo_list[i].type == '10') {
                this.repairImgPath = this.selectedCar.photo_list[i].path; //photo_list.type: 10(사고이력)
            }
            else {
                this.photo_path.push(this.selectedCar.photo_list[i]);
            }
        }
        this.car_number = this.selectedCar.car_number;
        this.repPhoto = this.selectedCar.photo_list[0].path;
        this.mileage = this.selectedCar.mileage;
        this.repair = this.selectedCar.repair;
        if (this.selectedCar.seizure == '0') {
            this.seizure = '무';
        }
        else {
            this.seizure = '유';
        }
        this.accident_count = this.selectedCar.accident_count;
        this.transfer_ownership = this.selectedCar.transfer_ownership;
        this.price = this.selectedCar.price;
        this.under_price = this.selectedCar.d_price - this.selectedCar.price;
        this.regist_type = this.selectedCar.regist_type;
        this.state = this.selectedCar.state;
        this.maker = this.selectedCar.maker;
        this.model = this.selectedCar.model;
        this.regist_date = this.selectedCar.regist_date.substr(0, 4) + '년' + this.selectedCar.regist_date.substr(5, 2) + '월' + this.selectedCar.regist_date.substr(8, 2) + '일';
        this.view_count = this.selectedCar.view_count;
        this.favorite_count = Number(this.selectedCar.favorite_count);
        this.model_level = this.selectedCar.model_level;
        this.is_favorite = this.selectedCar.is_favorite;
        this.is_report = this.selectedCar.is_report;
        this.year = this.selectedCar.year;
        this.month = this.selectedCar.month;
        this.puer = this.selectedCar.puer;
        this.gear = this.selectedCar.gear;
        this.model_type = this.selectedCar.model_type;
        this.color = this.selectedCar.color;
        if (this.selectedCar.options != null) {
            this.options = this.selectedCar.options; //--> 주요옵션 [1:네비게이션, 2:선루프, 3:블랙박스, 4:제조사보증, 5:후방센서, 6:START버튼, 7:후방카메라, 8:열선시트]
            var optionsArray = this.selectedCar.options.split(",");
            for (var i = 0; i <= optionsArray.length; i++) {
                switch (optionsArray[i]) {
                    case "1": {
                        this.navigationFlag = true;
                        break;
                    }
                    case "2": {
                        this.sunLoopFlag = true;
                        break;
                    }
                    case "3": {
                        this.blackBoxFlag = true;
                        break;
                    }
                    case "4": {
                        this.warrantyFlag = true;
                        break;
                    }
                    case "5": {
                        this.rearSensorFlag = true;
                        break;
                    }
                    case "6": {
                        this.startButtonFlag = true;
                        break;
                    }
                    case "7": {
                        this.rearCameraFlag = true;
                        break;
                    }
                    case "8": {
                        this.heatingSeatFlag = true;
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        }
        this.additory_options = this.selectedCar.additory_options;
        this.repair = this.selectedCar.repair;
        this.replacement = this.selectedCar.replacement;
        if (this.selectedCar.address != null) {
            this.address = this.selectedCar.address.replace("대한민국", "");
            var strArray = this.selectedCar.address.split(' ');
            this.location = strArray[2];
            this.address_lat = this.selectedCar.address_lat;
            this.address_lon = this.selectedCar.address_lon;
        }
        else {
            this.address = "차량의 위치 확인이 필요합니다.";
            this.location = "수내동"; //lat: 37.3713177, lng: 127.1223533
            this.address_lat = 37.3713177;
            this.address_lon = 127.1223533;
        }
        this.memo = this.selectedCar.memo;
        this.email = this.selectedCar.email;
        this.phone = this.selectedCar.phone;
        this.minPrice = 1000;
        this.maxPrice = 2000;
        this.carPrice = {
            lower: 1400,
            upper: 1700
        };
        this.outerLink_loan = this.outerLinkProvider.outerLinkList[0].link;
        this.outerLink_ins = this.outerLinkProvider.outerLinkList[1].link;
        //console.log('this.outerLink_ins: '+this.outerLink_ins);
        this.maker = this.makerModelProvider.getMakerName(this.selectedCar.maker); //--> 제조사명
        this.model = this.makerModelProvider.getModelName(this.selectedCar.model); //--> 모델명
        this.loadMap();
        this.ga.trackView('유사차량상세: ' + this.maker + '-' + this.model + ' ' + this.car_number); //trackView for Google Analytics
    };
    SimilarCarPage.prototype.loadMap = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (position) {
            var latLng = new google.maps.LatLng(_this.address_lat, _this.address_lon);
            var mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false,
                fullscreenControl: false
            };
            var element = document.getElementById('smap');
            _this.map = new google.maps.Map(element, mapOptions);
            var marker = new google.maps.Marker({
                map: _this.map,
                animation: google.maps.Animation.DROP,
                position: _this.map.getCenter()
            });
            marker.setVisible(true);
        }).catch(function (error) {
            console.log('Error getting location' + error);
        });
    };
    SimilarCarPage.prototype.selectFavoriteCar = function () {
        var _this = this;
        if (this.userInfoProvider.userProfile.login_flag == false) {
            var LoginPage_modal = this.modalCtrl.create('LoginPage', { naviPage: 'SimilarCarPage' });
            LoginPage_modal.present();
        }
        else {
            if (this.is_favorite == '0') {
                var favoriteQueryParams = 'sale=' + this.selectedCar.key +
                    '&email=' + this.userInfoProvider.userProfile.u_email +
                    '&pw=' + this.userInfoProvider.userProfile.u_pw +
                    '&favorite=' + '1'; //'1'=찜등록, '2'=찜취소
                var body = favoriteQueryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "set_sale_favorite.php";
                this.http.post(url, body, options)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    if (data.result != 'ok') {
                        var alert_1 = _this.alertCtrl.create({
                            title: '알림',
                            subTitle: _this.msgService.getMessage(data.result),
                            buttons: ['확인']
                        });
                        alert_1.present();
                    }
                    else {
                        _this.ga.trackEvent('유사차량상세', '찜: ' + _this.maker + '-' + _this.model + ' ' + _this.car_number); //trackEvent for Google Analytics
                        var alert_2 = _this.alertCtrl.create({
                            title: '알림',
                            subTitle: '선택하신 차량이 찜 목록에 저장되었습니다.',
                            buttons: ['확인']
                        });
                        alert_2.present();
                        _this.is_favorite = '1';
                        _this.favorite_count = _this.favorite_count + 1;
                        var set_key = _this.selectedCar.key;
                        var set_type = '2'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                        _this.setAnalytics.setAnalytics(set_key, set_type);
                        _this.carsReflashFlag = true;
                        _this.events.publish('cars:refrash', _this.carsReflashFlag);
                    }
                });
            }
            else {
                //console.log('car.key car.is_favorite: '+this.cars[i].key+'-'+this.cars[i].is_favorite);
                var favoriteQueryParams = 'sale=' + this.selectedCar.key +
                    '&email=' + this.userInfoProvider.userProfile.u_email +
                    '&pw=' + this.userInfoProvider.userProfile.u_pw +
                    '&favorite=' + '2'; //'1'=찜등록, '2'=찜취소
                var body = favoriteQueryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "set_sale_favorite.php";
                this.http.post(url, body, options)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    if (data.result != 'ok') {
                        var alert_3 = _this.alertCtrl.create({
                            title: '알림',
                            subTitle: _this.msgService.getMessage(data.result),
                            buttons: ['확인']
                        });
                        alert_3.present();
                    }
                    else {
                        var alert_4 = _this.alertCtrl.create({
                            title: '알림',
                            subTitle: '선택하신 차량이 찜 목록에서 삭제되었습니다.',
                            buttons: ['확인']
                        });
                        alert_4.present();
                        _this.is_favorite = '0';
                        _this.favorite_count = _this.favorite_count - 1;
                        var set_key = _this.selectedCar.key;
                        var set_type = '3'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                        _this.setAnalytics.setAnalytics(set_key, set_type);
                        _this.carsReflashFlag = true;
                        _this.events.publish('cars:refrash', _this.carsReflashFlag);
                    }
                });
            }
        }
    };
    SimilarCarPage.prototype.socialShare = function () {
        var _this = this;
        var image = this.repPhoto;
        var app_link = this.deviceProvider.deviceInfo.s_link;
        /**
         * Shares using the share sheet
         * @param message {string} The message you would like to share.
         * @param subject {string} The subject
         * @param file {string|string[]} URL(s) to file(s) or image(s), local path(s) to file(s) or image(s), or base64 data of an image. Only the first file/image will be used on Windows Phone.
         * @param url {string} A URL to share
         * @returns {Promise<any>}
         * share(message?: string, subject?: string, file?: string | string[], url?: string): Promise<any>;
         */
        this.socialSharing.share('믿고 맡기는 중고차 안심 직거래 천언더!!!', '믿고 맡기는 중고차 안심 직거래 천언더!!!', image, app_link).then(function () {
            _this.ga.trackEvent('유사차량상세', '공유: ' + _this.maker + '-' + _this.model + ' ' + _this.car_number); //trackEvent for Google Analytics
            //console.log('Shared via any App !!!');
        }).catch(function () {
            console.log('Sharing via any App is impossible');
            var alert = _this.alertCtrl.create({
                title: '알림',
                subTitle: '공유에 실패했습니다.',
                buttons: ['확인']
            });
            alert.present();
        });
    };
    SimilarCarPage.prototype.imageViewMore = function (eachImgPath) {
        var _this = this;
        if (!this.imageViewMoreTabFlag) {
            this.imageViewMoreTabFlag = true;
            var initialSlide = 0;
            for (var i = 0; i < this.photo_path.length; i++) {
                if (this.photo_path[i].path == eachImgPath) {
                    initialSlide = i;
                }
            }
            var ImageViewMore_modal = this.modalCtrl.create('ImageViewMorePage', {
                photos: this.photo_path,
                initialSlide: initialSlide,
            });
            ImageViewMore_modal.onDidDismiss(function (data) {
                _this.imageViewMoreTabFlag = false;
                _this.photo_path = [];
                for (var i = 0; i < _this.selectedCar.photo_list.length; i++) {
                    if (_this.selectedCar.photo_list[i].type == '9') {
                        _this.registImgPath = _this.selectedCar.photo_list[i].path; //photo_list.type: 9(차량원부)
                    }
                    else if (_this.selectedCar.photo_list[i].type == '10') {
                        _this.repairImgPath = _this.selectedCar.photo_list[i].path; //photo_list.type: 10(사고이력)
                    }
                    else {
                        _this.photo_path.push(_this.selectedCar.photo_list[i]);
                    }
                }
            });
            ImageViewMore_modal.present();
        }
    };
    SimilarCarPage.prototype.repairImgView = function (repairImg) {
        var _this = this;
        if (repairImg == undefined) {
            var alert_5 = this.alertCtrl.create({
                title: '알림',
                subTitle: '차량 수리이력 정보가 입력되지 않았습니다.',
                buttons: ['확인']
            });
            alert_5.present();
        }
        else {
            if (!this.imageViewMoreTabFlag) {
                this.imageViewMoreTabFlag = true;
                this.ga.trackEvent('유사차량상세', '수리이력조회: ' + this.maker + '-' + this.model + ' ' + this.car_number); //trackEvent for Google Analytics
                var ImageViewMore_modal = this.modalCtrl.create('ImageViewMorePage', {
                    photos: repairImg,
                    initialSlide: 0,
                });
                ImageViewMore_modal.onDidDismiss(function (data) {
                    _this.imageViewMoreTabFlag = false;
                });
                ImageViewMore_modal.present();
            }
        }
    };
    SimilarCarPage.prototype.registImgView = function (registImg) {
        var _this = this;
        if (registImg == undefined) {
            var alert_6 = this.alertCtrl.create({
                title: '알림',
                subTitle: '차량원부 정보가 입력되지 않았습니다.',
                buttons: ['확인']
            });
            alert_6.present();
        }
        else {
            if (!this.imageViewMoreTabFlag) {
                this.imageViewMoreTabFlag = true;
                this.ga.trackEvent('유사차량상세', '원부조회: ' + this.maker + '-' + this.model + ' ' + this.car_number); //trackEvent for Google Analytics
                var ImageViewMore_modal = this.modalCtrl.create('ImageViewMorePage', {
                    photos: registImg,
                    initialSlide: 0,
                });
                ImageViewMore_modal.onDidDismiss(function (data) {
                    _this.imageViewMoreTabFlag = false;
                });
                ImageViewMore_modal.present();
            }
        }
    };
    SimilarCarPage.prototype.report = function () {
        var _this = this;
        if (this.userInfoProvider.userProfile.login_flag == false) {
            var LoginPage_modal = this.modalCtrl.create('LoginPage', { naviPage: 'SimilarCarPage' });
            LoginPage_modal.present();
        }
        else {
            var ReportPage_modal = this.modalCtrl.create('ReportPage', {
                sale: this.selectedCar.key,
                email: this.userInfoProvider.userProfile.u_email,
                pw: this.userInfoProvider.userProfile.u_pw
            });
            ReportPage_modal.onDidDismiss(function (data) {
                _this.is_report = data;
            });
            ReportPage_modal.present();
        }
    };
    SimilarCarPage.prototype.loanLimit = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: '대출 한도를 알고싶으세요?',
            message: '귀하의 신용도에 따른 대출금 한도를 알려드립니다.',
            buttons: [
                {
                    text: '취소',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: '문의하기',
                    handler: function () {
                        if (_this.userInfoProvider.userProfile.login_flag == false) {
                            var LoginPage_modal = _this.modalCtrl.create('LoginPage', { naviPage: 'SimilarCarPage' });
                            LoginPage_modal.present();
                        }
                        else {
                            _this.ga.trackEvent('유사차량상세', '대출한도문의: ' + _this.maker + '-' + _this.model + ' ' + _this.car_number); //trackEvent for Google Analytics
                            var ChatBoardPage_modal = _this.modalCtrl.create('ChatBoardPage', {
                                email: _this.userInfoProvider.userProfile.u_email,
                                pw: _this.userInfoProvider.userProfile.u_pw,
                                target_email: _this.email //천언더 담당자 email
                            });
                            ChatBoardPage_modal.present();
                        }
                    }
                }
            ]
        });
        confirm.present();
    };
    SimilarCarPage.prototype.insuranceQustion = function () {
        this.ga.trackEvent('유사차량상세', '보험문의: ' + this.maker + '-' + this.model + ' ' + this.car_number); //trackEvent for Google Analytics
    };
    SimilarCarPage.prototype.chatWithUnder = function () {
        if (this.userInfoProvider.userProfile.login_flag == false) {
            var LoginPage_modal = this.modalCtrl.create('LoginPage', { naviPage: 'SimilarCarPage' });
            LoginPage_modal.present();
        }
        else {
            var set_key = this.selectedCar.key;
            var set_type = '5'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
            this.setAnalytics.setAnalytics(set_key, set_type);
            this.ga.trackEvent('유사차량상세', '채팅하기: ' + this.maker + '-' + this.model + ' ' + this.car_number); //trackEvent for Google Analytics
            var ChatBoardPage_modal = this.modalCtrl.create('ChatBoardPage', {
                email: this.userInfoProvider.userProfile.u_email,
                pw: this.userInfoProvider.userProfile.u_pw,
                target_email: this.email //천언더 담당자 email
            });
            ChatBoardPage_modal.present();
        }
    };
    SimilarCarPage.prototype.callToUnder = function () {
        var set_key = this.selectedCar.key;
        var set_type = '6'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
        this.setAnalytics.setAnalytics(set_key, set_type);
        this.ga.trackEvent('유사차량상세', '전화하기: ' + this.maker + '-' + this.model + ' ' + this.car_number); //trackEvent for Google Analytics
        //this.phone = '220886460';
        //let temp_phone = this.phone.split('-');
        //let temp_phone_length = temp_phone.length;
        //let temp_phone_lastStart = temp_phone_length - 4;
        console.log('this.phone: ' + this.phone);
        var temp_phone = 'tel:' + this.phone;
        //let temp_phone = 'tel:+82-'+this.phone.substr(1, 1)+'-'+this.phone.substr(2, 4)+'-'+this.phone.substr(6, 4);
        console.log('temp_phone: ' + temp_phone);
        window.location = temp_phone;
    };
    SimilarCarPage.prototype.scrollingCotroll = function (e) {
        if (e.scrollTop > this.contentHandle.getContentDimensions().contentHeight) {
            if (this.topOrBottom == "top") {
                this.contentBox.marginTop = 0;
            }
            else if (this.topOrBottom == "bottom") {
                this.contentBox.marginBottom = 0;
            }
        }
        else {
            if (this.topOrBottom == "top") {
                this.contentBox.marginTop = this.tabBarHeight;
            }
            else if (this.topOrBottom == "bottom") {
                this.contentBox.marginBottom = this.tabBarHeight;
            }
        }
    };
    SimilarCarPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return SimilarCarPage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Content */])
], SimilarCarPage.prototype, "content", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("contentRef"),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Content */])
], SimilarCarPage.prototype, "contentHandle", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */])
], SimilarCarPage.prototype, "slides", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('map'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], SimilarCarPage.prototype, "mapElement", void 0);
SimilarCarPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-similar-car',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/similar-car/similar-car.html"*/'<ion-header>\n  <div class="head-space"></div>\n</ion-header>\n<ion-content #contentRef (ionScroll)="scrollingCotroll($event)">\n<div>\n  <ion-slides pager paginationType="fraction" class="top-slides"> <!--  effect="coverflow" : slide, fade, cube, coverflow, flip-->\n    <ion-slide *ngFor="let pp of photo_path" class="top-slide">\n      <div id="top-container" tappable (click)="imageViewMore(pp.path)">\n        <img [src]="pp.path"/>\n      </div>\n    </ion-slide>\n  </ion-slides>\n  <ion-fab left top>\n    <button ion-fab (click)="dismiss()"\n            style="color: #ffffff; background-color: rgba( 255, 255, 255, 0.0 ); box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.0), 0 0 0 rgba(0, 0, 0, 0.0);">\n            <ion-icon name="ios-arrow-back"></ion-icon>\n    </button>\n  </ion-fab>\n  <div *ngIf="state !== \'완료\'">\n    <ion-fab center top>\n      <div *ngIf="is_favorite === \'0\'">\n        <button ion-fab (click)="selectFavoriteCar()"\n                style="color: #ffffff; background-color: rgba( 255, 255, 255, 0.0 ); box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.0), 0 0 0 rgba(0, 0, 0, 0.0);">\n                <ion-icon name="heart"></ion-icon>\n        </button>\n      </div>\n      <div *ngIf="is_favorite === \'1\'">\n        <button ion-fab (click)="selectFavoriteCar()"\n                style="color: #37aafe; background-color: rgba( 255, 255, 255, 0.0 ); box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.0), 0 0 0 rgba(0, 0, 0, 0.0);">\n                <ion-icon name="heart"></ion-icon>\n        </button>\n      </div>\n    </ion-fab>\n    <ion-fab right top>\n      <button ion-fab (click)="socialShare()"\n              style="color: #ffffff; background-color: rgba( 255, 255, 255, 0.0 ); box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.0), 0 0 0 rgba(0, 0, 0, 0.0);">\n        <ion-icon name="md-share"></ion-icon>\n      </button>\n    </ion-fab>\n  </div>\n</div>\n\n<ion-grid>\n  <ion-row style="height:45px;">\n    <ion-col col-8>\n      <h5 align="left">&nbsp;{{price | number:0}}만원 {{maker}} {{model}}</h5>\n    </ion-col>\n    <ion-col col-4>\n      <ion-item *ngIf="is_report !== \'1\'" no-lines align="center" style="padding-left: 10px;">\n        <button ion-button item-right small outline color="buttongray" style="margin-right: 0px; color:#626262;" (click)="report()">\n            <span style="font-size: 9pt;">신고하기</span></button>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-8 align="left">\n      &nbsp;{{location}} | {{year}}년{{month}}월 | {{mileage | number:0}}km  \n    </ion-col>\n    <ion-col col-4 align="right">\n      <ion-icon name="ios-eye-outline" item-left><span style="font-size: 11pt;">{{view_count | number:0}}</span></ion-icon>\n      <ion-icon name="ios-heart-outline" item-left><span style="font-size: 11pt;">{{favorite_count | number:0}}&nbsp;&nbsp;</span></ion-icon>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n\n<div *ngIf="regist_type != \'0\' && regist_type != \'0\'"> <!-- 유료 등록 차량-->\n  <div class="spacer" style="height: 1px; background-color:#eeeeee"></div>\n  <ion-grid>\n    <ion-row align="center">\n      <ion-col col-3>\n        <img tappable src="img/carDetail/accident_count.png" style="width: 80%; height: auto;" (click)="repairImgView(repairImgPath)"/>\n      </ion-col>\n      <ion-col col-3>\n        <img tappable src="img/carDetail/regist.png" style="width: 80%; height: auto;" (click)="registImgView(registImgPath)"/>\n      </ion-col>\n      <ion-col col-3>\n        <div class="item image-border" align="center">\n          <p align="center" class="textOver">{{seizure}}</p>\n          <img src="img/carDetail/seizure.png" style="width: 80%; height: auto;"/>\n        </div>\n      </ion-col>\n      <ion-col col-3>\n        <div class="item image-border" align="center">\n          <p align="center" class="textOver">{{transfer_ownership}}회</p>\n          <img src="img/carDetail/transfer_ownership.png" style="width: 80%; height: auto;"/>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <div class="spacer" style="height: 1px; background-color:#eeeeee" ></div>\n  <h5 align="center">딜러를 통한 구매 대비 약 {{under_price | number:0}}만원 저렴합니다.</h5>\n</div>\n\n<div class="spacer" style="height: 10px; background-color:#eeeeee" ></div>\n<h5>&nbsp;&nbsp;&nbsp;차량 정보</h5>\n<div class="spacer" style="height: 15px;" ></div>\n<ion-grid>\n  <ion-row>\n    <ion-col col-6 align="left"><span style="font-size: 12pt; color:#626262;">&nbsp;차량번호</span></ion-col>\n    <ion-col col-6 align="right"><span style="font-size: 12pt;">{{car_number}}&nbsp;</span></ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-6 align="left"><span style="font-size: 12pt; color:#626262;">&nbsp;연식</span></ion-col>\n    <ion-col col-6 align="right"><span style="font-size: 12pt;">{{year}}년{{month}}월&nbsp;</span></ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-6 align="left"><span style="font-size: 12pt; color:#626262;">&nbsp;주행거리</span></ion-col>\n    <ion-col col-6 align="right"><span style="font-size: 12pt;">{{mileage | number:0}}km&nbsp;</span></ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-6 align="left"><span style="font-size: 12pt; color:#626262;">&nbsp;연료/변속기</span></ion-col>\n    <ion-col col-6 align="right"><span style="font-size: 12pt;">{{puer}}/{{gear}}&nbsp;</span></ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-6 align="left"><span style="font-size: 12pt; color:#626262;">&nbsp;차종/색상</span></ion-col>\n    <ion-col col-6 align="right"><span style="font-size: 12pt;">{{model_type}}/{{color}}&nbsp;</span></ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-6 align="left"><span style="font-size: 12pt; color:#626262;">&nbsp;차량등급</span></ion-col>\n    <ion-col col-6 align="right"><span style="font-size: 12pt;">{{model_level}}&nbsp;</span></ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-6 align="left"><span style="font-size: 12pt; color:#626262;">&nbsp;등록일자</span></ion-col>\n    <ion-col col-6 align="right"><span style="font-size: 12pt;">{{regist_date}}&nbsp;</span></ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-3 align="left"><span style="font-size: 12pt; color:#626262;">&nbsp;추가옵션</span></ion-col>\n    <ion-col col-9 align="right"><span style="font-size: 12pt;">{{additory_options}}&nbsp;</span></ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-3 align="left"><span style="font-size: 12pt; color:#626262;">&nbsp;수리내역</span></ion-col>\n    <ion-col col-9 align="right"><span style="font-size: 12pt;">{{repair}}&nbsp;</span></ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-3 align="left"><span style="font-size: 12pt; color:#626262;">&nbsp;교체내역</span></ion-col>\n    <ion-col col-9 align="right"><span style="font-size: 12pt;">{{replacement}}&nbsp;</span></ion-col>\n  </ion-row>\n</ion-grid>\n<div class="spacer" style="height: 15px;" ></div>\n<div class="spacer" style="height: 10px; background-color:#eeeeee"></div>\n<ion-item-group>\n  <!-- [1:네비게이션, 2:선루프, 3:블랙박스, 4:제조사보증, 5:후방센서, 6:START버튼, 7:후방카메라, 8:열선시트] -->\n  <h5>&nbsp;&nbsp;&nbsp;주요옵션</h5>\n  <div class="spacer" style="height: 15px;" ></div>\n  <ion-grid>\n    <ion-row align="center">\n      <ion-col col-3 *ngIf="navigationFlag"><img src="img/option/navigation_on.png" style="width: 80%; height: auto;"/></ion-col>\n      <ion-col col-3 *ngIf="!navigationFlag"><img src="img/option/navigation_off.png" style="width: 80%; height: auto;"></ion-col>\n      <ion-col col-3 *ngIf="sunLoopFlag"><img src="img/option/sunLoop_on.png" style="width: 80%; height: auto;"/></ion-col>\n      <ion-col col-3 *ngIf="!sunLoopFlag"><img src="img/option/sunLoop_off.png" style="width: 80%; height: auto;"/></ion-col>\n      <ion-col col-3 *ngIf="blackBoxFlag"><img src="img/option/blackBox_on.png" style="width: 80%; height: auto;"/></ion-col>\n      <ion-col col-3 *ngIf="!blackBoxFlag"><img src="img/option/blackBox_off.png" style="width: 80%; height: auto;"/></ion-col>\n      <ion-col col-3 *ngIf="warrantyFlag"><img src="img/option/warranty_on.png" style="width: 80%; height: auto;"/></ion-col>\n      <ion-col col-3 *ngIf="!warrantyFlag"><img src="img/option/warranty_off.png" style="width: 80%; height: auto;"/></ion-col>\n    </ion-row>\n    <ion-row align="center"> \n      <ion-col col-3 *ngIf="rearSensorFlag"><img src="img/option/rearSensor_on.png" style="width: 80%; height: auto;"/></ion-col>\n      <ion-col col-3 *ngIf="!rearSensorFlag"><img src="img/option/rearSensor_off.png" style="width: 80%; height: auto;"/></ion-col>      \n      <ion-col col-3 *ngIf="startButtonFlag"><img src="img/option/startButton_on.png" style="width: 80%; height: auto;"/></ion-col>\n      <ion-col col-3 *ngIf="!startButtonFlag"><img src="img/option/startButton_off.png" style="width: 80%; height: auto;"/></ion-col>\n      <ion-col col-3 *ngIf="rearCameraFlag"><img src="img/option/rearCamera_on.png" style="width: 80%; height: auto;"/></ion-col>\n      <ion-col col-3 *ngIf="!rearCameraFlag"><img src="img/option/rearCamera_off.png" style="width: 80%; height: auto;"/></ion-col> \n      <ion-col col-3 *ngIf="heatingSeatFlag"><img src="img/option/heatingSeat_on.png" style="width: 80%; height: auto;"/></ion-col>\n      <ion-col col-3 *ngIf="!heatingSeatFlag"><img src="img/option/heatingSeat_off.png" style="width: 80%; height: auto;"/></ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-item-group>\n<div class="spacer" style="height: 15px;" ></div>\n<div class="spacer" style="height: 10px; background-color:#eeeeee"></div>\n\n<h5>&nbsp;&nbsp;&nbsp;차량 위치</h5>\n<div class="spacer" style="height: 15px;" ></div>\n<ion-item no-lines>{{address}}</ion-item>\n<div map id="smap" class="map"></div>\n<div class="spacer" style="height: 10px;"></div>\n<div class="spacer" style="height: 10px; background-color:#eeeeee"></div>\n\n<h5>&nbsp;&nbsp;&nbsp;내차 소개</h5>\n<div class="spacer" style="height: 15px;" ></div>\n<!--\n<video *ngIf="u_link !== null" style="border: 5px; padding-left: 10px; padding-right: 10px;" width="100%" controls>\n  <source [src]="u_link" type="video/mp4">\n</video>\n-->\n<ion-item no-lines text-wrap>\n  <p style="white-space: pre-wrap; color: black; font-size: 12pt;">{{memo}}</p>\n</ion-item>\n<div class="spacer" style="height: 15px;" ></div>\n\n<div class="spacer" style="height: 10px; background-color:#eeeeee"></div>\n<div class="spacer" style="height: 5px;" ></div>\n<button style="height: 40px;" ion-item no-lines (click)="loanLimit()">내 대출한도는 얼마인가요?</button>\n<div class="spacer" style="height: 5px;" ></div>\n\n<div class="spacer" style="height: 10px; background-color:#eeeeee"></div>\n<div class="spacer" style="height: 5px;" ></div>\n<a style="height: 40px;" ion-item no-lines [href]="outerLink_ins" (click)="insuranceQustion()">이 차의 보험료가 궁금하세요?</a>\n<div class="spacer" style="height: 5px;" ></div>\n\n<div class="spacer" style="height: 10px; background-color:#eeeeee"></div>\n<div class="spacer" style="height: 15px;" ></div>\n\n<ion-grid>\n  <ion-row>\n    <ion-col col-6 style="padding-top: 0px; padding-bottom: 0px;">\n      <button ion-button block color=\'1000Under\' icon-left (click)="callToUnder()">전화하기</button>\n    </ion-col>\n    <ion-col col-6 style="padding-top: 0px; padding-bottom: 0px;">\n      <button ion-button block color=\'1000Under\' icon-left (click)="chatWithUnder()">채팅하기</button>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n<br>\n<ion-fab class=\'myfab1\' left bottom>\n    <button ion-fab mini style="color: #000000; background-color: #ffffff; " (click)="dismiss()"><ion-icon name="arrow-back"></ion-icon></button>\n</ion-fab>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/similar-car/similar-car.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
        __WEBPACK_IMPORTED_MODULE_6__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_device_device__["a" /* DeviceProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_10__providers_maker_model_maker_model__["a" /* MakerModelProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_analytics__["a" /* GoogleAnalytics */],
        __WEBPACK_IMPORTED_MODULE_9__providers_outer_link_outer_link__["a" /* OuterLinkProvider */],
        __WEBPACK_IMPORTED_MODULE_11__providers_setanalytics_setanalytics__["a" /* SetanalyticsProvider */]])
], SimilarCarPage);

//# sourceMappingURL=similar-car.js.map

/***/ })

});
//# sourceMappingURL=12.main.js.map