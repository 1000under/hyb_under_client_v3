webpackJsonp([8],{

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__update_password__ = __webpack_require__(409);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatePasswordPageModule", function() { return UpdatePasswordPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UpdatePasswordPageModule = (function () {
    function UpdatePasswordPageModule() {
    }
    return UpdatePasswordPageModule;
}());
UpdatePasswordPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__update_password__["a" /* UpdatePasswordPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__update_password__["a" /* UpdatePasswordPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__update_password__["a" /* UpdatePasswordPage */]
        ]
    })
], UpdatePasswordPageModule);

//# sourceMappingURL=update-password.module.js.map

/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdatePasswordPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//-----> Providers


var UpdatePasswordPage = (function () {
    function UpdatePasswordPage(navCtrl, navParams, viewCtrl, alertCtrl, modalCtrl, http, userInfoProvider, msgService, ga) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.userInfoProvider = userInfoProvider;
        this.msgService = msgService;
        this.ga = ga;
        this.memberURI = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";
        //-------------------------------------------------> loginParams for userInfoProvider
        this.loginParams = [
            { loginApp: '' },
            { u_name: '' },
            { u_phone_num: '' },
            { u_email: '' },
            { u_pw: '' },
            { u_photo: '' },
            { u_type: '' },
            { cu_flag: false },
            { fb_flag: false },
            { kk_flag: false },
            { gg_flag: false },
            { login_flag: false },
            { isMember_flag: false },
        ];
        this.userProfile_pw = '';
        this.currentPassword = '';
        this.newPassword = '';
        this.confirmNewPassword = '';
        this.isSuccess = false;
        this.userProfile_pw = navParams.get("userProfile_pw");
    }
    UpdatePasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UpdatePasswordPage');
    };
    UpdatePasswordPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('비밀번호변경'); //trackView for Google Analytics
    };
    UpdatePasswordPage.prototype.submitUpdate = function () {
        var _this = this;
        var chkPwdResualt = chkPwd(this.newPassword);
        if (this.currentPassword == '') {
            var alert_1 = this.alertCtrl.create({
                title: '알림!',
                subTitle: '현재 비밀번호를 입력 해 주세요.',
                buttons: ['확인']
            });
            alert_1.present();
        }
        else if (this.newPassword == '') {
            var alert_2 = this.alertCtrl.create({
                title: '알림!',
                subTitle: '새 비밀번호를 입력 해 주세요.',
                buttons: ['확인']
            });
            alert_2.present();
        }
        else if (this.confirmNewPassword == '') {
            var alert_3 = this.alertCtrl.create({
                title: '알림!',
                subTitle: '새 비밀번호 확인을 입력 해 주세요.',
                buttons: ['확인']
            });
            alert_3.present();
        }
        else if (this.currentPassword != this.userProfile_pw) {
            var alert_4 = this.alertCtrl.create({
                title: '알림!',
                subTitle: '"현재 비밀번호"가 올바르지 않습니다.',
                buttons: ['확인']
            });
            alert_4.present();
        }
        else if (this.newPassword != this.confirmNewPassword) {
            var alert_5 = this.alertCtrl.create({
                title: '알림!',
                subTitle: '"비밀번호 확인"이 올바르지 않습니다.',
                buttons: ['확인']
            });
            alert_5.present();
        }
        else if (chkPwdResualt != 'good') {
            var alert_6 = this.alertCtrl.create({
                title: '알림!',
                subTitle: chkPwdResualt,
                buttons: ['확인']
            });
            alert_6.present();
        }
        else {
            console.log('<------------------ Input Items ---------------->');
            console.log('pw: ' + this.userProfile_pw);
            this.queryParams = 'email=' + this.userInfoProvider.userProfile.u_email +
                '&pw=' + this.userInfoProvider.userProfile.u_pw +
                '&rpw=' + this.confirmNewPassword;
            //----------------------------------------------------------------> get Similar cars
            var body_s = this.queryParams, type_s = 'application/x-www-form-urlencoded; charset=UTF-8', headers_s = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type_s }), options_s = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers_s }), url_s = this.memberURI + "set_member_modify.php";
            this.http.post(url_s, body_s, options_s)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    var alert_7 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: _this.msgService.getMessage(data.result),
                        buttons: ['확인']
                    });
                    alert_7.present();
                }
                else {
                    _this.loginParams.loginApp = '1000Under';
                    _this.loginParams.u_name = _this.userInfoProvider.userProfile.u_name;
                    _this.loginParams.u_phone_num = _this.userInfoProvider.userProfile.u_phone_num;
                    _this.loginParams.u_email = _this.userInfoProvider.userProfile.u_email;
                    _this.loginParams.u_pw = _this.confirmNewPassword;
                    _this.loginParams.u_type = '1'; //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                    _this.loginParams.cu_flag = true;
                    _this.loginParams.login_flag = true;
                    _this.loginParams.isMember_flag = true;
                    _this.userInfoProvider.setProfile(_this.loginParams);
                    var alert_8 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: '비밀번호 변경이 완료되었습니다.',
                        buttons: ['확인']
                    });
                    alert_8.present();
                    _this.isSuccess = true;
                    _this.viewCtrl.dismiss(_this.isSuccess);
                }
            });
        }
        function chkPwd(str) {
            var pw = str;
            var num = pw.search(/[0-9]/g);
            var eng = pw.search(/[a-z]/ig);
            //let spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);
            if (pw.length < 6 || pw.length > 15) {
                return '영.숫자 포함 6자리 ~ 15자리 이내로 입력해주세요.';
            }
            if (pw.search(/₩s/) != -1) {
                return '비밀번호는 공백없이 입력해주세요.';
            }
            if (num < 0 || eng < 0) {
                return '영문.숫자를 혼합하여 입력해주세요.';
            }
            return 'good';
        }
        ;
    };
    UpdatePasswordPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.loginParams);
    };
    return UpdatePasswordPage;
}());
UpdatePasswordPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-update-password',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/update-password/update-password.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button icon-only color="underblack" (click)="dismiss()">\n        <ion-icon name="ios-arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>비밀번호 변경</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class="spacer" style="height: 10px;"></div>\n  <div padding style="padding-left: 10px; padding-right: 10px;">\n    <h6 align="left">비밀번호 입력</h6>\n    <p align="left" style="color:#999999">변경할 비밀번호를 입력하세요.</p>\n    <div class="spacer" style="height: 10px;"></div>\n    <div class="spacer" style="height: 1px; background-color:#eeeeee"></div>\n    <div class="spacer" style="height: 20px;"></div>\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-input [(ngModel)]="currentPassword" type="password" placeholder="현재 비밀번호(영문,숫자조합 6~15자)"></ion-input>\n      </ion-item>\n    </div>\n    <div class="spacer" style="height: 5px;"></div>\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-input [(ngModel)]="newPassword" type="password" placeholder="새 비밀번호(영문,숫자조합 6~15자)"></ion-input>\n      </ion-item>\n    </div>\n    <div class="spacer" style="height: 5px;"></div>\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-input [(ngModel)]="confirmNewPassword" type="password" placeholder="새 비밀번호 확인"></ion-input>\n      </ion-item>\n    </div>\n    <div class="spacer" style="height: 10px;"></div>\n    <button ion-button block color=\'1000Under\' (click)="submitUpdate()">확인</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/update-password/update-password.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_4__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], UpdatePasswordPage);

//# sourceMappingURL=update-password.js.map

/***/ })

});
//# sourceMappingURL=8.main.js.map