webpackJsonp([28],{

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__favorite__ = __webpack_require__(384);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavoritePageModule", function() { return FavoritePageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FavoritePageModule = (function () {
    function FavoritePageModule() {
    }
    return FavoritePageModule;
}());
FavoritePageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__favorite__["a" /* FavoritePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__favorite__["a" /* FavoritePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__favorite__["a" /* FavoritePage */]
        ]
    })
], FavoritePageModule);

//# sourceMappingURL=favorite.module.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_fcm__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chats_count_chats_count__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_maker_model_maker_model__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_setanalytics_setanalytics__ = __webpack_require__(218);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavoritePage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//-----> Providers





var FavoritePage = (function () {
    /*
    private tabBarHeight;
    private topOrBottom:string;
    private contentBox;
    */
    function FavoritePage(platform, navCtrl, navParams, loadingCtrl, alertCtrl, modalCtrl, http, app, events, userInfoProvider, chatsCountProvider, makerModelProvider, msgService, setAnalytics, fcm, ga) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.app = app;
        this.events = events;
        this.userInfoProvider = userInfoProvider;
        this.chatsCountProvider = chatsCountProvider;
        this.makerModelProvider = makerModelProvider;
        this.msgService = msgService;
        this.setAnalytics = setAnalytics;
        this.fcm = fcm;
        this.ga = ga;
        this.showFavorite = false;
        this.carDetailTabFlag = false;
        //-------------------------------------------------> for data query
        this.cars = [];
        this.queryLength = 0;
        this.doInfiniteFlag = true;
        this.marketURI = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";
    }
    FavoritePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.events.subscribe('cars:refrash', function (reflashFlag) {
            if (reflashFlag == true) {
                _this.getFavoriteCars();
            }
        });
    };
    FavoritePage.prototype.ionViewDidLoad = function () {
        this.ga.trackView('찜'); //trackView for Google Analytics
        console.log('----------> GoogleAnalytics trackView: FavoritePage');
        this.login_flag = this.userInfoProvider.userProfile.login_flag;
        this.todayDate = new Date().toISOString().slice(0, 10); //dateFormat --> '2017-08-31'
        this.getFavoriteCars();
        console.log('ionViewDidLoad FavoriteCarsPage');
    };
    FavoritePage.prototype.ionViewDidEnter = function () {
        this.login_flag = this.userInfoProvider.userProfile.login_flag;
        this.carsReflashFlag = false;
        this.initPushNotification();
        this.getFavoriteCars();
        console.log('ionViewDidEnter FavoriteCarsPage');
    };
    FavoritePage.prototype.initPushNotification = function () {
        var _this = this;
        this.fcm.onNotification().subscribe(function (data) {
            if (_this.login_flag == true) {
                if (!data.wasTapped) {
                    if (data.type == 'chat_new') {
                        var dataObj = JSON.stringify(data);
                        //console.log('==============> Notification Data: '+dataObj);
                        if (_this.userInfoProvider.userProfile.login_flag) {
                            _this.chatsCountProvider.checkChatsCount(_this.userInfoProvider.userProfile.login_flag, _this.userInfoProvider.userProfile.u_email, _this.userInfoProvider.userProfile.u_pw);
                            var msgObj = JSON.parse(data.message);
                            var target_email = msgObj.target_email.replace(/\'/g, ''); //특정문자 제거
                            var target_name = msgObj.target_name.replace(/\'/g, ''); //특정문자 제거
                            //console.log('notification target_email====> '+target_email);
                            var notificationModal_modal_1 = _this.modalCtrl.create('NotificationModalPage', {
                                target_email: target_email,
                                target_name: target_name
                            });
                            var closedByTimeout_1 = false;
                            var timeoutHandle_1 = setTimeout(function () { closedByTimeout_1 = true; notificationModal_modal_1.dismiss(); }, 5000);
                            notificationModal_modal_1.onDidDismiss(function () {
                                if (closedByTimeout_1)
                                    return; //timeout 동안 action이 없으면 return
                                clearTimeout(timeoutHandle_1);
                            });
                            notificationModal_modal_1.present();
                        }
                    } //data.type == 'chat_new'
                }
                ;
            }
        });
    };
    FavoritePage.prototype.getFavoriteCars = function () {
        var _this = this;
        this.queryParams = 'order=1' + '&email=' + this.userInfoProvider.userProfile.u_email; // order=최신&email=zinns58@gmail.com
        var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_favorite.php";
        this.http.post(url, body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.result != 'ok') {
                _this.showFavorite = false;
            }
            else {
                _this.cars = [];
                for (var i = 0; i < Object.keys(data.sale_list).length; i++) {
                    data.sale_list[i].maker = _this.makerModelProvider.getMakerName(data.sale_list[i].maker);
                    data.sale_list[i].model = _this.makerModelProvider.getModelName(data.sale_list[i].model);
                    // 거래중 blind_date:"2017-08-31 00:00:00" >= this.todayDate
                    // console.log('blind_date: '+data.sale_list[i].blind_date.substr(0, 10));
                    if (data.sale_list[i].blind_date.substr(0, 10) >= _this.todayDate) {
                        data.sale_list[i].state = '거래중';
                    }
                    var strArray = data.sale_list[i].address.split(' ');
                    data.sale_list[i].address = strArray[2];
                    _this.cars.push(data.sale_list[i]);
                }
                _this.queryLength = Object.keys(data.sale_list).length;
                console.log('getFavoriteCars() ===> this.queryLength: ' + _this.queryLength);
                if (_this.queryLength >= 20) {
                    _this.doInfiniteFlag = true;
                }
                else {
                    _this.doInfiniteFlag = false;
                }
                _this.showFavorite = true;
            }
        });
    };
    FavoritePage.prototype.checkLogin = function () {
        var _this = this;
        if (this.userInfoProvider.userProfile.login_flag == false) {
            var LoginPage_modal = this.modalCtrl.create('LoginPage', { naviPage: 'FavoriteCarsPage' });
            LoginPage_modal.onDidDismiss(function (data) {
                _this.login_flag = data;
                /*
                if(this.login_flag == true) {
                  //this.app.getRootNav().getActiveChildNav().select(2);  // goto tab3Root - FavoritePage
                  this.navCtrl.setRoot(this.navCtrl.getActive().component);
                }
                */
            });
            LoginPage_modal.present();
        }
    };
    FavoritePage.prototype.naviCarDetailPage = function (selectedCar) {
        var _this = this;
        if (!this.carDetailTabFlag) {
            this.carDetailTabFlag = true;
            if (this.userInfoProvider.userProfile.login_flag == true) {
                this.queryParams = 'sale=' + selectedCar + '&email=' + this.userInfoProvider.userProfile.u_email;
            }
            else {
                this.queryParams = 'sale=' + selectedCar;
            }
            var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_detail.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    var alert_1 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: _this.msgService.getMessage(data.result),
                        buttons: ['확인']
                    });
                    alert_1.present();
                }
                else {
                    var CarDetailPage_modal = _this.modalCtrl.create('CarDetailPage', { carParam: data.sale_detail });
                    CarDetailPage_modal.onDidDismiss(function (data) {
                        _this.carDetailTabFlag = false;
                    });
                    CarDetailPage_modal.present();
                }
            });
        }
    };
    FavoritePage.prototype.deleteMyFavoriteCar = function (carKey) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: '찜목록에서 삭제하시겠습니까?',
            message: '"삭제"를 선택하면 찜목록에서 데이터가 삭제됩니다.',
            buttons: [
                {
                    text: '취소',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: '삭제',
                    handler: function () {
                        var favoriteQueryParams = 'sale=' + carKey +
                            '&email=' + _this.userInfoProvider.userProfile.u_email +
                            '&pw=' + _this.userInfoProvider.userProfile.u_pw +
                            '&favorite=' + '2'; //'1'=찜등록, '2'=찜취소
                        var body = favoriteQueryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = _this.marketURI + "set_sale_favorite.php";
                        _this.http.post(url, body, options)
                            .map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            if (data.result != 'ok') {
                                var alert_2 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: _this.msgService.getMessage(data.result),
                                    buttons: ['확인']
                                });
                                alert_2.present();
                            }
                            else {
                                var set_key = carKey;
                                var set_type = '3'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                                _this.setAnalytics.setAnalytics(set_key, set_type);
                                _this.carsReflashFlag = true;
                                _this.events.publish('cars:refrash', _this.carsReflashFlag);
                                //this.navCtrl.setRoot(this.navCtrl.getActive().component);
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    FavoritePage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        if (this.doInfiniteFlag == true) {
            var body = this.queryParams + '&key=' + this.queryLength, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_favorite.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    _this.doInfiniteFlag = false;
                }
                else {
                    for (var i = 0; i < Object.keys(data.sale_list).length; i++) {
                        data.sale_list[i].maker = _this.makerModelProvider.getMakerName(data.sale_list[i].maker);
                        data.sale_list[i].model = _this.makerModelProvider.getModelName(data.sale_list[i].model);
                        // 거래중 blind_date:"2017-08-31 00:00:00" >= this.todayDate
                        // console.log('blind_date: '+data.sale_list[i].blind_date.substr(0, 10));
                        if (data.sale_list[i].blind_date.substr(0, 10) >= _this.todayDate) {
                            data.sale_list[i].state = '거래중';
                        }
                        var strArray = data.sale_list[i].address.split(' ');
                        data.sale_list[i].address = strArray[2];
                        _this.cars.push(data.sale_list[i]);
                    }
                    if (Object.keys(data.pick_detail_list).length < 20) {
                        _this.doInfiniteFlag = false;
                    }
                    _this.queryLength = _this.queryLength + Object.keys(data.sale_list).length;
                    console.log('this.queryLength: ' + _this.queryLength);
                }
                //console.log('Next operation has ended');
                infiniteScroll.complete();
            });
        }
    };
    return FavoritePage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Content */])
], FavoritePage.prototype, "content", void 0);
FavoritePage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-favorite',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/favorite/favorite.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="primary">\n    <ion-title>찜</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div *ngIf="showFavorite && login_flag">\n      <div align="center">    \n        <div no-margin no-padding *ngFor="let car of cars">\n          <div *ngIf="car.photo_height > 500" class="image-wrapper">\n            <div *ngIf="car.state === \'판매중\'">\n              <div class="pcar-image"> \n                <div class="centered">\n                  <img [src]="car.photo_path" tappable (click)="naviCarDetailPage(car.key)"/>\n                </div>\n              </div>\n            </div>\n            <div *ngIf="car.state === \'검수\'">\n              <div class="pcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                                  \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n                <div class="centered">\n                  <img src="img/state/inspecting.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n                </div>\n              </div>\n            </div>\n            <div *ngIf="car.state === \'거래중\'">\n              <div class="pcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                                  \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n                <div class="centered">\n                  <img src="img/state/dealing.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n                </div>\n              </div>\n            </div>\n            <div *ngIf="car.state === \'완료\'">\n              <div class="pcar-image" [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                                \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n                <div class="centered">\n                  <img src="img/state/sold.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div *ngIf="car.photo_height <= 500" class="image-wrapper">\n            <div *ngIf="car.state === \'판매중\'">\n              <div class="lcar-image"> \n                <div class="centered">\n                  <img [src]="car.photo_path" tappable (click)="naviCarDetailPage(car.key)"/>\n                </div>\n              </div>\n            </div>\n            <div *ngIf="car.state === \'검수\'">\n              <div class="lcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                                  \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n                <div class="centered">\n                  <img src="img/state/inspecting.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n                </div>\n              </div>\n            </div>\n            <div *ngIf="car.state === \'거래중\'">\n              <div class="lcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                                  \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n                <div class="centered">\n                  <img src="img/state/dealing.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n                </div>\n              </div>\n            </div>\n            <div *ngIf="car.state === \'완료\'">\n              <div class="lcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                                  \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n                <div class="centered">\n                  <img src="img/state/sold.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n                </div>\n              </div>\n            </div>\n          </div>\n          <ion-row style="height: 45px;">\n            <ion-col col-10>\n              <h5 align="left">&nbsp;{{car.price | number:0}}만원 {{car.maker}} {{car.model}}</h5>\n            </ion-col>\n            <ion-col col-2>\n              <button ion-button  color="1000Under" clear icon-only (click)="deleteMyFavoriteCar(car.key)"><ion-icon mini name="heart"></ion-icon></button>\n            </ion-col>\n          </ion-row>            \n          <ion-row align="left">\n            <ion-col col-10>\n              &nbsp;{{car.address}} | {{car.year}}년 | {{car.mileage | number:0}}km |  \n              <ion-icon name="ios-eye-outline" item-left><span style="font-size: 11pt;">{{car.view_count | number:0}}</span></ion-icon>\n            </ion-col>\n          </ion-row>\n          <br>\n          <div class="spacer" style="height: 10px; background-color: #eeeeee;"></div>\n        </div>\n      </div>\n  </div>\n  <div *ngIf="showFavorite && !login_flag">\n      <div class="spacer" style="height: 160px;"></div>\n      <div align="center"><img src="img/notLoginFavorite.png" style="height: 15%; width: 15%"/></div>\n      <h5 align="center">마음에 드는 차량을 찜해보세요!</h5>\n      <p align="center"><span style="font-size: 10pt; color:#999999">국내 최대 중고차 안심직거래앱 천언더<br>지금 바로 로그인하시고 마음에 드는 차량을 찜하세요!</span></p>\n      <div align="center">\n        <button ion-button color="1000Under" style="box-shadow: none !important;" (click)="checkLogin()">천언더 로그인</button>\n      </div>\n  </div>\n  <div *ngIf="!showFavorite && login_flag">\n      <div class="spacer" style="height: 160px;"></div>\n      <div align="center"><img src="img/isLoginFavorite.png" style="height: 15%; width: 15%"/></div>\n      <h5 align="center">현재 찜한 차량이 없습니다.</h5>\n      <p align="center"><span style="font-size: 10pt; color:#999999">국내 최대 중고차 안심직거래앱 천언더<br>내 차가 될 미래의 차를 찜해보세요! 미래가 현실이 됩니다.</span></p>\n  </div>\n  <div *ngIf="!showFavorite && !login_flag">\n      <div class="spacer" style="height: 160px;"></div>\n      <div align="center"><img src="img/notLoginFavorite.png" style="height: 15%; width: 15%"/></div>\n      <h5 align="center">마음에 드는 차량을 찜해보세요!</h5>\n      <p align="center"><span style="font-size: 10pt; color:#999999">국내 최대 중고차 안심직거래앱 천언더<br>지금 바로 로그인하시고 마음에 드는 차량을 찜하세요!</span></p>\n      <div align="center">\n        <button ion-button color="1000Under" style="box-shadow: none !important;" (click)="checkLogin()">천언더 로그인</button>\n      </div>\n  </div>\n  <ion-infinite-scroll *ngIf="doInfiniteFlag" (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/favorite/favorite.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* App */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
        __WEBPACK_IMPORTED_MODULE_5__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_chats_count_chats_count__["a" /* ChatsCountProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_maker_model_maker_model__["a" /* MakerModelProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_9__providers_setanalytics_setanalytics__["a" /* SetanalyticsProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_fcm__["a" /* FCM */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], FavoritePage);

//# sourceMappingURL=favorite.js.map

/***/ })

});
//# sourceMappingURL=28.main.js.map