webpackJsonp([30],{

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chat_board__ = __webpack_require__(382);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatBoardPageModule", function() { return ChatBoardPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ChatBoardPageModule = (function () {
    function ChatBoardPageModule() {
    }
    return ChatBoardPageModule;
}());
ChatBoardPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__chat_board__["a" /* ChatBoardPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__chat_board__["a" /* ChatBoardPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__chat_board__["a" /* ChatBoardPage */]
        ]
    })
], ChatBoardPageModule);

//# sourceMappingURL=chat-board.module.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_fcm__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_chats_count_chats_count__ = __webpack_require__(112);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatBoardPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//-----> Providers & Pipes



//declare var FCMPlugin;
var ChatBoardPage = (function () {
    function ChatBoardPage(platform, viewCtrl, navCtrl, alertCtrl, params, http, events, zone, userInfoProvider, msgService, chatsCountProvider, fcm, ga) {
        this.platform = platform;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.params = params;
        this.http = http;
        this.events = events;
        this.zone = zone;
        this.userInfoProvider = userInfoProvider;
        this.msgService = msgService;
        this.chatsCountProvider = chatsCountProvider;
        this.fcm = fcm;
        this.ga = ga;
        this.messageURI = "http://cjsekfvm.cafe24.com/test_server/app/message/url/";
        this.message_detail_list = [];
        //-------------------------------------------------> loginParams for userInfoProvider
        this.loginParams = [
            { loginApp: '' },
            { u_name: '' },
            { u_phone_num: '' },
            { u_email: '' },
            { u_pw: '' },
            { u_photo: '' },
            { u_type: '' },
            { cu_flag: false },
            { fb_flag: false },
            { kk_flag: false },
            { gg_flag: false },
            { login_flag: false },
            { isMember_flag: false },
        ];
        this.u_email = this.params.get('email');
        this.u_pw = this.params.get('pw');
        this.target_email = this.params.get('target_email');
    }
    ChatBoardPage.prototype.ionViewDidLoad = function () {
        this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
        this.currentPage = 'ChatBoardPage';
        this.getMessage(this.target_email);
        //this.initPushNotification();
        console.log('ionViewDidLoad ChatBoardPage');
    };
    ChatBoardPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('채팅보드'); //trackView for Google Analytics
        this.scrollToBottom();
        this.initPushNotification();
    };
    ChatBoardPage.prototype.initPushNotification = function () {
        var _this = this;
        this.fcm.onNotification().subscribe(function (data) {
            if (!data.wasTapped) {
                if (data.type == 'chat_new') {
                    //alert('Received message at ChatBoardPage in Foreground');
                    if (_this.currentPage == 'ChatBoardPage') {
                        var msgObj = JSON.parse(data.message);
                        var target_email = msgObj.target_email.replace(/\'/g, ''); //특정문자 제거
                        _this.reflashPage(target_email);
                    }
                }
            }
            ;
        });
    };
    ChatBoardPage.prototype.reflashPage = function (target_email) {
        var _this = this;
        this.zone.run(function () {
            _this.getMessage(target_email);
            console.log('Force update ======> ChatBoardPage');
        });
    };
    ChatBoardPage.prototype.getMessage = function (target_email) {
        var _this = this;
        this.queryParams = 'email=' + this.u_email + '&pw=' + this.u_pw + '&target_email=' + target_email;
        var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.messageURI + "get_message_detail.php";
        this.http.post(url, body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.result != 'ok') {
                console.log('Nothing message: ' + data.result);
            }
            else {
                _this.message_detail_list = data.message_detail_list;
                _this.message_detail_list.sort(function (a, b) {
                    if (a.regist_date < b.regist_date)
                        return -1;
                    if (a.regist_date > b.regist_date)
                        return 1;
                    return 0;
                });
                for (var i = 0; i < _this.message_detail_list.length; i++) {
                    //2017-08-09 11:48:37 --> date: 'MM/dd H:mm'
                    var temp_date = _this.message_detail_list[i].regist_date.substr(5, 2) + '/' + _this.message_detail_list[i].regist_date.substr(8, 2) + ' '
                        + _this.message_detail_list[i].regist_date.substr(11, 2) + ':' + _this.message_detail_list[i].regist_date.substr(14, 2);
                    _this.message_detail_list[i].regist_date = temp_date;
                }
                _this.message_text = '';
                console.log('Got messages detail !! ');
                //this.scrollToBottom();
                setTimeout(function () { return _this.scrollToBottom(); }, 0);
            }
        });
    };
    ChatBoardPage.prototype.scrollToBottom = function () {
        var _this = this;
        setTimeout(function () {
            _this.content.scrollToBottom();
        });
    };
    ChatBoardPage.prototype.input_focus = function (e) {
        var _this = this;
        setTimeout(function () { return _this.scrollToBottom(); }, 0);
    };
    ChatBoardPage.prototype.chatSend = function () {
        var _this = this;
        if (this.message_text != undefined && this.message_text != '') {
            this.queryParams = 'email=' + this.u_email + '&pw=' + this.u_pw + '&target_email=' + this.target_email + '&message_text=' + this.message_text;
            var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.messageURI + "set_message_add.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    var alert_1 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: _this.msgService.getMessage(data.result),
                        buttons: ['확인']
                    });
                    alert_1.present();
                }
                else {
                    console.log('Saved messages detail');
                    _this.reflashPage(_this.target_email);
                }
            });
        }
    };
    ChatBoardPage.prototype.dismiss = function () {
        this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
        this.viewCtrl.dismiss();
    };
    return ChatBoardPage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Content */])
], ChatBoardPage.prototype, "content", void 0);
ChatBoardPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-chat-board',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/chat-board/chat-board.html"*/'<ion-header>\n  <div class="head-space"></div>  \n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button icon-only color="underblack" (click)="dismiss()">\n        <ion-icon name="ios-arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>천언더</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content padding>\n  <ion-list no-lines>\n    <div *ngFor="let msg of message_detail_list">\n      <div *ngIf="u_email === msg.sender_email">\n        <ion-item *ngIf="msg.read_state == \'YES\'">\n          <p item-right style="margin-bottom: 0px; margin-top: 0px; padding-top: 0px; padding-bottom: 0px;">\n            {{msg.regist_date}}<span style="font-size: 8pt; color:#929292;"> {{msg.receiver_name}}(읽음)</span>\n          </p>\n        </ion-item>\n        <ion-item *ngIf="msg.read_state == \'NO\'">\n          <p item-right style="margin-bottom: 0px; margin-top: 0px; padding-top: 0px; padding-bottom: 0px;">\n            {{msg.regist_date}}<span style="font-size: 8pt; color:#929292;"> {{msg.receiver_name}}</span><span style="font-size: 8pt; color:#f53d3d;">(안읽음)</span>\n          </p>\n        </ion-item>\n        <ion-item>\n          <p class="triangle-right left" *ngIf="msg.message_text !== null"\n              item-right style="margin-bottom: 0px; margin-top: 0px; padding-top: 10px; padding-bottom: 10px;">\n            {{msg.message_text}}\n          </p>\n        </ion-item>\n      </div>\n      <div *ngIf="u_email !== msg.sender_email">\n        <ion-item>\n          <p item-left style="margin-bottom: 0px; margin-top: 0px; padding-top: 0px; padding-bottom: 0px;">\n            {{msg.sender_name}} {{msg.regist_date}}\n          </p>\n        </ion-item>\n        <ion-item>\n          <ion-avatar item-start style="margin-bottom: 0px; margin-top: 0px;">\n            <img src="img/1000under-logo.png">\n          </ion-avatar>\n          <p class="triangle-isosceles right" *ngIf="msg.message_text !== null"\n              item-left style="margin-bottom: 0px; margin-top: 0px; padding-top: 10px; padding-bottom: 10px;">\n            {{msg.message_text}}\n          </p>\n        </ion-item>\n      </div>\n    </div>\n  </ion-list>\n</ion-content>\n<ion-footer>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-10>\n        <ion-textarea class="input-cover" [(ngModel)]="message_text" type="text" (focus)="input_focus($event)" placeholder="입력하세요...."></ion-textarea>\n      </ion-col>\n      <ion-col col-2>\n        <button ion-button clear color="underblack" style="background-color:#ffffff" (click)="chatSend()">전송</button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/chat-board/chat-board.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
        __WEBPACK_IMPORTED_MODULE_5__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_chats_count_chats_count__["a" /* ChatsCountProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_fcm__["a" /* FCM */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], ChatBoardPage);

//# sourceMappingURL=chat-board.js.map

/***/ })

});
//# sourceMappingURL=30.main.js.map