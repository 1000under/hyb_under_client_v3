webpackJsonp([32],{

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cars__ = __webpack_require__(380);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarsPageModule", function() { return CarsPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CarsPageModule = (function () {
    function CarsPageModule() {
    }
    return CarsPageModule;
}());
CarsPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__cars__["a" /* CarsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cars__["a" /* CarsPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__cars__["a" /* CarsPage */]
        ]
    })
], CarsPageModule);

//# sourceMappingURL=cars.module.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_fcm__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_chats_count_chats_count__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_maker_model_maker_model__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_translate_translate__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_setanalytics_setanalytics__ = __webpack_require__(218);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarsPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//-----> Providers






var CarsPage = (function () {
    function CarsPage(platform, ionicApp, viewCtrl, navCtrl, modalCtrl, loadingCtrl, alertCtrl, fcm, http, events, socialSharing, ga, translateProvider, userInfoProvider, chatsCountProvider, msgService, makerModelProvider, setAnalytics, myElement) {
        this.platform = platform;
        this.ionicApp = ionicApp;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.fcm = fcm;
        this.http = http;
        this.events = events;
        this.socialSharing = socialSharing;
        this.ga = ga;
        this.translateProvider = translateProvider;
        this.userInfoProvider = userInfoProvider;
        this.chatsCountProvider = chatsCountProvider;
        this.msgService = msgService;
        this.makerModelProvider = makerModelProvider;
        this.setAnalytics = setAnalytics;
        this.myElement = myElement;
        this.carDetailTabFlag = false;
        //-------------------------------------------------> for hide and show Header
        this.start = 0;
        this.threshold = 100; //100, 200
        this.slideHeaderPrevious = 0;
        //-------------------------------------------------> for data query
        this.cars = [];
        this.queryLength = 0;
        this.doInfiniteFlag = true;
        this.marketURI = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";
        this.splash = true;
        this.searchBrand = [];
        this.searchBrandFlag = false;
        this.searchAreaFlag = false;
        this.searchResult = '모든 모델 ・ 모든 지역 ・ 추가 조건 검색'; //searchBrand + searchArea
        this.searchCondition = [];
        this.searchConditionFlag = false;
        this.popularActive = false;
        this.newActive = false;
    }
    CarsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.events.subscribe('cars:refrash', function (reflashFlag) {
            if (reflashFlag == true) {
                _this.getCars(_this.queryParams);
            }
        });
    };
    CarsPage.prototype.ionViewDidLoad = function () {
        this.presentLoading();
        this.translateProvider.SetTranslateService('kor');
        this.todayDate = new Date().toISOString().slice(0, 10); //dateFormat --> '2017-08-31'
        this.searchBrand = '제조사/모델명';
        this.searchArea = '지역';
        //this.searchResult = '모든 모델 ・ 모든 지역 ・ 추가 조건 검색';
        this.searchCondition = ['더보기'];
        this.initSearchFlag = false;
        this.searchBrandFlag = false;
        this.searchAreaFlag = false;
        this.searchConditionFlag = false;
        this.newActive = true; // button incative
        this.popularActive = false; // button incative
        this.orderParam = 'order=1'; // 'order=1:최신, 2:인기
        this.queryParams = this.orderParam;
        this.getCars(this.queryParams);
        //this.showTop = false;
        //this.ngOnInit();
        console.log('ionViewDidLoad CarsPage');
    };
    CarsPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('차량'); //trackView for Google Analytics
        console.log('----------> GoogleAnalytics trackView: CarsPage');
        this.initPushNotification();
        this.showTop = false;
        this.ngOnInit();
        console.log('ionViewDidEnter CarsPage');
    };
    CarsPage.prototype.initPushNotification = function () {
        var _this = this;
        this.fcm.onNotification().subscribe(function (data) {
            if (_this.userInfoProvider.userProfile.login_flag == true) {
                if (!data.wasTapped) {
                    if (data.type == 'chat_new') {
                        var dataObj = JSON.stringify(data);
                        //console.log('==============> Notification Data: '+dataObj);
                        if (_this.userInfoProvider.userProfile.login_flag) {
                            _this.chatsCountProvider.checkChatsCount(_this.userInfoProvider.userProfile.login_flag, _this.userInfoProvider.userProfile.u_email, _this.userInfoProvider.userProfile.u_pw);
                            var msgObj = JSON.parse(data.message);
                            var target_email = msgObj.target_email.replace(/\'/g, ''); //특정문자 제거
                            var target_name = msgObj.target_name.replace(/\'/g, ''); //특정문자 제거
                            //console.log('notification target_email====> '+target_email);
                            var notificationModal_modal_1 = _this.modalCtrl.create('NotificationModalPage', {
                                target_email: target_email,
                                target_name: target_name
                            });
                            var closedByTimeout_1 = false;
                            var timeoutHandle_1 = setTimeout(function () { closedByTimeout_1 = true; notificationModal_modal_1.dismiss(); }, 5000);
                            notificationModal_modal_1.onDidDismiss(function () {
                                if (closedByTimeout_1)
                                    return; //timeout 동안 action이 없으면 return
                                clearTimeout(timeoutHandle_1);
                            });
                            notificationModal_modal_1.present();
                        }
                    } //data.type == 'chat_new'
                }
                ;
            }
        });
    };
    CarsPage.prototype.ngOnInit = function () {
        var _this = this;
        // Ionic scroll element
        this.ionScroll = this.myElement.nativeElement.getElementsByClassName('scroll-content')[0];
        this.start = -100; //-100
        this.ionScroll.addEventListener("scroll", function () {
            if (_this.ionScroll.scrollTop - (_this.start) > _this.threshold) {
                _this.showheader = false;
                _this.showTop = false;
                _this.hideheader = false;
            }
            else {
                _this.showheader = false;
                _this.showTop = true;
                _this.hideheader = true;
            }
        });
    };
    CarsPage.prototype.showTopSwitch = function () {
        if (this.showTop == true) {
            this.showTop = false;
            this.ionScroll.scrollTop = 155;
            this.ngOnInit();
        }
        else {
            this.showTop = true;
            this.ionScroll.scrollTop = 0;
            this.ngOnInit();
        }
    };
    CarsPage.prototype.getCars = function (param) {
        var _this = this;
        if (this.userInfoProvider.userProfile.login_flag == true) {
            this.carListParam = param + '&email=' + this.userInfoProvider.userProfile.u_email;
        }
        else {
            this.carListParam = param;
        }
        //console.log('==============> getCars(param) ======> '+this.carListParam);
        var body = this.carListParam, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_list.php";
        this.http.post(url, body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.result != 'ok') {
                var alert_1 = _this.alertCtrl.create({
                    title: '알림',
                    subTitle: _this.msgService.getMessage(data.result),
                    buttons: ['확인']
                });
                alert_1.present();
            }
            else {
                for (var i = 0; i < Object.keys(data.sale_list).length; i++) {
                    data.sale_list[i].maker = _this.makerModelProvider.getMakerName(data.sale_list[i].maker);
                    data.sale_list[i].model = _this.makerModelProvider.getModelName(data.sale_list[i].model);
                    // for 거래중 blind_date:"2017-08-31 00:00:00" >= this.todayDate
                    if (data.sale_list[i].blind_date.substr(0, 10) >= _this.todayDate) {
                        data.sale_list[i].state = '거래중';
                    }
                    var strArray = data.sale_list[i].address.split(' ');
                    data.sale_list[i].address = strArray[2];
                }
                _this.cars = data.sale_list;
                _this.queryLength = Object.keys(data.sale_list).length;
                console.log('getCars() ===> this.queryLength: ' + _this.queryLength);
                if (_this.queryLength >= 20) {
                    _this.doInfiniteFlag = true;
                }
                else {
                    _this.doInfiniteFlag = false;
                }
            }
        });
    };
    CarsPage.prototype.resetSearch = function () {
        this.ionScroll.scrollTop = 0;
        this.searchBrand = '제조사/모델명';
        this.searchArea = '지역';
        this.searchResult = '모든 모델 ・ 모든 지역 ・ 추가 조건 검색';
        this.searchCondition = ['더보기'];
        this.initSearchFlag = false;
        this.searchBrandFlag = false;
        this.searchAreaFlag = false;
        this.searchConditionFlag = false;
        this.newActive = true; // button incative
        this.popularActive = false; // button incative
        this.cars = [];
        this.doInfiniteFlag = false;
        this.orderParam = 'order=1'; //order=최신
        this.queryParams = this.orderParam;
        this.getCars(this.queryParams);
    };
    CarsPage.prototype.searchNew = function () {
        this.newActive = true; // button incative
        this.popularActive = false; // button incative
        this.cars = [];
        this.queryLength = 0;
        this.doInfiniteFlag = false;
        this.orderParam = 'order=1'; //order=최신
        if (this.searchBrandFlag && this.searchAreaFlag && this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.brandParams + this.areaParams + this.otherParams;
        }
        else if (this.searchBrandFlag && this.searchAreaFlag) {
            this.queryParams = this.orderParam + this.brandParams + this.areaParams;
        }
        else if (this.searchBrandFlag && this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.brandParams + this.otherParams;
        }
        else if (this.searchAreaFlag && this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.areaParams + this.otherParams;
        }
        else if (this.searchBrandFlag) {
            this.queryParams = this.orderParam + this.brandParams;
        }
        else if (this.searchAreaFlag) {
            this.queryParams = this.orderParam + this.areaParams;
        }
        else if (this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.otherParams;
        }
        else {
            this.queryParams = this.orderParam;
        }
        this.getCars(this.queryParams);
    };
    CarsPage.prototype.searchPopular = function () {
        this.newActive = false; // button incative
        this.popularActive = true; // button incative
        this.cars = [];
        this.queryLength = 0;
        this.doInfiniteFlag = false;
        this.orderParam = 'order=2'; //order=인기
        if (this.searchBrandFlag && this.searchAreaFlag && this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.brandParams + this.areaParams + this.otherParams;
        }
        else if (this.searchBrandFlag && this.searchAreaFlag) {
            this.queryParams = this.orderParam + this.brandParams + this.areaParams;
        }
        else if (this.searchBrandFlag && this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.brandParams + this.otherParams;
        }
        else if (this.searchAreaFlag && this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.areaParams + this.otherParams;
        }
        else if (this.searchBrandFlag) {
            this.queryParams = this.orderParam + this.brandParams;
        }
        else if (this.searchAreaFlag) {
            this.queryParams = this.orderParam + this.areaParams;
        }
        else if (this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.otherParams;
        }
        else {
            this.queryParams = this.orderParam;
        }
        this.getCars(this.queryParams);
    };
    CarsPage.prototype.naviCarDetailPage = function (selectedCar) {
        var _this = this;
        if (!this.carDetailTabFlag) {
            this.carDetailTabFlag = true;
            if (this.userInfoProvider.userProfile.login_flag == true) {
                this.carListParam = 'sale=' + selectedCar + '&email=' + this.userInfoProvider.userProfile.u_email;
            }
            else {
                this.carListParam = 'sale=' + selectedCar;
            }
            var body = this.carListParam, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_detail.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    var alert_2 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: _this.msgService.getMessage(data.result),
                        buttons: ['확인']
                    });
                    alert_2.present();
                }
                else {
                    var set_key = selectedCar;
                    var set_type = '1'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                    _this.setAnalytics.setAnalytics(set_key, set_type);
                    var CarDetailPage_modal = _this.modalCtrl.create('CarDetailPage', { carParam: data.sale_detail });
                    CarDetailPage_modal.onDidDismiss(function (data) {
                        _this.carDetailTabFlag = false;
                    });
                    CarDetailPage_modal.present();
                }
            });
        }
    };
    CarsPage.prototype.SearchBrand = function () {
        var _this = this;
        var SearchBrandPage_modal = this.modalCtrl.create('SearchBrandPage', { fromPage: 'carsPage' });
        SearchBrandPage_modal.onDidDismiss(function (data) {
            //console.log('this.ionicApp._modalPortal.getActive().index: '+this.ionicApp._modalPortal.getActive().index);
            if (_this.ionicApp._modalPortal.getActive().index == 0 && data != undefined) {
                _this.searchBrand = [];
                console.log('data.maker_name, data.model_name: ' + data.maker_name + '-' + data.model_name);
                if (data.maker_name == '전체' && data.model_name == '전체') {
                    _this.searchBrand = '제조사/모델명';
                    _this.brandParams = '';
                    _this.searchBrandFlag = false;
                    if (_this.searchAreaFlag == true) {
                        _this.searchResult = '제조사:전체, 지역:' + _this.searchArea;
                    }
                    else {
                        _this.searchResult = '제조사:전체, 지역:전국';
                    }
                    if (_this.searchAreaFlag && _this.searchConditionFlag) {
                        _this.queryParams = _this.orderParam + _this.areaParams + _this.otherParams;
                    }
                    else if (_this.searchAreaFlag) {
                        _this.queryParams = _this.orderParam + _this.areaParams;
                    }
                    else if (_this.searchConditionFlag) {
                        _this.queryParams = _this.orderParam + _this.otherParams;
                    }
                    else {
                        _this.queryParams = _this.orderParam;
                    }
                    _this.getCars(_this.queryParams);
                }
                else {
                    //{maker_key: "1", maker_name: "현대", model_key: "9", model_name: "i30"}
                    _this.searchBrand.push(data.maker_name + '-' + data.model_name);
                    _this.searchBrandFlag = true;
                    _this.initSearchFlag = true;
                    if (_this.searchAreaFlag == true) {
                        _this.searchResult = _this.searchBrand + ', 지역:' + _this.searchArea;
                    }
                    else {
                        _this.searchResult = _this.searchBrand + ', 지역:전국';
                    }
                    // Initialized cars[]
                    _this.cars = [];
                    _this.queryLength = 0;
                    _this.doInfiniteFlag = false;
                    if (data.model_name == '전체') {
                        _this.brandParams = '&maker=' + data.maker_key;
                    }
                    else {
                        _this.brandParams = '&model=' + data.model_key;
                    }
                    _this.queryParams = _this.orderParam + _this.brandParams;
                    if (_this.searchAreaFlag && _this.searchConditionFlag) {
                        _this.queryParams = _this.orderParam + _this.brandParams + _this.areaParams + _this.otherParams;
                    }
                    else if (_this.searchAreaFlag) {
                        _this.queryParams = _this.orderParam + _this.brandParams + _this.areaParams;
                    }
                    else if (_this.searchConditionFlag) {
                        _this.queryParams = _this.orderParam + _this.brandParams + _this.otherParams;
                    }
                    _this.getCars(_this.queryParams);
                    //console.log('제조사/모델명 queryParams: '+this.queryParams);
                }
            } // check registerBackButtonAction for android
        });
        SearchBrandPage_modal.present();
    };
    CarsPage.prototype.SearchLocation = function () {
        var _this = this;
        var searchLocationPage_modal = this.modalCtrl.create('SearchLocationPage');
        searchLocationPage_modal.onDidDismiss(function (data) {
            if (_this.ionicApp._modalPortal.getActive().index == 0 && data != undefined) {
                //console.log('Got area parameter ==>' +data);
                if (data == '전국') {
                    _this.searchArea = data;
                    _this.searchAreaFlag = false;
                    if (_this.searchBrandFlag == true) {
                        _this.searchResult = _this.searchBrand + ', 지역:' + data;
                    }
                    else {
                        _this.searchResult = '제조사:전체, 지역:' + data;
                    }
                    if (_this.searchBrandFlag && _this.searchConditionFlag) {
                        _this.queryParams = _this.orderParam + _this.brandParams + _this.otherParams;
                    }
                    else if (_this.searchBrandFlag) {
                        _this.queryParams = _this.orderParam + _this.brandParams;
                    }
                    else if (_this.searchConditionFlag) {
                        _this.queryParams = _this.orderParam + _this.otherParams;
                    }
                    else {
                        _this.queryParams = _this.orderParam;
                    }
                    _this.getCars(_this.queryParams);
                }
                else {
                    _this.searchAreaFlag = true;
                    _this.initSearchFlag = true;
                    _this.searchArea = data;
                    if (_this.searchBrandFlag == true) {
                        _this.searchResult = _this.searchBrand + ', 지역:' + data;
                    }
                    else {
                        _this.searchResult = '제조사:전체, 지역:' + data;
                    }
                    // Initialized cars[]
                    _this.cars = [];
                    _this.queryLength = 0;
                    _this.doInfiniteFlag = false;
                    _this.areaParams = '&address=' + data;
                    _this.queryParams = _this.orderParam + _this.areaParams;
                    if (_this.searchBrandFlag && _this.searchConditionFlag) {
                        _this.queryParams = _this.orderParam + _this.areaParams + _this.brandParams + _this.otherParams;
                    }
                    else if (_this.searchBrandFlag) {
                        _this.queryParams = _this.orderParam + _this.areaParams + _this.brandParams;
                    }
                    else if (_this.searchConditionFlag) {
                        _this.queryParams = _this.orderParam + _this.areaParams + _this.otherParams;
                    }
                    //console.log('지역 queryParams: '+this.queryParams);
                    _this.getCars(_this.queryParams);
                }
            } // check registerBackButtonAction for android
        });
        searchLocationPage_modal.present();
    };
    CarsPage.prototype.SearchOtherConditions = function () {
        var _this = this;
        var searchOtherConditionsPage_modal = this.modalCtrl.create('SearchOtherConditionsPage');
        searchOtherConditionsPage_modal.onDidDismiss(function (data) {
            console.log('Other condition return value: ' + data);
            if (_this.ionicApp._modalPortal.getActive().index == 0 && data != undefined) {
                if (data == "더보기") {
                    _this.searchCondition = data;
                    _this.otherParams = '';
                    _this.searchConditionFlag = false;
                    if (_this.searchBrandFlag && _this.searchAreaFlag) {
                        _this.queryParams = _this.orderParam + _this.brandParams + _this.areaParams;
                    }
                    else if (_this.searchBrandFlag) {
                        _this.queryParams = _this.orderParam + _this.brandParams;
                    }
                    else if (_this.searchAreaFlag) {
                        _this.queryParams = _this.orderParam + _this.areaParams;
                    }
                    else {
                        _this.queryParams = _this.orderParam;
                    }
                    _this.getCars(_this.queryParams);
                }
                else {
                    _this.searchCondition = [];
                    _this.searchConditionFlag = true;
                    _this.initSearchFlag = true;
                    // Initialized cars[]
                    _this.cars = [];
                    _this.queryLength = 0;
                    _this.doInfiniteFlag = false;
                    //this.searchCondition.push(data.type);
                    switch (data.type) {
                        case '1':
                            _this.searchCondition.push('전체');
                            break;
                        case '2':
                            _this.searchCondition.push('소형');
                            break;
                        case '3':
                            _this.searchCondition.push('중대형');
                            break;
                        case '4':
                            _this.searchCondition.push('SUV');
                            break;
                        case '5':
                            _this.searchCondition.push('수입차');
                            break;
                        default:
                            _this.searchCondition.push('전체');
                            break;
                    }
                    //this.searchCondition.push(data.puer);
                    switch (data.puer) {
                        case '1':
                            _this.searchCondition.push('전체');
                            break;
                        case '2':
                            _this.searchCondition.push('디젤');
                            break;
                        case '3':
                            _this.searchCondition.push('가솔린');
                            break;
                        case '4':
                            _this.searchCondition.push('하이브리드');
                            break;
                        case '5':
                            _this.searchCondition.push('전기');
                            break;
                        case '6':
                            _this.searchCondition.push('가스');
                            break;
                        case '7':
                            _this.searchCondition.push('기타');
                            break;
                        default:
                            _this.searchCondition.push('전체');
                            break;
                    }
                    //this.searchCondition.push(data.gear);
                    switch (data.gear) {
                        case '1':
                            _this.searchCondition.push('전체');
                            break;
                        case '2':
                            _this.searchCondition.push('수동');
                            break;
                        case '3':
                            _this.searchCondition.push('자동');
                            break;
                        case '4':
                            _this.searchCondition.push('기타');
                            break;
                        default:
                            _this.searchCondition.push('전체');
                            break;
                    }
                    _this.searchCondition.push(data.minPrice);
                    _this.searchCondition.push(data.maxPrice);
                    _this.searchCondition.push(data.minYear);
                    _this.searchCondition.push(data.maxYear);
                    _this.searchCondition.push(data.minKm);
                    _this.searchCondition.push(data.maxKm);
                    _this.otherParams = '&type=' + data.type + '&puer=' + data.puer + '&gear=' + data.gear + '&price=' + data.minPrice + ',' + data.maxPrice +
                        '&year=' + data.minYear + ',' + data.maxYear + '&mileage=' + data.minKm + ',' + data.maxKm;
                    _this.queryParams = _this.orderParam + _this.otherParams;
                    if (_this.searchBrandFlag && _this.searchAreaFlag) {
                        _this.queryParams = _this.orderParam + _this.otherParams + _this.brandParams + _this.areaParams;
                    }
                    else if (_this.searchBrandFlag) {
                        _this.queryParams = _this.orderParam + _this.otherParams + _this.brandParams;
                    }
                    else if (_this.searchAreaFlag) {
                        _this.queryParams = _this.orderParam + _this.otherParams + _this.areaParams;
                    }
                    _this.getCars(_this.queryParams);
                    //console.log('조건 queryParams: '+this.queryParams);
                }
            } // check registerBackButtonAction for android
        });
        searchOtherConditionsPage_modal.present();
    };
    CarsPage.prototype.selectFavoriteCar = function (selectedCar) {
        var _this = this;
        if (this.userInfoProvider.userProfile.login_flag == false) {
            var LoginPage_modal = this.modalCtrl.create('LoginPage', { naviPage: 'CarsPage' });
            LoginPage_modal.onDidDismiss(function (data) {
                if (data == true) {
                    _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                }
            });
            LoginPage_modal.present();
        }
        else {
            var _loop_1 = function (i) {
                if (this_1.cars[i].key == selectedCar.key) {
                    if (this_1.cars[i].is_favorite == '0') {
                        //console.log('car.key car.is_favorite: '+this.cars[i].key+'-'+this.cars[i].is_favorite);
                        var favoriteQueryParams = 'sale=' + selectedCar.key +
                            '&email=' + this_1.userInfoProvider.userProfile.u_email +
                            '&pw=' + this_1.userInfoProvider.userProfile.u_pw +
                            '&favorite=' + '1'; //'1'=찜등록, '2'=찜취소
                        var body = favoriteQueryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this_1.marketURI + "set_sale_favorite.php";
                        this_1.http.post(url, body, options)
                            .map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            if (data.result != 'ok') {
                                var alert_3 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: _this.msgService.getMessage(data.result),
                                    buttons: ['확인']
                                });
                                alert_3.present();
                            }
                            else {
                                _this.ga.trackEvent('차량상세', '찜: ' + selectedCar.maker + '-' + selectedCar.model); //trackEvent for Google Analytics
                                var alert_4 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: '선택하신 차량이 찜 목록에 저장되었습니다.',
                                    buttons: ['확인']
                                });
                                alert_4.present();
                                _this.cars[i].is_favorite = '1';
                                var set_key = selectedCar.key;
                                var set_type = '2'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                                _this.setAnalytics.setAnalytics(set_key, set_type);
                            }
                        });
                    }
                    else {
                        //console.log('car.key car.is_favorite: '+this.cars[i].key+'-'+this.cars[i].is_favorite);
                        var favoriteQueryParams = 'sale=' + selectedCar.key +
                            '&email=' + this_1.userInfoProvider.userProfile.u_email +
                            '&pw=' + this_1.userInfoProvider.userProfile.u_pw +
                            '&favorite=' + '2'; //'1'=찜등록, '2'=찜취소
                        var body = favoriteQueryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this_1.marketURI + "set_sale_favorite.php";
                        this_1.http.post(url, body, options)
                            .map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            if (data.result != 'ok') {
                                var alert_5 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: _this.msgService.getMessage(data.result),
                                    buttons: ['확인']
                                });
                                alert_5.present();
                            }
                            else {
                                var alert_6 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: '선택하신 차량이 찜 목록에서 삭제되었습니다.',
                                    buttons: ['확인']
                                });
                                alert_6.present();
                                _this.cars[i].is_favorite = '0';
                                var set_key = selectedCar.key;
                                var set_type = '3'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                                _this.setAnalytics.setAnalytics(set_key, set_type);
                            }
                        });
                    }
                }
            };
            var this_1 = this;
            for (var i = 0; i < this.cars.length; i++) {
                _loop_1(i);
            }
        }
    };
    CarsPage.prototype.addMyCar = function () {
        var _this = this;
        if (this.userInfoProvider.userProfile.login_flag == false) {
            var LoginPage_modal = this.modalCtrl.create('LoginPage', { naviPage: 'CarsPage' });
            LoginPage_modal.onDidDismiss(function (data) {
                if (data == true) {
                    var alert_7 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: '정상적으로 로그인 되었습니다.',
                        buttons: ['확인']
                    });
                    alert_7.present();
                }
            });
            LoginPage_modal.present();
        }
        else {
            this.ga.trackEvent('차량상세', '내차팔기'); //trackEvent for Google Analytics
            var RegistAgreePage_modal = this.modalCtrl.create('RegistAgreePage');
            RegistAgreePage_modal.present();
        }
    };
    CarsPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var param_string = this.queryParams;
        //console.log('this.doInfiniteFlag & param_string & this.queryLength =>'+this.doInfiniteFlag+'-'+param_string+'-'+this.queryLength);
        if (this.doInfiniteFlag == true) {
            if (this.userInfoProvider.userProfile.login_flag == true) {
                this.carListParam = param_string + '&email=' + this.userInfoProvider.userProfile.u_email;
            }
            else {
                this.carListParam = param_string;
            }
            var body = this.carListParam + '&key=' + this.queryLength, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_list.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    _this.doInfiniteFlag = false;
                }
                else {
                    //this.presentLoading();
                    for (var i = 0; i < Object.keys(data.sale_list).length; i++) {
                        data.sale_list[i].maker = _this.makerModelProvider.getMakerName(data.sale_list[i].maker);
                        data.sale_list[i].model = _this.makerModelProvider.getModelName(data.sale_list[i].model);
                        // 거래중 blind_date:"2017-08-31 00:00:00" >= this.todayDate
                        if (data.sale_list[i].blind_date.substr(0, 10) >= _this.todayDate) {
                            data.sale_list[i].state = '거래중';
                        }
                        // for display area
                        var strArray = data.sale_list[i].address.split(' ');
                        if (_this.searchArea != '지역') {
                            if (strArray[1] == _this.searchArea) {
                                data.sale_list[i].address = strArray[2];
                                _this.cars.push(data.sale_list[i]);
                            }
                        }
                        else {
                            data.sale_list[i].address = strArray[2];
                            //console.log('adderss place name: '+data.sale_list[i].address);
                            _this.cars.push(data.sale_list[i]);
                        }
                    }
                    if (Object.keys(data.sale_list).length < 20) {
                        _this.doInfiniteFlag = false;
                    }
                    _this.queryLength = _this.queryLength + Object.keys(data.sale_list).length;
                    console.log('doInfinite.queryLength ===> ' + Object.keys(data.sale_list).length);
                    console.log('total queryLength ===> ' + _this.queryLength);
                }
                // console.log('Next operation has ended');
                infiniteScroll.complete();
            });
        }
    };
    CarsPage.prototype.presentLoading = function () {
        this.loadingCtrl.create({
            content: 'Loading Data...',
            duration: 1000,
            showBackdrop: false,
            dismissOnPageChange: true
        }).present();
    };
    return CarsPage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Content */])
], CarsPage.prototype, "content", void 0);
CarsPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-cars',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/cars/cars.html"*/'<ion-header no-padding [ngClass]="{\'hide-header\':showheader,\'show-header\':hideheader}">\n  <div class="head-space"></div>\n  <ion-navbar color=\'1000Under\'>\n    <ion-list style="background-color: #37aafe" no-lines style="margin-bottom: 0px;">\n      <div  *ngIf="showTop" style="padding-left: 10px; padding-right: 10px;">\n        <ion-row style="height: 45px;">\n          <ion-col col-1 style="padding-left: 0px;">\n            <button ion-button item-start clear icon-only color="underwhite" (click)="showTopSwitch()">\n              <ion-icon name="ios-arrow-up"></ion-icon>\n            </button>\n          </ion-col>\n          <ion-col col-9></ion-col>\n          <ion-col col-2 style="padding-right: 10px; padding-left: 0px;">\n            <button *ngIf="initSearchFlag" ion-button clear item-right color="underwhite" \n                    style="font-weight: bold; padding-left: 14px;padding-right: 0px;" (click)="resetSearch()">재설정\n            </button>\n          </ion-col>\n        </ion-row>\n        <button ion-item block style="height: 44px;" color="underwhite" (click)="SearchBrand()">\n          <ion-icon name="car" item-start color="underblack"></ion-icon>{{searchBrand}}\n          <ion-icon md="ios-arrow-forward" item-end color="undergray"></ion-icon>\n        </button>\n        <div class="spacer" style="height: 5px;"></div>\n        <button ion-item block style="height: 44px;" color="underwhite" (click)="SearchLocation()">\n          <ion-icon name="pin" item-left color="underblack"></ion-icon>{{searchArea}}\n          <ion-icon md="ios-arrow-forward" item-end color="undergray"></ion-icon>\n        </button>\n        <div class="spacer" style="height: 5px;"></div>\n        <button ion-item block style="height: 44px;" color="underwhite" (click)="SearchOtherConditions()">\n          <ion-icon name="search" item-left color="underblack"></ion-icon>{{searchCondition}}\n          <ion-icon md="ios-arrow-forward" item-end color="undergray"></ion-icon>\n        </button>\n      </div>\n      <div *ngIf="!showTop" style="padding-left: 10px; padding-right: 10px;">\n        <div class="spacer" style="height: 15px;"></div>\n        <button ion-item style="height: 44px;" (click)="showTopSwitch()">\n          <ion-icon name="search" item-left></ion-icon>{{searchResult}}\n        </button>\n      </div>\n      <div class="spacer" style="height: 15px;"></div>\n    </ion-list>\n  </ion-navbar>\n  <ion-item no-lines style="padding-left: 5px; height: 30px; background-color: #ffffff">\n    <button ion-button color="underblack" style="font-weight: bold;" [clear]="!newActive" (click)="searchNew()">최신</button>\n    <button ion-button color="underblack" style="font-weight: bold;" [clear]="!popularActive" (click)="searchPopular()">인기</button>\n  </ion-item>\n</ion-header>\n\n<ion-content>\n  <div *ngIf="showTop">\n      <div class="spacer" style="height: 130px;"></div>\n  </div>\n  <div align="center">\n    <div no-margin no-padding tappable *ngFor="let car of cars">\n      <div *ngIf="car.photo_height > 500" class="image-wrapper">\n        <div *ngIf="car.state === \'판매중\'">\n          <div class="pcar-image"> \n            <div class="centered">\n              <img [src]="car.photo_path" (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="car.state === \'검수\'">\n          <div class="pcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                              \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n            <div class="centered">\n              <img src="img/state/inspecting.png" style="height: 150px; width: 150px;" (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="car.state === \'거래중\'">\n          <div class="pcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                              \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n            <div class="centered">\n              <img src="img/state/dealing.png" style="height: 150px; width: 150px;" (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="car.state === \'완료\'">\n          <div class="pcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                              \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n            <div class="centered">\n              <img src="img/state/sold.png" style="height: 150px; width: 150px;" (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div *ngIf="car.photo_height <= 500" class="image-wrapper">\n        <div *ngIf="car.state === \'판매중\'">\n          <div class="lcar-image"> \n            <div class="centered">\n              <img [src]="car.photo_path" (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="car.state === \'검수\'">\n          <div class="lcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                              \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n            <div class="centered">\n              <img src="img/state/inspecting.png" style="height: 150px; width: 150px;" (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="car.state === \'거래중\'">\n          <div class="lcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                              \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n            <div class="centered">\n              <img src="img/state/dealing.png" style="height: 150px; width: 150px;" (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="car.state === \'완료\'">\n          <div class="lcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                              \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n            <div class="centered">\n              <img src="img/state/sold.png" style="height: 150px; width: 150px;" (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n      </div>\n      <ion-row style="height: 45px;">\n        <ion-col col-10>\n          <h5 align="left">&nbsp;{{car.price | number:0}}만원 {{car.maker}} {{car.model}}</h5>\n        </ion-col>\n        <ion-col col-2 *ngIf="car.is_favorite === \'1\' && car.state === \'판매중\'">\n          <button ion-button  color="1000Under" clear icon-only (click)="selectFavoriteCar(car)"><ion-icon mini name="heart"></ion-icon></button>\n        </ion-col>\n        <ion-col col-2 *ngIf="car.is_favorite === \'0\' && car.state === \'판매중\'">\n          <button  ion-button  color="undergray" clear icon-only (click)="selectFavoriteCar(car)"><ion-icon mini name="heart-outline"></ion-icon></button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-10 align="left">\n          &nbsp;{{car.address}} | {{car.year}}년{{car.month}}월 | {{car.mileage | number:0}}km  \n        </ion-col>\n        <ion-col col-2>\n          <ion-icon name="ios-eye-outline" item-left><span style="font-size: 11pt;">{{car.view_count | number:0}}</span></ion-icon>\n        </ion-col>\n      </ion-row>\n      <br>\n      <div class="spacer" style="height: 10px; background-color: #eeeeee;"></div>\n    </div>\n  </div>\n  <ion-fab right bottom>\n      <button ion-fab style="color: #ffffff; background-color: #48b2ff; width: 44px; height: 44px;" (click)="addMyCar()"><ion-icon name="md-add"></ion-icon></button>\n  </ion-fab>\n  <ion-infinite-scroll *ngIf="doInfiniteFlag" (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/cars/cars.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* IonicApp */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_fcm__["a" /* FCM */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_analytics__["a" /* GoogleAnalytics */],
        __WEBPACK_IMPORTED_MODULE_10__providers_translate_translate__["a" /* TranslateProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_chats_count_chats_count__["a" /* ChatsCountProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_9__providers_maker_model_maker_model__["a" /* MakerModelProvider */],
        __WEBPACK_IMPORTED_MODULE_11__providers_setanalytics_setanalytics__["a" /* SetanalyticsProvider */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
], CarsPage);

//# sourceMappingURL=cars.js.map

/***/ })

});
//# sourceMappingURL=32.main.js.map