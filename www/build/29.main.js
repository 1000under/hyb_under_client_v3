webpackJsonp([29],{

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chats__ = __webpack_require__(383);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatsPageModule", function() { return ChatsPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ChatsPageModule = (function () {
    function ChatsPageModule() {
    }
    return ChatsPageModule;
}());
ChatsPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__chats__["a" /* ChatsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__chats__["a" /* ChatsPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__chats__["a" /* ChatsPage */]
        ]
    })
], ChatsPageModule);

//# sourceMappingURL=chats.module.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_fcm__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_chats_count_chats_count__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatsPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//-----> Providers



//declare var FCMPlugin;
var ChatsPage = (function () {
    function ChatsPage(platform, viewCtrl, navCtrl, app, params, modalCtrl, alertCtrl, http, userInfoProvider, chatsCountProvider, msgService, zone, fcm, ga) {
        this.platform = platform;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.app = app;
        this.params = params;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.userInfoProvider = userInfoProvider;
        this.chatsCountProvider = chatsCountProvider;
        this.msgService = msgService;
        this.zone = zone;
        this.fcm = fcm;
        this.ga = ga;
        this.msgListFlag = false;
        this.messageURI = "http://cjsekfvm.cafe24.com/test_server/app/message/url/";
        this.message_list = [];
    }
    ChatsPage.prototype.ionViewDidLoad = function () {
        this.login_flag = this.userInfoProvider.userProfile.login_flag;
        this.isMember_flag = this.userInfoProvider.userProfile.isMember_flag;
        this.u_email = this.userInfoProvider.userProfile.u_email;
        this.u_pw = this.userInfoProvider.userProfile.u_pw;
        this.currentPage = 'ChatsPage';
        console.log('ionViewDidLoad ChatsPage');
    };
    ChatsPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('채팅'); //trackView for Google Analytics
        console.log('----------> GoogleAnalytics trackView: ChatsPage');
        this.login_flag = this.userInfoProvider.userProfile.login_flag;
        this.isMember_flag = this.userInfoProvider.userProfile.isMember_flag;
        this.u_email = this.userInfoProvider.userProfile.u_email;
        this.u_pw = this.userInfoProvider.userProfile.u_pw;
        this.initPushNotification();
        this.getMessageList();
        console.log('ionViewDidEnter ChatsPage');
    };
    ChatsPage.prototype.initPushNotification = function () {
        var _this = this;
        this.fcm.onNotification().subscribe(function (data) {
            if (_this.login_flag == true) {
                if (!data.wasTapped) {
                    if (data.type == 'chat_new') {
                        //alert('Received message at ChatsPage in Foreground');
                        _this.reflashPage();
                        var msgObj = JSON.parse(data.message);
                        var target_email = msgObj.target_email.replace(/\'/g, ''); //특정문자 제거
                        var target_name = msgObj.target_name.replace(/\'/g, ''); //특정문자 제거
                        //console.log('notification target_email====> '+target_email);
                        var notificationModal_modal_1 = _this.modalCtrl.create('NotificationModalPage', {
                            target_email: 'ChatsPage',
                            target_name: target_name
                        });
                        var closedByTimeout_1 = false;
                        var timeoutHandle_1 = setTimeout(function () { closedByTimeout_1 = true; notificationModal_modal_1.dismiss(); }, 5000);
                        notificationModal_modal_1.onDidDismiss(function () {
                            if (closedByTimeout_1)
                                return; //timeout 동안 action이 없으면 return
                            clearTimeout(timeoutHandle_1);
                            _this.reflashPage();
                        });
                        notificationModal_modal_1.present();
                    }
                }
                ;
            }
        });
    };
    ChatsPage.prototype.reflashPage = function () {
        var _this = this;
        this.zone.run(function () {
            _this.getMessageList();
            console.log('Force update ======> ChatsPage');
        });
    };
    ChatsPage.prototype.getMessageList = function () {
        var _this = this;
        this.queryParams = 'email=' + this.u_email +
            '&pw=' + this.u_pw;
        var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.messageURI + "get_message_list.php";
        this.http.post(url, body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.result != 'ok') {
                _this.msgListFlag = false;
            }
            else {
                _this.message_list = data.message_list;
                _this.msgListFlag = true;
                _this.message_list.sort(function (a, b) {
                    if (a.regist_date < b.regist_date)
                        return 1;
                    if (a.regist_date > b.regist_date)
                        return -1;
                    return 0;
                });
                for (var i = 0; i < _this.message_list.length; i++) {
                    //2017-08-09 11:48:37 --> date: 'MM/dd H:mm'
                    var temp_date = _this.message_list[i].regist_date.substr(5, 2) + '/' + _this.message_list[i].regist_date.substr(8, 2) + ' '
                        + _this.message_list[i].regist_date.substr(11, 2) + ':' + _this.message_list[i].regist_date.substr(14, 2);
                    _this.message_list[i].regist_date = temp_date;
                    //console.log('ChatsPage unread_count ==============>'+this.message_list[i].target_name+': '+this.message_list[i].unread_count);
                }
                _this.chatsCountProvider.checkChatsCount(_this.userInfoProvider.userProfile.login_flag, _this.userInfoProvider.userProfile.u_email, _this.userInfoProvider.userProfile.u_pw);
            }
            //console.log('ChatsPage msgListFlag ==============>'+this.msgListFlag);
        });
    };
    ChatsPage.prototype.checkLogin = function () {
        var _this = this;
        if (this.login_flag == false) {
            var LoginPage_modal = this.modalCtrl.create('LoginPage', { naviPage: 'ChatsPage' });
            LoginPage_modal.onDidDismiss(function (data) {
                _this.login_flag = data;
                if (_this.login_flag == true) {
                    _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                }
            });
            LoginPage_modal.present();
        }
    };
    ChatsPage.prototype.chatBoard = function (selectedChat) {
        var _this = this;
        var ChatBoardPage_modal = this.modalCtrl.create('ChatBoardPage', {
            email: this.u_email,
            pw: this.u_pw,
            target_email: selectedChat.target_email,
            unread_count: selectedChat.unread_count
        });
        ChatBoardPage_modal.onDidDismiss(function (data) {
            console.log('ChatBoardPage Dismiss current page =====>' + _this.currentPage);
            _this.initPushNotification();
            _this.reflashPage();
            //this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
        });
        ChatBoardPage_modal.present();
    };
    ChatsPage.prototype.chatBoardInit = function () {
        var _this = this;
        var ChatBoardPage_modal = this.modalCtrl.create('ChatBoardPage', {
            email: this.u_email,
            pw: this.u_pw,
            target_email: 'admin@1000under.com'
        });
        ChatBoardPage_modal.onDidDismiss(function (data) {
            _this.initPushNotification();
            _this.reflashPage();
            //this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
        });
        ChatBoardPage_modal.present();
    };
    ChatsPage.prototype.deleteChat = function (selectedChat) {
        var _this = this;
        console.log('deleteChat(target_email): ' + selectedChat.target_email);
        this.queryParams = 'email=' + this.u_email + '&pw=' + this.u_pw + '&target_email=' + selectedChat.target_email;
        var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.messageURI + "set_message_delete.php";
        this.http.post(url, body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.result != 'ok') {
                var alert_1 = _this.alertCtrl.create({
                    title: '알림',
                    subTitle: _this.msgService.getMessage(data.result),
                    buttons: ['확인']
                });
                alert_1.present();
            }
            else {
                //this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
                _this.getMessageList();
            }
        });
    };
    return ChatsPage;
}());
ChatsPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-chats',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/chats/chats.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="primary">\n    <ion-title>채팅</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div *ngIf="login_flag">\n    <div *ngIf="msgListFlag">\n      <ion-list no-lines>\n        <ion-item-sliding *ngFor="let ml of message_list">\n          <ion-item no-lines tappable (click)="chatBoard(ml)">\n            <ion-avatar item-start>\n              <img src="img/1000under-logo.png">\n            </ion-avatar>\n            <h2>{{ml.target_name}} <span style="font-size: 10pt;">{{ml.regist_date}}</span></h2>\n            <p>{{ml.message_text}}</p>\n            <ion-badge *ngIf="ml.unread_count > 0" color="danger" item-end>{{ml.unread_count}}</ion-badge>\n          </ion-item>\n          <ion-item-options>\n            <button ion-button color="danger" icon-only icon-left (click)="deleteChat(ml)">\n              <ion-icon name="ios-trash-outline"></ion-icon>\n            </button>\n          </ion-item-options>\n        </ion-item-sliding> \n      </ion-list>\n    </div>\n    <div *ngIf="!msgListFlag">\n      <div class="spacer" style="height: 160px;"></div>\n      <div align="center"><img src="img/chat.png" style="height: 15%; width: 15%"/></div>\n      <h5 align="center">항상 준비되어 있는 천언더입니다.</h5>\n      <p align="center"><span style="font-size: 10pt; color:#999999">국내 최대 중고차 안심직거래앱 천언더<br>전담직원과 함께 내 차를 상담하세요.</span></p>  \n      <ion-fab right bottom>\n        <button ion-fab style="color: #ffffff; background-color: #48b2ff; width: 44px; height: 44px;" (click)="chatBoardInit()"><ion-icon name="md-add"></ion-icon></button>\n      </ion-fab>\n    </div>\n  </div>\n  <div *ngIf="!login_flag">\n    <div class="spacer" style="height: 160px;"></div>\n    <div align="center"><img src="img/chat.png" style="height: 15%; width: 15%"/></div>\n    <h5 align="center">전담직원이 항상 대기중입니다!</h5>\n    <p align="center"><span style="font-size: 10pt; color:#999999">국내 최대 중고차 안심직거래앱 천언더<br>지금 바로 로그인하시고 전담직원과 상담하세요!</span></p>  \n    <div align="center">\n      <button ion-button color="1000Under" style="box-shadow: none !important;" (click)="checkLogin()">천언더 로그인</button>\n    </div>\n  </div>\n  <ion-fab *ngIf="login_flag" right bottom>\n    <button ion-fab style="color: #ffffff; background-color: #48b2ff; width: 44px; height: 44px;" (click)="chatBoardInit()"><ion-icon name="md-add"></ion-icon></button>\n  </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/chats/chats.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* App */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_5__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_chats_count_chats_count__["a" /* ChatsCountProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_fcm__["a" /* FCM */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], ChatsPage);

//# sourceMappingURL=chats.js.map

/***/ })

});
//# sourceMappingURL=29.main.js.map