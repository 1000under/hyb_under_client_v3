webpackJsonp([5],{

/***/ 306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_my_car__ = __webpack_require__(377);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddMyCarPageModule", function() { return AddMyCarPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddMyCarPageModule = (function () {
    function AddMyCarPageModule() {
    }
    return AddMyCarPageModule;
}());
AddMyCarPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__add_my_car__["a" /* AddMyCarPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__add_my_car__["a" /* AddMyCarPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__add_my_car__["a" /* AddMyCarPage */]
        ]
    })
], AddMyCarPageModule);

//# sourceMappingURL=add-my-car.module.js.map

/***/ }),

/***/ 376:
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/*! @preserve
 * numeral.js
 * version : 2.0.6
 * author : Adam Draper
 * license : MIT
 * http://adamwdraper.github.com/Numeral-js/
 */

(function (global, factory) {
    if (true) {
        !(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory();
    } else {
        global.numeral = factory();
    }
}(this, function () {
    /************************************
        Variables
    ************************************/

    var numeral,
        _,
        VERSION = '2.0.6',
        formats = {},
        locales = {},
        defaults = {
            currentLocale: 'en',
            zeroFormat: null,
            nullFormat: null,
            defaultFormat: '0,0',
            scalePercentBy100: true
        },
        options = {
            currentLocale: defaults.currentLocale,
            zeroFormat: defaults.zeroFormat,
            nullFormat: defaults.nullFormat,
            defaultFormat: defaults.defaultFormat,
            scalePercentBy100: defaults.scalePercentBy100
        };


    /************************************
        Constructors
    ************************************/

    // Numeral prototype object
    function Numeral(input, number) {
        this._input = input;

        this._value = number;
    }

    numeral = function(input) {
        var value,
            kind,
            unformatFunction,
            regexp;

        if (numeral.isNumeral(input)) {
            value = input.value();
        } else if (input === 0 || typeof input === 'undefined') {
            value = 0;
        } else if (input === null || _.isNaN(input)) {
            value = null;
        } else if (typeof input === 'string') {
            if (options.zeroFormat && input === options.zeroFormat) {
                value = 0;
            } else if (options.nullFormat && input === options.nullFormat || !input.replace(/[^0-9]+/g, '').length) {
                value = null;
            } else {
                for (kind in formats) {
                    regexp = typeof formats[kind].regexps.unformat === 'function' ? formats[kind].regexps.unformat() : formats[kind].regexps.unformat;

                    if (regexp && input.match(regexp)) {
                        unformatFunction = formats[kind].unformat;

                        break;
                    }
                }

                unformatFunction = unformatFunction || numeral._.stringToNumber;

                value = unformatFunction(input);
            }
        } else {
            value = Number(input)|| null;
        }

        return new Numeral(input, value);
    };

    // version number
    numeral.version = VERSION;

    // compare numeral object
    numeral.isNumeral = function(obj) {
        return obj instanceof Numeral;
    };

    // helper functions
    numeral._ = _ = {
        // formats numbers separators, decimals places, signs, abbreviations
        numberToFormat: function(value, format, roundingFunction) {
            var locale = locales[numeral.options.currentLocale],
                negP = false,
                optDec = false,
                leadingCount = 0,
                abbr = '',
                trillion = 1000000000000,
                billion = 1000000000,
                million = 1000000,
                thousand = 1000,
                decimal = '',
                neg = false,
                abbrForce, // force abbreviation
                abs,
                min,
                max,
                power,
                int,
                precision,
                signed,
                thousands,
                output;

            // make sure we never format a null value
            value = value || 0;

            abs = Math.abs(value);

            // see if we should use parentheses for negative number or if we should prefix with a sign
            // if both are present we default to parentheses
            if (numeral._.includes(format, '(')) {
                negP = true;
                format = format.replace(/[\(|\)]/g, '');
            } else if (numeral._.includes(format, '+') || numeral._.includes(format, '-')) {
                signed = numeral._.includes(format, '+') ? format.indexOf('+') : value < 0 ? format.indexOf('-') : -1;
                format = format.replace(/[\+|\-]/g, '');
            }

            // see if abbreviation is wanted
            if (numeral._.includes(format, 'a')) {
                abbrForce = format.match(/a(k|m|b|t)?/);

                abbrForce = abbrForce ? abbrForce[1] : false;

                // check for space before abbreviation
                if (numeral._.includes(format, ' a')) {
                    abbr = ' ';
                }

                format = format.replace(new RegExp(abbr + 'a[kmbt]?'), '');

                if (abs >= trillion && !abbrForce || abbrForce === 't') {
                    // trillion
                    abbr += locale.abbreviations.trillion;
                    value = value / trillion;
                } else if (abs < trillion && abs >= billion && !abbrForce || abbrForce === 'b') {
                    // billion
                    abbr += locale.abbreviations.billion;
                    value = value / billion;
                } else if (abs < billion && abs >= million && !abbrForce || abbrForce === 'm') {
                    // million
                    abbr += locale.abbreviations.million;
                    value = value / million;
                } else if (abs < million && abs >= thousand && !abbrForce || abbrForce === 'k') {
                    // thousand
                    abbr += locale.abbreviations.thousand;
                    value = value / thousand;
                }
            }

            // check for optional decimals
            if (numeral._.includes(format, '[.]')) {
                optDec = true;
                format = format.replace('[.]', '.');
            }

            // break number and format
            int = value.toString().split('.')[0];
            precision = format.split('.')[1];
            thousands = format.indexOf(',');
            leadingCount = (format.split('.')[0].split(',')[0].match(/0/g) || []).length;

            if (precision) {
                if (numeral._.includes(precision, '[')) {
                    precision = precision.replace(']', '');
                    precision = precision.split('[');
                    decimal = numeral._.toFixed(value, (precision[0].length + precision[1].length), roundingFunction, precision[1].length);
                } else {
                    decimal = numeral._.toFixed(value, precision.length, roundingFunction);
                }

                int = decimal.split('.')[0];

                if (numeral._.includes(decimal, '.')) {
                    decimal = locale.delimiters.decimal + decimal.split('.')[1];
                } else {
                    decimal = '';
                }

                if (optDec && Number(decimal.slice(1)) === 0) {
                    decimal = '';
                }
            } else {
                int = numeral._.toFixed(value, 0, roundingFunction);
            }

            // check abbreviation again after rounding
            if (abbr && !abbrForce && Number(int) >= 1000 && abbr !== locale.abbreviations.trillion) {
                int = String(Number(int) / 1000);

                switch (abbr) {
                    case locale.abbreviations.thousand:
                        abbr = locale.abbreviations.million;
                        break;
                    case locale.abbreviations.million:
                        abbr = locale.abbreviations.billion;
                        break;
                    case locale.abbreviations.billion:
                        abbr = locale.abbreviations.trillion;
                        break;
                }
            }


            // format number
            if (numeral._.includes(int, '-')) {
                int = int.slice(1);
                neg = true;
            }

            if (int.length < leadingCount) {
                for (var i = leadingCount - int.length; i > 0; i--) {
                    int = '0' + int;
                }
            }

            if (thousands > -1) {
                int = int.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + locale.delimiters.thousands);
            }

            if (format.indexOf('.') === 0) {
                int = '';
            }

            output = int + decimal + (abbr ? abbr : '');

            if (negP) {
                output = (negP && neg ? '(' : '') + output + (negP && neg ? ')' : '');
            } else {
                if (signed >= 0) {
                    output = signed === 0 ? (neg ? '-' : '+') + output : output + (neg ? '-' : '+');
                } else if (neg) {
                    output = '-' + output;
                }
            }

            return output;
        },
        // unformats numbers separators, decimals places, signs, abbreviations
        stringToNumber: function(string) {
            var locale = locales[options.currentLocale],
                stringOriginal = string,
                abbreviations = {
                    thousand: 3,
                    million: 6,
                    billion: 9,
                    trillion: 12
                },
                abbreviation,
                value,
                i,
                regexp;

            if (options.zeroFormat && string === options.zeroFormat) {
                value = 0;
            } else if (options.nullFormat && string === options.nullFormat || !string.replace(/[^0-9]+/g, '').length) {
                value = null;
            } else {
                value = 1;

                if (locale.delimiters.decimal !== '.') {
                    string = string.replace(/\./g, '').replace(locale.delimiters.decimal, '.');
                }

                for (abbreviation in abbreviations) {
                    regexp = new RegExp('[^a-zA-Z]' + locale.abbreviations[abbreviation] + '(?:\\)|(\\' + locale.currency.symbol + ')?(?:\\))?)?$');

                    if (stringOriginal.match(regexp)) {
                        value *= Math.pow(10, abbreviations[abbreviation]);
                        break;
                    }
                }

                // check for negative number
                value *= (string.split('-').length + Math.min(string.split('(').length - 1, string.split(')').length - 1)) % 2 ? 1 : -1;

                // remove non numbers
                string = string.replace(/[^0-9\.]+/g, '');

                value *= Number(string);
            }

            return value;
        },
        isNaN: function(value) {
            return typeof value === 'number' && isNaN(value);
        },
        includes: function(string, search) {
            return string.indexOf(search) !== -1;
        },
        insert: function(string, subString, start) {
            return string.slice(0, start) + subString + string.slice(start);
        },
        reduce: function(array, callback /*, initialValue*/) {
            if (this === null) {
                throw new TypeError('Array.prototype.reduce called on null or undefined');
            }

            if (typeof callback !== 'function') {
                throw new TypeError(callback + ' is not a function');
            }

            var t = Object(array),
                len = t.length >>> 0,
                k = 0,
                value;

            if (arguments.length === 3) {
                value = arguments[2];
            } else {
                while (k < len && !(k in t)) {
                    k++;
                }

                if (k >= len) {
                    throw new TypeError('Reduce of empty array with no initial value');
                }

                value = t[k++];
            }
            for (; k < len; k++) {
                if (k in t) {
                    value = callback(value, t[k], k, t);
                }
            }
            return value;
        },
        /**
         * Computes the multiplier necessary to make x >= 1,
         * effectively eliminating miscalculations caused by
         * finite precision.
         */
        multiplier: function (x) {
            var parts = x.toString().split('.');

            return parts.length < 2 ? 1 : Math.pow(10, parts[1].length);
        },
        /**
         * Given a variable number of arguments, returns the maximum
         * multiplier that must be used to normalize an operation involving
         * all of them.
         */
        correctionFactor: function () {
            var args = Array.prototype.slice.call(arguments);

            return args.reduce(function(accum, next) {
                var mn = _.multiplier(next);
                return accum > mn ? accum : mn;
            }, 1);
        },
        /**
         * Implementation of toFixed() that treats floats more like decimals
         *
         * Fixes binary rounding issues (eg. (0.615).toFixed(2) === '0.61') that present
         * problems for accounting- and finance-related software.
         */
        toFixed: function(value, maxDecimals, roundingFunction, optionals) {
            var splitValue = value.toString().split('.'),
                minDecimals = maxDecimals - (optionals || 0),
                boundedPrecision,
                optionalsRegExp,
                power,
                output;

            // Use the smallest precision value possible to avoid errors from floating point representation
            if (splitValue.length === 2) {
              boundedPrecision = Math.min(Math.max(splitValue[1].length, minDecimals), maxDecimals);
            } else {
              boundedPrecision = minDecimals;
            }

            power = Math.pow(10, boundedPrecision);

            // Multiply up by precision, round accurately, then divide and use native toFixed():
            output = (roundingFunction(value + 'e+' + boundedPrecision) / power).toFixed(boundedPrecision);

            if (optionals > maxDecimals - boundedPrecision) {
                optionalsRegExp = new RegExp('\\.?0{1,' + (optionals - (maxDecimals - boundedPrecision)) + '}$');
                output = output.replace(optionalsRegExp, '');
            }

            return output;
        }
    };

    // avaliable options
    numeral.options = options;

    // avaliable formats
    numeral.formats = formats;

    // avaliable formats
    numeral.locales = locales;

    // This function sets the current locale.  If
    // no arguments are passed in, it will simply return the current global
    // locale key.
    numeral.locale = function(key) {
        if (key) {
            options.currentLocale = key.toLowerCase();
        }

        return options.currentLocale;
    };

    // This function provides access to the loaded locale data.  If
    // no arguments are passed in, it will simply return the current
    // global locale object.
    numeral.localeData = function(key) {
        if (!key) {
            return locales[options.currentLocale];
        }

        key = key.toLowerCase();

        if (!locales[key]) {
            throw new Error('Unknown locale : ' + key);
        }

        return locales[key];
    };

    numeral.reset = function() {
        for (var property in defaults) {
            options[property] = defaults[property];
        }
    };

    numeral.zeroFormat = function(format) {
        options.zeroFormat = typeof(format) === 'string' ? format : null;
    };

    numeral.nullFormat = function (format) {
        options.nullFormat = typeof(format) === 'string' ? format : null;
    };

    numeral.defaultFormat = function(format) {
        options.defaultFormat = typeof(format) === 'string' ? format : '0.0';
    };

    numeral.register = function(type, name, format) {
        name = name.toLowerCase();

        if (this[type + 's'][name]) {
            throw new TypeError(name + ' ' + type + ' already registered.');
        }

        this[type + 's'][name] = format;

        return format;
    };


    numeral.validate = function(val, culture) {
        var _decimalSep,
            _thousandSep,
            _currSymbol,
            _valArray,
            _abbrObj,
            _thousandRegEx,
            localeData,
            temp;

        //coerce val to string
        if (typeof val !== 'string') {
            val += '';

            if (console.warn) {
                console.warn('Numeral.js: Value is not string. It has been co-erced to: ', val);
            }
        }

        //trim whitespaces from either sides
        val = val.trim();

        //if val is just digits return true
        if (!!val.match(/^\d+$/)) {
            return true;
        }

        //if val is empty return false
        if (val === '') {
            return false;
        }

        //get the decimal and thousands separator from numeral.localeData
        try {
            //check if the culture is understood by numeral. if not, default it to current locale
            localeData = numeral.localeData(culture);
        } catch (e) {
            localeData = numeral.localeData(numeral.locale());
        }

        //setup the delimiters and currency symbol based on culture/locale
        _currSymbol = localeData.currency.symbol;
        _abbrObj = localeData.abbreviations;
        _decimalSep = localeData.delimiters.decimal;
        if (localeData.delimiters.thousands === '.') {
            _thousandSep = '\\.';
        } else {
            _thousandSep = localeData.delimiters.thousands;
        }

        // validating currency symbol
        temp = val.match(/^[^\d]+/);
        if (temp !== null) {
            val = val.substr(1);
            if (temp[0] !== _currSymbol) {
                return false;
            }
        }

        //validating abbreviation symbol
        temp = val.match(/[^\d]+$/);
        if (temp !== null) {
            val = val.slice(0, -1);
            if (temp[0] !== _abbrObj.thousand && temp[0] !== _abbrObj.million && temp[0] !== _abbrObj.billion && temp[0] !== _abbrObj.trillion) {
                return false;
            }
        }

        _thousandRegEx = new RegExp(_thousandSep + '{2}');

        if (!val.match(/[^\d.,]/g)) {
            _valArray = val.split(_decimalSep);
            if (_valArray.length > 2) {
                return false;
            } else {
                if (_valArray.length < 2) {
                    return ( !! _valArray[0].match(/^\d+.*\d$/) && !_valArray[0].match(_thousandRegEx));
                } else {
                    if (_valArray[0].length === 1) {
                        return ( !! _valArray[0].match(/^\d+$/) && !_valArray[0].match(_thousandRegEx) && !! _valArray[1].match(/^\d+$/));
                    } else {
                        return ( !! _valArray[0].match(/^\d+.*\d$/) && !_valArray[0].match(_thousandRegEx) && !! _valArray[1].match(/^\d+$/));
                    }
                }
            }
        }

        return false;
    };


    /************************************
        Numeral Prototype
    ************************************/

    numeral.fn = Numeral.prototype = {
        clone: function() {
            return numeral(this);
        },
        format: function(inputString, roundingFunction) {
            var value = this._value,
                format = inputString || options.defaultFormat,
                kind,
                output,
                formatFunction;

            // make sure we have a roundingFunction
            roundingFunction = roundingFunction || Math.round;

            // format based on value
            if (value === 0 && options.zeroFormat !== null) {
                output = options.zeroFormat;
            } else if (value === null && options.nullFormat !== null) {
                output = options.nullFormat;
            } else {
                for (kind in formats) {
                    if (format.match(formats[kind].regexps.format)) {
                        formatFunction = formats[kind].format;

                        break;
                    }
                }

                formatFunction = formatFunction || numeral._.numberToFormat;

                output = formatFunction(value, format, roundingFunction);
            }

            return output;
        },
        value: function() {
            return this._value;
        },
        input: function() {
            return this._input;
        },
        set: function(value) {
            this._value = Number(value);

            return this;
        },
        add: function(value) {
            var corrFactor = _.correctionFactor.call(null, this._value, value);

            function cback(accum, curr, currI, O) {
                return accum + Math.round(corrFactor * curr);
            }

            this._value = _.reduce([this._value, value], cback, 0) / corrFactor;

            return this;
        },
        subtract: function(value) {
            var corrFactor = _.correctionFactor.call(null, this._value, value);

            function cback(accum, curr, currI, O) {
                return accum - Math.round(corrFactor * curr);
            }

            this._value = _.reduce([value], cback, Math.round(this._value * corrFactor)) / corrFactor;

            return this;
        },
        multiply: function(value) {
            function cback(accum, curr, currI, O) {
                var corrFactor = _.correctionFactor(accum, curr);
                return Math.round(accum * corrFactor) * Math.round(curr * corrFactor) / Math.round(corrFactor * corrFactor);
            }

            this._value = _.reduce([this._value, value], cback, 1);

            return this;
        },
        divide: function(value) {
            function cback(accum, curr, currI, O) {
                var corrFactor = _.correctionFactor(accum, curr);
                return Math.round(accum * corrFactor) / Math.round(curr * corrFactor);
            }

            this._value = _.reduce([this._value, value], cback);

            return this;
        },
        difference: function(value) {
            return Math.abs(numeral(this._value).subtract(value).value());
        }
    };

    /************************************
        Default Locale && Format
    ************************************/

    numeral.register('locale', 'en', {
        delimiters: {
            thousands: ',',
            decimal: '.'
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal: function(number) {
            var b = number % 10;
            return (~~(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
        },
        currency: {
            symbol: '$'
        }
    });

    

(function() {
        numeral.register('format', 'bps', {
            regexps: {
                format: /(BPS)/,
                unformat: /(BPS)/
            },
            format: function(value, format, roundingFunction) {
                var space = numeral._.includes(format, ' BPS') ? ' ' : '',
                    output;

                value = value * 10000;

                // check for space before BPS
                format = format.replace(/\s?BPS/, '');

                output = numeral._.numberToFormat(value, format, roundingFunction);

                if (numeral._.includes(output, ')')) {
                    output = output.split('');

                    output.splice(-1, 0, space + 'BPS');

                    output = output.join('');
                } else {
                    output = output + space + 'BPS';
                }

                return output;
            },
            unformat: function(string) {
                return +(numeral._.stringToNumber(string) * 0.0001).toFixed(15);
            }
        });
})();


(function() {
        var decimal = {
            base: 1000,
            suffixes: ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        },
        binary = {
            base: 1024,
            suffixes: ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']
        };

    var allSuffixes =  decimal.suffixes.concat(binary.suffixes.filter(function (item) {
            return decimal.suffixes.indexOf(item) < 0;
        }));
        var unformatRegex = allSuffixes.join('|');
        // Allow support for BPS (http://www.investopedia.com/terms/b/basispoint.asp)
        unformatRegex = '(' + unformatRegex.replace('B', 'B(?!PS)') + ')';

    numeral.register('format', 'bytes', {
        regexps: {
            format: /([0\s]i?b)/,
            unformat: new RegExp(unformatRegex)
        },
        format: function(value, format, roundingFunction) {
            var output,
                bytes = numeral._.includes(format, 'ib') ? binary : decimal,
                suffix = numeral._.includes(format, ' b') || numeral._.includes(format, ' ib') ? ' ' : '',
                power,
                min,
                max;

            // check for space before
            format = format.replace(/\s?i?b/, '');

            for (power = 0; power <= bytes.suffixes.length; power++) {
                min = Math.pow(bytes.base, power);
                max = Math.pow(bytes.base, power + 1);

                if (value === null || value === 0 || value >= min && value < max) {
                    suffix += bytes.suffixes[power];

                    if (min > 0) {
                        value = value / min;
                    }

                    break;
                }
            }

            output = numeral._.numberToFormat(value, format, roundingFunction);

            return output + suffix;
        },
        unformat: function(string) {
            var value = numeral._.stringToNumber(string),
                power,
                bytesMultiplier;

            if (value) {
                for (power = decimal.suffixes.length - 1; power >= 0; power--) {
                    if (numeral._.includes(string, decimal.suffixes[power])) {
                        bytesMultiplier = Math.pow(decimal.base, power);

                        break;
                    }

                    if (numeral._.includes(string, binary.suffixes[power])) {
                        bytesMultiplier = Math.pow(binary.base, power);

                        break;
                    }
                }

                value *= (bytesMultiplier || 1);
            }

            return value;
        }
    });
})();


(function() {
        numeral.register('format', 'currency', {
        regexps: {
            format: /(\$)/
        },
        format: function(value, format, roundingFunction) {
            var locale = numeral.locales[numeral.options.currentLocale],
                symbols = {
                    before: format.match(/^([\+|\-|\(|\s|\$]*)/)[0],
                    after: format.match(/([\+|\-|\)|\s|\$]*)$/)[0]
                },
                output,
                symbol,
                i;

            // strip format of spaces and $
            format = format.replace(/\s?\$\s?/, '');

            // format the number
            output = numeral._.numberToFormat(value, format, roundingFunction);

            // update the before and after based on value
            if (value >= 0) {
                symbols.before = symbols.before.replace(/[\-\(]/, '');
                symbols.after = symbols.after.replace(/[\-\)]/, '');
            } else if (value < 0 && (!numeral._.includes(symbols.before, '-') && !numeral._.includes(symbols.before, '('))) {
                symbols.before = '-' + symbols.before;
            }

            // loop through each before symbol
            for (i = 0; i < symbols.before.length; i++) {
                symbol = symbols.before[i];

                switch (symbol) {
                    case '$':
                        output = numeral._.insert(output, locale.currency.symbol, i);
                        break;
                    case ' ':
                        output = numeral._.insert(output, ' ', i + locale.currency.symbol.length - 1);
                        break;
                }
            }

            // loop through each after symbol
            for (i = symbols.after.length - 1; i >= 0; i--) {
                symbol = symbols.after[i];

                switch (symbol) {
                    case '$':
                        output = i === symbols.after.length - 1 ? output + locale.currency.symbol : numeral._.insert(output, locale.currency.symbol, -(symbols.after.length - (1 + i)));
                        break;
                    case ' ':
                        output = i === symbols.after.length - 1 ? output + ' ' : numeral._.insert(output, ' ', -(symbols.after.length - (1 + i) + locale.currency.symbol.length - 1));
                        break;
                }
            }


            return output;
        }
    });
})();


(function() {
        numeral.register('format', 'exponential', {
        regexps: {
            format: /(e\+|e-)/,
            unformat: /(e\+|e-)/
        },
        format: function(value, format, roundingFunction) {
            var output,
                exponential = typeof value === 'number' && !numeral._.isNaN(value) ? value.toExponential() : '0e+0',
                parts = exponential.split('e');

            format = format.replace(/e[\+|\-]{1}0/, '');

            output = numeral._.numberToFormat(Number(parts[0]), format, roundingFunction);

            return output + 'e' + parts[1];
        },
        unformat: function(string) {
            var parts = numeral._.includes(string, 'e+') ? string.split('e+') : string.split('e-'),
                value = Number(parts[0]),
                power = Number(parts[1]);

            power = numeral._.includes(string, 'e-') ? power *= -1 : power;

            function cback(accum, curr, currI, O) {
                var corrFactor = numeral._.correctionFactor(accum, curr),
                    num = (accum * corrFactor) * (curr * corrFactor) / (corrFactor * corrFactor);
                return num;
            }

            return numeral._.reduce([value, Math.pow(10, power)], cback, 1);
        }
    });
})();


(function() {
        numeral.register('format', 'ordinal', {
        regexps: {
            format: /(o)/
        },
        format: function(value, format, roundingFunction) {
            var locale = numeral.locales[numeral.options.currentLocale],
                output,
                ordinal = numeral._.includes(format, ' o') ? ' ' : '';

            // check for space before
            format = format.replace(/\s?o/, '');

            ordinal += locale.ordinal(value);

            output = numeral._.numberToFormat(value, format, roundingFunction);

            return output + ordinal;
        }
    });
})();


(function() {
        numeral.register('format', 'percentage', {
        regexps: {
            format: /(%)/,
            unformat: /(%)/
        },
        format: function(value, format, roundingFunction) {
            var space = numeral._.includes(format, ' %') ? ' ' : '',
                output;

            if (numeral.options.scalePercentBy100) {
                value = value * 100;
            }

            // check for space before %
            format = format.replace(/\s?\%/, '');

            output = numeral._.numberToFormat(value, format, roundingFunction);

            if (numeral._.includes(output, ')')) {
                output = output.split('');

                output.splice(-1, 0, space + '%');

                output = output.join('');
            } else {
                output = output + space + '%';
            }

            return output;
        },
        unformat: function(string) {
            var number = numeral._.stringToNumber(string);
            if (numeral.options.scalePercentBy100) {
                return number * 0.01;
            }
            return number;
        }
    });
})();


(function() {
        numeral.register('format', 'time', {
        regexps: {
            format: /(:)/,
            unformat: /(:)/
        },
        format: function(value, format, roundingFunction) {
            var hours = Math.floor(value / 60 / 60),
                minutes = Math.floor((value - (hours * 60 * 60)) / 60),
                seconds = Math.round(value - (hours * 60 * 60) - (minutes * 60));

            return hours + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds);
        },
        unformat: function(string) {
            var timeArray = string.split(':'),
                seconds = 0;

            // turn hours and minutes into seconds and add them all up
            if (timeArray.length === 3) {
                // hours
                seconds = seconds + (Number(timeArray[0]) * 60 * 60);
                // minutes
                seconds = seconds + (Number(timeArray[1]) * 60);
                // seconds
                seconds = seconds + Number(timeArray[2]);
            } else if (timeArray.length === 2) {
                // minutes
                seconds = seconds + (Number(timeArray[0]) * 60);
                // seconds
                seconds = seconds + Number(timeArray[1]);
            }
            return Number(seconds);
        }
    });
})();

return numeral;
}));


/***/ }),

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_image_picker__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_numeral__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_numeral___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_numeral__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddMyCarPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


 //-------> Test connect to server .php







//-----> Providers


var AddMyCarPage = (function () {
    function AddMyCarPage(viewCtrl, navCtrl, navParams, alertCtrl, modalCtrl, loadingCtrl, http, app, platform, userInfoProvider, msgService, imagePicker, transfer, file, filePath, geolocation, ga) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.app = app;
        this.platform = platform;
        this.userInfoProvider = userInfoProvider;
        this.msgService = msgService;
        this.imagePicker = imagePicker;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.geolocation = geolocation;
        this.ga = ga;
        this.marketURI = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";
        this.searchBrand = [];
        this.showModelFlag = false;
        this.navigationFlag = false;
        this.sunLoopFlag = false;
        this.blackBoxFlag = false;
        this.warrantyFlag = false;
        this.rearSensorFlag = false;
        this.startButtonFlag = false;
        this.rearCameraFlag = false;
        this.heatingSeatFlag = false;
        this.normalTypeFlag = false;
        this.specialTypeFlag = false;
        //images;
        this.rep = '';
        this.registDoc = '';
        this.once = '';
        this.navigation_on = '';
        this.navigation_off = '';
        this.sunLoop_on = '';
        this.sunLoop_off = '';
        this.blackBox_on = '';
        this.blackBox_off = '';
        this.warranty_on = '';
        this.warranty_off = '';
        this.rearSensor_on = '';
        this.rearSensor_off = '';
        this.startButton_on = '';
        this.startButton_off = '';
        this.rearCamera_on = '';
        this.rearCamera_off = '';
        this.heatingSeat_on = '';
        this.heatingSeat_off = '';
        this.getRepImgUrl = ''; //--> 대표 이미지 url
        this.repImgFileName = 'noImg'; //--> 대표 이미지 nane
        this.getRegistImgUrl = ''; //--> 등록증 이미지 url
        this.registImgFileName = 'noImg'; //--> 등록증 이미지 name
        this.getOtherImgCnt = 0; //--> 추가 이미지 수
        this.getOtherImgUrl = [7]; //--> 추가 이미지 url
        this.otherImgFileName = [7]; //--> 추가 이미지 name
        this.photo_main_list = []; //--> Blob file for 대표, 등록증 이미지
        this.photo_sub_list = []; //--> Blob file for 추가 이미지
        this.location = 'initial'; //--> 차량 위치
        this.searchPlace = '';
        this.address = ''; //--> 차량 소재지 주소
        this.address_lat = 0; //--> 위도
        this.address_lon = 0; //--> 경도
        this.owner_name = ''; //--> 소유주 이름
        this.name = ''; //--> 맴버 이름
        this.email = ''; //--> 맴버 이메일
        this.pw = '';
        this.phone = ''; //--> 맴버 전화번호
        this.member_photo = ''; //--> 맴버 사진
        this.price = 0; //--> 차량 가격
        this.str_price = '';
        this.mileage = 0; //--> 주행거리
        this.str_mileage = '';
        this.maker = ''; //--> 제조사
        this.maker_key = '';
        this.model = ''; //--> 모델
        this.model_key = '';
        this.model_sub = ''; //--> 세부모델
        this.model_sub_key = '';
        this.model_level = ''; //--> 모델레벨
        this.regist_type = ''; //--> 등록타입  [regist_type] * 등록타입(0:무료, 1:유료A), 2:유료B
        this.car_number = ''; //--> 차량 등록 번호
        this.yearMonth = ''; //--> 최초등록년월
        this.year = ''; //--> 최초등록년
        this.month = ''; //--> 최초등록월
        this.puer = ''; //--> 연료
        this.gear = ''; //--> 변속기
        this.color = ''; //--> 색상
        this.additory_options = ''; //--> 추가옵션
        this.repair = ''; //--> 정비이력
        this.replacement = ''; //--> 교환이력
        this.options = []; //--> 주요옵션 [1:블랙박스, 2:열선시트, 3:네비게이션, 4:후방카메라, 5:후방센서, 6:선루프, 7:START버튼, 8:제조사보증]
        this.introduce = ''; //--> 내차 소개
        this.specialFee = 0; //--> 스페셜 등록 수수료 
        //500만원 이하 차량은 5.5만원(부가세 포함) 
        //500만원초과 1000만원 이하 7.7만원
        //1000만원 초과 2000만원 이하 11만원
        //2000만원 초과 차량은 16.5만원입니다~!
        this.puers = [
            { id: '1', puer: '디젤' },
            { id: '2', puer: '가솔린' },
            { id: '3', puer: '하이브리드' },
            { id: '4', puer: '전기' },
            { id: '5', puer: '가스' },
            { id: '6', puer: '기타' }
        ];
        this.gears = [
            { id: '1', gear: '자동' },
            { id: '2', gear: '수동' },
            { id: '3', gear: '기타' },
        ];
        this.colors = [
            { c_color: '검정색' },
            { c_color: '흰색' },
            { c_color: '은색' },
            { c_color: '은회색' },
            { c_color: '쥐색' },
            { c_color: '청색' },
            { c_color: '하늘색' },
            { c_color: '갈대색' },
            { c_color: '갈색' },
            { c_color: '자주색' },
            { c_color: '빨간색' }
        ];
        for (var i = 0; i < 7; i++) {
            this.otherImgFileName[i] = 'noImg';
        }
        this.rep = "img/icon/regist/rep.png";
        this.registDoc = "img/icon/regist/registDoc.png";
        this.once = "img/icon/regist/once.png";
        this.navigation_on = "img/option/navigation_on.png";
        this.navigation_off = "img/option/navigation_off.png";
        this.sunLoop_on = "img/option/sunLoop_on.png";
        this.sunLoop_off = "img/option/sunLoop_off.png";
        this.blackBox_on = "img/option/blackBox_on.png";
        this.blackBox_off = "img/option/blackBox_off.png";
        this.warranty_on = "img/option/warranty_on.png";
        this.warranty_off = "img/option/warranty_off.png";
        this.rearSensor_on = "img/option/rearSensor_on.png";
        this.rearSensor_off = "img/option/rearSensor_off.png";
        this.startButton_on = "img/option/startButton_on.png";
        this.startButton_off = "img/option/startButton_off.png";
        this.rearCamera_on = "img/option/rearCamera_on.png";
        this.rearCamera_off = "img/option/rearCamera_off.png";
        this.heatingSeat_on = "img/option/heatingSeat_on.png";
        this.heatingSeat_off = "img/option/heatingSeat_off.png";
    }
    AddMyCarPage.prototype.ionViewDidLoad = function () {
        this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
        this.email = this.userInfoProvider.userProfile.u_email;
        this.pw = this.userInfoProvider.userProfile.u_pw;
        this.loadMap();
        console.log('ionViewDidLoad AddMyCarPage');
    };
    AddMyCarPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('내차등록'); //trackView for Google Analytics
    };
    AddMyCarPage.prototype.loadMap = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (position) {
            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            var mapOptions = {
                center: latLng,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false,
                fullscreenControl: false
            };
            var element = document.getElementById('amap');
            _this.map = new google.maps.Map(element, mapOptions);
            var input_from = (document.getElementById("place_input"));
            var options = {
                types: [],
                componentRestrictions: { country: "kr" }
            };
            // create the two autocompletes on the place_input fields
            var autocomplete = new google.maps.places.Autocomplete(input_from, options);
            autocomplete.bindTo("bounds", _this.map);
            // we need to save a reference to this as we lose it in the callbacks
            var self = _this;
            //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                map: _this.map,
                animation: google.maps.Animation.DROP,
                position: _this.map.getCenter()
            });
            // add the first listener
            autocomplete.addListener("place_changed", function () {
                infowindow.close();
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }
                else {
                    self.location = place.name;
                    self.address = place.formatted_address;
                    self.address_lat = place.geometry.location.lat();
                    self.address_lon = place.geometry.location.lng();
                }
                // Set the position of the marker using the place ID and location.
                marker.setPlace(/** @type {!google.maps.Place} */ ({
                    placeId: place.place_id,
                    location: place.geometry.location
                }));
                marker.setVisible(true);
                infowindow.setContent("<div><strong>" + place.name + "</strong><br>" + "<br>" + place.formatted_address);
                infowindow.open(this.map, marker);
                self.f_introduce.setFocus();
            });
        });
    };
    AddMyCarPage.prototype.addRepImage = function () {
        var _this = this;
        var options = {
            maximumImagesCount: 1, width: 800, height: 800, quality: 100
        };
        this.imagePicker.getPictures(options).then(function (results) {
            if (results[0] != undefined) {
                _this.getRepImgUrl = results[0];
                console.log('this.getRepImgUrl: ' + _this.getRepImgUrl);
                if (_this.platform.is('android')) {
                    _this.filePath.resolveNativePath(_this.getRepImgUrl)
                        .then(function (filePath) {
                        //Create a new name for the Image
                        var d = new Date();
                        var n = d.getTime();
                        _this.repImgFileName = "representImage_" + n + ".jpg"; //한글 이름 않됨 !!!!
                        _this.CreateMainImage(filePath, 0);
                    });
                }
                else {
                    //Create a new name for the Image
                    var d = new Date();
                    var n = d.getTime();
                    _this.repImgFileName = "representImage_" + n + ".jpg"; //한글 이름 않됨 !!!!
                    _this.CreateMainImage(_this.getRepImgUrl, 0);
                }
            }
        }, function (err) {
            console.log('imagePicker.getPictures ERROR: ' + err);
        });
    };
    AddMyCarPage.prototype.addRegistImage = function () {
        var _this = this;
        var options = {
            maximumImagesCount: 1, width: 800, height: 800, quality: 100
        };
        this.imagePicker.getPictures(options).then(function (results) {
            if (results[0] != undefined) {
                _this.getRegistImgUrl = results[0];
                console.log('this.getRegistImgUrl: ' + _this.getRegistImgUrl);
                if (_this.platform.is('android')) {
                    _this.filePath.resolveNativePath(_this.getRegistImgUrl)
                        .then(function (filePath) {
                        //Create a new name for the Image
                        var d = new Date();
                        var n = d.getTime();
                        _this.registImgFileName = "registImage_" + n + ".jpg"; //한글 이름 않됨 !!!!
                        _this.CreateMainImage(filePath, 1);
                    });
                }
                else {
                    //Create a new name for the Image
                    var d = new Date();
                    var n = d.getTime();
                    _this.registImgFileName = "registImage_" + n + ".jpg"; //한글 이름 않됨 !!!!
                    _this.CreateMainImage(_this.getRegistImgUrl, 1);
                }
            }
        }, function (err) {
            console.log('imagePicker.getPictures ERROR: ' + err);
        });
    };
    AddMyCarPage.prototype.addOtherImage = function () {
        var _this = this;
        //this.getOtherImgCnt = 1;
        var options = {
            maximumImagesCount: 7,
            width: 800,
            height: 800,
            quality: 80 // 0-100, default 100 which is highest quality
        };
        for (var i = 0; i < 7; i++) {
            this.otherImgFileName[i] = 'noImg';
        }
        this.imagePicker.getPictures(options).then(function (results) {
            if (results.length != 0) {
                if (results.length > 7) {
                    _this.getOtherImgCnt = 7;
                }
                else {
                    _this.getOtherImgCnt = results.length;
                }
                var _loop_1 = function (i) {
                    _this.getOtherImgUrl[i] = results[i];
                    if (_this.platform.is('android')) {
                        _this.filePath.resolveNativePath(_this.getOtherImgUrl[i])
                            .then(function (filePath) {
                            //Create a new name for the Image
                            var d = new Date();
                            var n = d.getTime();
                            _this.otherImgFileName[i] = "subImage_" + [i] + "_" + n + ".jpg"; //한글 이름 않됨 !!!!
                            _this.CreateSubImage(filePath, i);
                        });
                    }
                    else {
                        //Create a new name for the Image
                        var d = new Date();
                        var n = d.getTime();
                        _this.otherImgFileName[i] = "subImage_" + [i] + "_" + n + ".jpg"; //한글 이름 않됨 !!!!
                        _this.CreateSubImage(_this.getOtherImgUrl[i], i);
                    }
                };
                for (var i = 0; i < _this.getOtherImgCnt; i++) {
                    _loop_1(i);
                }
            }
        }, function (err) {
            console.log('imagePicker.getPictures ERROR: ' + err);
        });
    };
    //Main Image파일 생성
    AddMyCarPage.prototype.CreateMainImage = function (fileName, index) {
        var _this = this;
        this.file.resolveLocalFilesystemUrl(fileName).then(function (fileEntry) {
            fileEntry.file(function (resFile) {
                var reader = new FileReader();
                reader.onloadend = function (evt) {
                    var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                    imgBlob.name = fileName;
                    _this.photo_main_list[index] = imgBlob;
                    console.log('Create MainImage: ' + fileName);
                };
                reader.onerror = function (e) {
                    console.log("Failed file read: " + e.toString());
                };
                reader.readAsArrayBuffer(resFile);
            });
        }, function (err) {
            console.log(err);
            //alert(JSON.stringify(err))
        });
    };
    //Sub Image파일 생성
    AddMyCarPage.prototype.CreateSubImage = function (fileName, index) {
        var _this = this;
        this.file.resolveLocalFilesystemUrl(fileName).then(function (fileEntry) {
            fileEntry.file(function (resFile) {
                var reader = new FileReader();
                reader.onloadend = function (evt) {
                    var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                    imgBlob.name = fileName;
                    _this.photo_sub_list[index] = imgBlob;
                    console.log('Create SubImage: ' + fileName);
                };
                reader.onerror = function (e) {
                    console.log("Failed file read: " + e.toString());
                };
                reader.readAsArrayBuffer(resFile);
            });
        }, function (err) {
            console.log(err);
            //alert(JSON.stringify(err))
        });
    };
    AddMyCarPage.prototype.sameOwner = function () {
        this.owner_name = this.userInfoProvider.userProfile.u_name;
        if (this.userInfoProvider.userProfile.u_phone_num == 'google' ||
            this.userInfoProvider.userProfile.u_phone_num == 'facebook' ||
            this.userInfoProvider.userProfile.u_phone_num == 'kakao') {
            this.phone = '';
        }
        else {
            this.phone = this.userInfoProvider.userProfile.u_phone_num;
        }
    };
    AddMyCarPage.prototype.SearchBrand = function () {
        var _this = this;
        this.model_sub_key = '0';
        this.model_sub = '';
        var searchBrandPage_modal = this.modalCtrl.create('SearchBrandPage', { fromPage: 'addMyCarPage' });
        searchBrandPage_modal.onDidDismiss(function (data) {
            _this.searchBrand = [];
            if (data.maker_name == '전체' || data.model_name == '전체') {
                _this.showModelFlag = false;
            }
            else {
                //{maker_key: "1", maker_name: "현대", model_key: "9", model_name: "i30"}
                //console.log('maker_key + model_key + level_key ===> '+data.maker_key+' + '+ data.model_key+' + '+ data.level_key+' ;')
                _this.searchBrand.push(data.maker_name + '-' + data.model_name);
                _this.maker_key = data.maker_key;
                _this.maker = data.maker_name;
                _this.model_key = data.model_key;
                _this.model = data.model_name;
                _this.model_sub_key = data.level_key;
                _this.model_sub = data.level_name;
                _this.showModelFlag = true;
            }
            _this.f_model_level.setFocus();
        });
        searchBrandPage_modal.present();
    };
    //-------------------------------------------------> for next input focus
    AddMyCarPage.prototype.f_owner_name_eventHandler = function (e) {
        if (e.keyCode == 13) {
            this.f_phone.setFocus();
        }
    };
    AddMyCarPage.prototype.f_phone_eventHandler = function (e) {
        if (e.keyCode == 13) {
            if (!this.chk_phoneNumber(this.phone)) {
                var alert_1 = this.alertCtrl.create({
                    title: '알림!',
                    subTitle: '잘못된 전화번호입니다. 숫자만 입력해주세요',
                    buttons: ['확인']
                });
                alert_1.present();
                this.phone = '';
            }
            else {
                this.f_str_price.setFocus();
            }
        }
    };
    AddMyCarPage.prototype.onChangePrice = function (evt) {
        this.str_price = evt.replace(/[^0-9.]/g, "");
        if (this.str_price) {
            var myNumeral = __WEBPACK_IMPORTED_MODULE_9_numeral___default()(this.str_price);
            var value = myNumeral.value();
            this.price = Number(value);
            if (this.price > 0) {
                this.str_price = __WEBPACK_IMPORTED_MODULE_9_numeral___default()(this.price).format('0,0');
                console.log("onChangePrice str_price_formatted: " + this.str_price);
                this.specialTypeFlag = true;
                this.regist_type = '1'; //등록타입(0:무료, 1:유료)
                //500만원 이하 차량은 5.5만원(부가세 포함) 
                //500만원초과 1000만원 이하 7.7만원
                //1000만원 초과 2000만원 이하 11만원
                //2000만원 초과 차량은 16.5만원입니다~!
                if (this.price < 500) {
                    this.specialFee = 55000;
                }
                else if (this.price >= 500 && this.price < 1000) {
                    this.specialFee = 77000;
                }
                else if (this.price >= 1000 && this.price < 2000) {
                    this.specialFee = 110000;
                }
                else if (this.price >= 2000) {
                    this.specialFee = 165000;
                }
            }
            else {
                var alert_2 = this.alertCtrl.create({
                    title: '알림',
                    subTitle: '차량가격을 입력해주세요',
                    buttons: ['확인']
                });
                alert_2.present();
            }
        }
        else {
            this.str_price = '0';
            this.price = 0;
        }
    };
    AddMyCarPage.prototype.f_model_level_eventHandler = function (e) {
        if (e.keyCode == 13) {
            this.f_car_number.setFocus();
        }
    };
    AddMyCarPage.prototype.f_car_number_eventHandler = function (e) {
        if (e.keyCode == 13) {
            this.f_str_mileage.setFocus();
        }
    };
    AddMyCarPage.prototype.onChangeMileage = function (evt) {
        this.str_mileage = evt.replace(/[^0-9.]/g, "");
        if (this.str_mileage) {
            var myNumeral = __WEBPACK_IMPORTED_MODULE_9_numeral___default()(this.str_mileage);
            var value = myNumeral.value();
            this.mileage = Number(value);
            if (this.mileage > 0) {
                this.str_mileage = __WEBPACK_IMPORTED_MODULE_9_numeral___default()(value).format('0,0');
            }
            else {
                var alert_3 = this.alertCtrl.create({
                    title: '알림',
                    subTitle: '주행거리를 입력해주세요',
                    buttons: ['확인']
                });
                alert_3.present();
            }
        }
        else {
            this.str_mileage = '0';
            this.mileage = 0;
        }
    };
    //-------------------------------------------------> for next input focus
    AddMyCarPage.prototype.chk_phoneNumber = function (phoneNumber) {
        //console.log('check this.phoneNumber :' +phoneNumber);
        var regExp = /^[0-9]+$/;
        if (!regExp.test(phoneNumber)) {
            return false;
        }
        else {
            return true;
        }
    };
    AddMyCarPage.prototype.selectedColor = function (e) {
        console.log('color:' + this.color);
    };
    AddMyCarPage.prototype.selectedPuer = function (e) {
        console.log('puer:' + this.puer);
    };
    AddMyCarPage.prototype.selectedGear = function (e) {
        console.log('gear:' + this.gear);
        //this.f_additory_options.setFocus();
        /*
        console.log('selectedGear change event:' +e);
        if(e != undefined) {
          setTimeout(() => {
            //Keyboard.show() // for android
            this.f_additory_options.setFocus();
          },200);
        }
        */
    };
    // [1:네비게이션, 2:선루프, 3:블랙박스, 4:제조사보증, 5:후방센서, 6:START버튼, 7:후방카메라, 8:열선시트]
    AddMyCarPage.prototype.navigationOnOff = function () {
        if (this.navigationFlag == false) {
            this.navigationFlag = true;
            this.options.push('1');
        }
        else {
            this.navigationFlag = false;
            var index = this.options.indexOf('1');
            if (index > -1) {
                this.options.splice(index, 1);
            }
        }
        //console.log('this.options : '+this.options);
    };
    AddMyCarPage.prototype.sunLoopOnOff = function () {
        if (this.sunLoopFlag == false) {
            this.sunLoopFlag = true;
            this.options.push('2');
        }
        else {
            this.sunLoopFlag = false;
            var index = this.options.indexOf('2');
            if (index > -1) {
                this.options.splice(index, 1);
            }
        }
        //console.log('this.options : '+this.options);
    };
    AddMyCarPage.prototype.blackBoxOnOff = function () {
        if (this.blackBoxFlag == false) {
            this.blackBoxFlag = true;
            this.options.push('3');
        }
        else {
            this.blackBoxFlag = false;
            var index = this.options.indexOf('3');
            if (index > -1) {
                this.options.splice(index, 1);
            }
        }
        //console.log('this.options : '+this.options);
    };
    AddMyCarPage.prototype.warrantyOnOff = function () {
        if (this.warrantyFlag == false) {
            this.warrantyFlag = true;
            this.options.push('4');
        }
        else {
            this.warrantyFlag = false;
            var index = this.options.indexOf('4');
            if (index > -1) {
                this.options.splice(index, 1);
            }
        }
        //console.log('this.options : '+this.options);
    };
    AddMyCarPage.prototype.rearSensorOnOff = function () {
        if (this.rearSensorFlag == false) {
            this.rearSensorFlag = true;
            this.options.push('5');
        }
        else {
            this.rearSensorFlag = false;
            var index = this.options.indexOf('5');
            if (index > -1) {
                this.options.splice(index, 1);
            }
        }
        //console.log('this.options : '+this.options);
    };
    AddMyCarPage.prototype.startButtonOnOff = function () {
        if (this.startButtonFlag == false) {
            this.startButtonFlag = true;
            this.options.push('6');
        }
        else {
            this.startButtonFlag = false;
            var index = this.options.indexOf('6');
            if (index > -1) {
                this.options.splice(index, 1);
            }
        }
        //console.log('this.options : '+this.options);
    };
    AddMyCarPage.prototype.rearCameraOnOff = function () {
        if (this.rearCameraFlag == false) {
            this.rearCameraFlag = true;
            this.options.push('7');
        }
        else {
            this.rearCameraFlag = false;
            var index = this.options.indexOf('7');
            if (index > -1) {
                this.options.splice(index, 1);
            }
        }
        //console.log('this.options : '+this.options);
    };
    AddMyCarPage.prototype.heatingSeatOnOff = function () {
        if (this.heatingSeatFlag == false) {
            this.heatingSeatFlag = true;
            this.options.push('8');
        }
        else {
            this.heatingSeatFlag = false;
            var index = this.options.indexOf('8');
            if (index > -1) {
                this.options.splice(index, 1);
            }
        }
        //console.log('this.options : '+this.options);
    };
    AddMyCarPage.prototype.submitRegiste = function () {
        if (this.owner_name == '') {
            var alert_4 = this.alertCtrl.create({
                title: '알림',
                subTitle: '차량 소유주 이름을 입력해주세요',
                buttons: ['확인']
            });
            alert_4.present();
        }
        else if (!this.chk_phoneNumber(this.phone)) {
            var alert_5 = this.alertCtrl.create({
                title: '알림',
                subTitle: '잘못된 전화번호입니다. 차량 소유주 연락처를 입력해주세요',
                buttons: ['확인']
            });
            alert_5.present();
            this.phone = '';
        }
        else if (this.price == 0) {
            var alert_6 = this.alertCtrl.create({
                title: '알림',
                subTitle: '차량가격을 입력해주세요.',
                buttons: ['확인']
            });
            alert_6.present();
        }
        else if (this.model_key == '') {
            var alert_7 = this.alertCtrl.create({
                title: '알림',
                subTitle: '차량 모델을 선택해주세요',
                buttons: ['확인']
            });
            alert_7.present();
        }
        else if (this.model_level == '') {
            var alert_8 = this.alertCtrl.create({
                title: '알림',
                subTitle: '차량 세부등급을 입력해주세요',
                buttons: ['확인']
            });
            alert_8.present();
        }
        else if (this.car_number == '') {
            var alert_9 = this.alertCtrl.create({
                title: '알림',
                subTitle: '차량등록번호를 입력해주세요',
                buttons: ['확인']
            });
            alert_9.present();
        }
        else if (this.yearMonth == '') {
            var alert_10 = this.alertCtrl.create({
                title: '알림',
                subTitle: '차량연식을 선택해주세요',
                buttons: ['확인']
            });
            alert_10.present();
        }
        else if (this.mileage == 0) {
            var alert_11 = this.alertCtrl.create({
                title: '알림',
                subTitle: '주행거리를 입력해주세요.',
                buttons: ['확인']
            });
            alert_11.present();
        }
        else if (this.puer == '') {
            var alert_12 = this.alertCtrl.create({
                title: '알림',
                subTitle: '연료타입을 선택해주세요.',
                buttons: ['확인']
            });
            alert_12.present();
        }
        else if (this.gear == '') {
            var alert_13 = this.alertCtrl.create({
                title: '알림',
                subTitle: '변속기 타입을 선택해주세요.',
                buttons: ['확인']
            });
            alert_13.present();
        }
        else if (this.color == '') {
            var alert_14 = this.alertCtrl.create({
                title: '알림',
                subTitle: '색상을 선택해주세요.',
                buttons: ['확인']
            });
            alert_14.present();
        }
        else if (this.address == '') {
            var alert_15 = this.alertCtrl.create({
                title: '알림',
                subTitle: '휴대폰의 "위치서비스"를 켜고 차량위치를 입력해주세요.',
                buttons: ['확인']
            });
            alert_15.present();
        }
        else if (this.repImgFileName === 'noImg' || this.registImgFileName === 'noImg') {
            var alert_16 = this.alertCtrl.create({
                title: '알림',
                subTitle: '대표/등록증 이미지를 선택해주세요.',
                buttons: ['확인']
            });
            alert_16.present();
        }
        else {
            this.year = this.yearMonth.substr(0, 4);
            this.month = this.yearMonth.substr(5, 2);
            console.log('<------------------ Input Items ---------------->');
            console.log('model: ' + this.model_key);
            console.log('model_sub: ' + this.model_sub_key);
            console.log('email: ' + this.email);
            console.log('pw: ' + this.pw);
            console.log('phone: ' + this.phone);
            console.log('gear: ' + this.gear);
            console.log('puer: ' + this.puer);
            console.log('car_number: ' + this.car_number);
            console.log('year: ' + this.year);
            console.log('month: ' + this.month);
            console.log('mileage: ' + this.mileage);
            console.log('price: ' + this.price);
            console.log('options: ' + this.options.toString());
            console.log('introduce: ' + this.introduce);
            console.log('model_level: ' + this.model_level);
            console.log('additory_options: ' + this.additory_options);
            console.log('repair: ' + this.repair);
            console.log('replacement: ' + this.replacement);
            console.log('photo_main[0]' + this.photo_main_list[0] + this.repImgFileName);
            console.log('photo_main[1]' + this.photo_main_list[1] + this.registImgFileName);
            for (var i = 0; i < this.photo_sub_list.length; ++i) {
                console.log('photo_sub[]' + this.photo_sub_list[i] + this.otherImgFileName[i]);
            }
            console.log('address: ' + this.address);
            console.log('address_lat: ' + this.address_lat);
            console.log('address_lon: ' + this.address_lon);
            console.log('owner_name: ' + this.owner_name);
            console.log('color: ' + this.color);
            console.log('regist_type: ' + this.regist_type);
            /*
            console.log('<---------------- Non Input Items -------------->');
            console.log('maker_key: ' +this.maker_key);
            console.log('maker: ' +this.maker );
            console.log('model_sub_key: ' +this.model_sub_key);
            console.log('model_sub: ' +this.model_sub);
            console.log('maker_key: ' +this.maker_key);
            console.log('yearMonth: ' +this.yearMonth ); //2000-11
            console.log('region: ' +this.location );
            console.log('phone: ' +this.phone);
            console.log('repImg : ' +this.repImgFileName);      //대표이미지
            console.log('registImg: ' +this.registImgFileName); //등록증이미지
            console.log('otherImgCnt: ' +this.getOtherImgCnt);  //추가이미지 수
            */
            var loading_1 = this.loadingCtrl.create({
                spinner: 'bubbles',
                content: '차량 등록중 입니다...'
            });
            loading_1.present();
            var body = new FormData();
            var xhr_1 = new XMLHttpRequest();
            var env_1 = this;
            ///////////////////////////////////////////////////////////////////////////////////////////////
            body.append('model', this.model_key);
            body.append('model_sub', this.model_sub_key);
            body.append('email', this.email);
            body.append('pw', this.pw);
            body.append('phone', this.phone);
            body.append('gear', this.gear);
            body.append('puer', this.puer);
            body.append('car_number', this.car_number);
            body.append('year', this.year);
            body.append('month', this.month);
            body.append('mileage', String(this.mileage));
            body.append('price', String(this.price));
            body.append('options', this.options.toString());
            body.append('introduce', this.introduce);
            body.append('model_level', this.model_level);
            body.append('additory_options', this.additory_options);
            body.append('repair', this.repair);
            body.append('replacement', this.replacement);
            //메인 이미지
            body.append('photo_main[]', this.photo_main_list[0], this.repImgFileName);
            //서류 이미지
            body.append('photo_main[]', this.photo_main_list[1], this.registImgFileName);
            //서브 이미지
            for (var i = 0; i < this.photo_sub_list.length; ++i) {
                body.append('photo_sub[]', this.photo_sub_list[i], this.otherImgFileName[i]);
            }
            body.append('address', this.address);
            body.append('address_lat', String(this.address_lat));
            body.append('address_lon', String(this.address_lon));
            body.append('owner_name', this.owner_name);
            body.append('color', this.color);
            body.append('regist_type', this.regist_type); // 등록타입(0:무료, 1:유료A)
            ///////////////////////////////////////////////////////////////////////////////////////////////////
            var url_s = this.marketURI + "set_sale_regist.php";
            xhr_1.open("POST", url_s, true);
            xhr_1.send(body);
            xhr_1.onreadystatechange = function () {
                if (xhr_1.readyState == 4) {
                    if (xhr_1.status == 200) {
                        loading_1.dismiss();
                        console.log('xhr.responseText' + xhr_1.responseText);
                        var response = JSON.parse(xhr_1.responseText); //xhr.responseText{"result":"error_no_parameter"}
                        if (response.result == 'ok') {
                            var alert_17 = env_1.alertCtrl.create({
                                title: '알림',
                                subTitle: '차량 등록이 완료되었습니다.',
                                buttons: ['확인']
                            });
                            alert_17.present();
                            var result = true;
                            //document.querySelector(".tabbar")['style'].display = 'flex';
                            env_1.viewCtrl.dismiss(result);
                        }
                        else {
                            var alert_18 = env_1.alertCtrl.create({
                                title: '알림',
                                subTitle: env_1.msgService.getMessage(response.result),
                                buttons: ['확인']
                            });
                            alert_18.present();
                        }
                    }
                    else {
                        console.log('Server response result xhr.readyState-xhr.status + : ' + xhr_1.readyState + ' - ' + xhr_1.status); //response result code
                        //alert('xhr.status: '+xhr.status);
                        //alert('xhr.readyState: '+xhr.readyState);
                    }
                } // end of if (xhr.readyState == 4)
            }; //--> end of xhr.onreadystatechange = function ()
        } //--> end of check image
    }; //--> end of submitRegiste()
    AddMyCarPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return AddMyCarPage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('map'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], AddMyCarPage.prototype, "mapElement", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('places'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], AddMyCarPage.prototype, "places", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('f_owner_name'),
    __metadata("design:type", Object)
], AddMyCarPage.prototype, "f_owner_name", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('f_phone'),
    __metadata("design:type", Object)
], AddMyCarPage.prototype, "f_phone", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('f_str_price'),
    __metadata("design:type", Object)
], AddMyCarPage.prototype, "f_str_price", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('f_model_level'),
    __metadata("design:type", Object)
], AddMyCarPage.prototype, "f_model_level", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('f_car_number'),
    __metadata("design:type", Object)
], AddMyCarPage.prototype, "f_car_number", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('f_str_mileage'),
    __metadata("design:type", Object)
], AddMyCarPage.prototype, "f_str_mileage", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('f_additory_options'),
    __metadata("design:type", Object)
], AddMyCarPage.prototype, "f_additory_options", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('f_introduce'),
    __metadata("design:type", Object)
], AddMyCarPage.prototype, "f_introduce", void 0);
AddMyCarPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-add-my-car',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/add-my-car/add-my-car.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button icon-only color="underblack" (click)="dismiss()">\n        <ion-icon name="ios-arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>내차등록</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-item no-lines style="padding-left: 10px;">\n    <ion-label>차량정보 입력<span style="font-size: 10pt; color:#37aafe">*</span><br>\n      <span style="font-size: 10pt; color:#37aafe">*</span><span style="font-size: 10pt; color:#999999">표시는 필수입력 항목입니다.</span><br>\n      <span style="font-size: 10pt; color:#37aafe">*</span><span style="font-size: 10pt; color:#999999">사진은 가로방향 사진으로 올려주세요.</span>\n    </ion-label>\n  </ion-item>\n\n  <div style="padding-left: 5px; padding-right: 5px;">\n    <div class="spacer" style="height: 5px;"></div>\n    <div class="spacer" style="height: 1px; background-color:#eeeeee;"></div>\n  </div>\n\n  <ion-grid>\n    <ion-row align="center">\n      <ion-col col-4 *ngIf="repImgFileName === \'noImg\'"><img [src]="rep" class="img-size" (click)="addRepImage()"/></ion-col>\n      <ion-col col-4 *ngIf="repImgFileName !== \'noImg\'"><img [src]="getRepImgUrl" class="img-size" (click)="addRepImage()"/></ion-col>\n      <ion-col col-4 *ngIf="registImgFileName === \'noImg\'"><img [src]="registDoc" class="img-size" (click)="addRegistImage()"/></ion-col>\n      <ion-col col-4 *ngIf="registImgFileName !== \'noImg\'"><img [src]="getRegistImgUrl" class="img-size" (click)="addRegistImage()"/></ion-col>\n      <ion-col col-4 *ngIf="otherImgFileName[0] === \'noImg\'"><img [src]="once" class="img-size" (click)="addOtherImage()"/></ion-col>\n      <ion-col col-4 *ngIf="otherImgFileName[0] !== \'noImg\'"><img [src]="getOtherImgUrl[0]" class="img-size" (click)="addOtherImage()"/></ion-col>\n    </ion-row>\n    <ion-row *ngIf="getOtherImgCnt > 1" align="center">\n      <ion-col col-4 *ngIf="otherImgFileName[1] === \'noImg\'"></ion-col>\n      <ion-col col-4 *ngIf="otherImgFileName[1] !== \'noImg\'"><img [src]="getOtherImgUrl[1]" class="img-size"/></ion-col>\n      <ion-col col-4 *ngIf="otherImgFileName[2] === \'noImg\'"></ion-col>\n      <ion-col col-4 *ngIf="otherImgFileName[2] !== \'noImg\'"><img [src]="getOtherImgUrl[2]" class="img-size"/></ion-col>\n      <ion-col col-4 *ngIf="otherImgFileName[3] === \'noImg\'"></ion-col>\n      <ion-col col-4 *ngIf="otherImgFileName[3] !== \'noImg\'"><img [src]="getOtherImgUrl[3]" class="img-size"/></ion-col>\n    </ion-row>\n    <ion-row *ngIf="getOtherImgCnt > 4" align="center">\n      <ion-col col-4 *ngIf="otherImgFileName[4] === \'noImg\'"></ion-col>\n      <ion-col col-4 *ngIf="otherImgFileName[4] !== \'noImg\'"><img [src]="getOtherImgUrl[4]" class="img-size"/></ion-col>\n      <ion-col col-4 *ngIf="otherImgFileName[5] === \'noImg\'"></ion-col>\n      <ion-col col-4 *ngIf="otherImgFileName[5] !== \'noImg\'"><img [src]="getOtherImgUrl[5]" class="img-size"/></ion-col>\n      <ion-col col-4 *ngIf="otherImgFileName[6] === \'noImg\'"></ion-col>\n      <ion-col col-4 *ngIf="otherImgFileName[6] !== \'noImg\'"><img [src]="getOtherImgUrl[6]" class="img-size"/></ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid>\n    <ion-row style="height:40px;">\n      <ion-col col-6>\n        <ion-item no-lines style="padding-left: 0px;"><ion-label>차량 소유주 정보<span style="font-size: 10pt; color:#37aafe">*</span></ion-label></ion-item>\n      </ion-col>\n      <ion-col col-6>\n        <ion-item no-lines align="center" style="padding-left: 10px;">\n          <button ion-button item-right small outline color="dark" style="margin-right: 0px;" (click)="sameOwner()">사용자 정보와 동일</button>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <div class="spacer" style="height: 10px;"></div>\n  <div style="padding-left: 5px; padding-right: 5px;">\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-input #f_owner_name [(ngModel)]="owner_name" type="text" placeholder="소유주 이름" (keypress)="f_owner_name_eventHandler($event)" clearInput></ion-input>\n      </ion-item>\n    </div>\n    <div class="spacer" style="height: 10px;"></div>\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-input #f_phone [(ngModel)]="phone" type="text" placeholder="소유주 연락처" (keypress)="f_phone_eventHandler($event)" clearInput></ion-input>\n      </ion-item>\n    </div>\n  </div>\n  <div class="spacer" style="height: 10px;"></div>\n\n  <ion-item no-lines style="padding-left: 10px;"><ion-label>차량기본정보<span style="font-size: 10pt; color:#37aafe">*</span></ion-label></ion-item>\n  <div style="padding-left: 5px; padding-right: 5px;">\n    <div class="outline-box">\n      <ion-row style="height: 54px;">\n        <ion-col col-9 style="padding-left: 0px;">\n          <ion-item no-lines class="outline-item">\n            <ion-input #f_str_price [(ngModel)]="str_price" type="text" placeholder="차량판매가" \n                      (change)="onChangePrice($event.target.value)" clearInput></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-3>\n          <ion-item no-lines>\n            <ion-label class="outline-item" align="right"><span style="font-size: 11pt;">만원</span></ion-label>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </div>\n    <div class="spacer" style="height: 10px;"></div>\n    <div *ngIf="!showModelFlag" class="outline-box">\n      <button ion-item no-lines detail-none class="outline-item outline-button" block color="underwhite" (click)="SearchBrand()"><font color="#999999">제조사/모델</font>\n        <ion-icon name="ios-arrow-forward" item-end color="undergray"></ion-icon>\n      </button>\n    </div>\n    <div *ngIf="showModelFlag" class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-input [(ngModel)]="searchBrand" type="text" (click)="SearchBrand()" readonly></ion-input>\n      </ion-item>\n    </div>\n    <div *ngIf="model_sub_key != \'0\'">\n      <div class="spacer" style="height: 10px;"></div>\n      <div class="outline-box">\n        <ion-item no-lines class="outline-item">\n          <ion-input [(ngModel)]="model_sub" type="text" placeholder="세부모델" readonly></ion-input>\n        </ion-item>\n      </div>\n    </div>\n    <div class="spacer" style="height: 10px;"></div>\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-input #f_model_level [(ngModel)]="model_level" type="text" placeholder="세부등급(ex: 모던, 프리미엄..)" (keypress)="f_model_level_eventHandler($event)" clearInput></ion-input>\n      </ion-item>\n    </div>\n    <div class="spacer" style="height: 10px;"></div>\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-input #f_car_number [(ngModel)]="car_number" type="text" placeholder="차량번호" (keypress)="f_car_number_eventHandler($event)" clearInput></ion-input>\n      </ion-item>\n    </div>\n    <div class="spacer" style="height: 10px;"></div>\n    <div class="outline-box">\n      <ion-row>\n        <ion-col col-9 style="padding-left: 0px;">\n          <ion-item no-lines class="outline-item">\n            <ion-input #f_str_mileage [(ngModel)]="str_mileage" type="text" placeholder="주행거리" \n                      (change)="onChangeMileage($event.target.value)" clearInput></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-3>\n          <ion-item no-lines>\n            <ion-label class="outline-item" align="right"><span style="font-size: 11pt;">Km</span></ion-label>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </div>\n    <div class="spacer" style="height: 5px;"></div>\n    <ion-row>\n      <ion-col width-60 style="padding-left: 0px;">\n        <div class="outline-box">\n          <ion-item no-lines class="outline-item outline-button">\n            <ion-label style="color:#999999;">연식</ion-label>\n            <ion-datetime [(ngModel)]="yearMonth" clearInput style="padding-left: 0px;" \n                          displayFormat="YYYY년 MM월" pickerFormat="YYYY-MM" cancelText="취소" doneText="완료"></ion-datetime>\n          </ion-item>\n        </div>\n      </ion-col>\n      <ion-col width-60 style="padding-right: 0px;">\n        <div class="outline-box">\n          <ion-item no-lines class="outline-item outline-button">\n            <ion-label style="color:#999999;">색상</ion-label>\n            <ion-select [(ngModel)]="color" class="select-item" cancelText="취소" okText="선택" (ionChange)="selectedColor($event)">\n              <ion-option *ngFor="let c of colors" [value]="c.c_color">{{ c.c_color }}</ion-option>\n            </ion-select>\n          </ion-item>        \n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col width-60 style="padding-left: 0px;">\n        <div class="outline-box">\n          <ion-item no-lines class="outline-item outline-button">\n            <ion-label style="color:#999999">연료 </ion-label>\n            <ion-select [(ngModel)]="puer" class="select-item" cancelText="취소" okText="선택" (ionChange)="selectedPuer($event)">\n              <ion-option *ngFor="let f of puers" [value]="f.id">{{ f.puer }}</ion-option>\n            </ion-select>\n          </ion-item>\n        </div>\n      </ion-col>\n      <ion-col width-60 style="padding-right: 0px;">\n        <div class="outline-box">\n          <ion-item no-lines class="outline-item outline-button">\n            <ion-label style="color:#999999">변속기 </ion-label>\n            <ion-select [(ngModel)]="gear" class="select-item" cancelText="취소" okText="선택" (ionChange)="selectedGear($event)">\n              <ion-option *ngFor="let g of gears" [value]="g.id">{{ g.gear }}</ion-option>\n            </ion-select>\n          </ion-item>\n        </div>\n      </ion-col>\n    </ion-row>\n    <div class="spacer" style="height: 5px;"></div>\n\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-textarea #f_additory_options [(ngModel)]="additory_options" type="text" placeholder="추가옵션(선택입력)" clearInput></ion-textarea>\n      </ion-item>\n    </div>\n    <div class="spacer" style="height: 10px;"></div>\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-textarea [(ngModel)]="repair" type="text" placeholder="수리내역(선택입력)" clearInput></ion-textarea>\n      </ion-item>\n    </div>\n    <div class="spacer" style="height: 10px;"></div>\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-textarea [(ngModel)]="replacement" type="text" placeholder="교체내역(선택입력)" clearInput></ion-textarea>\n      </ion-item>\n    </div>\n  </div>\n\n  <ion-item no-lines><ion-label>주요옵션<span style="font-size: 10pt; color:#37aafe">*</span></ion-label></ion-item>\n  <!-- [1:네비게이션, 2:선루프, 3:블랙박스, 4:제조사보증, 5:후방센서, 6:START버튼, 7:후방카메라, 8:열선시트] -->\n  <ion-grid>\n    <ion-row align="center">\n      <ion-col col-3 *ngIf="!navigationFlag"><img [src]="navigation_off" style="width: 80%; height: auto;" (click)="navigationOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="navigationFlag"><img [src]="navigation_on" style="width: 80%; height: auto;" (click)="navigationOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!sunLoopFlag"><img [src]="sunLoop_off" style="width: 80%; height: auto;" (click)="sunLoopOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="sunLoopFlag"><img [src]="sunLoop_on" style="width: 80%; height: auto;" (click)="sunLoopOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!blackBoxFlag"><img [src]="blackBox_off" style="width: 80%; height: auto;" (click)="blackBoxOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="blackBoxFlag"><img [src]="blackBox_on" style="width: 80%; height: auto;" (click)="blackBoxOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!warrantyFlag"><img [src]="warranty_off" style="width: 80%; height: auto;" (click)="warrantyOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="warrantyFlag"><img [src]="warranty_on" style="width: 80%; height: auto;" (click)="warrantyOnOff()"/></ion-col>\n    </ion-row>\n    <ion-row align="center"> \n      <ion-col col-3 *ngIf="!rearSensorFlag"><img [src]="rearSensor_off" style="width: 80%; height: auto;" (click)="rearSensorOnOff()"/></ion-col> \n      <ion-col col-3 *ngIf="rearSensorFlag"><img [src]="rearSensor_on" style="width: 80%; height: auto;" (click)="rearSensorOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!startButtonFlag"><img [src]="startButton_off" style="width: 80%; height: auto;" (click)="startButtonOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="startButtonFlag"><img [src]="startButton_on" style="width: 80%; height: auto;" (click)="startButtonOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!rearCameraFlag"><img [src]="rearCamera_off" style="width: 80%; height: auto;" (click)="rearCameraOnOff()"/></ion-col> \n      <ion-col col-3 *ngIf="rearCameraFlag"><img [src]="rearCamera_on" style="width: 80%; height: auto;" (click)="rearCameraOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!heatingSeatFlag"><img [src]="heatingSeat_off" style="width: 80%; height: auto;" (click)="heatingSeatOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="heatingSeatFlag"><img [src]="heatingSeat_on" style="width: 80%; height: auto;" (click)="heatingSeatOnOff()"/></ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-item no-lines style="padding-left: 10px;"><ion-label>차량 위치<span style="font-size: 10pt; color:#37aafe">*</span></ion-label></ion-item>\n  <!-- Google map -->\n  <ion-item no-lines>\n    <input id="place_input" name="place_input" type="text" placeholder="위치입력" [(ngModel)]="searchPlace">\n  </ion-item>\n  <div map id="amap" class="map"></div>\n  <!-- Google map -->\n  <div class="spacer" style="height: 10px;"></div>\n\n  <ion-item no-lines style="padding-left: 10px;"><ion-label>내차 소개</ion-label></ion-item>\n  <div padding style="padding-left: 5px; padding-right: 5px;">\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-textarea #f_introduce [(ngModel)]="introduce" type="text" style="position: static;" placeholder="내차를 자세히 소개해주세요." clearInput></ion-textarea>\n      </ion-item>\n    </div>\n  </div>\n  <div *ngIf="specialTypeFlag && !normalTypeFlag" style="padding-left: 5px; padding-right: 5px;">\n    <ion-row>\n      <ion-col align="center" style="background-color:#48b2ff">\n        <font size="2" color="white"><strong>*천언더를 통해서 거래가 되어야만<br>{{specialFee | number:0}}원의 수수료가 부과됩니다.</strong></font>\n      </ion-col>\n    </ion-row>\n  </div>\n  <ion-row style="padding-left: 5px; padding-right: 5px;">\n    <button ion-button block color=\'1000Under\' (click)="submitRegiste()">등록하기</button>\n  </ion-row>\n</ion-content>'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/add-my-car/add-my-car.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_4__ionic_native_image_picker__["a" /* ImagePicker */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* App */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_10__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_11__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_image_picker__["a" /* ImagePicker */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_transfer__["a" /* Transfer */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__["a" /* FilePath */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
        __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], AddMyCarPage);

//# sourceMappingURL=add-my-car.js.map

/***/ })

});
//# sourceMappingURL=5.main.js.map