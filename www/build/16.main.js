webpackJsonp([16],{

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_brand__ = __webpack_require__(398);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchBrandPageModule", function() { return SearchBrandPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SearchBrandPageModule = (function () {
    function SearchBrandPageModule() {
    }
    return SearchBrandPageModule;
}());
SearchBrandPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__search_brand__["a" /* SearchBrandPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__search_brand__["a" /* SearchBrandPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__search_brand__["a" /* SearchBrandPage */]
        ]
    })
], SearchBrandPageModule);

//# sourceMappingURL=search-brand.module.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchBrandPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SearchBrandPage = (function () {
    function SearchBrandPage(alertCtrl, viewCtrl, navCtrl, navParams, http, ga) {
        var _this = this;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.ga = ga;
        //-------------------------------------------------> for data query
        this.cars = [];
        this.baseURI = "http://cjsekfvm.cafe24.com/test_server/app/car/url/";
        this.modelFlag = false;
        this.levelFlag = false;
        this.showLevelButton = false;
        this.brandParams = {
            maker_key: '0',
            maker_name: '전체',
            model_key: '0',
            model_name: '전체',
            level_key: '0',
            level_name: '전체'
        };
        this.model_list = [];
        /*
        maker_key:"1"
        maker_name:"현대"
        model_key:"1"
        model_name:"쏘나타"
        model_sub_key:"51"
        model_sub_name:"쏘나타 뉴 라이즈(17년~현재)"
        */
        this.makers = [];
        this.models = [];
        this.levels = [];
        this.fromPage = navParams.get("fromPage");
        console.log('this.fromPage: ' + this.fromPage);
        if (this.fromPage == 'carsPage') {
            this.showLevelButton = false;
        }
        else {
            this.showLevelButton = true;
        }
        var body = '', type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.baseURI + "get_car.php";
        this.http.post(url, body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.result != 'ok') {
                var alert_1 = _this.alertCtrl.create({
                    title: '알림',
                    subTitle: data.result,
                    buttons: ['확인']
                });
                alert_1.present();
            }
            else {
                //--> this.makers.push({maker_key: '0', maker_name: '전체'});
                _this.model_list = data.model_list;
                var temp_maker_key = '';
                for (var i = 0; i < _this.model_list.length; i++) {
                    if (_this.model_list[i].maker_key != temp_maker_key) {
                        temp_maker_key = _this.model_list[i].maker_key;
                        _this.makers.push(_this.model_list[i]);
                        //console.log('maker_key_name: '+this.model_list[i].maker_key+'-'+this.model_list[i].maker_name);
                    }
                }
            }
        });
    }
    SearchBrandPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchModelModelPage');
    };
    SearchBrandPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('제조사/모델선택'); //trackView for Google Analytics
    };
    SearchBrandPage.prototype.showMaker = function () {
        this.makersFlag = true;
        this.models = [];
        this.levels = [];
        this.brandParams.model_key = '0';
        this.brandParams.model_name = '';
        this.brandParams.level_key = '0';
        this.brandParams.level_name = '';
    };
    SearchBrandPage.prototype.selectedMaker = function (e, selectedMaker) {
        console.log('selectedMaker: ' + selectedMaker.maker_key + ': ' + selectedMaker.maker_name);
        if (this.showLevelButton == false) {
            this.models.push({ model_key: '0', model_name: '전체' });
        }
        this.brandParams.maker_key = selectedMaker.maker_key;
        this.brandParams.maker_name = selectedMaker.maker_name;
        var temp_model_key = '';
        for (var i = 0; i < this.model_list.length; i++) {
            if (this.model_list[i].maker_key == this.brandParams.maker_key &&
                this.model_list[i].model_key != temp_model_key) {
                temp_model_key = this.model_list[i].model_key;
                this.models.push(this.model_list[i]);
                //console.log('model_key_name: '+this.model_list[i].model_key+'-'+this.model_list[i].model_name);
            }
        }
        this.makersFlag = false;
        this.modelFlag = true;
    };
    SearchBrandPage.prototype.showModel = function () {
        this.modelFlag = !this.modelFlag;
        this.levels = [];
        this.brandParams.level_key = '0';
        this.brandParams.level_name = '전체';
    };
    SearchBrandPage.prototype.selectedModel = function (e, selectedModel) {
        console.log('selectedModel: ' + selectedModel.model_key + ': ' + selectedModel.model_name);
        // console.log('selectedModel name: ' +selectedModel.model_name);
        this.brandParams.model_key = selectedModel.model_key;
        this.brandParams.model_name = selectedModel.model_name;
        if (this.showLevelButton == false) {
            this.viewCtrl.dismiss(this.brandParams);
        }
        else {
            var temp_level_key = '';
            for (var i = 0; i < this.models.length; i++) {
                if (this.model_list[i].maker_key == this.brandParams.maker_key &&
                    this.model_list[i].model_key == this.brandParams.model_key &&
                    this.model_list[i].model_sub_key != temp_level_key) {
                    temp_level_key = this.model_list[i].model_sub_key;
                    this.levels.push(this.model_list[i]);
                    //console.log('level_key_name: '+this.model_list[i].model_sub_key+'-'+this.model_list[i].model_sub_name);
                }
            }
            if (this.levels.length == 0) {
                this.brandParams.level_key = '0';
                this.brandParams.level_name = 'NaN';
                this.viewCtrl.dismiss(this.brandParams);
            }
            else {
                this.levelFlag = true;
            }
        }
        this.makersFlag = false;
        this.modelFlag = false;
    };
    SearchBrandPage.prototype.showLevel = function () {
        this.levelFlag = !this.levelFlag;
    };
    SearchBrandPage.prototype.selectedLevel = function (e, selectedLevel) {
        console.log('selectedLevel: ' + selectedLevel.model_sub_key + ': ' + selectedLevel.model_sub_name);
        this.brandParams.level_key = selectedLevel.model_sub_key;
        this.brandParams.level_name = selectedLevel.model_sub_name;
        this.viewCtrl.dismiss(this.brandParams);
    };
    SearchBrandPage.prototype.selected = function () {
        //console.log('brandParams: ' +this.brandParams);
        this.viewCtrl.dismiss(this.brandParams);
    };
    SearchBrandPage.prototype.closeModal = function () {
        if (this.levelFlag == true) {
            var alert_2 = this.alertCtrl.create({
                title: '알림',
                subTitle: '세부모델을 선택해주세요.',
                buttons: ['확인']
            });
            alert_2.present();
        }
        else {
            this.brandParams.maker_key = '0';
            this.brandParams.maker_name = '전체';
            this.brandParams.model_key = '0';
            this.brandParams.model_name = '전체';
            this.brandParams.level_key = '0';
            this.brandParams.level_name = '전체';
            this.viewCtrl.dismiss(this.brandParams);
        }
    };
    return SearchBrandPage;
}());
SearchBrandPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-search-brand',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/search-brand/search-brand.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button color="underblack" icon-only (click)="closeModal()">\n        <ion-icon name="md-close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>제조사/모델 선택</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content padding>\n  <button ion-button block color=\'1000Under\' (click)="showMaker()">제조사 :: {{brandParams.maker_name}}</button>\n  <div *ngIf="makersFlag">\n    <ion-item *ngFor="let ma of makers">\n      <ion-label>{{ma.maker_name}}</ion-label>\n      <ion-checkbox checked="false" color="dark" (ionChange)="selectedMaker($event, ma)"></ion-checkbox>\n    </ion-item>\n  </div>\n  <button ion-button block color=\'1000Under\' (click)="showModel()">모델 :: {{brandParams.model_name}}</button>\n  <div *ngIf="modelFlag">\n    <ion-item *ngFor="let mo of models">\n      <ion-label>{{mo.model_name}}</ion-label>\n      <ion-checkbox checked="false" color="dark"(ionChange)="selectedModel($event, mo)"></ion-checkbox>\n    </ion-item>\n  </div>\n  <div *ngIf="showLevelButton">\n    <button ion-button block color=\'1000Under\' (click)="showLevel()">세부모델 :: {{brandParams.level_name}}</button>\n    <div *ngIf="levelFlag">\n      <ion-item *ngFor="let le of levels">\n        <ion-label>{{le.model_sub_name}}</ion-label>\n        <ion-checkbox checked="false" color="dark"(ionChange)="selectedLevel($event, le)"></ion-checkbox>\n      </ion-item>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/search-brand/search-brand.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], SearchBrandPage);

//# sourceMappingURL=search-brand.js.map

/***/ })

});
//# sourceMappingURL=16.main.js.map