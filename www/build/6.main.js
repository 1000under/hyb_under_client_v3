webpackJsonp([6],{

/***/ 340:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__what_service__ = __webpack_require__(411);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WhatServicePageModule", function() { return WhatServicePageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var WhatServicePageModule = (function () {
    function WhatServicePageModule() {
    }
    return WhatServicePageModule;
}());
WhatServicePageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__what_service__["a" /* WhatServicePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__what_service__["a" /* WhatServicePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__what_service__["a" /* WhatServicePage */]
        ]
    })
], WhatServicePageModule);

//# sourceMappingURL=what-service.module.js.map

/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WhatServicePage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//-----> Providers

var WhatServicePage = (function () {
    function WhatServicePage(viewCtrl, navCtrl, navParams, alertCtrl, modalCtrl, userInfoProvider, ga) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.userInfoProvider = userInfoProvider;
        this.ga = ga;
        this.selectFlag = false;
        this.faqs = []; //parameter from SubHomePage
        this.faqs = this.navParams.get("naviParam");
    }
    WhatServicePage.prototype.ionViewDidLoad = function () {
        this.login_flag = this.userInfoProvider.userProfile.login_flag;
        this.isMember_flag = this.userInfoProvider.userProfile.isMember_flag;
        console.log('ionViewDidLoad WhatServicePage');
    };
    WhatServicePage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('자주하는질문'); //trackView for Google Analytics
    };
    WhatServicePage.prototype.selectedFAQ = function (title, index) {
        //console.log('title: '+title+'- index: '+index);
        this.selectFlag = !this.selectFlag;
        this.faq_index = index;
    };
    WhatServicePage.prototype.chatWithUnder = function () {
        var _this = this;
        if (this.userInfoProvider.userProfile.login_flag == false) {
            var LoginPage_modal = this.modalCtrl.create('LoginPage', { naviPage: 'WhatServicePage' });
            LoginPage_modal.onDidDismiss(function (data) {
                _this.login_flag = data;
            });
            LoginPage_modal.present();
        }
        else {
            this.ga.trackEvent('자주하는질문', '채팅하기'); //trackEvent for Google Analytics
            var ChatBoardPage_modal = this.modalCtrl.create('ChatBoardPage', {
                email: this.userInfoProvider.userProfile.u_email,
                pw: this.userInfoProvider.userProfile.u_pw,
                target_email: 'admin@1000under.com' //천언더 담당자 email
            });
            ChatBoardPage_modal.present();
        }
    };
    WhatServicePage.prototype.callToUnder = function () {
        //window.location = 'tel:+82-2-2088-6460';
        this.ga.trackEvent('자주하는질문', '전화하기'); //trackEvent for Google Analytics
        window.location = 'tel:02-2088-6460';
    };
    WhatServicePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return WhatServicePage;
}());
WhatServicePage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-what-service',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/what-service/what-service.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button icon-only color="underblack" (click)="dismiss()">\n        <ion-icon name="md-close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>FAQ</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-list padding no-lines *ngFor="let f of faqs; let i = index" style="margin-bottom: 0px; padding-top: 5px; padding-bottom: 0px; padding-left: 10px; padding-right: 10px;">\n    <button class="qa-btn" ion-item detail-none no-lines block clear style="padding-left: 0px;" (click)="selectedFAQ(f.title, i)">\n      <span style="color:#353535; font-size: 12pt;">{{f.title}}</span>\n      <ion-icon *ngIf="selectFlag == true && i == faq_index" name="ios-arrow-up" item-end color="undergray"></ion-icon>\n    </button>\n    <div *ngIf="selectFlag == true && i == faq_index">\n      <p><span style="color:#626262; font-size: 12pt;">{{f.memo}}</span></p>\n    </div>  \n  </ion-list>\n</ion-content>\n<ion-footer>\n  <ion-toolbar color="underwhite">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-6 style="padding-top: 0px; padding-bottom: 0px;">\n          <button ion-button block color=\'1000Under\' icon-left (click)="callToUnder()">전화하기</button>\n        </ion-col>\n        <ion-col col-6 style="padding-top: 0px; padding-bottom: 0px;">\n          <button ion-button block color=\'1000Under\' icon-left (click)="chatWithUnder()">채팅하기</button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/what-service/what-service.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], WhatServicePage);

//# sourceMappingURL=what-service.js.map

/***/ })

});
//# sourceMappingURL=6.main.js.map