webpackJsonp([26],{

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__image_view_more__ = __webpack_require__(386);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImageViewMorePageModule", function() { return ImageViewMorePageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ImageViewMorePageModule = (function () {
    function ImageViewMorePageModule() {
    }
    return ImageViewMorePageModule;
}());
ImageViewMorePageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__image_view_more__["a" /* ImageViewMorePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__image_view_more__["a" /* ImageViewMorePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__image_view_more__["a" /* ImageViewMorePage */]
        ]
    })
], ImageViewMorePageModule);

//# sourceMappingURL=image-view-more.module.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_photo_viewer__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageViewMorePage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ImageViewMorePage = (function () {
    function ImageViewMorePage(navCtrl, navParams, viewCtrl, photoViewer, ga) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.photoViewer = photoViewer;
        this.ga = ga;
        this.photo_path = []; //parameter from CarDetailPage
        this.photo_path = navParams.get("photos");
        this.initialIndex = navParams.get("initialSlide");
        //console.log('this.initialSlide ----> '+this.initialIndex);
    }
    ImageViewMorePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ImageViewMorePage');
    };
    ImageViewMorePage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('이미지상세보기'); //trackView for Google Analytics
    };
    ImageViewMorePage.prototype.zoomImage = function (imageDaata) {
        this.photoViewer.show(imageDaata);
    };
    ImageViewMorePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return ImageViewMorePage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('mySlider'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */])
], ImageViewMorePage.prototype, "slides", void 0);
ImageViewMorePage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-image-view-more',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/image-view-more/image-view-more.html"*/'<ion-header>\n  <div class="head-space"></div>\n</ion-header>\n\n<ion-content class="background">\n  <ion-fab left top>\n    <button ion-fab (click)="dismiss()"\n            style="color: #ffffff; background-color: rgba( 255, 255, 255, 0.0 ); box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.0), 0 0 0 rgba(0, 0, 0, 0.0);">\n            <ion-icon name="ios-arrow-back"></ion-icon>\n    </button>\n  </ion-fab>\n  <ion-slides [initialSlide]="initialIndex" loop="true">\n    <ion-slide *ngFor="let pp of photo_path">\n      <img [src]="pp.path"  (click)="zoomImage(pp.path)">\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/image-view-more/image-view-more.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_photo_viewer__["a" /* PhotoViewer */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], ImageViewMorePage);

//# sourceMappingURL=image-view-more.js.map

/***/ })

});
//# sourceMappingURL=26.main.js.map