webpackJsonp([21],{

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notification_modal__ = __webpack_require__(392);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationModalPageModule", function() { return NotificationModalPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NotificationModalPageModule = (function () {
    function NotificationModalPageModule() {
    }
    return NotificationModalPageModule;
}());
NotificationModalPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__notification_modal__["a" /* NotificationModalPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notification_modal__["a" /* NotificationModalPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__notification_modal__["a" /* NotificationModalPage */]
        ]
    })
], NotificationModalPageModule);

//# sourceMappingURL=notification-modal.module.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationModalPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NotificationModalPage = (function () {
    function NotificationModalPage(navCtrl, navParams, viewCtrl, modalCtrl, userInfoProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.userInfoProvider = userInfoProvider;
        this.target_email = this.navParams.get('target_email');
        this.target_name = this.navParams.get('target_name');
    }
    NotificationModalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotificationModalPage');
    };
    NotificationModalPage.prototype.goChatBoard = function () {
        var ChatBoardPage_modal = this.modalCtrl.create('ChatBoardPage', {
            email: this.userInfoProvider.userProfile.u_email,
            pw: this.userInfoProvider.userProfile.u_pw,
            target_email: this.target_email
        });
        ChatBoardPage_modal.present();
    };
    NotificationModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return NotificationModalPage;
}());
NotificationModalPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-notification-modal',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/notification-modal/notification-modal.html"*/'<ion-content class="main-view">\n  <!-- <div class="overlay" (click)="dismiss()"></div> -->\n  <div class="modal_content" (click)="dismiss()">\n    <ion-item no-lines>\n      <ion-avatar item-start>\n        <img src="img/1000under-logo.png">\n      </ion-avatar>\n      <h2>{{target_name}}</h2>\n      <p>새로운 메시지가 도착했습니다.</p>\n      <button *ngIf="target_email != \'ChatsPage\'" ion-button item-end color="undergray" (click)="goChatBoard()">보기</button>\n    </ion-item>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/notification-modal/notification-modal.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_user_info_user_info__["a" /* UserInfoProvider */]])
], NotificationModalPage);

//# sourceMappingURL=notification-modal.js.map

/***/ })

});
//# sourceMappingURL=21.main.js.map