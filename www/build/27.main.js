webpackJsonp([27],{

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(385);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomePageModule = (function () {
    function HomePageModule() {
    }
    return HomePageModule;
}());
HomePageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]
        ]
    })
], HomePageModule);

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_fcm__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_device_device__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_chats_count_chats_count__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_outer_link_outer_link__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_maker_model_maker_model__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_setanalytics_setanalytics__ = __webpack_require__(218);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//-----> Providers







var HomePage = (function () {
    function HomePage(platform, viewCtrl, navCtrl, modalCtrl, alertCtrl, http, navParams, fcm, storage, ga, geolocation, deviceProvider, userInfoProvider, chatsCountProvider, msgService, outerLinkProvider, makerModelProvider, setAnalytics) {
        this.platform = platform;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.navParams = navParams;
        this.fcm = fcm;
        this.storage = storage;
        this.ga = ga;
        this.geolocation = geolocation;
        this.deviceProvider = deviceProvider;
        this.userInfoProvider = userInfoProvider;
        this.chatsCountProvider = chatsCountProvider;
        this.msgService = msgService;
        this.outerLinkProvider = outerLinkProvider;
        this.makerModelProvider = makerModelProvider;
        this.setAnalytics = setAnalytics;
        this.queryLength = 0;
        this.boardURI = "http://cjsekfvm.cafe24.com/test_server/app/board/url/";
        this.marketURI = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";
        this.recommend_list = []; //추천매물 data.recommend_list
        this.pick_list = []; //모아보기 data.pick_list
        this.notices = []; //공지시항 data.board_list. "type_board":"공지"
        this.tips = []; //TIP    data.board_list. "type_board":"팁"
        this.faqs = []; //Q&A    data.board_list. "type_board":"QA"
        this.faqsMoreFlag = false;
        this.carDetailTabFlag = false;
        this.clickUnder1000 = false;
    }
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.showIntro();
        setTimeout(function () { _this.getBoardList(); }, 400);
        this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
        console.log('ionViewDidLoad HomePage');
    };
    HomePage.prototype.showIntro = function () {
        var _this = this;
        this.storage.get('showIntro').then(function (val) {
            console.log('----------> showIntro value: ' + val);
            if (val != 0) {
                var IntroPage_modal = _this.modalCtrl.create('IntroPage');
                IntroPage_modal.present();
            }
            _this.checkLocationService();
        });
    };
    HomePage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('홈'); //trackView for Google Analytics
        console.log('----------> GoogleAnalytics trackView: HomePage');
        if (this.userInfoProvider.userProfile.login_flag) {
            this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
        }
        this.initPushNotification();
        console.log('ionViewDidEnter HomePage');
    };
    HomePage.prototype.checkLocationService = function () {
        var _this = this;
        this.geolocation.getCurrentPosition({ timeout: 7000 }).then(function (success) {
            console.log('----------> LocationService ON');
        }, function (error) {
            console.log('==========> LocationService OFF');
            if (_this.platform.is('ios')) {
                var alert_1 = _this.alertCtrl.create({
                    title: '알림',
                    subTitle: '지도에서 차량의 위치를 확인/등록하려면 휴대폰의 설정->개인정보보호 에서 "위치서비스"를 켜야 합니다.',
                    buttons: ['확인']
                });
                alert_1.present();
            }
            else if (_this.platform.is('android')) {
                var alert_2 = _this.alertCtrl.create({
                    title: '알림',
                    subTitle: '지도에서 차량의 위치를 확인/등록하려면 휴대폰에서 "위치서비스"를 켜야 합니다.',
                    buttons: ['확인']
                });
                alert_2.present();
            }
            else {
                var alert_3 = _this.alertCtrl.create({
                    title: '알림',
                    subTitle: '지도에서 차량의 위치를 확인/등록하려면 "위치서비스"를 켜야 합니다.',
                    buttons: ['확인']
                });
                alert_3.present();
            }
        });
    };
    HomePage.prototype.getBoardList = function () {
        var _this = this;
        if (this.userInfoProvider.userProfile.login_flag == true) {
            this.queryParams = 'device=' + this.deviceProvider.d_Type + '&email=' + this.userInfoProvider.userProfile.u_email;
        }
        else {
            this.queryParams = 'device=' + this.deviceProvider.d_Type;
        }
        var body_s = this.queryParams, type_s = 'application/x-www-form-urlencoded; charset=UTF-8', headers_s = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type_s }), options_s = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers_s }), url_s = this.boardURI + "get_board_list.php";
        this.http.post(url_s, body_s, options_s)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.result != 'ok') {
                var alert_4 = _this.alertCtrl.create({
                    title: '알림',
                    subTitle: _this.msgService.getMessage(data.result),
                    buttons: ['확인']
                });
                alert_4.present();
            }
            else {
                for (var i = 0; i < data.recommend_list.length; i++) {
                    data.recommend_list[i].maker = _this.makerModelProvider.getMakerName(data.recommend_list[i].maker);
                    data.recommend_list[i].model = _this.makerModelProvider.getModelName(data.recommend_list[i].model);
                    _this.recommend_list.push(data.recommend_list[i]);
                }
                _this.recommend_list = data.recommend_list;
                _this.pick_list = data.pick_list;
                var cnt = 0;
                for (var i = 0; i < data.board_list.length; i++) {
                    if (data.board_list[i].type_board == '공지') {
                        _this.notices.push(data.board_list[i]);
                    }
                    else if (data.board_list[i].type_board == '팁') {
                        _this.tips.push(data.board_list[i]);
                    }
                    else {
                        cnt = cnt + 1;
                        data.board_list[i].title = (cnt).toString() + '. ' + data.board_list[i].title;
                        _this.faqs.push(data.board_list[i]);
                    }
                }
                //alert('getBoardList');
            }
        });
    };
    HomePage.prototype.initPushNotification = function () {
        var _this = this;
        this.fcm.onNotification().subscribe(function (data) {
            if (data.wasTapped) {
                if (data.type == 'chat_new') {
                    _this.userInfoProvider.getProfile();
                    console.log('----------> Background notification userProfile.login_flag: ' + _this.userInfoProvider.userProfile.login_flag);
                    if (_this.userInfoProvider.userProfile.login_flag) {
                        _this.chatsCountProvider.checkChatsCount(_this.userInfoProvider.userProfile.login_flag, _this.userInfoProvider.userProfile.u_email, _this.userInfoProvider.userProfile.u_pw);
                    }
                } //data.type == 'chat_new'
            }
            else {
                //alert('Received message in Foreground');
                if (data.type == 'chat_new') {
                    var dataObj = JSON.stringify(data);
                    //console.log('----------> Notification Data: '+dataObj);
                    if (_this.userInfoProvider.userProfile.login_flag) {
                        _this.chatsCountProvider.checkChatsCount(_this.userInfoProvider.userProfile.login_flag, _this.userInfoProvider.userProfile.u_email, _this.userInfoProvider.userProfile.u_pw);
                        var msgObj = JSON.parse(data.message);
                        var target_email = msgObj.target_email.replace(/\'/g, ''); //특정문자 제거
                        var target_name = msgObj.target_name.replace(/\'/g, ''); //특정문자 제거
                        //console.log('----------> notification target_email: '+target_email);
                        var notificationModal_modal_1 = _this.modalCtrl.create('NotificationModalPage', {
                            target_email: target_email,
                            target_name: target_name
                        });
                        var closedByTimeout_1 = false;
                        var timeoutHandle_1 = setTimeout(function () { closedByTimeout_1 = true; notificationModal_modal_1.dismiss(); }, 5000);
                        notificationModal_modal_1.onDidDismiss(function () {
                            if (closedByTimeout_1)
                                return; //timeout 동안 action이 없으면 return
                            clearTimeout(timeoutHandle_1);
                        });
                        notificationModal_modal_1.present();
                    }
                }
                else {
                    /*
                    const dataObj = JSON.stringify(data);
                    //console.log('----------> Banner Data: '+dataObj);
                    let msgObj = JSON.parse(data.message);
                    let target_email = msgObj.target_email.replace(/\'/g,''); //특정문자 제거
                    let target_name = msgObj.target_name.replace(/\'/g,''); //특정문자 제거
                    let bannerModal_modal = this.modalCtrl.create('BannerModalPage');
                    bannerModal_modal.present();
                    */
                } //data.type != 'chat_new'
            }
        });
    };
    HomePage.prototype.naviCarDetailPage = function (selectedCar) {
        var _this = this;
        this.ga.trackEvent('추천매물', '추천매물: ' + selectedCar.maker + '-' + selectedCar.model); //trackEvent for Google Analytics
        if (!this.carDetailTabFlag) {
            this.carDetailTabFlag = true;
            if (this.userInfoProvider.userProfile.login_flag == true) {
                this.carDetailParam = 'sale=' + selectedCar.key + '&email=' + this.userInfoProvider.userProfile.u_email;
            }
            else {
                this.carDetailParam = 'sale=' + selectedCar.key;
            }
            var body = this.carDetailParam, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_detail.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    var alert_5 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: _this.msgService.getMessage(data.result),
                        buttons: ['확인']
                    });
                    alert_5.present();
                }
                else {
                    var set_key = selectedCar;
                    var set_type = '1'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                    _this.setAnalytics.setAnalytics(set_key, set_type);
                    var CarDetailPage_modal = _this.modalCtrl.create('CarDetailPage', { carParam: data.sale_detail });
                    CarDetailPage_modal.onDidDismiss(function (data) {
                        _this.carDetailTabFlag = false;
                    });
                    CarDetailPage_modal.present();
                }
            });
        }
    };
    HomePage.prototype.selectedPickList = function (selectedPick) {
        this.ga.trackEvent('모아보기', '모아보기: ' + selectedPick.title); //trackEvent for Google Analytics
        var categoryViewPage_modal = this.modalCtrl.create('CategoryViewPage', { categoryParam: selectedPick });
        categoryViewPage_modal.present();
    };
    HomePage.prototype.selectedNotice = function (selectedNotice) {
        this.ga.trackEvent('공지사항', '공지사항: ' + selectedNotice.title); //trackEvent for Google Analytics
        var noticeModalPage_modal = this.modalCtrl.create('NoticePage', { notice_param: selectedNotice });
        noticeModalPage_modal.present();
    };
    HomePage.prototype.selectedTip = function (selectedTip) {
        var tipModalPage_modal = this.modalCtrl.create('TipPage', { tip_param: selectedTip });
        tipModalPage_modal.present();
    };
    HomePage.prototype.selectedFAQ = function (selectedFAQ) {
        var whatServicePage_modal = this.modalCtrl.create('WhatServicePage', { naviParam: this.faqs });
        whatServicePage_modal.present();
    };
    HomePage.prototype.termsUse = function () {
        //data.policy_list.type == '이용';
        var termsUsePage_modal = this.modalCtrl.create('TermsUsePage', { termUseContent: this.outerLinkProvider.termUseContent });
        termsUsePage_modal.present();
    };
    HomePage.prototype.privacyPolicy = function () {
        //data.policy_list.type == '개인';
        var privacyPolicyPage_modal = this.modalCtrl.create('PrivacyPolicyPage', { privacyPolicyContent: this.outerLinkProvider.privacyPolicyContent });
        privacyPolicyPage_modal.present();
    };
    HomePage.prototype.under1000 = function () {
        this.clickUnder1000 = !this.clickUnder1000;
    };
    return HomePage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */])
], HomePage.prototype, "slides", void 0);
HomePage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/home/home.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="primary">\n    <div align="center"><img src="img/homeTitle_img.png" style="width:15%; height:20%;"/></div>\n  </ion-toolbar>\n</ion-header>\n<ion-content class="body-color">\n  <ion-slides pager class="top-slides">\n    <ion-slide *ngFor="let pl of pick_list" class="top-slide">\n      <img [src]="pl.photo_path" tappable (click)="selectedPickList(pl)"/>\n    </ion-slide>\n  </ion-slides>\n  <h5 style="margin-top: 30px; margin-bottom: 15px; padding-left: 12px;">추천매물</h5>\n  <ion-slides class="car-slides" [slidesPerView]="\'auto\'" style="padding-left: 5px; padding-right: 5px;">\n    <ion-slide class="car-slide"  *ngFor="let car of recommend_list" style="margin-left: 5px; margin-right: 5px;">\n      <div *ngIf="car.state === \'판매중\'">\n        <div class="car-container" tappable (click)="naviCarDetailPage(car)" style="margin-left: 4px; background:#F4F6F6;">\n          <img [src]="car.photo_path"/>\n        </div>\n      </div>\n      <div *ngIf="car.state === \'검수\'">\n        <img class="img-over" src="img/state/inspecting.png"/>\n        <div class="car-container" tappable (click)="naviCarDetailPage(car)" style="margin-left: 4px; background:#F4F6F6;">\n          <img [src]="car.photo_path" style="opacity:0.7;"/>\n        </div>\n      </div>\n      <div *ngIf="car.state === \'거래중\'">\n        <img class="img-over" src="img/state/dealing.png"/>\n        <div class="car-container" tappable (click)="naviCarDetailPage(car)" style="margin-left: 4px; background:#F4F6F6;">\n          <img [src]="car.photo_path" style="opacity:0.7;"/>\n        </div>\n      </div>\n      <div *ngIf="car.state === \'완료\'">\n        <img class="img-over" src="img/state/sold.png"/>\n        <div class="car-container" tappable (click)="naviCarDetailPage(car)" style="margin-left: 4px; background:#F4F6F6;">\n          <img [src]="car.photo_path" style="opacity:0.7;"/>\n        </div>\n      </div>\n      <div class="spacer" style="height: 5px;"></div>\n      <p align="left" style="height: 15pt; margin-bottom: 0px; margin-top: 0px; padding-left: 5px;">\n        <span style="font-size: 11pt; color:black;">{{car.price | number:0}}만원 {{car.maker}} {{car.model}}</span></p>\n      <p align="left" style="height: 15pt; margin-bottom: 0px; margin-top: 0px; padding-left: 5px;">\n        <span style="font-size: 11pt; color:black;">{{car.year}}년식 | {{car.mileage | number:0}}Km</span></p>\n    </ion-slide>\n  </ion-slides>\n  <h5 style="margin-top: 35px; margin-bottom: 15px; padding-left: 12px;">공지사항</h5>\n  <ion-slides pager class="notice-slides" style="padding-left: 10px; padding-right: 10px;">\n    <ion-slide *ngFor="let n of notices" class="notice-slide">\n      <img [src]="n.photo" tappable (click)="selectedNotice(n)"/>\n    </ion-slide>\n  </ion-slides>\n\n  <h5 style="margin-top: 30px; margin-bottom: 15px; padding-left: 12px;">TIP</h5>\n  <ion-row wrap style="padding-left: 5px; padding-right: 5px;">\n      <ion-col col-6 *ngFor="let t of tips" style="padding-top: 0px;">\n          <button class="btn-start" ion-item no-lines block clear color="tipbtn" style="padding-left: 5px;" (click)="selectedTip(t)">\n            <span style="color:#353535; font-size: 12pt;">{{t.title}}</span>\n            <ion-icon md="ios-arrow-forward" item-end color="homeArrowBtn"></ion-icon>\n          </button>\n      </ion-col>\n  </ion-row>\n\n  <h5 style="margin-top: 30px; margin-bottom: 15px; padding-left: 12px;">고객센터</h5>\n  <ion-row wrap style="padding-left: 5px; padding-right: 5px;">\n    <ion-col style="padding-top: 0px;">\n      <button class="btn-start" ion-item no-lines block clear style="padding-left: 10px;" (click)="selectedFAQ()">\n        <span style="color:#353535; font-size: 12pt;">자주하는 질문</span>\n        <ion-icon md="ios-arrow-forward" item-end color="homeArrowBtn"></ion-icon>\n      </button>\n    </ion-col>\n  </ion-row>\n  <div class="spacer" style="height: 15px;"></div>\n  <div class="spacer" style="height: 1px; background-color:#eeeeee"></div>\n  <ion-item no-lines style="padding-left: 5px; height: 40px;">\n    &nbsp;&nbsp;<button class="qa-btn" ion-button color="undergray" clear small style="padding-left: 0px; padding-right: 0px;" (click)="under1000()">천언더(주)사업자정보 |</button>\n    <button class="qa-btn" ion-button color="undergray" clear small style="padding-left: 0px; padding-right: 0px;" (click)="termsUse()">이용약관 |</button>\n    <button class="qa-btn" ion-button color="undergray" clear small style="padding-left: 0px; padding-right: 0px;" (click)="privacyPolicy()">개인정보 취급방침</button>\n  </ion-item>\n  <div *ngIf="clickUnder1000">\n    <div class="spacer" style="height: 1px; background-color:#eeeeee"></div>\n    <ion-grid>\n      <ion-row style="height:25px;">\n        <ion-col col-4 style="font-size:small"><p align="left" style="color:#929292; margin-top:0px; margin-bottom:10px;">&nbsp;&nbsp;대표</p></ion-col>\n        <ion-col col-8 style="font-size:small"><p align="left" style="color:#929292; margin-top:0px; margin-bottom:10px;">김성국</p></ion-col>\n      </ion-row>\n      <ion-row style="height:25px;">\n        <ion-col col-4 style="font-size:small"><p align="left" style="color:#929292; margin-top:0px; margin-bottom:10px;">&nbsp;&nbsp;주소</p></ion-col>\n        <ion-col col-8 style="font-size:small"><p align="left" style="color:#929292; margin-top:0px; margin-bottom:10px;">경기도 성남시 분당구 황새울로 234</p></ion-col>\n      </ion-row>\n      <ion-row style="height:25px;">\n        <ion-col col-4 style="font-size:small"><p align="left" style="color:#929292; margin-top:0px; margin-bottom:10px;"></ion-col>\n        <ion-col col-8 style="font-size:small"><p align="left" style="color:#929292; margin-top:0px; margin-bottom:10px;">트라팰리스 1163호</p></ion-col>\n      </ion-row>\n      <ion-row style="height:25px;">\n        <ion-col col-4 style="font-size:small"><p align="left" style="color:#929292; margin-top:0px; margin-bottom:10px;">&nbsp;&nbsp;사업자번호</p></ion-col>\n        <ion-col col-8 style="font-size:small"><p align="left" style="color:#929292; margin-top:0px; margin-bottom:10px;">337-87-00484</p></ion-col>\n      </ion-row>\n      <ion-row style="height:25px;">\n        <ion-col col-4 style="font-size:small"><p align="left" style="color:#929292; margin-top:0px; margin-bottom:10px;">&nbsp;&nbsp;고객센터</p></ion-col>\n        <ion-col col-8 style="font-size:small"><p align="left" style="color:#929292; margin-top:0px; margin-bottom:10px;">02-2088-6460~3</p></ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <ion-item no-lines class="sns-area" style="padding-left: 5px; background-color:#ffffff;">\n    &nbsp;&nbsp;<a href="https://www.facebook.com/car1000under/"><img src="img/icon/sns/facebook.png" style="width: 6%; height: auto;"/></a>&nbsp;&nbsp;\n    <a href="https://www.instagram.com/chununder/"><img src="img/icon/sns/instagram.png" style="width: 6%; height: auto;"/></a>&nbsp;&nbsp;\n    <a href="http://blog.naver.com/1000_under/"><img src="img/icon/sns/blog.png" style="width: 6%; height: auto;"/></a>&nbsp;&nbsp;\n    <a href="http://www.1000under.com/"><img src="img/icon/sns/homepage.png" style="width: 6%; height: auto;"/></a>\n  </ion-item>\n  <div class="spacer" style="height: 10px;"></div>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/home/home.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_fcm__["a" /* FCM */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_analytics__["a" /* GoogleAnalytics */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
        __WEBPACK_IMPORTED_MODULE_7__providers_device_device__["a" /* DeviceProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_9__providers_chats_count_chats_count__["a" /* ChatsCountProvider */],
        __WEBPACK_IMPORTED_MODULE_10__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_11__providers_outer_link_outer_link__["a" /* OuterLinkProvider */],
        __WEBPACK_IMPORTED_MODULE_12__providers_maker_model_maker_model__["a" /* MakerModelProvider */],
        __WEBPACK_IMPORTED_MODULE_13__providers_setanalytics_setanalytics__["a" /* SetanalyticsProvider */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=27.main.js.map