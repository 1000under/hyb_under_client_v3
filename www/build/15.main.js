webpackJsonp([15],{

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_location__ = __webpack_require__(399);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchLocationPageModule", function() { return SearchLocationPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SearchLocationPageModule = (function () {
    function SearchLocationPageModule() {
    }
    return SearchLocationPageModule;
}());
SearchLocationPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__search_location__["a" /* SearchLocationPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__search_location__["a" /* SearchLocationPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__search_location__["a" /* SearchLocationPage */]
        ]
    })
], SearchLocationPageModule);

//# sourceMappingURL=search-location.module.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchLocationPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SearchLocationPage = (function () {
    function SearchLocationPage(viewCtrl, navCtrl, navParams, ga) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ga = ga;
        this.area = '전국';
        this.viewArea = '';
        this.areaLevel1 = [
            { area: "전국" }, { area: "서울특별시" }, { area: "경기도" }, { area: "강원도" }, { area: "충청북도" }, { area: "충청남도" },
            { area: "경상북도" }, { area: "경상남도" }, { area: "전라북도" }, { area: "전라남도" }, { area: "제주특별자치도" },
            { area: "인천광역시" }, { area: "부산광역시" }, { area: "대전광역시" }, { area: "울산광역시" }, { area: "대구광역시" },
            { area: "광주광역시" }
        ];
    }
    SearchLocationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchLocationPage');
    };
    SearchLocationPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('지역선택'); //trackView for Google Analytics
    };
    SearchLocationPage.prototype.selectedArea = function (e, selectedArea) {
        this.area = selectedArea;
        console.log('this.area:' + this.area);
        this.viewCtrl.dismiss(this.area);
    };
    SearchLocationPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss(this.area);
    };
    return SearchLocationPage;
}());
SearchLocationPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-search-location',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/search-location/search-location.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button color="underblack" icon-only (click)="closeModal()">\n        <ion-icon name="md-close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>지역 선택</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content padding>\n  <ion-item *ngFor="let area1 of areaLevel1">\n    <ion-label>{{area1.area}}</ion-label>\n    <ion-checkbox color="energized" checked="false" color="dark"(ionChange)="selectedArea($event, area1.area)"></ion-checkbox>\n  </ion-item>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/search-location/search-location.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], SearchLocationPage);

//# sourceMappingURL=search-location.js.map

/***/ })

});
//# sourceMappingURL=15.main.js.map