webpackJsonp([4],{

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_other_conditions__ = __webpack_require__(400);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchOtherConditionsPageModule", function() { return SearchOtherConditionsPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SearchOtherConditionsPageModule = (function () {
    function SearchOtherConditionsPageModule() {
    }
    return SearchOtherConditionsPageModule;
}());
SearchOtherConditionsPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__search_other_conditions__["a" /* SearchOtherConditionsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__search_other_conditions__["a" /* SearchOtherConditionsPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__search_other_conditions__["a" /* SearchOtherConditionsPage */]
        ]
    })
], SearchOtherConditionsPageModule);

//# sourceMappingURL=search-other-conditions.module.js.map

/***/ }),

/***/ 376:
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/*! @preserve
 * numeral.js
 * version : 2.0.6
 * author : Adam Draper
 * license : MIT
 * http://adamwdraper.github.com/Numeral-js/
 */

(function (global, factory) {
    if (true) {
        !(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory();
    } else {
        global.numeral = factory();
    }
}(this, function () {
    /************************************
        Variables
    ************************************/

    var numeral,
        _,
        VERSION = '2.0.6',
        formats = {},
        locales = {},
        defaults = {
            currentLocale: 'en',
            zeroFormat: null,
            nullFormat: null,
            defaultFormat: '0,0',
            scalePercentBy100: true
        },
        options = {
            currentLocale: defaults.currentLocale,
            zeroFormat: defaults.zeroFormat,
            nullFormat: defaults.nullFormat,
            defaultFormat: defaults.defaultFormat,
            scalePercentBy100: defaults.scalePercentBy100
        };


    /************************************
        Constructors
    ************************************/

    // Numeral prototype object
    function Numeral(input, number) {
        this._input = input;

        this._value = number;
    }

    numeral = function(input) {
        var value,
            kind,
            unformatFunction,
            regexp;

        if (numeral.isNumeral(input)) {
            value = input.value();
        } else if (input === 0 || typeof input === 'undefined') {
            value = 0;
        } else if (input === null || _.isNaN(input)) {
            value = null;
        } else if (typeof input === 'string') {
            if (options.zeroFormat && input === options.zeroFormat) {
                value = 0;
            } else if (options.nullFormat && input === options.nullFormat || !input.replace(/[^0-9]+/g, '').length) {
                value = null;
            } else {
                for (kind in formats) {
                    regexp = typeof formats[kind].regexps.unformat === 'function' ? formats[kind].regexps.unformat() : formats[kind].regexps.unformat;

                    if (regexp && input.match(regexp)) {
                        unformatFunction = formats[kind].unformat;

                        break;
                    }
                }

                unformatFunction = unformatFunction || numeral._.stringToNumber;

                value = unformatFunction(input);
            }
        } else {
            value = Number(input)|| null;
        }

        return new Numeral(input, value);
    };

    // version number
    numeral.version = VERSION;

    // compare numeral object
    numeral.isNumeral = function(obj) {
        return obj instanceof Numeral;
    };

    // helper functions
    numeral._ = _ = {
        // formats numbers separators, decimals places, signs, abbreviations
        numberToFormat: function(value, format, roundingFunction) {
            var locale = locales[numeral.options.currentLocale],
                negP = false,
                optDec = false,
                leadingCount = 0,
                abbr = '',
                trillion = 1000000000000,
                billion = 1000000000,
                million = 1000000,
                thousand = 1000,
                decimal = '',
                neg = false,
                abbrForce, // force abbreviation
                abs,
                min,
                max,
                power,
                int,
                precision,
                signed,
                thousands,
                output;

            // make sure we never format a null value
            value = value || 0;

            abs = Math.abs(value);

            // see if we should use parentheses for negative number or if we should prefix with a sign
            // if both are present we default to parentheses
            if (numeral._.includes(format, '(')) {
                negP = true;
                format = format.replace(/[\(|\)]/g, '');
            } else if (numeral._.includes(format, '+') || numeral._.includes(format, '-')) {
                signed = numeral._.includes(format, '+') ? format.indexOf('+') : value < 0 ? format.indexOf('-') : -1;
                format = format.replace(/[\+|\-]/g, '');
            }

            // see if abbreviation is wanted
            if (numeral._.includes(format, 'a')) {
                abbrForce = format.match(/a(k|m|b|t)?/);

                abbrForce = abbrForce ? abbrForce[1] : false;

                // check for space before abbreviation
                if (numeral._.includes(format, ' a')) {
                    abbr = ' ';
                }

                format = format.replace(new RegExp(abbr + 'a[kmbt]?'), '');

                if (abs >= trillion && !abbrForce || abbrForce === 't') {
                    // trillion
                    abbr += locale.abbreviations.trillion;
                    value = value / trillion;
                } else if (abs < trillion && abs >= billion && !abbrForce || abbrForce === 'b') {
                    // billion
                    abbr += locale.abbreviations.billion;
                    value = value / billion;
                } else if (abs < billion && abs >= million && !abbrForce || abbrForce === 'm') {
                    // million
                    abbr += locale.abbreviations.million;
                    value = value / million;
                } else if (abs < million && abs >= thousand && !abbrForce || abbrForce === 'k') {
                    // thousand
                    abbr += locale.abbreviations.thousand;
                    value = value / thousand;
                }
            }

            // check for optional decimals
            if (numeral._.includes(format, '[.]')) {
                optDec = true;
                format = format.replace('[.]', '.');
            }

            // break number and format
            int = value.toString().split('.')[0];
            precision = format.split('.')[1];
            thousands = format.indexOf(',');
            leadingCount = (format.split('.')[0].split(',')[0].match(/0/g) || []).length;

            if (precision) {
                if (numeral._.includes(precision, '[')) {
                    precision = precision.replace(']', '');
                    precision = precision.split('[');
                    decimal = numeral._.toFixed(value, (precision[0].length + precision[1].length), roundingFunction, precision[1].length);
                } else {
                    decimal = numeral._.toFixed(value, precision.length, roundingFunction);
                }

                int = decimal.split('.')[0];

                if (numeral._.includes(decimal, '.')) {
                    decimal = locale.delimiters.decimal + decimal.split('.')[1];
                } else {
                    decimal = '';
                }

                if (optDec && Number(decimal.slice(1)) === 0) {
                    decimal = '';
                }
            } else {
                int = numeral._.toFixed(value, 0, roundingFunction);
            }

            // check abbreviation again after rounding
            if (abbr && !abbrForce && Number(int) >= 1000 && abbr !== locale.abbreviations.trillion) {
                int = String(Number(int) / 1000);

                switch (abbr) {
                    case locale.abbreviations.thousand:
                        abbr = locale.abbreviations.million;
                        break;
                    case locale.abbreviations.million:
                        abbr = locale.abbreviations.billion;
                        break;
                    case locale.abbreviations.billion:
                        abbr = locale.abbreviations.trillion;
                        break;
                }
            }


            // format number
            if (numeral._.includes(int, '-')) {
                int = int.slice(1);
                neg = true;
            }

            if (int.length < leadingCount) {
                for (var i = leadingCount - int.length; i > 0; i--) {
                    int = '0' + int;
                }
            }

            if (thousands > -1) {
                int = int.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + locale.delimiters.thousands);
            }

            if (format.indexOf('.') === 0) {
                int = '';
            }

            output = int + decimal + (abbr ? abbr : '');

            if (negP) {
                output = (negP && neg ? '(' : '') + output + (negP && neg ? ')' : '');
            } else {
                if (signed >= 0) {
                    output = signed === 0 ? (neg ? '-' : '+') + output : output + (neg ? '-' : '+');
                } else if (neg) {
                    output = '-' + output;
                }
            }

            return output;
        },
        // unformats numbers separators, decimals places, signs, abbreviations
        stringToNumber: function(string) {
            var locale = locales[options.currentLocale],
                stringOriginal = string,
                abbreviations = {
                    thousand: 3,
                    million: 6,
                    billion: 9,
                    trillion: 12
                },
                abbreviation,
                value,
                i,
                regexp;

            if (options.zeroFormat && string === options.zeroFormat) {
                value = 0;
            } else if (options.nullFormat && string === options.nullFormat || !string.replace(/[^0-9]+/g, '').length) {
                value = null;
            } else {
                value = 1;

                if (locale.delimiters.decimal !== '.') {
                    string = string.replace(/\./g, '').replace(locale.delimiters.decimal, '.');
                }

                for (abbreviation in abbreviations) {
                    regexp = new RegExp('[^a-zA-Z]' + locale.abbreviations[abbreviation] + '(?:\\)|(\\' + locale.currency.symbol + ')?(?:\\))?)?$');

                    if (stringOriginal.match(regexp)) {
                        value *= Math.pow(10, abbreviations[abbreviation]);
                        break;
                    }
                }

                // check for negative number
                value *= (string.split('-').length + Math.min(string.split('(').length - 1, string.split(')').length - 1)) % 2 ? 1 : -1;

                // remove non numbers
                string = string.replace(/[^0-9\.]+/g, '');

                value *= Number(string);
            }

            return value;
        },
        isNaN: function(value) {
            return typeof value === 'number' && isNaN(value);
        },
        includes: function(string, search) {
            return string.indexOf(search) !== -1;
        },
        insert: function(string, subString, start) {
            return string.slice(0, start) + subString + string.slice(start);
        },
        reduce: function(array, callback /*, initialValue*/) {
            if (this === null) {
                throw new TypeError('Array.prototype.reduce called on null or undefined');
            }

            if (typeof callback !== 'function') {
                throw new TypeError(callback + ' is not a function');
            }

            var t = Object(array),
                len = t.length >>> 0,
                k = 0,
                value;

            if (arguments.length === 3) {
                value = arguments[2];
            } else {
                while (k < len && !(k in t)) {
                    k++;
                }

                if (k >= len) {
                    throw new TypeError('Reduce of empty array with no initial value');
                }

                value = t[k++];
            }
            for (; k < len; k++) {
                if (k in t) {
                    value = callback(value, t[k], k, t);
                }
            }
            return value;
        },
        /**
         * Computes the multiplier necessary to make x >= 1,
         * effectively eliminating miscalculations caused by
         * finite precision.
         */
        multiplier: function (x) {
            var parts = x.toString().split('.');

            return parts.length < 2 ? 1 : Math.pow(10, parts[1].length);
        },
        /**
         * Given a variable number of arguments, returns the maximum
         * multiplier that must be used to normalize an operation involving
         * all of them.
         */
        correctionFactor: function () {
            var args = Array.prototype.slice.call(arguments);

            return args.reduce(function(accum, next) {
                var mn = _.multiplier(next);
                return accum > mn ? accum : mn;
            }, 1);
        },
        /**
         * Implementation of toFixed() that treats floats more like decimals
         *
         * Fixes binary rounding issues (eg. (0.615).toFixed(2) === '0.61') that present
         * problems for accounting- and finance-related software.
         */
        toFixed: function(value, maxDecimals, roundingFunction, optionals) {
            var splitValue = value.toString().split('.'),
                minDecimals = maxDecimals - (optionals || 0),
                boundedPrecision,
                optionalsRegExp,
                power,
                output;

            // Use the smallest precision value possible to avoid errors from floating point representation
            if (splitValue.length === 2) {
              boundedPrecision = Math.min(Math.max(splitValue[1].length, minDecimals), maxDecimals);
            } else {
              boundedPrecision = minDecimals;
            }

            power = Math.pow(10, boundedPrecision);

            // Multiply up by precision, round accurately, then divide and use native toFixed():
            output = (roundingFunction(value + 'e+' + boundedPrecision) / power).toFixed(boundedPrecision);

            if (optionals > maxDecimals - boundedPrecision) {
                optionalsRegExp = new RegExp('\\.?0{1,' + (optionals - (maxDecimals - boundedPrecision)) + '}$');
                output = output.replace(optionalsRegExp, '');
            }

            return output;
        }
    };

    // avaliable options
    numeral.options = options;

    // avaliable formats
    numeral.formats = formats;

    // avaliable formats
    numeral.locales = locales;

    // This function sets the current locale.  If
    // no arguments are passed in, it will simply return the current global
    // locale key.
    numeral.locale = function(key) {
        if (key) {
            options.currentLocale = key.toLowerCase();
        }

        return options.currentLocale;
    };

    // This function provides access to the loaded locale data.  If
    // no arguments are passed in, it will simply return the current
    // global locale object.
    numeral.localeData = function(key) {
        if (!key) {
            return locales[options.currentLocale];
        }

        key = key.toLowerCase();

        if (!locales[key]) {
            throw new Error('Unknown locale : ' + key);
        }

        return locales[key];
    };

    numeral.reset = function() {
        for (var property in defaults) {
            options[property] = defaults[property];
        }
    };

    numeral.zeroFormat = function(format) {
        options.zeroFormat = typeof(format) === 'string' ? format : null;
    };

    numeral.nullFormat = function (format) {
        options.nullFormat = typeof(format) === 'string' ? format : null;
    };

    numeral.defaultFormat = function(format) {
        options.defaultFormat = typeof(format) === 'string' ? format : '0.0';
    };

    numeral.register = function(type, name, format) {
        name = name.toLowerCase();

        if (this[type + 's'][name]) {
            throw new TypeError(name + ' ' + type + ' already registered.');
        }

        this[type + 's'][name] = format;

        return format;
    };


    numeral.validate = function(val, culture) {
        var _decimalSep,
            _thousandSep,
            _currSymbol,
            _valArray,
            _abbrObj,
            _thousandRegEx,
            localeData,
            temp;

        //coerce val to string
        if (typeof val !== 'string') {
            val += '';

            if (console.warn) {
                console.warn('Numeral.js: Value is not string. It has been co-erced to: ', val);
            }
        }

        //trim whitespaces from either sides
        val = val.trim();

        //if val is just digits return true
        if (!!val.match(/^\d+$/)) {
            return true;
        }

        //if val is empty return false
        if (val === '') {
            return false;
        }

        //get the decimal and thousands separator from numeral.localeData
        try {
            //check if the culture is understood by numeral. if not, default it to current locale
            localeData = numeral.localeData(culture);
        } catch (e) {
            localeData = numeral.localeData(numeral.locale());
        }

        //setup the delimiters and currency symbol based on culture/locale
        _currSymbol = localeData.currency.symbol;
        _abbrObj = localeData.abbreviations;
        _decimalSep = localeData.delimiters.decimal;
        if (localeData.delimiters.thousands === '.') {
            _thousandSep = '\\.';
        } else {
            _thousandSep = localeData.delimiters.thousands;
        }

        // validating currency symbol
        temp = val.match(/^[^\d]+/);
        if (temp !== null) {
            val = val.substr(1);
            if (temp[0] !== _currSymbol) {
                return false;
            }
        }

        //validating abbreviation symbol
        temp = val.match(/[^\d]+$/);
        if (temp !== null) {
            val = val.slice(0, -1);
            if (temp[0] !== _abbrObj.thousand && temp[0] !== _abbrObj.million && temp[0] !== _abbrObj.billion && temp[0] !== _abbrObj.trillion) {
                return false;
            }
        }

        _thousandRegEx = new RegExp(_thousandSep + '{2}');

        if (!val.match(/[^\d.,]/g)) {
            _valArray = val.split(_decimalSep);
            if (_valArray.length > 2) {
                return false;
            } else {
                if (_valArray.length < 2) {
                    return ( !! _valArray[0].match(/^\d+.*\d$/) && !_valArray[0].match(_thousandRegEx));
                } else {
                    if (_valArray[0].length === 1) {
                        return ( !! _valArray[0].match(/^\d+$/) && !_valArray[0].match(_thousandRegEx) && !! _valArray[1].match(/^\d+$/));
                    } else {
                        return ( !! _valArray[0].match(/^\d+.*\d$/) && !_valArray[0].match(_thousandRegEx) && !! _valArray[1].match(/^\d+$/));
                    }
                }
            }
        }

        return false;
    };


    /************************************
        Numeral Prototype
    ************************************/

    numeral.fn = Numeral.prototype = {
        clone: function() {
            return numeral(this);
        },
        format: function(inputString, roundingFunction) {
            var value = this._value,
                format = inputString || options.defaultFormat,
                kind,
                output,
                formatFunction;

            // make sure we have a roundingFunction
            roundingFunction = roundingFunction || Math.round;

            // format based on value
            if (value === 0 && options.zeroFormat !== null) {
                output = options.zeroFormat;
            } else if (value === null && options.nullFormat !== null) {
                output = options.nullFormat;
            } else {
                for (kind in formats) {
                    if (format.match(formats[kind].regexps.format)) {
                        formatFunction = formats[kind].format;

                        break;
                    }
                }

                formatFunction = formatFunction || numeral._.numberToFormat;

                output = formatFunction(value, format, roundingFunction);
            }

            return output;
        },
        value: function() {
            return this._value;
        },
        input: function() {
            return this._input;
        },
        set: function(value) {
            this._value = Number(value);

            return this;
        },
        add: function(value) {
            var corrFactor = _.correctionFactor.call(null, this._value, value);

            function cback(accum, curr, currI, O) {
                return accum + Math.round(corrFactor * curr);
            }

            this._value = _.reduce([this._value, value], cback, 0) / corrFactor;

            return this;
        },
        subtract: function(value) {
            var corrFactor = _.correctionFactor.call(null, this._value, value);

            function cback(accum, curr, currI, O) {
                return accum - Math.round(corrFactor * curr);
            }

            this._value = _.reduce([value], cback, Math.round(this._value * corrFactor)) / corrFactor;

            return this;
        },
        multiply: function(value) {
            function cback(accum, curr, currI, O) {
                var corrFactor = _.correctionFactor(accum, curr);
                return Math.round(accum * corrFactor) * Math.round(curr * corrFactor) / Math.round(corrFactor * corrFactor);
            }

            this._value = _.reduce([this._value, value], cback, 1);

            return this;
        },
        divide: function(value) {
            function cback(accum, curr, currI, O) {
                var corrFactor = _.correctionFactor(accum, curr);
                return Math.round(accum * corrFactor) / Math.round(curr * corrFactor);
            }

            this._value = _.reduce([this._value, value], cback);

            return this;
        },
        difference: function(value) {
            return Math.abs(numeral(this._value).subtract(value).value());
        }
    };

    /************************************
        Default Locale && Format
    ************************************/

    numeral.register('locale', 'en', {
        delimiters: {
            thousands: ',',
            decimal: '.'
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal: function(number) {
            var b = number % 10;
            return (~~(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
        },
        currency: {
            symbol: '$'
        }
    });

    

(function() {
        numeral.register('format', 'bps', {
            regexps: {
                format: /(BPS)/,
                unformat: /(BPS)/
            },
            format: function(value, format, roundingFunction) {
                var space = numeral._.includes(format, ' BPS') ? ' ' : '',
                    output;

                value = value * 10000;

                // check for space before BPS
                format = format.replace(/\s?BPS/, '');

                output = numeral._.numberToFormat(value, format, roundingFunction);

                if (numeral._.includes(output, ')')) {
                    output = output.split('');

                    output.splice(-1, 0, space + 'BPS');

                    output = output.join('');
                } else {
                    output = output + space + 'BPS';
                }

                return output;
            },
            unformat: function(string) {
                return +(numeral._.stringToNumber(string) * 0.0001).toFixed(15);
            }
        });
})();


(function() {
        var decimal = {
            base: 1000,
            suffixes: ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        },
        binary = {
            base: 1024,
            suffixes: ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']
        };

    var allSuffixes =  decimal.suffixes.concat(binary.suffixes.filter(function (item) {
            return decimal.suffixes.indexOf(item) < 0;
        }));
        var unformatRegex = allSuffixes.join('|');
        // Allow support for BPS (http://www.investopedia.com/terms/b/basispoint.asp)
        unformatRegex = '(' + unformatRegex.replace('B', 'B(?!PS)') + ')';

    numeral.register('format', 'bytes', {
        regexps: {
            format: /([0\s]i?b)/,
            unformat: new RegExp(unformatRegex)
        },
        format: function(value, format, roundingFunction) {
            var output,
                bytes = numeral._.includes(format, 'ib') ? binary : decimal,
                suffix = numeral._.includes(format, ' b') || numeral._.includes(format, ' ib') ? ' ' : '',
                power,
                min,
                max;

            // check for space before
            format = format.replace(/\s?i?b/, '');

            for (power = 0; power <= bytes.suffixes.length; power++) {
                min = Math.pow(bytes.base, power);
                max = Math.pow(bytes.base, power + 1);

                if (value === null || value === 0 || value >= min && value < max) {
                    suffix += bytes.suffixes[power];

                    if (min > 0) {
                        value = value / min;
                    }

                    break;
                }
            }

            output = numeral._.numberToFormat(value, format, roundingFunction);

            return output + suffix;
        },
        unformat: function(string) {
            var value = numeral._.stringToNumber(string),
                power,
                bytesMultiplier;

            if (value) {
                for (power = decimal.suffixes.length - 1; power >= 0; power--) {
                    if (numeral._.includes(string, decimal.suffixes[power])) {
                        bytesMultiplier = Math.pow(decimal.base, power);

                        break;
                    }

                    if (numeral._.includes(string, binary.suffixes[power])) {
                        bytesMultiplier = Math.pow(binary.base, power);

                        break;
                    }
                }

                value *= (bytesMultiplier || 1);
            }

            return value;
        }
    });
})();


(function() {
        numeral.register('format', 'currency', {
        regexps: {
            format: /(\$)/
        },
        format: function(value, format, roundingFunction) {
            var locale = numeral.locales[numeral.options.currentLocale],
                symbols = {
                    before: format.match(/^([\+|\-|\(|\s|\$]*)/)[0],
                    after: format.match(/([\+|\-|\)|\s|\$]*)$/)[0]
                },
                output,
                symbol,
                i;

            // strip format of spaces and $
            format = format.replace(/\s?\$\s?/, '');

            // format the number
            output = numeral._.numberToFormat(value, format, roundingFunction);

            // update the before and after based on value
            if (value >= 0) {
                symbols.before = symbols.before.replace(/[\-\(]/, '');
                symbols.after = symbols.after.replace(/[\-\)]/, '');
            } else if (value < 0 && (!numeral._.includes(symbols.before, '-') && !numeral._.includes(symbols.before, '('))) {
                symbols.before = '-' + symbols.before;
            }

            // loop through each before symbol
            for (i = 0; i < symbols.before.length; i++) {
                symbol = symbols.before[i];

                switch (symbol) {
                    case '$':
                        output = numeral._.insert(output, locale.currency.symbol, i);
                        break;
                    case ' ':
                        output = numeral._.insert(output, ' ', i + locale.currency.symbol.length - 1);
                        break;
                }
            }

            // loop through each after symbol
            for (i = symbols.after.length - 1; i >= 0; i--) {
                symbol = symbols.after[i];

                switch (symbol) {
                    case '$':
                        output = i === symbols.after.length - 1 ? output + locale.currency.symbol : numeral._.insert(output, locale.currency.symbol, -(symbols.after.length - (1 + i)));
                        break;
                    case ' ':
                        output = i === symbols.after.length - 1 ? output + ' ' : numeral._.insert(output, ' ', -(symbols.after.length - (1 + i) + locale.currency.symbol.length - 1));
                        break;
                }
            }


            return output;
        }
    });
})();


(function() {
        numeral.register('format', 'exponential', {
        regexps: {
            format: /(e\+|e-)/,
            unformat: /(e\+|e-)/
        },
        format: function(value, format, roundingFunction) {
            var output,
                exponential = typeof value === 'number' && !numeral._.isNaN(value) ? value.toExponential() : '0e+0',
                parts = exponential.split('e');

            format = format.replace(/e[\+|\-]{1}0/, '');

            output = numeral._.numberToFormat(Number(parts[0]), format, roundingFunction);

            return output + 'e' + parts[1];
        },
        unformat: function(string) {
            var parts = numeral._.includes(string, 'e+') ? string.split('e+') : string.split('e-'),
                value = Number(parts[0]),
                power = Number(parts[1]);

            power = numeral._.includes(string, 'e-') ? power *= -1 : power;

            function cback(accum, curr, currI, O) {
                var corrFactor = numeral._.correctionFactor(accum, curr),
                    num = (accum * corrFactor) * (curr * corrFactor) / (corrFactor * corrFactor);
                return num;
            }

            return numeral._.reduce([value, Math.pow(10, power)], cback, 1);
        }
    });
})();


(function() {
        numeral.register('format', 'ordinal', {
        regexps: {
            format: /(o)/
        },
        format: function(value, format, roundingFunction) {
            var locale = numeral.locales[numeral.options.currentLocale],
                output,
                ordinal = numeral._.includes(format, ' o') ? ' ' : '';

            // check for space before
            format = format.replace(/\s?o/, '');

            ordinal += locale.ordinal(value);

            output = numeral._.numberToFormat(value, format, roundingFunction);

            return output + ordinal;
        }
    });
})();


(function() {
        numeral.register('format', 'percentage', {
        regexps: {
            format: /(%)/,
            unformat: /(%)/
        },
        format: function(value, format, roundingFunction) {
            var space = numeral._.includes(format, ' %') ? ' ' : '',
                output;

            if (numeral.options.scalePercentBy100) {
                value = value * 100;
            }

            // check for space before %
            format = format.replace(/\s?\%/, '');

            output = numeral._.numberToFormat(value, format, roundingFunction);

            if (numeral._.includes(output, ')')) {
                output = output.split('');

                output.splice(-1, 0, space + '%');

                output = output.join('');
            } else {
                output = output + space + '%';
            }

            return output;
        },
        unformat: function(string) {
            var number = numeral._.stringToNumber(string);
            if (numeral.options.scalePercentBy100) {
                return number * 0.01;
            }
            return number;
        }
    });
})();


(function() {
        numeral.register('format', 'time', {
        regexps: {
            format: /(:)/,
            unformat: /(:)/
        },
        format: function(value, format, roundingFunction) {
            var hours = Math.floor(value / 60 / 60),
                minutes = Math.floor((value - (hours * 60 * 60)) / 60),
                seconds = Math.round(value - (hours * 60 * 60) - (minutes * 60));

            return hours + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds);
        },
        unformat: function(string) {
            var timeArray = string.split(':'),
                seconds = 0;

            // turn hours and minutes into seconds and add them all up
            if (timeArray.length === 3) {
                // hours
                seconds = seconds + (Number(timeArray[0]) * 60 * 60);
                // minutes
                seconds = seconds + (Number(timeArray[1]) * 60);
                // seconds
                seconds = seconds + Number(timeArray[2]);
            } else if (timeArray.length === 2) {
                // minutes
                seconds = seconds + (Number(timeArray[0]) * 60);
                // seconds
                seconds = seconds + Number(timeArray[1]);
            }
            return Number(seconds);
        }
    });
})();

return numeral;
}));


/***/ }),

/***/ 400:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_numeral__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_numeral___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_numeral__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchOtherConditionsPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SearchOtherConditionsPage = (function () {
    function SearchOtherConditionsPage(viewCtrl, navCtrl, navParams, alertCtrl, ga) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.ga = ga;
        this.selectedType = '1'; //------> 전체: 1 Defualt selected
        this.largeFlag = false; //------> 대형: 3
        this.mediumFlag = false; //------> 중형: 3
        this.semiMediumFlag = false; //------> 준중형: 3
        this.smallFlag = false; //------> 소형: 2
        this.extraSmallFlag = false; //------> 경차: 2
        this.suvFlag = false; //------> SUV: 4
        this.importFlag = false; //------> 수입차: 5
        this.etcFlag = false; //------> 기타: 5
        this.priceFlag = false;
        this.directPriceFlag = false;
        this.minPrice = 0;
        this.maxPrice = 5000;
        this.yearFlag = false;
        this.directYearFlag = false;
        this.kmFlag = false;
        this.directKmFlag = false;
        this.minKm = 0;
        this.maxKm = 300000;
        this.otherParams = {
            minPrice: 0,
            maxPrice: 0,
            minYear: '',
            maxYear: '',
            minKm: 0,
            maxKm: 0,
            type: '1',
            puer: '1',
            gear: '1'
        };
        this.puer = '1';
        this.puers = [
            { id: '1', puer: '전체' },
            { id: '2', puer: '디젤' },
            { id: '3', puer: '가솔린' },
            { id: '4', puer: '하이브리드' },
            { id: '5', puer: '전기' },
            { id: '6', puer: '가스' },
            { id: '7', puer: '기타' }
        ];
        this.gear = '1';
        this.gears = [
            { id: '1', gear: '전체' },
            { id: '2', gear: '수동' },
            { id: '3', gear: '오토' },
            { id: '4', gear: '기타' }
        ];
        this.carPrice = {
            lower: this.minPrice,
            upper: this.maxPrice
        };
        this.viewMinPrice = this.minPrice;
        this.viewMaxPrice = this.maxPrice;
        this.thisYear = new Date();
        this.thisYear = dateToYYYYMMDD(this.thisYear);
        //----- Date format function-----//
        function dateToYYYYMMDD(date) {
            return date.getFullYear();
        }
        this.maxYear = this.thisYear;
        this.minYear = this.thisYear - 20;
        this.carYear = {
            lower: this.minYear,
            upper: this.maxYear
        };
        this.viewMaxYear = this.maxYear;
        this.viewMinYear = this.minYear;
        this.carKm = {
            lower: this.minKm,
            upper: this.maxKm
        };
        this.viewMaxKm = this.maxKm;
        this.viewMinKm = this.minKm;
    }
    SearchOtherConditionsPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('차량검색조건'); //trackView for Google Analytics
    };
    SearchOtherConditionsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchOtherConditionsPage');
    };
    //차량타입 ---> 1:전체, 2:소형, 3:중대형, 4:SUV, 5:수입차
    SearchOtherConditionsPage.prototype.largeOnOff = function () {
        if (this.largeFlag == false) {
            this.largeFlag = true;
            this.selectedType = '3';
            this.mediumFlag = false;
            this.semiMediumFlag = false;
            this.smallFlag = false;
            this.extraSmallFlag = false;
            this.suvFlag = false;
            this.importFlag = false;
            this.etcFlag = false;
            //console.log(this.largeFlag, this.mediumFlag, this.semiMediumFlag, this.smallFlag, this.suvFlag, this.importFlag, this.etcFlag);
        }
        else {
            this.largeFlag = false;
        }
    };
    SearchOtherConditionsPage.prototype.mediumOnOff = function () {
        if (this.mediumFlag == false) {
            this.mediumFlag = true;
            this.selectedType = '3';
            this.largeFlag = false;
            this.semiMediumFlag = false;
            this.smallFlag = false;
            this.extraSmallFlag = false;
            this.suvFlag = false;
            this.importFlag = false;
            this.etcFlag = false;
        }
        else {
            this.mediumFlag = false;
        }
    };
    SearchOtherConditionsPage.prototype.semiMediumOnOff = function () {
        if (this.semiMediumFlag == false) {
            this.semiMediumFlag = true;
            this.selectedType = '3';
            this.largeFlag = false;
            this.mediumFlag = false;
            this.smallFlag = false;
            this.extraSmallFlag = false;
            this.suvFlag = false;
            this.importFlag = false;
            this.etcFlag = false;
        }
        else {
            this.semiMediumFlag = false;
        }
    };
    SearchOtherConditionsPage.prototype.smallOnOff = function () {
        if (this.smallFlag == false) {
            this.smallFlag = true;
            this.selectedType = '2';
            this.largeFlag = false;
            this.mediumFlag = false;
            this.semiMediumFlag = false;
            this.extraSmallFlag = false;
            this.suvFlag = false;
            this.importFlag = false;
            this.etcFlag = false;
        }
        else {
            this.smallFlag = false;
        }
    };
    SearchOtherConditionsPage.prototype.extraSmallOnOff = function () {
        if (this.extraSmallFlag == false) {
            this.extraSmallFlag = true;
            this.selectedType = '2';
            this.largeFlag = false;
            this.mediumFlag = false;
            this.semiMediumFlag = false;
            this.smallFlag = false;
            this.suvFlag = false;
            this.importFlag = false;
            this.etcFlag = false;
        }
        else {
            this.extraSmallFlag = false;
        }
    };
    SearchOtherConditionsPage.prototype.suvOnOff = function () {
        if (this.suvFlag == false) {
            this.suvFlag = true;
            this.selectedType = '4';
            this.largeFlag = false;
            this.mediumFlag = false;
            this.semiMediumFlag = false;
            this.smallFlag = false;
            this.extraSmallFlag = false;
            this.importFlag = false;
            this.etcFlag = false;
        }
        else {
            this.suvFlag = false;
        }
    };
    SearchOtherConditionsPage.prototype.importOnOff = function () {
        if (this.importFlag == false) {
            this.importFlag = true;
            this.selectedType = '5';
            this.largeFlag = false;
            this.mediumFlag = false;
            this.semiMediumFlag = false;
            this.smallFlag = false;
            this.extraSmallFlag = false;
            this.suvFlag = false;
            this.etcFlag = false;
        }
        else {
            this.importFlag = false;
        }
    };
    SearchOtherConditionsPage.prototype.etcOnOff = function () {
        if (this.etcFlag == false) {
            this.etcFlag = true;
            this.selectedType = '5';
            this.largeFlag = false;
            this.mediumFlag = false;
            this.semiMediumFlag = false;
            this.smallFlag = false;
            this.extraSmallFlag = false;
            this.suvFlag = false;
            this.importFlag = false;
        }
        else {
            this.etcFlag = false;
        }
    };
    SearchOtherConditionsPage.prototype.directPrice = function () {
        var _this = this;
        this.directPriceFlag = true;
        var loginPrompt = this.alertCtrl.create({
            title: '차량 가격(만원)',
            message: '원하시는 최소/최대 값을 입력하세요.',
            inputs: [
                {
                    name: 'minPrice',
                    placeholder: '최소 가격',
                    type: 'number'
                },
                {
                    name: 'maxPrice',
                    placeholder: '최대 가격',
                    type: 'number'
                }
            ],
            buttons: [
                {
                    text: '취소',
                    handler: function (promptData) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: '확인',
                    handler: function (promptData) {
                        var myNumeral = __WEBPACK_IMPORTED_MODULE_3_numeral___default()(promptData.minPrice);
                        var value = myNumeral.value();
                        console.log('this.myNumeral minPrice:' + value);
                        if (value == null || value == 'NaN') {
                            _this.inputMinPrice = 0;
                        }
                        else {
                            _this.inputMinPrice = Number(value);
                        }
                        myNumeral = __WEBPACK_IMPORTED_MODULE_3_numeral___default()(promptData.maxPrice);
                        value = myNumeral.value();
                        console.log('this.myNumeral maxPrice:' + value);
                        if (value == null || value == 'NaN') {
                            _this.inputMaxPrice = 0;
                        }
                        else {
                            _this.inputMaxPrice = Number(value);
                        }
                        if (_this.inputMinPrice > _this.inputMaxPrice) {
                            var alert_1 = _this.alertCtrl.create({
                                title: '알림',
                                subTitle: '최소값/최대값을 확인하세요.',
                                buttons: ['확인']
                            });
                            alert_1.present();
                        }
                        else {
                            _this.carPrice = {
                                lower: _this.inputMinPrice,
                                upper: _this.inputMaxPrice
                            };
                            _this.updatePrice(_this.inputMinPrice, _this.inputMaxPrice);
                        }
                    }
                }
            ]
        });
        loginPrompt.present();
    };
    SearchOtherConditionsPage.prototype.updatePrice = function (lower, upper) {
        this.viewMinPrice = lower;
        this.viewMaxPrice = upper;
        this.carPrice.lower = lower;
        this.carPrice.upper = upper;
        if (this.viewMinPrice == this.minPrice && this.viewMaxPrice == this.maxPrice) {
            this.priceFlag = false;
        }
        else {
            this.priceFlag = true;
        }
    };
    SearchOtherConditionsPage.prototype.directYear = function () {
        var _this = this;
        this.directYearFlag = true;
        var loginPrompt = this.alertCtrl.create({
            title: '연식(연도)',
            message: '원하시는 최소/최대 값을 입력하세요.',
            inputs: [
                {
                    name: 'minYear',
                    placeholder: '최소 연식',
                    type: 'number'
                },
                {
                    name: 'maxYear',
                    placeholder: '최대 연식',
                    type: 'number'
                }
            ],
            buttons: [
                {
                    text: '취소',
                    handler: function (promptData) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: '확인',
                    handler: function (promptData) {
                        var myNumeral = __WEBPACK_IMPORTED_MODULE_3_numeral___default()(promptData.minYear);
                        var value = myNumeral.value();
                        console.log('this.myNumeral minYear:' + value);
                        if (value == null || value == 'NaN') {
                            _this.inputMinYear = 0;
                        }
                        else {
                            _this.inputMinYear = Number(value);
                        }
                        myNumeral = __WEBPACK_IMPORTED_MODULE_3_numeral___default()(promptData.maxYear);
                        value = myNumeral.value();
                        console.log('this.myNumeral maxPrice:' + value);
                        if (value == null || value == 'NaN') {
                            _this.inputMaxYear = 0;
                        }
                        else {
                            _this.inputMaxYear = Number(value);
                        }
                        if (_this.inputMinYear > _this.inputMaxYear) {
                            var alert_2 = _this.alertCtrl.create({
                                title: '알림',
                                subTitle: '최소값/최대값을 확인하세요.',
                                buttons: ['확인']
                            });
                            alert_2.present();
                        }
                        else {
                            _this.carYear = {
                                lower: _this.inputMinYear,
                                upper: _this.inputMaxYear
                            };
                            _this.updateYear(_this.inputMinYear, _this.inputMaxYear);
                        }
                    }
                }
            ]
        });
        loginPrompt.present();
    };
    SearchOtherConditionsPage.prototype.updateYear = function (lower, upper) {
        this.viewMinYear = lower;
        this.viewMaxYear = upper;
        this.carYear.lower = lower;
        this.carYear.upper = upper;
        if (this.viewMinYear == this.minYear && this.viewMaxYear == this.maxYear) {
            this.yearFlag = false;
        }
        else {
            this.yearFlag = true;
        }
    };
    SearchOtherConditionsPage.prototype.directKm = function () {
        var _this = this;
        this.directKmFlag = true;
        var loginPrompt = this.alertCtrl.create({
            title: '주행 거리(Km)',
            message: '원하시는 최소/최대 값을 입력하세요.',
            inputs: [
                {
                    name: 'minKm',
                    placeholder: '최소 주행 거리',
                    type: 'number'
                },
                {
                    name: 'maxKm',
                    placeholder: '최대 주행 거리',
                    type: 'number'
                }
            ],
            buttons: [
                {
                    text: '취소',
                    handler: function (promptData) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: '확인',
                    handler: function (promptData) {
                        var myNumeral = __WEBPACK_IMPORTED_MODULE_3_numeral___default()(promptData.minKm);
                        var value = myNumeral.value();
                        console.log('this.myNumeral minKm:' + value);
                        if (value == null || value == 'NaN') {
                            _this.inputMinKm = 0;
                        }
                        else {
                            _this.inputMinKm = Number(value);
                        }
                        myNumeral = __WEBPACK_IMPORTED_MODULE_3_numeral___default()(promptData.maxKm);
                        value = myNumeral.value();
                        console.log('this.myNumeral maxKm:' + value);
                        if (value == null || value == 'NaN') {
                            _this.inputMaxKm = 0;
                        }
                        else {
                            _this.inputMaxKm = Number(value);
                        }
                        if (_this.inputMinKm > _this.inputMaxKm) {
                            var alert_3 = _this.alertCtrl.create({
                                title: '알림',
                                subTitle: '최소값/최대값을 확인하세요.',
                                buttons: ['확인']
                            });
                            alert_3.present();
                        }
                        else {
                            _this.carKm = {
                                lower: _this.inputMinKm,
                                upper: _this.inputMaxKm
                            };
                            _this.updateKm(_this.inputMinKm, _this.inputMaxKm);
                        }
                    }
                }
            ]
        });
        loginPrompt.present();
    };
    SearchOtherConditionsPage.prototype.updateKm = function (lower, upper) {
        this.viewMinKm = lower;
        this.viewMaxKm = upper;
        this.carKm.lower = lower;
        this.carKm.upper = upper;
        if (this.viewMinKm == this.minKm && this.viewMaxKm == this.maxKm) {
            this.kmFlag = false;
        }
        else {
            this.kmFlag = true;
        }
    };
    SearchOtherConditionsPage.prototype.selectedPuer = function (e) {
        console.log('car_puer:' + this.puer);
    };
    SearchOtherConditionsPage.prototype.selectedGear = function () {
        console.log('car_gear:' + this.gear);
    };
    SearchOtherConditionsPage.prototype.selected = function () {
        if ((this.inputMinPrice > this.inputMaxPrice) || (this.inputMinYear > this.inputMaxYear) || (this.inputMinKm > this.inputMaxKm)) {
            var alert_4 = this.alertCtrl.create({
                title: '알림',
                subTitle: '최소값/최대값을 확인하세요.',
                buttons: ['확인']
            });
            alert_4.present();
        }
        else {
            if (this.inputMinPrice > 0) {
                this.viewMinPrice = this.inputMinPrice;
            }
            if (this.inputMaxPrice > 0) {
                this.viewMaxPrice = this.inputMaxPrice;
            }
            if (this.inputMinKm > 0) {
                this.viewMinKm = this.inputMinKm;
            }
            if (this.inputMaxKm > 0) {
                this.viewMaxKm = this.inputMaxKm;
            }
            if (this.inputMinYear > 0) {
                this.viewMinYear = this.inputMinYear;
            }
            if (this.inputMaxYear > 0) {
                this.viewMaxYear = this.inputMaxYear;
            }
            this.otherParams = {
                minPrice: this.viewMinPrice,
                maxPrice: this.viewMaxPrice,
                minYear: this.viewMinYear,
                maxYear: this.viewMaxYear,
                minKm: this.viewMinKm,
                maxKm: this.viewMaxKm,
                type: this.selectedType,
                puer: this.puer,
                gear: this.gear
            };
            console.log('Price: ' + this.viewMinPrice + ' - ' + this.viewMaxPrice);
            console.log('Year: ' + this.viewMinYear + ' - ' + this.viewMaxYear);
            console.log('Km: ' + this.viewMinKm + ' - ' + this.viewMaxKm);
            this.viewCtrl.dismiss(this.otherParams);
        }
    };
    SearchOtherConditionsPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss('더보기');
    };
    return SearchOtherConditionsPage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */])
], SearchOtherConditionsPage.prototype, "slides", void 0);
SearchOtherConditionsPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-search-other-conditions',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/search-other-conditions/search-other-conditions.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button color="underblack" icon-only (click)="closeModal()">\n        <ion-icon name="md-close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>차량 검색 조건</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div class="spacer" style="height: 10px;"></div>\n  <h6>&nbsp;&nbsp;&nbsp;차종</h6>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-3 *ngIf="largeFlag"><img tappable src="img/icon/search/large_on.png" style="width: 80%; height: auto;" (click)="largeOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!largeFlag"><img tappable src="img/icon/search/large_off.png" style="width: 80%; height: auto;" (click)="largeOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="mediumFlag"><img tappable src="img/icon/search/medium_on.png" style="width: 80%; height: auto;" (click)="mediumOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!mediumFlag"><img tappable src="img/icon/search/medium_off.png" style="width: 80%; height: auto;" (click)="mediumOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="semiMediumFlag"><img tappable src="img/icon/search/semi-medium_on.png" style="width: 80%; height: auto;" (click)="semiMediumOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!semiMediumFlag"><img tappable src="img/icon/search/semi-medium_off.png" style="width: 80%; height: auto;" (click)="semiMediumOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="smallFlag"><img tappable src="img/icon/search/small_on.png" style="width: 80%; height: auto;" (click)="smallOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!smallFlag"><img tappable src="img/icon/search/small_off.png" style="width: 80%; height: auto;" (click)="smallOnOff()"/></ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-3 *ngIf="extraSmallFlag"><img tappable src="img/icon/search/extra-small_on.png" style="width: 80%; height: auto;" (click)="extraSmallOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!extraSmallFlag"><img tappable src="img/icon/search/extra-small_off.png" style="width: 80%; height: auto;" (click)="extraSmallOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="suvFlag"><img tappable src="img/icon/search/suv_on.png" style="width: 80%; height: auto;" (click)="suvOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!suvFlag"><img tappable src="img/icon/search/suv_off.png" style="width: 80%; height: auto;" (click)="suvOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="importFlag"><img tappable src="img/icon/search/import_on.png" style="width: 80%; height: auto;" (click)="importOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!importFlag"><img tappable src="img/icon/search/import_off.png" style="width: 80%; height: auto;" (click)="importOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="etcFlag"><img tappable src="img/icon/search/etc_on.png" style="width: 80%; height: auto;" (click)="etcOnOff()"/></ion-col>\n      <ion-col col-3 *ngIf="!etcFlag"><img tappable src="img/icon/search/etc_off.png" style="width: 80%; height: auto;" (click)="etcOnOff()"/></ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-card>    \n    <ion-item no-lines>\n      <h2>가격(만원)</h2>\n      <button ion-button item-right icon-right [outline]="!directPriceFlag" color="dark" (click)="directPrice()">직접입력\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-item>\n    <div>\n      <div *ngIf="!priceFlag">\n        <p align="center" style="margin-top: 5px; margin-bottom: 5px;">전체</p>\n      </div>\n      <div *ngIf="priceFlag">\n        <p align="center" style="margin-top: 5px; margin-bottom: 5px;">{{viewMinPrice | number:0}} - {{viewMaxPrice | number:0}}만원</p>\n      </div>\n      <ion-range (ionChange)="updatePrice(carPrice.lower, carPrice.upper);" dualKnobs="true" min={{minPrice}} max={{maxPrice}} step="10" snaps="false" pin="false" \n                [(ngModel)]="carPrice" color="primary" style="padding-top: 0px; padding-bottom: 0px;">\n        <ion-label range-left>최소</ion-label>\n        <ion-label range-right>최대</ion-label>\n      </ion-range>\n    </div>\n  </ion-card>\n  <ion-card>\n    <ion-item no-lines>\n      <h2>연식(년도)</h2>\n      <button ion-button item-right icon-right [outline]="!directYearFlag" color="dark" (click)="directYear()">직접입력\n        <ion-icon name="add" is-active="false"></ion-icon>\n      </button>\n    </ion-item>\n    <div>\n      <div *ngIf="!yearFlag">\n        <p align="center" style="margin-top: 5px; margin-bottom: 5px;">전체</p>\n      </div>\n      <div *ngIf="yearFlag">\n        <p align="center" style="margin-top: 5px; margin-bottom: 5px;">{{viewMinYear}} - {{viewMaxYear}}년</p>\n      </div>\n      <ion-range (ionChange)="updateYear(carYear.lower, carYear.upper);" dualKnobs="true" min={{minYear}} max={{maxYear}} step="1" snaps="false" pin="false"\n                [(ngModel)]="carYear" color="primary" style="padding-top: 0px; padding-bottom: 0px;">\n        <ion-label range-left>최소</ion-label>\n        <ion-label range-right>최대</ion-label>\n      </ion-range>\n    </div>\n  </ion-card>\n  <ion-card>\n    <ion-item no-lines>\n      <h2>주행거리(Km)</h2>\n      <button ion-button item-right icon-right [outline]="!directKmFlag" color="dark" (click)="directKm()">직접입력\n        <ion-icon name="add" is-active="false"></ion-icon>\n      </button>  \n    </ion-item>\n    <div>\n      <div *ngIf="!kmFlag">\n        <p align="center" style="margin-top: 5px; margin-bottom: 5px;">전체</p>\n      </div>\n      <div *ngIf="kmFlag">\n        <p align="center" style="margin-top: 5px; margin-bottom: 5px;">{{viewMinKm | number:0}} - {{viewMaxKm | number:0}}Km</p>\n      </div>\n      <ion-range (ionChange)="updateKm(carKm.lower, carKm.upper);" dualKnobs="true" min={{minKm}} max={{maxKm}} step="20000" snaps="false" pin="false" \n                [(ngModel)]="carKm" color="primary" style="padding-top: 0px; padding-bottom: 0px;">\n        <ion-label range-left>최소</ion-label>\n        <ion-label range-right>최대</ion-label>\n      </ion-range>\n    </div>\n  </ion-card>\n  <ion-card>\n    <ion-item no-lines>\n      <ion-label>연료</ion-label>\n      <ion-select [(ngModel)]="puer" cancelText="취소" okText="선택" (ionChange)="selectedPuer($event)">\n        <ion-option *ngFor="let p of puers" [value]="p.id">{{ p.puer }}</ion-option>\n      </ion-select>\n    </ion-item>\n  </ion-card>\n  <ion-card>\n    <ion-item no-lines>\n      <ion-label>변속기</ion-label>\n      <ion-select [(ngModel)]="gear" cancelText="취소" okText="선택" (ionChange)="selectedGear()">\n        <ion-option *ngFor="let t of gears" [value]="t.id">{{ t.gear }}</ion-option>\n      </ion-select>\n    </ion-item>\n  </ion-card>\n  <ion-grid>\n    <ion-row style="padding-left: 5px; padding-right: 5px;">\n      <button ion-button block color=\'1000Under\' (click)="selected()">선택 완료</button>\n    </ion-row>\n  </ion-grid>\n  <div class="spacer" style="height: 10px;"></div>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/search-other-conditions/search-other-conditions.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], SearchOtherConditionsPage);

//# sourceMappingURL=search-other-conditions.js.map

/***/ })

});
//# sourceMappingURL=4.main.js.map