webpackJsonp([31],{

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__category_view__ = __webpack_require__(381);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryViewPageModule", function() { return CategoryViewPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CategoryViewPageModule = (function () {
    function CategoryViewPageModule() {
    }
    return CategoryViewPageModule;
}());
CategoryViewPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__category_view__["a" /* CategoryViewPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__category_view__["a" /* CategoryViewPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__category_view__["a" /* CategoryViewPage */]
        ]
    })
], CategoryViewPageModule);

//# sourceMappingURL=category-view.module.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_maker_model_maker_model__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_setanalytics_setanalytics__ = __webpack_require__(218);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryViewPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//-----> Providers




var CategoryViewPage = (function () {
    function CategoryViewPage(viewCtrl, navCtrl, navParams, modalCtrl, loadingCtrl, alertCtrl, http, userInfoProvider, makerModelProvider, msgService, setAnalytics, socialSharing, ga) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.userInfoProvider = userInfoProvider;
        this.makerModelProvider = makerModelProvider;
        this.msgService = msgService;
        this.setAnalytics = setAnalytics;
        this.socialSharing = socialSharing;
        this.ga = ga;
        //-------------------------------------------------> for data query
        this.pickCars = [];
        this.myFavoriteCars = [];
        this.queryLength = 0;
        this.doInfiniteFlag = true;
        this.marketURI = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";
        this.boardURI = "http://cjsekfvm.cafe24.com/test_server/app/board/url/";
        this.carDetailTabFlag = false;
        this.selectedPick = navParams.get("categoryParam"); // categoryParam: type: "5", image: "img/category/import.png"
        this.queryParams = 'pick=' + this.selectedPick.key;
    }
    CategoryViewPage.prototype.ionViewDidLoad = function () {
        this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
        this.login_flag = this.userInfoProvider.userProfile.login_flag;
        this.isMember_flag = this.userInfoProvider.userProfile.isMember_flag;
        this.getCars();
        console.log('ionViewDidLoad CategoryViewPage');
    };
    CategoryViewPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('모아보기: ' + this.selectedPick.title); //trackView for Google Analytics
        console.log('ionViewDidEnter CategoryViewPage');
    };
    CategoryViewPage.prototype.getCars = function () {
        var _this = this;
        this.getMyFavoriteCars();
        if (this.login_flag == true) {
            this.carDetailParam = this.queryParams + '&email=' + this.userInfoProvider.userProfile.u_email;
        }
        else {
            this.carDetailParam = this.queryParams;
        }
        this.pickCars = [];
        var body = this.carDetailParam, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.boardURI + "get_board_pick_detail_list.php";
        this.http.post(url, body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.pick_detail_list == undefined) {
                var alert_1 = _this.alertCtrl.create({
                    title: '알림',
                    subTitle: '검색결과가 없습니다.',
                    buttons: ['확인']
                });
                alert_1.present();
                _this.viewCtrl.dismiss();
            }
            else {
                for (var i = 0; i < Object.keys(data.pick_detail_list).length; i++) {
                    data.pick_detail_list[i].maker = _this.makerModelProvider.getMakerName(data.pick_detail_list[i].maker);
                    data.pick_detail_list[i].model = _this.makerModelProvider.getModelName(data.pick_detail_list[i].model);
                    // for 거래중 date_blind:"2017-08-31 00:00:00" >= this.todayDate
                    if (data.pick_detail_list[i].date_blind.substr(0, 10) >= _this.todayDate) {
                        data.pick_detail_list[i].state = '거래중';
                    }
                    // for find my favorite cars
                    for (var j = 0; j < _this.myFavoriteCars.length; j++) {
                        if (data.pick_detail_list[i].key == _this.myFavoriteCars[j].key) {
                            data.pick_detail_list[i].is_favorite = '1';
                        }
                    }
                    _this.pickCars.push(data.pick_detail_list[i]);
                }
                _this.queryLength = Object.keys(data.pick_detail_list).length;
                console.log('getCars() ===> this.queryLength: ' + _this.queryLength);
                if (_this.queryLength >= 20) {
                    _this.doInfiniteFlag = true;
                }
                else {
                    _this.doInfiniteFlag = false;
                }
            }
        });
    };
    CategoryViewPage.prototype.getMyFavoriteCars = function () {
        var _this = this;
        this.myFavoriteCars = [];
        if (this.userInfoProvider.userProfile.login_flag == true) {
            var body = 'order=1' + '&email=' + this.userInfoProvider.userProfile.u_email, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_favorite.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result == 'ok') {
                    for (var i = 0; i < Object.keys(data.sale_list).length; i++) {
                        _this.myFavoriteCars.push(data.sale_list[i]);
                    }
                }
            });
        }
    };
    CategoryViewPage.prototype.naviCarDetailPage = function (selectedCar) {
        var _this = this;
        if (!this.carDetailTabFlag) {
            this.carDetailTabFlag = true;
            if (this.login_flag == true) {
                this.carDetailParam = 'sale=' + selectedCar + '&email=' + this.userInfoProvider.userProfile.u_email;
            }
            else {
                this.carDetailParam = 'sale=' + selectedCar;
            }
            var body = this.carDetailParam, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_detail.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    var alert_2 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: _this.msgService.getMessage(data.result),
                        buttons: ['확인']
                    });
                    alert_2.present();
                }
                else {
                    var CarDetailPage_modal = _this.modalCtrl.create('CarDetailPage', { carParam: data.sale_detail });
                    CarDetailPage_modal.onDidDismiss(function (data) {
                        _this.carDetailTabFlag = false;
                    });
                    CarDetailPage_modal.present();
                }
            });
        }
    };
    CategoryViewPage.prototype.selectFavoriteCar = function (selectedCar) {
        var _this = this;
        if (this.userInfoProvider.userProfile.login_flag == false) {
            var LoginPage_modal = this.modalCtrl.create('LoginPage');
            LoginPage_modal.onDidDismiss(function (data) {
                _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            });
            LoginPage_modal.present();
        }
        else {
            var _loop_1 = function (i) {
                if (this_1.pickCars[i].key == selectedCar.key) {
                    if (this_1.pickCars[i].is_favorite == '0') {
                        //console.log('car.key car.is_favorite: '+this.cars[i].key+'-'+this.cars[i].is_favorite);
                        var favoriteQueryParams = 'sale=' + selectedCar.key +
                            '&email=' + this_1.userInfoProvider.userProfile.u_email +
                            '&pw=' + this_1.userInfoProvider.userProfile.u_pw +
                            '&favorite=' + '1'; //'1'=찜등록, '2'=찜취소
                        var body = favoriteQueryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this_1.marketURI + "set_sale_favorite.php";
                        this_1.http.post(url, body, options)
                            .map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            if (data.result != 'ok') {
                                var alert_3 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: _this.msgService.getMessage(data.result),
                                    buttons: ['확인']
                                });
                                alert_3.present();
                            }
                            else {
                                _this.ga.trackEvent('찜', '찜: ' + selectedCar.maker + '-' + selectedCar.model); //trackEvent for Google Analytics
                                var alert_4 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: '선택하신 차량이 찜 목록에 저장되었습니다.',
                                    buttons: ['확인']
                                });
                                alert_4.present();
                                _this.pickCars[i].is_favorite = '1';
                                var set_key = selectedCar.key;
                                var set_type = '2'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                                _this.setAnalytics.setAnalytics(set_key, set_type);
                            }
                        });
                    }
                    else {
                        //console.log('car.key car.is_favorite: '+this.cars[i].key+'-'+this.cars[i].is_favorite);
                        var favoriteQueryParams = 'sale=' + selectedCar.key +
                            '&email=' + this_1.userInfoProvider.userProfile.u_email +
                            '&pw=' + this_1.userInfoProvider.userProfile.u_pw +
                            '&favorite=' + '2'; //'1'=찜등록, '2'=찜취소
                        var body = favoriteQueryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this_1.marketURI + "set_sale_favorite.php";
                        this_1.http.post(url, body, options)
                            .map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            if (data.result != 'ok') {
                                var alert_5 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: _this.msgService.getMessage(data.result),
                                    buttons: ['확인']
                                });
                                alert_5.present();
                            }
                            else {
                                var alert_6 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: '선택하신 차량이 찜 목록에서 삭제되었습니다.',
                                    buttons: ['확인']
                                });
                                alert_6.present();
                                _this.pickCars[i].is_favorite = '0';
                                var set_key = selectedCar.key;
                                var set_type = '3'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                                _this.setAnalytics.setAnalytics(set_key, set_type);
                            }
                        });
                    }
                }
            };
            var this_1 = this;
            for (var i = 0; i < this.pickCars.length; i++) {
                _loop_1(i);
            }
        }
    };
    CategoryViewPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        if (this.doInfiniteFlag == true) {
            //this.queryParams = 'pick='+this.selectedPick;
            if (this.login_flag == true) {
                this.carDetailParam = this.queryParams + '&email=' + this.userInfoProvider.userProfile.u_email;
            }
            else {
                this.carDetailParam = this.queryParams;
            }
            var body = this.carDetailParam + '&key=' + this.queryLength, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.boardURI + "get_board_pick_detail_list.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    _this.doInfiniteFlag = false;
                }
                else {
                    for (var i = 0; i < Object.keys(data.pick_detail_list).length; i++) {
                        data.pick_detail_list[i].maker = _this.makerModelProvider.getMakerName(data.pick_detail_list[i].maker);
                        data.pick_detail_list[i].model = _this.makerModelProvider.getModelName(data.pick_detail_list[i].model);
                        // for 거래중 date_blind:"2017-08-31 00:00:00" >= this.todayDate
                        if (data.pick_detail_list[i].date_blind.substr(0, 10) >= _this.todayDate) {
                            data.pick_detail_list[i].state = '거래중';
                        }
                        // for find my favorite cars
                        for (var j = 0; j < _this.myFavoriteCars.length; j++) {
                            if (data.pick_detail_list[i].key == _this.myFavoriteCars[j].key) {
                                data.pick_detail_list[i].is_favorite = '1';
                            }
                        }
                        _this.pickCars.push(data.pick_detail_list[i]);
                    }
                    if (Object.keys(data.pick_detail_list).length < 20) {
                        _this.doInfiniteFlag = false;
                    }
                    _this.queryLength = _this.queryLength + Object.keys(data.pick_detail_list).length;
                }
                //console.log('Next operation has ended');
                infiniteScroll.complete();
            });
        }
    };
    CategoryViewPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return CategoryViewPage;
}());
CategoryViewPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-category-view',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/category-view/category-view.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button icon-only color="underblack" (click)="dismiss()">\n        <ion-icon name="ios-arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>모아 보기</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <img [src]="selectedPick.photo_path" style="display: block; margin-left: auto; margin-right: auto;"/>\n  <div class="spacer" style="height: 10px;"></div>\n  <div class="spacer" style="height: 10px; background-color: #eeeeee;"></div>\n  <div align="center">\n    <div no-margin no-padding *ngFor="let car of pickCars">\n      <div *ngIf="car.photo_height > 500" class="image-wrapper">\n        <div *ngIf="car.state === \'판매중\'">\n          <div class="pcar-image"> \n            <div class="centered">\n              <img [src]="car.photo_path" tappable (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="car.state === \'검수\'">\n          <div class="pcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                              \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n            <div class="centered">\n              <img src="img/state/inspecting.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="car.state === \'거래중\'">\n          <div class="pcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                              \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n            <div class="centered">\n              <img src="img/state/dealing.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="car.state === \'완료\'">\n          <div class="pcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                              \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n            <div class="centered">\n              <img src="img/state/sold.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div *ngIf="car.photo_height <= 500" class="image-wrapper">\n        <div *ngIf="car.state === \'판매중\'">\n          <div class="lcar-image"> \n            <div class="centered">\n              <img [src]="car.photo_path" tappable (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="car.state === \'검수\'">\n          <div class="lcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                              \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n            <div class="centered">\n              <img src="img/state/inspecting.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="car.state === \'거래중\'">\n          <div class="lcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                              \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n            <div class="centered">\n              <img src="img/state/dealing.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="car.state === \'완료\'">\n          <div class="lcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                              \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n            <div class="centered">\n              <img src="img/state/sold.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n            </div>\n          </div>\n        </div>\n      </div>\n      <ion-row style="height: 45px;">\n        <ion-col col-10>\n          <h5 align="left">&nbsp;{{car.price | number:0}}만원 {{car.maker}} {{car.model}}</h5>\n        </ion-col>\n        <ion-col col-2 *ngIf="car.is_favorite === \'1\' && car.state === \'판매중\'">\n          <button ion-button  color="1000Under" clear icon-only (click)="selectFavoriteCar(car)"><ion-icon mini name="heart"></ion-icon></button>\n        </ion-col>\n        <ion-col col-2 *ngIf="car.is_favorite === \'0\' && car.state === \'판매중\'">\n          <button  ion-button  color="undergray" clear icon-only (click)="selectFavoriteCar(car)"><ion-icon mini name="heart-outline"></ion-icon></button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-10 align="left">\n          &nbsp;{{car.year}}년{{car.month}}월 | {{car.mileage | number:0}}km  \n        </ion-col>\n        <ion-col col-2>\n          <ion-icon name="ios-eye-outline" item-left><span style="font-size: 11pt;">{{car.view | number:0}}</span></ion-icon>\n        </ion-col>\n      </ion-row>\n      <br>\n      <div class="spacer" style="height: 10px; background-color: #eeeeee;"></div>\n    </div>\n  </div>\n  <ion-infinite-scroll *ngIf="doInfiniteFlag" (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/category-view/category-view.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_5__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_maker_model_maker_model__["a" /* MakerModelProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_setanalytics_setanalytics__["a" /* SetanalyticsProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], CategoryViewPage);

//# sourceMappingURL=category-view.js.map

/***/ })

});
//# sourceMappingURL=31.main.js.map