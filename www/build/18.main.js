webpackJsonp([18],{

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__report__ = __webpack_require__(396);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportPageModule", function() { return ReportPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ReportPageModule = (function () {
    function ReportPageModule() {
    }
    return ReportPageModule;
}());
ReportPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__report__["a" /* ReportPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__report__["a" /* ReportPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__report__["a" /* ReportPage */]
        ]
    })
], ReportPageModule);

//# sourceMappingURL=report.module.js.map

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_setanalytics_setanalytics__ = __webpack_require__(218);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//-----> Providers


var ReportPage = (function () {
    function ReportPage(navCtrl, navParams, viewCtrl, alertCtrl, http, msgService, setAnalytics, ga) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.msgService = msgService;
        this.setAnalytics = setAnalytics;
        this.ga = ga;
        this.marketURI = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";
        this.type = '';
        this.memo = '';
        this.reportTypes = [
            { id: "01", type: "판매완료" },
            { id: "02", type: "딜러매물" },
            { id: "03", type: "정보오류" },
            { id: "04", type: "허위매물" },
            { id: "05", type: "기타" },
        ];
        this.sale = navParams.get("sale");
        this.email = navParams.get("email");
        this.pw = navParams.get("pw");
        //console.log('navParams.get: '+this.sale+' - '+this.email+' - '+this.pw);
    }
    ReportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReportPage');
    };
    ReportPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('차량신고'); //trackView for Google Analytics
    };
    ReportPage.prototype.submitReport = function () {
        var _this = this;
        if (this.type == undefined || this.type == '') {
            var alert_1 = this.alertCtrl.create({
                title: '알림',
                subTitle: '신고 타입을 선택해 주세요.',
                buttons: ['확인']
            });
            alert_1.present();
        }
        else if (this.type == '기타' && (this.memo == undefined || this.memo == '')) {
            var alert_2 = this.alertCtrl.create({
                title: '알림',
                subTitle: '신고 내용을 입력해 주세요.',
                buttons: ['확인']
            });
            alert_2.present();
        }
        else {
            console.log('this.type:' + this.type);
            this.queryParams = 'sale=' + this.sale +
                '&type=' + this.type +
                '&email=' + this.email +
                '&pw=' + this.pw +
                '&memo=' + this.memo;
            //----------------------------------------------------------------> get Similar cars
            var body_s = this.queryParams, type_s = 'application/x-www-form-urlencoded; charset=UTF-8', headers_s = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type_s }), options_s = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers_s }), url_s = this.marketURI + "set_sale_report.php";
            this.http.post(url_s, body_s, options_s)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    var alert_3 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: _this.msgService.getMessage(data.result),
                        buttons: ['확인']
                    });
                    alert_3.present();
                }
                else {
                    var alert_4 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: '정상 처리 되었습니다.',
                        buttons: ['확인']
                    });
                    alert_4.present();
                    var set_key = _this.sale;
                    var set_type = '4'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                    _this.setAnalytics.setAnalytics(set_key, set_type);
                    _this.viewCtrl.dismiss('1'); //reported
                }
            });
        }
    };
    ReportPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss('0'); //cancel
    };
    return ReportPage;
}());
ReportPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-report',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/report/report.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button icon-only  color="underblack" (click)="closeModal()">\n        <ion-icon name="md-close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>차량 신고</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-item radio-group *ngFor="let rt of reportTypes" [(ngModel)]="type">\n    <ion-label>{{rt.type}}</ion-label>\n    <ion-radio value="{{rt.type}}"></ion-radio>\n  </ion-item>\n  <div class="spacer" style="height: 15px;"></div>\n  <ion-item no-lines>\n    <ion-textarea [(ngModel)]="memo" placeholder="신고 사유를 입력해주세요."></ion-textarea>\n  </ion-item>\n  <div class="spacer" style="height: 15px;"></div>\n  <ion-grid>\n    <ion-row style="padding-left: 5px; padding-right: 5px;">\n      <button ion-button block color=\'1000Under\' (click)="submitReport()">신고하기</button>\n    </ion-row>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/report/report.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_4__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_setanalytics_setanalytics__["a" /* SetanalyticsProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], ReportPage);

//# sourceMappingURL=report.js.map

/***/ })

});
//# sourceMappingURL=18.main.js.map