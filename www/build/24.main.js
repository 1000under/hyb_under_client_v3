webpackJsonp([24],{

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loading__ = __webpack_require__(388);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingPageModule", function() { return LoadingPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoadingPageModule = (function () {
    function LoadingPageModule() {
    }
    return LoadingPageModule;
}());
LoadingPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__loading__["a" /* LoadingPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__loading__["a" /* LoadingPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__loading__["a" /* LoadingPage */]
        ]
    })
], LoadingPageModule);

//# sourceMappingURL=loading.module.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_device_device__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_outer_link_outer_link__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_maker_model_maker_model__ = __webpack_require__(111);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//-----> Providers





var LoadingPage = (function () {
    function LoadingPage(navCtrl, navParams, loadingCtrl, platform, storage, deviceProvider, userInfoProvider, msgService, outerLinkProvider, makerModelProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.storage = storage;
        this.deviceProvider = deviceProvider;
        this.userInfoProvider = userInfoProvider;
        this.msgService = msgService;
        this.outerLinkProvider = outerLinkProvider;
        this.makerModelProvider = makerModelProvider;
        this.platform.ready().then(function () {
            _this.deviceProvider.setDeviceInfo();
            _this.userInfoProvider.initLogin();
            _this.outerLinkProvider.getOuterLink();
            _this.outerLinkProvider.getPolicy();
            _this.makerModelProvider.getModelList();
        });
    }
    LoadingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomePage');
    };
    LoadingPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.makerModelProvider.getModelList(); // second request for get to MakerName & ModelName 
        this.storage.get('showIntro').then(function (val) {
            console.log('showIntro value: ' + val);
            if (val == 0) {
                //this.navCtrl.pop();
                //this.navCtrl.push('TabsPage');
                _this.navCtrl.setRoot('TabsPage');
            }
            else {
                setTimeout(function () {
                    _this.navCtrl.setRoot('IntroPage');
                }, 100);
                //this.navCtrl.push('IntroPage');
            }
        }, function () {
            setTimeout(function () {
                _this.navCtrl.setRoot('IntroPage');
            }, 100);
            //this.navCtrl.push('IntroPage');
        });
    };
    return LoadingPage;
}());
LoadingPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-loading',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/loading/loading.html"*/'<ion-content></ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/loading/loading.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_3__providers_device_device__["a" /* DeviceProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_outer_link_outer_link__["a" /* OuterLinkProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_maker_model_maker_model__["a" /* MakerModelProvider */]])
], LoadingPage);

//# sourceMappingURL=loading.js.map

/***/ })

});
//# sourceMappingURL=24.main.js.map