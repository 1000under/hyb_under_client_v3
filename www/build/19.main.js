webpackJsonp([19],{

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__regist_agree__ = __webpack_require__(395);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistAgreePageModule", function() { return RegistAgreePageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegistAgreePageModule = (function () {
    function RegistAgreePageModule() {
    }
    return RegistAgreePageModule;
}());
RegistAgreePageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__regist_agree__["a" /* RegistAgreePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__regist_agree__["a" /* RegistAgreePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__regist_agree__["a" /* RegistAgreePage */]
        ]
    })
], RegistAgreePageModule);

//# sourceMappingURL=regist-agree.module.js.map

/***/ }),

/***/ 395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistAgreePage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RegistAgreePage = (function () {
    function RegistAgreePage(navCtrl, navParams, modalCtrl, viewCtrl, ga) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.ga = ga;
    }
    RegistAgreePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegistAgreePage');
    };
    RegistAgreePage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('내차등록동의'); //trackView for Google Analytics
    };
    RegistAgreePage.prototype.agree = function () {
        var _this = this;
        this.ga.trackEvent('내차팔기', '동의하기'); //trackEvent for Google Analytics
        var AddMyCarPage_modal = this.modalCtrl.create('AddMyCarPage');
        AddMyCarPage_modal.onDidDismiss(function (data) {
            _this.viewCtrl.dismiss(data);
        });
        AddMyCarPage_modal.present();
    };
    RegistAgreePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return RegistAgreePage;
}());
RegistAgreePage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-regist-agree',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/regist-agree/regist-agree.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button icon-only color="underblack" (click)="dismiss()">\n        <ion-icon name="ios-arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>매칭수수료 안내</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div align="center"><img src="img/regist_info.png"  style="height: auto;"/></div>\n  <div class="spacer" style="height: 10px;"></div>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6 style="padding-top: 0px; padding-bottom: 0px;">\n        <button ion-button block outline color=\'undergray\' icon-left (click)="dismiss()">\n          <span style="font-size: 9pt;">아니오, 동의하지 않습니다.</span></button>\n      </ion-col>\n      <ion-col col-6 style="padding-top: 0px; padding-bottom: 0px;">\n        <button ion-button block color=\'1000Under\' icon-left (click)="agree()">\n          <span style="font-size: 9pt;">네, 동의하고 등록합니다.</span></button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/regist-agree/regist-agree.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], RegistAgreePage);

//# sourceMappingURL=regist-agree.js.map

/***/ })

});
//# sourceMappingURL=19.main.js.map