webpackJsonp([14],{

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__set_keyword__ = __webpack_require__(401);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SetKeywordPageModule", function() { return SetKeywordPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SetKeywordPageModule = (function () {
    function SetKeywordPageModule() {
    }
    return SetKeywordPageModule;
}());
SetKeywordPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__set_keyword__["a" /* SetKeywordPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__set_keyword__["a" /* SetKeywordPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__set_keyword__["a" /* SetKeywordPage */]
        ]
    })
], SetKeywordPageModule);

//# sourceMappingURL=set-keyword.module.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SetKeywordPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//-----> Providers


var SetKeywordPage = (function () {
    function SetKeywordPage(viewCtrl, navCtrl, navParams, alertCtrl, http, zone, userInfoProvider, msgService, ga) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.zone = zone;
        this.userInfoProvider = userInfoProvider;
        this.msgService = msgService;
        this.ga = ga;
        this.memberURI = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";
        this.showKeywordFlag = false;
        //keyword_list = [ "중대형차", "SUV", "자동변속기", "이벤트" ];
        this.keyword_list = [];
        this.keyword = '';
    }
    SetKeywordPage.prototype.ionViewDidLoad = function () {
        this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
        this.getKeyword();
        console.log('ionViewDidLoad SetKeywordPage');
    };
    SetKeywordPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('관심키워드설정'); //trackView for Google Analytics
    };
    SetKeywordPage.prototype.reflashPage = function () {
        var _this = this;
        this.zone.run(function () {
            _this.getKeyword();
            //console.log('Force update ======> ChatBoardPage');
        });
    };
    SetKeywordPage.prototype.getKeyword = function () {
        var _this = this;
        this.queryParams = 'email=' + this.userInfoProvider.userProfile.u_email +
            '&pw=' + this.userInfoProvider.userProfile.u_pw;
        var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.memberURI + "get_member.php";
        this.http.post(url, body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.keyword_list != undefined) {
                _this.keyword_list = data.keyword_list;
            }
        });
    };
    SetKeywordPage.prototype.addKeyword = function () {
        //console.log('this.addKeyword: ' +this.keyword);
        var value = this.keyword.replace(/\s/g, ''); //문자열 내의 공백 제거
        if (this.keyword === '' || value === '') {
            var alert_1 = this.alertCtrl.create({
                title: '알림!',
                subTitle: '키워드를 입력해 주세요.',
                buttons: ['확인']
            });
            alert_1.present();
        }
        else {
            this.keyword_list.push(value);
            this.submitKeyword();
        }
        this.keyword = '';
    };
    SetKeywordPage.prototype.removeKeyWord = function (selectedKeyword) {
        var index = this.keyword_list.indexOf(selectedKeyword);
        if (index > -1) {
            this.keyword_list.splice(index, 1);
        }
        this.submitKeyword();
        //console.log('Remove this.keyword_list: ' +this.keyword_list);
        this.keyword = '';
    };
    SetKeywordPage.prototype.submitKeyword = function () {
        var _this = this;
        if (this.keyword_list == null) {
            var alert_2 = this.alertCtrl.create({
                title: '알림!',
                subTitle: '키워드를 입력해 주십시요.',
                buttons: ['확인']
            });
            alert_2.present();
        }
        else {
            this.queryParams = 'email=' + this.userInfoProvider.userProfile.u_email +
                '&pw=' + this.userInfoProvider.userProfile.u_pw +
                '&keywords=' + this.keyword_list;
            var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.memberURI + "set_member_keyword.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    var alert_3 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: _this.msgService.getMessage(data.result),
                        buttons: ['확인']
                    });
                    alert_3.present();
                }
                else {
                    //console.log('Add this.keyword_list to Server' +this.keyword_list);
                    _this.reflashPage();
                }
            });
        }
        ; //====> else end.
    }; //====> submit logic end.
    SetKeywordPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    return SetKeywordPage;
}());
SetKeywordPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-set-keyword',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/set-keyword/set-keyword.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button icon-only  color="underblack" (click)="closeModal()">\n        <ion-icon name="md-close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>관심 키워드 설정</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-item no-lines>\n    <div class="spacer" style="height: 10px;" ></div>\n    <h2>ex) SUV, 중형세단</h2>\n  </ion-item>\n  <ion-list *ngFor="let kl of keyword_list" style="margin-bottom: 15px;">\n      <ion-item no-lines>\n        <h2>{{kl}}</h2>\n        <button ion-button clear item-end (click)="removeKeyWord(kl)"><ion-icon name="md-close" color="underblack"></ion-icon></button>\n      </ion-item>\n  </ion-list>\n</ion-content>\n\n<ion-footer>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-10>\n        <ion-input [(ngModel)]="keyword" type="text" placeholder="키워드를 입력하세요...."></ion-input>\n      </ion-col>\n      <ion-col col-2>\n          <button ion-fab mini (click)="addKeyword(keyword)"><ion-icon name="add"></ion-icon></button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer>'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/set-keyword/set-keyword.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
        __WEBPACK_IMPORTED_MODULE_4__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], SetKeywordPage);

//# sourceMappingURL=set-keyword.js.map

/***/ })

});
//# sourceMappingURL=14.main.js.map