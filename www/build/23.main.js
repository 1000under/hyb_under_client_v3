webpackJsonp([23],{

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__my_cars__ = __webpack_require__(390);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyCarsPageModule", function() { return MyCarsPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MyCarsPageModule = (function () {
    function MyCarsPageModule() {
    }
    return MyCarsPageModule;
}());
MyCarsPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__my_cars__["a" /* MyCarsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__my_cars__["a" /* MyCarsPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__my_cars__["a" /* MyCarsPage */]
        ]
    })
], MyCarsPageModule);

//# sourceMappingURL=my-cars.module.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_maker_model_maker_model__ = __webpack_require__(111);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyCarsPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//-----> Providers



var MyCarsPage = (function () {
    function MyCarsPage(viewCtrl, navCtrl, navParams, modalCtrl, loadingCtrl, alertCtrl, http, zone, ga, makerModelProvider, userInfoProvider, msgService) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.zone = zone;
        this.ga = ga;
        this.makerModelProvider = makerModelProvider;
        this.userInfoProvider = userInfoProvider;
        this.msgService = msgService;
        this.showNothing = false;
        this.carDetailTabFlag = false;
        this.updateMyCarTabFlag = false;
        //-------------------------------------------------> for data query
        this.cars = [];
        this.queryLength = 0;
        this.doInfiniteFlag = true;
        this.marketURI = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";
    }
    MyCarsPage.prototype.ionViewDidLoad = function () {
        this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
        this.getSaleMyCar();
        console.log('ionViewDidLoad MyCarsPage');
    };
    MyCarsPage.prototype.ionViewDidEnter = function () {
        this.ga.trackView('내등록차량'); //trackView for Google Analytics
    };
    MyCarsPage.prototype.reflashPage = function () {
        var _this = this;
        this.zone.run(function () {
            _this.getSaleMyCar();
            console.log('Force update ======> MyCarsPage');
        });
    };
    MyCarsPage.prototype.getSaleMyCar = function () {
        var _this = this;
        this.cars = [];
        this.queryParams = 'email=' + this.userInfoProvider.userProfile.u_email;
        var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_me.php";
        this.http.post(url, body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.result !== 'ok') {
                _this.showNothing = true;
            }
            else {
                _this.showNothing = false;
                //data.sale_list[0].state = "완료";
                for (var i = 0; i < Object.keys(data.sale_list).length; i++) {
                    data.sale_list[i].maker = _this.makerModelProvider.getMakerName(data.sale_list[i].maker);
                    data.sale_list[i].model = _this.makerModelProvider.getModelName(data.sale_list[i].model);
                    var strArray = data.sale_list[i].address.split(' ');
                    data.sale_list[i].address = strArray[2];
                }
                _this.cars = data.sale_list;
                _this.queryLength = Object.keys(data.sale_list).length;
                console.log('this.queryLength: ' + _this.queryLength);
            }
        });
    };
    MyCarsPage.prototype.naviCarDetailPage = function (selectedCar) {
        var _this = this;
        if (!this.carDetailTabFlag) {
            this.carDetailTabFlag = true;
            this.queryParams = 'sale=' + selectedCar;
            var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_detail.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    var alert_1 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: _this.msgService.getMessage(data.result),
                        buttons: ['확인']
                    });
                    alert_1.present();
                }
                else {
                    var CarDetailPage_modal = _this.modalCtrl.create('CarDetailPage', {
                        carParam: data.sale_detail
                    });
                    CarDetailPage_modal.onDidDismiss(function (data) {
                        _this.carDetailTabFlag = false;
                    });
                    CarDetailPage_modal.present();
                }
            });
        }
    };
    MyCarsPage.prototype.updateMyCar = function (selectedCar) {
        var _this = this;
        if (!this.updateMyCarTabFlag) {
            this.updateMyCarTabFlag = true;
            this.queryParams = 'sale=' + selectedCar;
            var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_detail.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    var alert_2 = _this.alertCtrl.create({
                        title: '알림',
                        subTitle: _this.msgService.getMessage(data.result),
                        buttons: ['확인']
                    });
                    alert_2.present();
                }
                else {
                    var UpdateMyCarPage_modal = _this.modalCtrl.create('UpdateMyCarPage', {
                        carParam: data.sale_detail
                    });
                    UpdateMyCarPage_modal.onDidDismiss(function (data) {
                        _this.updateMyCarTabFlag = false;
                        if (data == true) {
                            _this.reflashPage();
                        }
                    });
                    UpdateMyCarPage_modal.present();
                }
            });
        }
    };
    MyCarsPage.prototype.soldMyCar = function (carKey) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: '판매가 완료되었습니까?',
            message: '"확인"를 선택하시면 판매완료로 처리됩니다.',
            buttons: [
                {
                    text: '취소',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: '확인',
                    handler: function () {
                        _this.queryParams = 'sale=' + carKey +
                            '&email=' + _this.userInfoProvider.userProfile.u_email +
                            '&pw=' + _this.userInfoProvider.userProfile.u_pw +
                            '&state=' + '2'; // 1:판매중, 2:완료, 3:삭제, 4:신고, 5:검수
                        console.log('this.queryParams:' + _this.queryParams);
                        var body = _this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = _this.marketURI + "set_sale_state.php";
                        _this.http.post(url, body, options)
                            .map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            if (data.result != 'ok') {
                                var alert_3 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: _this.msgService.getMessage(data.result),
                                    buttons: ['확인']
                                });
                                alert_3.present();
                            }
                            else {
                                var alert_4 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: '정상 처리되었습니다.',
                                    buttons: ['확인']
                                });
                                alert_4.present();
                                _this.reflashPage();
                                //this.navCtrl.setRoot(this.navCtrl.getActive().component);
                            }
                        }); //====> (subscribe(data => {)
                    } //====> '동의' handler:
                }
            ] //====> buttons:
        });
        confirm.present();
    };
    MyCarsPage.prototype.deleteMyCar = function (carKey) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: '삭제하시겠습니까?',
            message: '"동의"를 선택하시면 차량이 삭제됩니다.',
            buttons: [
                {
                    text: '취소',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: '동의',
                    handler: function () {
                        _this.queryParams = 'sale=' + carKey +
                            '&email=' + _this.userInfoProvider.userProfile.u_email +
                            '&pw=' + _this.userInfoProvider.userProfile.u_pw;
                        var body = _this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = _this.marketURI + "set_sale_delete.php";
                        _this.http.post(url, body, options)
                            .map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            console.log('=======> deleteMyCar server response: ' + data.result);
                            if (data.result != 'ok') {
                                var alert_5 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: _this.msgService.getMessage(data.result),
                                    buttons: ['확인']
                                });
                                alert_5.present();
                            }
                            else {
                                var alert_6 = _this.alertCtrl.create({
                                    title: '알림',
                                    subTitle: '정상 처리되었습니다.',
                                    buttons: ['확인']
                                });
                                alert_6.present();
                                _this.reflashPage();
                                //this.navCtrl.setRoot(this.navCtrl.getActive().component);
                            }
                        });
                    } //====> '동의' handler:
                }
            ] //====> buttons:
        });
        confirm.present();
    };
    MyCarsPage.prototype.addMyCar = function () {
        var _this = this;
        this.ga.trackEvent('내등록차량', '내차팔기'); //trackEvent for Google Analytics
        var RegistAgreePage_modal = this.modalCtrl.create('RegistAgreePage');
        RegistAgreePage_modal.onDidDismiss(function (data) {
            if (data == true) {
                _this.reflashPage();
            }
        });
        RegistAgreePage_modal.present();
    };
    MyCarsPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        if (this.doInfiniteFlag == true) {
            var body = this.queryParams + '&key=' + this.queryLength, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.marketURI + "get_sale_me.php";
            this.http.post(url, body, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data.result != 'ok') {
                    _this.doInfiniteFlag = false;
                }
                else {
                    for (var i = 0; i < Object.keys(data.sale_list).length; i++) {
                        data.sale_list[i].maker = _this.makerModelProvider.getMakerName(data.sale_list[i].maker);
                        data.sale_list[i].model = _this.makerModelProvider.getModelName(data.sale_list[i].model);
                        var strArray = data.sale_list[i].address.split(' ');
                        data.sale_list[i].address = strArray[2];
                        _this.cars.push(data.sale_list[i]);
                    }
                    if (Object.keys(data.pick_detail_list).length < 20) {
                        _this.doInfiniteFlag = false;
                    }
                    _this.queryLength = _this.queryLength + Object.keys(data.sale_list).length;
                    console.log('this.queryLength: ' + _this.queryLength);
                }
            });
            infiniteScroll.complete();
        }
    };
    MyCarsPage.prototype.backToProfile = function () {
        this.navCtrl.pop();
        //this.viewCtrl.dismiss();
    };
    return MyCarsPage;
}());
MyCarsPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-my-cars',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/my-cars/my-cars.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-title>내 등록 차량</ion-title>\n    <ion-buttons left>\n      <button ion-button icon-only color="underblack" (click)="backToProfile()">\n        <ion-icon name="ios-arrow-back"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div *ngIf="showNothing" align="center">\n      <div class="spacer" style="height: 160px;"></div>\n      <div align="center"><img src="img/addMyCar.png" style="height: 15%; width: 15%"/></div>\n      <h5 align="center">딜러보다 더 받고 팔 준비 되셨나요?</h5>\n      <p align="center"><span style="font-size: 10pt; color:#999999">국내 최대 중고차 안심직거래앱 천언더<br>전담직원과 함께 내 차를 판매해보세요.</span></p> \n  </div>\n  <div *ngIf="!showNothing">\n      <div align="center">    \n        <div no-margin no-padding *ngFor="let car of cars">\n          <div *ngIf="car.photo_height > 500" class="image-wrapper">\n            <div *ngIf="car.state === \'판매중\'">\n              <div class="pcar-image"> \n                <div class="centered">\n                  <img [src]="car.photo_path" tappable (click)="naviCarDetailPage(car.key)"/>\n                </div>\n              </div>\n            </div>\n            <div *ngIf="car.state === \'검수\'">\n              <div class="pcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                                  \'background-repeat\': \'no-repeat\', \'background-position\': \'center center\'}"> \n                <div class="centered">\n                  <img src="img/state/inspecting.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n                </div>\n              </div>\n            </div>\n            <div *ngIf="car.state === \'완료\'">\n              <div class="pcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                                  \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n                <div class="centered">\n                  <img src="img/state/sold.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div *ngIf="car.photo_height <= 500" class="image-wrapper">\n              <div *ngIf="car.state === \'판매중\'">\n                <div class="lcar-image"> \n                  <div class="centered">\n                    <img [src]="car.photo_path" tappable (click)="naviCarDetailPage(car.key)"/>\n                  </div>\n                </div>\n              </div>\n              <div *ngIf="car.state === \'검수\'">\n                <div class="lcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                                    \'background-repeat\': \'no-repeat\', \'background-position\': \'center center\'}"> \n                  <div class="centered">\n                    <img src="img/state/inspecting.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n                  </div>\n                </div>\n              </div>\n              <div *ngIf="car.state === \'완료\'">\n                <div class="lcar-image"  [ngStyle]="{\'background-image\': \'url(\' + car.photo_path + \')\', \'opacity\':0.7, \n                                                    \'background-repeat\': \'no-repeat\',  \'background-position\': \'center center\'}"> \n                  <div class="centered">\n                    <img src="img/state/sold.png" style="height: 150px; width: 150px;" tappable (click)="naviCarDetailPage(car.key)"/>\n                  </div>\n                </div>\n              </div>\n            </div>\n          <ion-row style="height: 45px;">\n            <ion-col col-10>\n              <h5 align="left">&nbsp;{{car.price | number:0}}만원 {{car.maker}} {{car.model}}</h5>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col col-10 align="left">\n              &nbsp;{{car.address}} | {{car.year}}년{{car.month}}월 | {{car.mileage | number:0}}km  \n            </ion-col>\n            <ion-col col-2>\n              <ion-icon name="eye" item-left> {{car.view_count | number:0}}</ion-icon>\n            </ion-col>\n          </ion-row>\n          <div *ngIf="car.state === \'판매중\'">\n            <ion-row style="height: 45px;">\n              <ion-col col-4 text-center style="padding-top: 0px;">\n                <button ion-button clear color="undergray" (click)="updateMyCar(car.key)">\n                  수정하기\n                </button>\n              </ion-col>\n              <ion-col col-4 text-center style="padding-top: 0px;">\n                <button ion-button clear color="undergray" (click)="soldMyCar(car.key)">\n                  판매완료\n                </button>\n              </ion-col>\n              <ion-col col-4 text-center style="padding-top: 0px;">\n                <button ion-button clear color="danger" (click)="deleteMyCar(car.key)">\n                  삭제하기\n                </button>\n              </ion-col>\n            </ion-row>\n          </div>\n          <div *ngIf="car.state === \'검수\'">\n            <ion-row style="height: 45px;">\n              <ion-col col-4 text-center style="padding-top: 0px;">\n                <button ion-button clear color="default" (click)="updateMyCar(car.key)">\n                  수정하기\n                </button>\n              </ion-col>\n              <ion-col col-4 text-center style="padding-top: 0px;">\n                <!-- <button ion-button clear color="default" (click)="soldMyCar(car.key)">\n                  판매완료\n                </button> -->\n              </ion-col>\n              <ion-col col-4 text-center style="padding-top: 0px;">\n                <button ion-button clear color="danger" (click)="deleteMyCar(car.key)">\n                  삭제하기\n                </button>\n              </ion-col>\n            </ion-row>\n          </div>\n          <div *ngIf="car.state === \'완료\'">\n            <ion-row style="height: 45px;">\n              <ion-col col-4 text-center style="padding-top: 0px;">\n                <!-- <button ion-button clear color="default" icon-left (click)="updateMyCar(car.key)">\n                  수정하기\n                </button> -->\n              </ion-col>\n              <ion-col col-4 text-center style="padding-top: 0px;">\n                <button ion-button clear color="1000Under">\n                  판매완료\n                </button>\n              </ion-col>\n              <ion-col col-4 text-center style="padding-top: 0px;">\n                <button ion-button clear color="danger" (click)="deleteMyCar(car.key)">\n                  삭제하기\n                </button>\n              </ion-col>\n            </ion-row>\n          </div>\n        <div class="spacer" style="height: 10px; background-color: #eeeeee;"></div>\n      </div>      \n    </div>\n  </div>\n  <ion-fab right bottom>\n    <button ion-fab style="color: #ffffff; background-color: #48b2ff; width: 44px; height: 44px;" (click)="addMyCar()"><ion-icon name="md-add"></ion-icon></button>\n  </ion-fab>\n  <ion-infinite-scroll *ngIf="doInfiniteFlag" (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/my-cars/my-cars.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_analytics__["a" /* GoogleAnalytics */],
        __WEBPACK_IMPORTED_MODULE_6__providers_maker_model_maker_model__["a" /* MakerModelProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */]])
], MyCarsPage);

//# sourceMappingURL=my-cars.js.map

/***/ })

});
//# sourceMappingURL=23.main.js.map