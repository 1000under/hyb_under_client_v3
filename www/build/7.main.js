webpackJsonp([7],{

/***/ 339:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__update_profile__ = __webpack_require__(410);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProfilePageModule", function() { return UpdateProfilePageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var UpdateProfilePageModule = (function () {
    function UpdateProfilePageModule() {
    }
    return UpdateProfilePageModule;
}());
UpdateProfilePageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__update_profile__["a" /* UpdateProfilePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__update_profile__["a" /* UpdateProfilePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__update_profile__["a" /* UpdateProfilePage */]
        ]
    })
], UpdateProfilePageModule);

//# sourceMappingURL=update-profile.module.js.map

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_image_picker__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_transfer__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_path__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_analytics__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_device_device__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_user_info_user_info__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_response_msg_service_response_msg_service__ = __webpack_require__(110);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateProfilePage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








//-----> Providers



var UpdateProfilePage = (function () {
    function UpdateProfilePage(navCtrl, navParams, viewCtrl, alertCtrl, modalCtrl, loadingCtrl, http, platform, deviceProvider, userInfoProvider, msgService, imagePicker, transfer, file, filePath, ga) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.platform = platform;
        this.deviceProvider = deviceProvider;
        this.userInfoProvider = userInfoProvider;
        this.msgService = msgService;
        this.imagePicker = imagePicker;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.ga = ga;
        this.memberURI = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";
        //-------------------------------------------------> loginParams for userInfoProvider
        this.loginParams = [
            { u_name: '' },
            { u_phone_num: '' },
            { u_email: '' },
            { u_pw: '' },
            { u_photo: '' },
            { u_type: '' },
            { cu_flag: false },
            { fb_flag: false },
            { kk_flag: false },
            { gg_flag: false },
            { login_flag: false },
            { isMember_flag: false },
        ];
        this.delFlag = false;
        this.userImgFlag = false;
        this.userImgReplaceFlag = false;
        this.userImgFileName = 'noImg';
        this.name = '';
        this.phoneNumber = '';
        this.email = '';
        this.password = '';
        this.photo_user = []; //--> Blob file for 사용자
        this.photo_delete = ''; //(YES => 1, NO => 2 입니다)
    }
    UpdateProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UpdateProfilePage');
    };
    UpdateProfilePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.ga.trackView('프로필수정'); //trackView for Google Analytics
        this.login_flag = this.userInfoProvider.userProfile.login_flag;
        this.isMember_flag = this.userInfoProvider.userProfile.isMember_flag;
        this.kk_flag = this.userInfoProvider.userProfile.kk_flag;
        if (this.userInfoProvider.userProfile.u_type == '1') {
            this.snsUser = false;
        }
        else {
            this.snsUser = true;
        }
        this.queryParams = 'email=' + this.userInfoProvider.userProfile.u_email +
            '&pw=' + this.userInfoProvider.userProfile.u_pw;
        var body = this.queryParams, type = 'application/x-www-form-urlencoded; charset=UTF-8', headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Headers */]({ 'Content-Type': type }), options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers }), url = this.memberURI + "get_member.php";
        this.http.post(url, body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.result == 'ok') {
                _this.name = data.member.name;
                _this.email = data.member.email;
                _this.phoneNumber = data.member.phone_num;
                _this.password = _this.userInfoProvider.userProfile.u_pw;
                if (data.member.photo != "" && data.member.photo != null && (data.member.photo.indexOf('NaN') == -1)) {
                    _this.getUserImgUrl = data.member.photo;
                    _this.delFlag = false;
                    _this.isPhotoDelete = false;
                    _this.userImgFlag = true;
                    _this.photo_delete = '1'; //'NO'
                    console.log('data.member.photo: ' + data.member.photo);
                }
                else {
                    _this.isPhotoDelete = true;
                    _this.userImgFlag = false;
                    _this.photo_delete = '2'; //'YES'
                }
                ;
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: '알림',
                    subTitle: _this.msgService.getMessage(data.result),
                    buttons: ['확인']
                });
                alert_1.present();
            }
        });
        console.log('ionViewDidEnter UpdateProfilePage');
    };
    UpdateProfilePage.prototype.changePassword = function () {
        var updatePasswordPage_modal = this.modalCtrl.create('UpdatePasswordPage', { userProfile_pw: this.userInfoProvider.userProfile.u_pw });
        updatePasswordPage_modal.present();
    };
    UpdateProfilePage.prototype.checkPhoto = function () {
        console.log('this.isPhotoDelete: ' + this.isPhotoDelete);
        if (this.userImgFlag == false && this.isPhotoDelete == false) {
            this.isPhotoDelete = true;
            this.photo_delete = '2'; //'YES'
            this.userImgFlag = false;
        }
        else {
            if (this.isPhotoDelete == true) {
                this.photo_delete = '2'; //'YES'
                this.userImgFlag = false;
            }
            else {
                this.photo_delete = '1'; //'NO'
                this.userImgFlag = true;
            }
        }
        console.log('this.photo_delete: ' + this.photo_delete);
    };
    UpdateProfilePage.prototype.updateUserImg = function () {
        var _this = this;
        var options = {
            maximumImagesCount: 1, width: 800, height: 800, quality: 100
        };
        this.imagePicker.getPictures(options).then(function (results) {
            if (results[0] != undefined) {
                _this.getUserImgUrl = results[0];
                console.log('this.getUserImgUrl: ' + _this.getUserImgUrl);
                _this.userImgReplaceFlag = true;
                _this.userImgFlag = true;
                _this.isPhotoDelete = false;
                if (_this.platform.is('android')) {
                    _this.filePath.resolveNativePath(_this.getUserImgUrl)
                        .then(function (filePath) {
                        //Create a new name for the Image
                        var d = new Date();
                        var n = d.getTime();
                        _this.userImgFileName = _this.userInfoProvider.userProfile.u_email + "_user_" + n + ".jpg"; //한글 이름 않됨 !!!!
                        _this.CreateUserImage(filePath, 0);
                    });
                }
                else {
                    //Create a new name for the Image
                    var d = new Date();
                    var n = d.getTime();
                    _this.userImgFileName = _this.userInfoProvider.userProfile.u_email + "_user_" + n + ".jpg"; //한글 이름 않됨 !!!!
                    _this.CreateUserImage(_this.getUserImgUrl, 0);
                }
            }
        }, function (err) {
            _this.userImgReplaceFlag = false;
            _this.userImgFlag = false;
            _this.isPhotoDelete = true;
            console.log('imagePicker.getPictures ERROR: ' + err);
        });
    };
    //Main Image파일 생성
    UpdateProfilePage.prototype.CreateUserImage = function (fileName, index) {
        var _this = this;
        this.file.resolveLocalFilesystemUrl(fileName).then(function (fileEntry) {
            fileEntry.file(function (resFile) {
                var reader = new FileReader();
                reader.onloadend = function (evt) {
                    var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                    imgBlob.name = fileName;
                    _this.photo_user[index] = imgBlob;
                    console.log('Create UserImage: ' + fileName);
                };
                reader.onerror = function (e) {
                    console.log("Failed file read: " + e.toString());
                };
                reader.readAsArrayBuffer(resFile);
            });
        }, function (err) {
            console.log(err);
            alert(JSON.stringify(err));
        });
    };
    UpdateProfilePage.prototype.submitUpdate = function () {
        console.log('<------------------ Input Items ---------------->');
        console.log('email: ' + this.userInfoProvider.userProfile.u_email);
        console.log('pw: ' + this.userInfoProvider.userProfile.u_pw);
        if (this.userImgReplaceFlag == true) {
            console.log('photo: ' + this.userImgFileName);
        }
        console.log('photo_delete: ' + this.photo_delete);
        console.log('name: ' + this.name);
        //console.log('push_enable: ' +this.push_enable);
        if (this.name === '') {
            var alert_2 = this.alertCtrl.create({
                title: '알림!',
                subTitle: '이름을 입력해 주십시요.',
                buttons: ['확인']
            });
            alert_2.present();
        }
        else {
            var loading_1 = this.loadingCtrl.create({
                spinner: 'bubbles',
                content: '정보 수정중 입니다...'
            });
            loading_1.present();
            var body = new FormData();
            var xhr_1 = new XMLHttpRequest();
            var env_1 = this;
            ///////////////////////////////////////////////////////////////////////
            body.append('email', this.userInfoProvider.userProfile.u_email);
            body.append('pw', this.userInfoProvider.userProfile.u_pw);
            if (this.userImgReplaceFlag == true) {
                body.append('photo', this.photo_user[0], this.userImgFileName);
                this.photo_delete = '1'; //'NO'
            }
            body.append('photo_delete', this.photo_delete);
            body.append('name', this.name);
            //body.append('push_enable', this.push_enable);
            ///////////////////////////////////////////////////////////////////////
            var url_s = this.memberURI + "set_member_modify.php";
            xhr_1.open("POST", url_s, true);
            xhr_1.send(body);
            xhr_1.onreadystatechange = function () {
                if (xhr_1.readyState == 4) {
                    if (xhr_1.status == 200) {
                        loading_1.dismiss();
                        //alert(xhr.responseText); //테스트를 위해 alert으로 바꿨습니다.
                        console.log('xhr.responseText' + xhr_1.responseText);
                        var response = JSON.parse(xhr_1.responseText); //xhr.responseText{"result":"error_no_parameter"}
                        if (response.result == 'ok') {
                            //env.loginParams.loginApp = '1000Under';
                            env_1.loginParams.u_name = env_1.name;
                            env_1.loginParams.u_phone_num = env_1.phoneNumber;
                            env_1.loginParams.u_email = env_1.email;
                            env_1.loginParams.u_pw = env_1.password;
                            if (env_1.photo_delete == '2') {
                                env_1.loginParams.u_photo = 'NaN';
                            }
                            env_1.loginParams.u_type = env_1.userInfoProvider.userProfile.u_type; //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                            if (env_1.userInfoProvider.userProfile.u_type == '1') {
                                env_1.loginParams.cu_flag = true;
                            }
                            else if (env_1.userInfoProvider.userProfile.u_type == '2') {
                                env_1.loginParams.fb_flag = true;
                            }
                            else if (env_1.userInfoProvider.userProfile.u_type == '3') {
                                env_1.loginParams.kk_flag = true;
                            }
                            else if (env_1.userInfoProvider.userProfile.u_type == '4') {
                                env_1.loginParams.gg_flag = true;
                            }
                            env_1.loginParams.login_flag = true;
                            env_1.loginParams.isMember_flag = true;
                            env_1.userInfoProvider.setProfile(env_1.loginParams);
                            var alert_3 = env_1.alertCtrl.create({
                                title: '알림',
                                subTitle: '프로필 수정이 완료되었습니다.',
                                buttons: ['확인']
                            });
                            alert_3.present();
                            env_1.viewCtrl.dismiss(env_1.loginParams.login_flag);
                        }
                        else {
                            var alert_4 = env_1.alertCtrl.create({
                                title: '알림',
                                subTitle: env_1.msgService.getMessage(response.result),
                                buttons: ['확인']
                            });
                            alert_4.present();
                        }
                    }
                    else {
                        console.log('xhr.status != 200 : ' + xhr_1.status);
                    }
                } // end of if (xhr.readyState == 4)
            }; //--> end of xhr.onreadystatechange = function ()   
        }
    };
    UpdateProfilePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.login_flag);
    };
    return UpdateProfilePage;
}());
UpdateProfilePage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-update-profile',template:/*ion-inline-start:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/update-profile/update-profile.html"*/'<ion-header>\n  <div class="head-space"></div>\n  <ion-toolbar color="underwhite">\n    <ion-buttons left>\n      <button ion-button icon-only color="underblack" (click)="dismiss()">\n        <ion-icon name="md-close"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>프로필 수정</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class="spacer" style="height: 30px;"></div>\n  <div *ngIf="userImgFlag" class="item image-border" align="center" tappable (click)="updateUserImg()">\n    <img class="img-camera" src="img/camera.png"/>\n    <img *ngIf="!delFlag" class="img-circular" [src]="getUserImgUrl"/>\n    <img *ngIf="delFlag" class="img-circular" [src]="getUserImgUrl" style="opacity:0.2;"/>\n  </div>\n  <div *ngIf="!userImgFlag" class="item image-border" align="center" tappable (click)="updateUserImg()">\n    <img class="img-camera" src="img/camera.png"/>\n    <img class="img-circular" src="img/noUserImg.png"/>\n  </div>\n  <div class="spacer" style="height: 30px;"></div>\n  <div padding style="padding-left: 10px; padding-right: 10px;">\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-input [(ngModel)]="name" type="text"></ion-input>\n      </ion-item>\n    </div>\n    <div *ngIf="!kk_flag">\n      <div class="spacer" style="height: 5px;"></div>\n      <div class="outline-box">\n        <ion-item no-lines class="outline-item">\n          <ion-input [(ngModel)]="email" style="color:#929292;" readonly></ion-input>\n        </ion-item>\n      </div>\n    </div>\n    <div *ngIf="!snsUser">\n      <div class="spacer" style="height: 5px;"></div>\n      <div class="outline-box">\n        <button ion-item no-lines detail-none class="outline-item" block color="underwhite" (click)="changePassword()">비밀번호 변경\n          <ion-icon name="ios-arrow-forward" item-end color="undergray"></ion-icon>\n        </button>\n      </div>\n    </div>\n    <div class="spacer" style="height: 5px;"></div>\n    <div class="outline-box">\n      <ion-item no-lines class="outline-item">\n        <ion-label>사진 삭제</ion-label>\n        <ion-toggle [(ngModel)]="isPhotoDelete" color="1000Under" (ionChange)="checkPhoto()"></ion-toggle>\n      </ion-item>\n    </div>\n    <div class="spacer" style="height: 10px;"></div>\n    <button ion-button block color="1000Under" (click)="submitUpdate()">수정하기</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/gimsuyeong/AppDev/hyb_under_client_v3/src/pages/update-profile/update-profile.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_image_picker__["a" /* ImagePicker */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_8__providers_device_device__["a" /* DeviceProvider */],
        __WEBPACK_IMPORTED_MODULE_9__providers_user_info_user_info__["a" /* UserInfoProvider */],
        __WEBPACK_IMPORTED_MODULE_10__providers_response_msg_service_response_msg_service__["a" /* ResponseMsgServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_image_picker__["a" /* ImagePicker */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_transfer__["a" /* Transfer */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_path__["a" /* FilePath */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_analytics__["a" /* GoogleAnalytics */]])
], UpdateProfilePage);

//# sourceMappingURL=update-profile.js.map

/***/ })

});
//# sourceMappingURL=7.main.js.map