import { Injectable } from '@angular/core';
import { Platform, AlertController, ModalController, ToastController, } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import { FCM } from '@ionic-native/fcm';

//-----> Providers
import { DeviceProvider } from '../../providers/device/device';
import { ChatsCountProvider } from '../../providers/chats-count/chats-count';
import 'rxjs/add/operator/map';

@Injectable()
export class UserInfoProvider {
   
  private memberURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";
  queryParams: any;

  //-------------------------------------------------> for Is 1000Under user?
  userProfile = { 
    u_name:      '',
    u_phone_num: '',
    u_email:     '',
    u_pw:        '',
    u_photo:     '',
    u_type:      '',      //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
    cu_flag:      false,
    fb_flag:      false,
    kk_flag:      false,
    gg_flag:      false,
    login_flag:   false,
    isMember_flag: false,
  };
  
  selected_version: any;
  fcmToken: any;
  
  constructor(
    public http: Http, 
    public storage: Storage, 
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private toastCtrl: ToastController,
    public fcm: FCM,
    public platform: Platform,
    public chatsCountProvider: ChatsCountProvider,
    public deviceProvider: DeviceProvider,) {
      this.platform.ready().then(() => {
        this.initLogin();
      });
      console.log('Hello UserInfoProvider Provider');
  }

  initLogin() {
    this.getFcmToken();
    this.deviceProvider.getDeviceInfo(); 
    this.storage.get('userProfile').then(data => {
      if(data == null) {  // Guest login
        this.setGuestLogin();
      } else {
        console.log('----------> userProfile login_flag: '+data.login_flag);
        console.log('----------> userProfile u_email: '+data.u_email);

        this.userProfile.u_name =  data.u_name;
        this.userProfile.u_phone_num =  data.u_phone_num;
        this.userProfile.u_email =  data.u_email;
        this.userProfile.u_pw =  data.u_pw;
        this.userProfile.u_photo =  data.u_photo;
        this.userProfile.u_type =  data.u_type;
        this.userProfile.cu_flag =  data.cu_flag;
        this.userProfile.fb_flag =  data.fb_flag;
        this.userProfile.kk_flag =  data.kk_flag;
        this.userProfile.gg_flag =  data.gg_flag;
        this.userProfile.login_flag =  data.login_flag;
        this.userProfile.isMember_flag = data.isMember_flag;

        this.getMember(this.userProfile.u_email, this.userProfile.u_pw);  // for compare app_version between device_info and getMember.php
                 
      } // Member login
    }, () => {
      console.log('userProfile this.storage.get else');
    });
  }

  getMember(u_email, u_pw) {
    this.queryParams = 'email=' +u_email+
                       '&pw='  +u_pw; 
    let body_1   : string	 = this.queryParams,
        type_1 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers_1: any		 = new Headers({ 'Content-Type': type_1}),
        options_1: any 		 = new RequestOptions({ headers: headers_1 }),
        url_1 	 : any		 = this.memberURI + 'get_member.php';
    this.http.post(url_1, body_1, options_1)
    .map(res => res.json())
    .subscribe(data => { 
        if (data.result != 'ok') {
          //this.resetProfile();
          console.log('========> Failed!! get_member.php: ' +data.result);
        } else {
          this.userProfile.u_name =  data.member.name;
          this.userProfile.u_phone_num =  data.member.phone_num;
          this.userProfile.u_email =  data.member.email;
          this.userProfile.u_photo =  data.member.photo;
          this.selected_version =  data.member.version;
          console.log('----------> Member current app version: '+data.member.version+' - '+data.member.version_link);
          
          //data.member.version = '1.5'; //version check test 
          
          if((this.deviceProvider.deviceInfo.d_Type == '1' && data.member.device == 'A' && this.deviceProvider.deviceInfo.s_version != data.member.version) ||
            (this.deviceProvider.deviceInfo.d_Type == '2' && data.member.device == 'I' && this.deviceProvider.deviceInfo.s_version != data.member.version)) {
              let confirm = this.alertCtrl.create({
              title: '앱 업그레이드',
              message: '새로운 버전의 앱으로 업그레이드 합니다.',
              buttons: [
                {
                  text: '취소',
                  handler: () => {
                    //console.log('Disagree clicked');
                    this.selected_version = data.member.version;
                  }
                },  // cancel upgrade app
                {
                  text: '동의',
                  handler: () => {
                    //console.log('Agree clicked');
                    this.selected_version = this.deviceProvider.deviceInfo.s_version;
                    window.location.href = this.deviceProvider.deviceInfo.s_link;
                  }
                }  // agree upgrade app 
              ]
            });
            confirm.present();
          }

          console.log('----------> DeviceProvider deviceInfo.s_version: '+this.deviceProvider.deviceInfo.s_version);
          console.log('----------> DeviceProvider deviceInfo.d_pushKey: '+this.deviceProvider.deviceInfo.d_pushKey);
          console.log('----------> User Selected 1000under App version: '+this.selected_version);

          this.queryParams =  'email='          +this.userProfile.u_email+
                              '&pw='            +this.userProfile.u_pw+
                              '&type='          +this.userProfile.u_type+                   //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                              '&push_key='      +this.deviceProvider.deviceInfo.d_pushKey+
                              '&photo='         +this.userProfile.u_photo+ 
                              '&push_enable='   +'YES'+                                       //push enable 1: YES, 2: NO
                              '&device='        +this.deviceProvider.deviceInfo.d_Type+
                              '&device_num='    +this.deviceProvider.deviceInfo.d_UUID+
                              '&version='       +this.selected_version;
          console.log('----------> Member login Param: '+this.queryParams);
          this.setMemberLogin(this.queryParams);

          this.storage.set('userProfile', this.userProfile).then(()=>{
            console.log('----------> Save userProfile from  result of get_member.php');
          },(e)=>{
            console.log('==========> Unable userProfile to save',e);
          })  //====> store userProfile
        } // 회원 정보 (data.result == 'ok')
    });
  }
  
	getFcmToken() {
    this.fcm.getToken().then(token=>{
      if(token !== null) {
        console.log('----------> this.fcm.getToken: ' + token);
        this.fcmToken = token;
        this.deviceProvider.setPushKey(token);
      }
      //alert('getToken: '+token);
    });
    this.fcm.onTokenRefresh().subscribe(token=>{
      if(token !== null) {
        console.log('----------> this.fcm.onTokenRefresh: ' + token);
        this.fcmToken = token;
        this.deviceProvider.setPushKey(token);
      }
      //alert('onTokenRefresh: '+token);
    });

  }

  setGuestLogin() {
    this.queryParams = 'push_key='       +this.deviceProvider.deviceInfo.d_pushKey+
                        '&push_enable='  +'YES'+     //push enable YES, NO
                        '&device='       +this.deviceProvider.deviceInfo.d_Type+
                        '&device_num='   +this.deviceProvider.deviceInfo.d_UUID+
                        '&version='      +this.deviceProvider.deviceInfo.s_version;   //App version
    console.log('----------> Guest login Param: '+this.queryParams);
    
    let body   : string	 = this.queryParams,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.memberURI + 'set_guest.php';
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
        if (data.result != 'ok') {
          console.log('==========> Guest user login Failed!! :' +data.result);
        } else {
          this.deviceProvider.setPushKey(this.fcmToken);
          console.log('----------> Guest user login Success!!');
        }
    });
    this.userProfile.login_flag =  false;
    this.userProfile.isMember_flag = false;
  }

  setMemberLogin(queryString) {
    let body   : string	 = queryString,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.memberURI + 'set_member_login.php';
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
        if (data.result != 'ok') {
          console.log('==========> Member login Failed!! : ' +data.result);
        } else {
          this.chatsCountProvider.checkChatsCount(this.userProfile.login_flag, this.userProfile.u_email, this.userProfile.u_pw);
          console.log('----------> Member login Success!!');            
        } // 회원 로그인 (data.result == 'ok')
    });
  }

  getProfile() {
    //this.storage.get('userProfile').then((data) => {
    this.storage.get('userProfile').then(data => {

        this.userProfile.u_name =  data.u_name;
        this.userProfile.u_phone_num =  data.u_phone_num;
        this.userProfile.u_email =  data.u_email;
        this.userProfile.u_pw =  data.u_pw;
        this.userProfile.u_photo =  data.u_photo;
        this.userProfile.u_type =  data.u_type;
        this.userProfile.cu_flag =  data.cu_flag;
        this.userProfile.fb_flag =  data.fb_flag;
        this.userProfile.kk_flag =  data.kk_flag;
        this.userProfile.gg_flag =  data.gg_flag;
        this.userProfile.login_flag =  data.login_flag;
        this.userProfile.isMember_flag =  data.isMember_flag;

        console.log('----------> Got userProfile from userProfile');
    });
  }

  //store the profile
  setProfile(setParams) {

    this.userProfile.u_name =  setParams.u_name;
    this.userProfile.u_phone_num =  setParams.u_phone_num;
    this.userProfile.u_email =  setParams.u_email;
    this.userProfile.u_pw =  setParams.u_pw;
    this.userProfile.u_photo =  setParams.u_photo;
    this.userProfile.u_type =  setParams.u_type;
    this.userProfile.cu_flag =  setParams.cu_flag;
    this.userProfile.fb_flag =  setParams.fb_flag;
    this.userProfile.kk_flag =  setParams.kk_flag;
    this.userProfile.gg_flag =  setParams.gg_flag;
    this.userProfile.login_flag = setParams.login_flag;
    this.userProfile.isMember_flag = setParams.isMember_flag;

    this.storage.set('userProfile', this.userProfile).then(()=>{
      console.log('----------> Save userProfile to Storage');
    },(e)=>{
      console.log('==========> Unable userProfile to save'+e);
    })  //====> store userProfile
    
  }    

  //this case is user logout, reset userProfile flag to false 
  resetProfile(setParams){
    this.userProfile.u_name =  setParams.u_name;
    this.userProfile.u_phone_num =  setParams.u_phone_num;
    this.userProfile.u_email =  setParams.u_email;
    this.userProfile.u_pw =  setParams.u_pw;
    this.userProfile.u_photo =  setParams.u_photo;
    this.userProfile.u_type =  setParams.u_type;
    this.userProfile.cu_flag =  setParams.cu_flag;
    this.userProfile.fb_flag =  setParams.fb_flag;
    this.userProfile.kk_flag =  setParams.kk_flag;
    this.userProfile.gg_flag =  setParams.gg_flag;
    this.userProfile.login_flag =  setParams.login_flag;
    this.userProfile.isMember_flag =  setParams.isMember_flag;
    this.storage.set('userProfile', this.userProfile).then(()=>{
      console.log('----------> Save userProfile to Storage for Logout ');
    },(e)=>{
      console.log('==========> Unable userProfile to save for Logout'+e);
    })  //====> store userProfile
  }

  //clear the whole local storage
  clearProfile() {
    this.storage.remove('userProfile').then(() => {
      console.log('----------> Remove userProfile on Storage');
        this.userProfile = {
          u_name:       '',
          u_phone_num:  '',
          u_email:      '',
          u_pw:         '',
          u_photo:      '',
          u_type:       '',
          cu_flag:      false,
          fb_flag:      false,
          kk_flag:      false,
          gg_flag:      false,
          login_flag:   false,
          isMember_flag: false,
        }
    });
  }
  
}
