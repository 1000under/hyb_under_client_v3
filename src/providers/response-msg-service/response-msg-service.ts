import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the ResponseMsgServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ResponseMsgServiceProvider {

  message = [
      {code: 'error_no_parameter',                      msg_kr: '입력 정보가 충분하지 않습니다'},
      {code: 'error_no_result',                         msg_kr: '검색결과가 없습니다'},
      {code: 'error_no_member',                         msg_kr: '회원정보가 없습니다. 신규 회원가입이 필요합니다.'},
      {code: 'error_db_result',                         msg_kr: 'DB정보가 없습니다'},
      {code: 'error_billing_result',                    msg_kr: '과금정보 생성 실패했습니다'},
      {code: 'error_over_regist_count',                 msg_kr: '차량등록은 개인당 2대입니다'},
      {code: 'error_over_regist_phone_number',          msg_kr: '차량등록은 전화번호당 2대입니다'},
      {code: 'error_alreahy_regist_car_number',         msg_kr: '이미 동일한 자동차등록번호가 존재합니다'},
      {code: 'error_no_car_image',                      msg_kr: '차량이미지가 없습니다'},
      {code: 'error_no_match_car_and_member',           msg_kr: '등록된 차량과 회원정보가 일치하지 않습니다'},
      {code: 'error_no_change_chacking_step',           msg_kr: '검수중인 차량의 상태정보는 변경할수 없습니다'},
      {code: 'error_no_modify_sale_car_number',         msg_kr: '판매중인 차량은 차량번호를 수정할수 없습니다'},
      {code: 'error_no_exist_car',                      msg_kr: '존재하지않는 차량입니다'},
      {code: 'error_already_sale_report',               msg_kr: '이미 신고한 차량입니다'},
      {code: 'error_not_match_pw',                      msg_kr: '암호가 일치하지 않습니다'},
      {code: 'error_not_enough_count',                  msg_kr: '입력하는 개수가 부족합니다'},
      {code: 'error_already_regist_member',             msg_kr: '이미 가입된 계정입니다'},
      {code: 'error_already_regist_sns',                msg_kr: '이미 SNS 가입된 계정입니다'},
      {code: 'error_already_regist_under',              msg_kr: '자체회원으로 가입된 이메일입니다'},
      {code: 'error_black_member',                      msg_kr: '블랙회원입니다 관리자에게 문의하세요'},
      {code: 'error_already_leave_member',              msg_kr: '이미 탈퇴한 회원입니다'},
      {code: 'error_no_change_sale_step',               msg_kr: '판매상태를 변경할수 없습니다'},         //2017-06-09
      {code: 'error_no_change_only_sale_complete',      msg_kr: '판매완료만 변경 가능합니다'},          //2017-06-09
      {code: 'error_no_exist_image',                    msg_kr: '이미지가 존재하지않습니다'},           //2017-09-05
      {code: 'error_no_target',                         msg_kr: '상대방 회원정보가 존재하지 않습니다'},   //2017-09-05
      {code: 'error_no_match_auth_code',                msg_kr: '인증코드가 옳바르지 않습니다'},        //2017-09-19
  ]

  constructor() {
    console.log('Hello MessageServiceProvider Provider');
  }

  getMessage(code){
    console.log('request msg_kr: '+code);
    for (let i = 0;  i< this.message.length; i++) {
      if(this.message[i].code == code) {
        return this.message[i].msg_kr;
      }
    }
  }

}

