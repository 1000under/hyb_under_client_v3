import { Injectable } from '@angular/core';
import { SQLite } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';

/*
  Generated class for the PhotoServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PhotoServiceProvider {

  // SQLite
  db: any = null;
  
  constructor() {
    console.log('Hello PhotoServiceProvider Provider');

    this.db = new SQLite();
    this.db.openDatabase({ name: 'cache.db', location: 'default' }).then(() => {
      this.db.executeSql('CREATE TABLE IF NOT EXISTS photos (id INTEGER PRIMARY KEY AUTOINCREMENT, url TEXT NOT NULL UNIQUE, imageBase64String TEXT)', {}).then(() => {}, (err) => { 
        console.error('Unable to execute sql: ', err);
      });
    }, (err) => {
      console.error('Unable to open database: ', err);
    });
  }

  // Find a photo in our DB
  public findPhotoByURL(url: string) {
    let sql = 'SELECT imageBase64String FROM photos WHERE url = \"' + url + '\" limit 1';
    return this.db.executeSql(sql);
  }

  // Save a new photo to the DB
  public savePhoto(url: string, imageBase64String: string) {
    this.db.openDatabase({ name: 'cache.db', location: 'default' }).then(() => {
      let sql = 'INSERT INTO photos (url, imageBase64String) VALUES (?,?)';
      this.db.executeSql(sql, [url, imageBase64String]);
      console.log('Insert database');
    }, (err) => {
        console.error('not Insert database: ', err);
    });
    //
    //return this.db.executeSql(sql, [url, imageBase64String]);
  }
}
