import { Injectable } from '@angular/core';
import { ToastController, Platform } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import 'rxjs/add/operator/map';

/*
  Generated class for the CameraProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

declare var cordova: any;

@Injectable()
export class CameraProvider {
  
    //서버로 보내줄 데이터 목록
    public SendImgaeList: Array<string>;

    //뷰에서 보여줄 목록
    public base64ImageList: Array<string>;

    //public imagePicker: ImagePicker,
    constructor(public toastCtrl: ToastController, 
                public platform: Platform,
                public camera: Camera,
                public file: File,
                public filePath: FilePath) {
        console.log('Hello CameraProvider Provider');

        this.base64ImageList = [];
        this.SendImgaeList = [];
    }

    //카메라로 사진 찍어서 이미지 데이터 가져오기
    takeNewImage(): Promise<any> {
        var option = {
            sourceType:1,
            quality: 20,
            destinationType: 0,
            encodingType: 0
        };

        this.SendImgaeList = [];
        
        return this.camera.getPicture(option).then((imageData)=>{

            this.cameraSuccessCallback(imageData);
            var currentName = imageData.substring(imageData.lastIndexOf('/') + 1);
            var correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            
            return "data:image/jpeg;base64," + imageData;

        },(error)=>{
            this.cameraErrorCallback(error);

            return null;
        });
    }

    //앨범에서 이미지 데이터 가져오기
    chooseImage(): Promise<any> {
        var option = {
            sourceType:0,
            quality: 20,
            destinationType: 0,
            encodingType: 0
        };

        this.SendImgaeList = [];

        return this.camera.getPicture(option).then((imageData)=>{

            this.cameraSuccessCallback(imageData); 
            this.filePath.resolveNativePath(imageData).then(filePath => {
                let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                let currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            });

            return "data:image/jpeg;base64," + imageData;
        },(error)=>{
            this.cameraErrorCallback(error);

            return null;
        });
    }
  
    cameraSuccessCallback(imageData) {
        console.log("Getting image data as base64 string from camera/Gallery success.");
        this.presentToast(imageData);
    }

    cameraErrorCallback(message) {
        alert('Loading image failed due to: ' + message);
    }

    // //여러개의 이미지 파일 선택
    // openGallery (): Promise<any> {
    //     let options = {
    //         maximumImagesCount: 8
    //     }

    //     this.SendImgaeList = [];
    //     this.base64ImageList = [];

    //     return this.imagePicker.getPictures(options).then((results) => {
    //         for (var i = 0; i < results.length; i++) {
    //             this.base64ImageList.push("data:image/jpeg;base64," + results[i]);
    //             //this.presentToast('Image URI: ' + results[i]);

    //             FilePath.resolveNativePath(results).then(filePath => {
    //                 let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
    //                 let currentName = results.substring(results.lastIndexOf('/') + 1, results.lastIndexOf('?'));
    //                 this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    //             });
    //         }

    //         return this.base64ImageList;

    //     }, (err) => { });
    // }

    public createFileName() {
        var d = new Date(),
        n = d.getTime(),
        newFileName =  n + ".jpg";
        return newFileName;
    }

    public copyFileToLocalDir(namePath, currentName, newFileName) {
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
            this.SendImgaeList.push(this.pathForImage(newFileName));
        }, error => {
            this.presentToast('Error while storing file.');
        });
    }

    private presentToast(text) {
            let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    pathForImage(img) {
        if (img === null) {
            return '';
        } else {
            return cordova.file.dataDirectory + img;
        }
    }
}
