import { Injectable } from '@angular/core';
import { Device } from '@ionic-native/device';
import { Sim } from '@ionic-native/sim';
import { Platform, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

//-----> Providers
import { MakerModelProvider } from '../../providers/maker-model/maker-model';

@Injectable()
export class DeviceProvider {

  private builderURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/builder/url/";
  queryParams: any;
  
  deviceInfo: any = {
    d_UUID:     '',
    d_Model:    '',
    d_Platform: '',
    d_Version:  '',
    d_Type:     '',
    d_pushKey:  '',
    s_version:  '',
    s_link:     '',
  };

  d_Type: any = '';
  s_version: any = '';
  s_link: any = '';

  //-------------------------------------------------> for SIM
  carrierName: any;                     // i,a
  countryCode: any;                     // i,a
  mcc: any;                             // i,a
  mnc: any;                             // i,a
  callState: any;                       // a
  dataActivity: any;                    // a
  networkType: any;                     // a
  phoneType: any;                       // a
  simState: any;                        // a
  isNetworkRoaming: any;                // a
  phoneCount: any;                      // a
  activeSubscriptionInfoCount: any;     // a
  activeSubscriptionInfoCountMax: any;  // a
  phoneNumber: any;                     // a
  deviceId: any;                        // a
  deviceSoftwareVersion: any;           // a
  simSerialNumber: any;                 // a
  subscriberId: any;                    // a
  cards: any = [];                      // a

  constructor(
    public platform: Platform, 
    public device: Device,
    public sim: Sim,
    public http: Http,  
    public storage: Storage,
    public alertCtrl: AlertController,
    public makerModelProvider : MakerModelProvider,) {
      this.platform.ready().then(() => {
        this.setDeviceInfo();
      });
      console.log('Hello DeviceProvider Provider');
  }
  /*
  getSimInfo() {
    this.sim.hasReadPermission().then(
      (info) => {
        console.log('Has permission:', info);
      }
    );

    this.sim.requestReadPermission().then(
      () => console.log('Permission granted'),
      () => console.log('Permission denied')
    );

    this.sim.getSimInfo().then(
      (info) => {
        console.log('Sim info: '+info);
        console.log('this.carrierName: ' +info.carrierName);
        console.log('this.countryCode: ' +info.countryCode);
        console.log('this.mcc: ' +info.mcc);
        console.log('this.mnc: ' +info.mnc);
        console.log('this.callState: ' +info.callState);
        console.log('this.dataActivity: ' +info.dataActivity);
        console.log('this.networkType: ' +info.networkType);
        console.log('this.phoneType: ' +info.phoneType);
        console.log('this.simState: ' +info.simState);
        console.log('this.isNetworkRoaming: ' +info.isNetworkRoaming);
        console.log('this.phoneCount: ' +info.phoneCount);
        console.log('this.activeSubscriptionInfoCount: ' +info.activeSubscriptionInfoCount);
        console.log('this.activeSubscriptionInfoCountMax: ' +info.activeSubscriptionInfoCountMax);
        console.log('this.phoneNumber: ' +info.phoneNumber);
        console.log('this.deviceId: ' +info.deviceId);
        console.log('this.deviceSoftwareVersion: ' +info.deviceSoftwareVersion);
        console.log('this.simSerialNumber: ' +info.simSerialNumber);
        console.log('this.subscriberId: ' +info.subscriberId);
        console.log('this.cards: ' +info.cards);
      },
      (err) => console.log('Unable to get sim info: ', err)
    );
  }
  */
  setDeviceInfo() {
    if(this.platform.is('android')) {
      this.d_Type = '1';  //Android
    } else {
      this.d_Type = '2';  //iOS
    }

    this.deviceInfo.d_UUID = this.device.uuid;
    this.deviceInfo.d_Model = this.device.model;
    this.deviceInfo.d_Platform = this.device.platform;
    this.deviceInfo.d_Version = this.device.version;
    this.deviceInfo.d_Type = this.d_Type;
    this.getVersion();
    console.log('----------> Initial DeviceProvider is loaded');

  }

  getVersion() {
    this.queryParams = 'device='+this.d_Type; 
    let body   : string	 = this.queryParams,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.builderURI + 'get_version.php';
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
        if (data.result != 'ok') {
          console.log('==========> Failed!! get_version()');
        } else {
          console.log('----------> Success!! get_version()');
          this.deviceInfo.s_version = data.version.version;
          this.deviceInfo.s_link = data.version.link;
          this.storage.set('deviceInfo', this.deviceInfo).then(()=>{
            console.log('----------> Save deviceInfo to Storage');
          },(e)=>{
            console.log('==========> Unable deviceInfo to save'+e);
          });
        }
    });
  }


  setPushKey(pushKey){
    this.deviceInfo.d_pushKey =  pushKey;
    this.storage.set('deviceInfo', this.deviceInfo).then(()=>{
      console.log('----------> Save pushKey to Storage for Push Key : '+pushKey);
     },(e)=>{
      console.log('==========> Unable deviceInfo to save for Push Key : '+e);
     })  //====> store userProfile
   }

   getDeviceInfo() {
    //this.storage.get('userProfile').then((data) => {
    this.storage.get('deviceInfo').then(data => {

        this.deviceInfo.d_UUID =  data.d_UUID;
        this.deviceInfo.d_Model =  data.d_Model;
        this.deviceInfo.d_Platform =  data.d_Platform;
        this.deviceInfo.d_Version =  data.d_Version;
        this.deviceInfo.d_Type =  data.d_Type;
        this.deviceInfo.d_pushKey =  data.d_pushKey;
        this.deviceInfo.s_version =  data.s_version;
        this.deviceInfo.s_link =  data.s_link;

        console.log('----------> Got deviceInfo from deviceInfo');
    });
  }

}