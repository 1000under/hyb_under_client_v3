import { Injectable } from '@angular/core';
import {TranslateService} from 'ng2-translate';
import 'rxjs/add/operator/map';

/*
  Generated class for the TranslateProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class TranslateProvider {

    constructor(public translate: TranslateService) {
      translate.setDefaultLang('kor');
    }

    //설정
    SetTranslateService(lang : string){
        this.translate.use(lang);
    }
}
