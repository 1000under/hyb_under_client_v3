import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class MakerModelProvider {

  //-------------------------------------------------> for data query
  cars: any =[];
  private baseURI 		: string  = "http://cjsekfvm.cafe24.com/test_server/app/car/url/";

  model_list: any = [];
  makers: any = [];
  models: any = [];
  levels: any = [];

  /*
  maker_key:"1"
  maker_name:"현대"
  model_key:"1"
  model_name:"쏘나타"
  model_sub_key:"51"
  model_sub_name:"쏘나타 뉴 라이즈(17년~현재)"
  */

  maker_name: any = '';
  model_name: any = '';
  model_sub_name: any = '';

  constructor(public http: Http) {
    this.getModelList();
    console.log('Hello MakerModelProvider Provider');
  }

  getModelList() {
    let body   : string	 = '',
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.baseURI + "get_car.php";
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 

      this.model_list = data.model_list;

      let temp_maker_key = '';
      for (let i = 0; i < this.model_list.length; i++) {
        if (this.model_list[i].maker_key != temp_maker_key) {
          temp_maker_key = this.model_list[i].maker_key;
          if(this.model_list[i].maker_name == '쉐보레(GM대우)') {
            this.model_list[i].maker_name = '쉐보레';
          }
          this.makers.push({
            maker_key: this.model_list[i].maker_key, 
            maker_name: this.model_list[i].maker_name
          });
        }
      }

      let temp_model_key = '';
      for (let i = 0; i < this.model_list.length; i++) {
        if (this.model_list[i].model_key != temp_model_key) {
          temp_model_key = this.model_list[i].model_key;
          this.models.push({
            model_key: this.model_list[i].model_key, 
            model_name: this.model_list[i].model_name
          });
        }
      }

      let temp_model_sub_key = '';
      for (let i = 0; i < this.model_list.length; i++) {
        if (this.model_list[i].model_sub_key != temp_model_sub_key) {
          temp_model_sub_key = this.model_list[i].model_sub_key;
          this.levels.push({
            model_sub_key: this.model_list[i].model_sub_key, 
            model_sub_name: this.model_list[i].model_sub_name
          });
        }
      }
      console.log('----------> Initial MakeModelProvider is loaded');
    }); 
  }

  getMakerName(maker_key) {
    for (let i = 0; i < this.makers.length; i++) {
      if (this.makers[i].maker_key == maker_key) {
        this.maker_name = this.makers[i].maker_name;
        return this.maker_name;
      }
    }
  }

  getModelName(model_key) {
    for (let i = 0; i < this.models.length; i++) {
      if (this.models[i].model_key == model_key) {
        this.model_name = this.models[i].model_name;
        return this.model_name;        
      }
    }
  }

  getModelSubName(model_sub_key) {
    for (let i = 0; i < this.levels.length; i++) {
      if (this.levels[i].model_sub_key == model_sub_key) {
        this.model_sub_name = this.levels[i].model_sub_name;
        return this.model_sub_name;          
      }
    }
  }

}
