import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';

//-----> Providers
import { DeviceProvider } from '../../providers/device/device';
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';

import 'rxjs/add/operator/map';


@Injectable()
export class SetanalyticsProvider {

  //-------------------------------------------------> for data query
  private baseURI 		: string  = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";  
  queryParams: any;

  constructor(
    public http: Http,
    public alertCtrl: AlertController,
    public deviceProvider: DeviceProvider,
    public userInfoProvider: UserInfoProvider,
    public msgService: ResponseMsgServiceProvider,) {
    console.log('Hello SetanalyticsProvider Provider');
  }

  setAnalytics(set_sale_key, set_type) {

    let set_device_num = this.deviceProvider.deviceInfo.d_UUID;
    let set_is_member = '';
    if(this.userInfoProvider.userProfile.isMember_flag == true) {
      set_is_member = '1';
    } else {
      set_is_member = '2';
    }

    this.queryParams = 'sale='        +set_sale_key+      // car key
                       '&is_member='  +set_is_member+     // 1=YES, 2=NO
                       '&type='       +set_type+          // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                       '&device_num=' +set_device_num+
                       '&email='      +this.userInfoProvider.userProfile.u_email;   //user email
    console.log('setAnalytics.queryParams: '+ this.queryParams);
    
    let body   : string	 = this.queryParams,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.baseURI + "set_sale_analytics.php";
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
      if (data.result != 'ok') {
        console.log('set Analytics Failed!! ==>' +data.result);
      } else {
        console.log('set Analytics Success!!');
      }
    });

  }

}
