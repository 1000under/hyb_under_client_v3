import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/map';


@Injectable()
export class ChatsCountProvider {

  private messageURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/message/url/";
  queryParams: any;
  unread_count: number = 0;

  constructor(
    public http: Http,
    public events: Events, ) {
    console.log('Hello ChatsCountProvider Provider');
  }

  checkChatsCount(login_flag, u_email, u_pw) {
    this.unread_count = 0;
    //console.log('============> ChatsCountProvider login_flag, u_email, u_pw :'+login_flag+' ,'+u_email+' ,'+u_pw);
    if(login_flag == true) {
      this.queryParams = 'email='+u_email+
                        '&pw='+u_pw;  
      let body   : string	 = this.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.messageURI + "get_message_list.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
        if (data.result != 'ok') {
          this.unread_count = 0;
        } else {
            for (let i = 0; i < Object.keys(data.message_list).length; i++) {
              this.unread_count = this.unread_count + Number(data.message_list[i].unread_count);
            }
            //console.log('============> ChatsCountProvider this.unread_count :'+this.unread_count);
            this.events.publish('chat:updated', this.unread_count);
        }
      });
    }
  }

  logoutChatsCount() {
    this.unread_count = 0;
    this.events.publish('chat:updated', this.unread_count);
    console.log('============> ChatsCountProvider logoutChatsCount.unread_count :'+this.unread_count);
  }
  /*
  addChatsCount(add_count) {
    this.unread_count = this.unread_count + add_count;
    //console.log('============> ChatsCountProvider addChatsCount.unread_count :'+this.unread_count);
  }
  */
}
