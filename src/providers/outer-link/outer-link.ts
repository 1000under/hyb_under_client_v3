import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class OuterLinkProvider {

  private builderURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/builder/url/";

/*
[{"type_link":"대출",
  "order_view":"1",
  "title":"농협캐피탈\\중고차대출",
  "link":"https:\/\/www.nhcapital.co.kr\/landing\/mo\/m_landingmain.nh?src=image&kw=026F79"},

 {"type_link":"보험",
  "order_view":"2",
  "title":"보험사별통합비교",
  "link":"http:\/\/direct-carbohum.kr\/S026\/ontont1"}]
*/

  outerLinkList: any = [];
  privacyPolicyContent: any = '';
  termUseContent: any = '';

  constructor(public http: Http) {
    this.getOuterLink();
    this.getPolicy();
    console.log('Hello OuterLinkProvider Provider');
  }

  getOuterLink() {
    let body   : string	 = '',
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.builderURI + 'get_outer_link.php';
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
        if (data.result != 'ok') {
          console.log('========> Failed!! get_outer_link.php' +data.result);
        } else {
          this.outerLinkList = data.outer_link_list;
        } 
        console.log('----------> Initial OuterLinkProvider Link is loaded');
    });
  }

  getPolicy() {
    let body   : string	 = '',
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.builderURI + 'get_policy.php';
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
        if (data.result != 'ok') {
          console.log('========> Failed!! get_policy.php' +data.result);
        } else {
          for (let i = 0; i < data.policy_list.length; i++) {
            if(data.policy_list[i].type == '개인') {
              this.privacyPolicyContent = data.policy_list[i].memo;
            } else {
              this.termUseContent = data.policy_list[i].memo;
            }
          }
        }
        console.log('----------> Initial OuterLinkProvider Policy is loaded'); 
    });
  }
  
}
