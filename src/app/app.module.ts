import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { Http } from '@angular/http';

// import native ----------->
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Device } from '@ionic-native/device';
import { Sim } from '@ionic-native/sim';
import { IonicStorageModule } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import { TranslateModule , TranslateStaticLoader, TranslateLoader} from 'ng2-translate/ng2-translate';
import { Geolocation } from '@ionic-native/geolocation';
import { Facebook } from '@ionic-native/facebook';
import { FCM } from '@ionic-native/fcm';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { ImagePicker } from '@ionic-native/image-picker';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { KakaoTalk } from 'ionic-plugin-kakaotalk';
// import native <-----------

import { MyApp } from './app.component';

import { OuterLinkProvider } from '../providers/outer-link/outer-link';
import { ResponseMsgServiceProvider } from '../providers/response-msg-service/response-msg-service';
import { CameraProvider } from '../providers/camera/camera';
import { DeviceProvider } from '../providers/device/device';
import { PhotoServiceProvider } from '../providers/photo-service/photo-service';
import { TranslateProvider } from '../providers/translate/translate';
import { UserInfoProvider } from '../providers/user-info/user-info';
import { MakerModelProvider } from '../providers/maker-model/maker-model';
import { SetanalyticsProvider } from '../providers/setanalytics/setanalytics';
import { ChatsCountProvider } from '../providers/chats-count/chats-count';

export function createTranslateLoader(http: Http) {
    return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
	declarations: [
		MyApp,
	],
	imports: [
		BrowserModule,
		IonicModule.forRoot(MyApp, { 
			scrollAssist: true,			//for ion-input 
			autoFocusAssist: true
		}),
		HttpModule,
		IonicStorageModule.forRoot(),
		TranslateModule.forRoot({
			provide: TranslateLoader,
			useFactory: (createTranslateLoader),
			deps: [Http]
		}),
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
	],
	providers: [
		StatusBar,
		SplashScreen,
		Device,
		Sim,
		IonicStorageModule,
		SocialSharing,
		Geolocation,
		Facebook,
		FCM,
		File,
		Transfer,
		FilePath,
		ImagePicker,
		PhotoViewer,
		GoogleAnalytics,
		KakaoTalk,
		{provide: ErrorHandler, useClass: IonicErrorHandler},
		OuterLinkProvider,
		ResponseMsgServiceProvider,
		CameraProvider,
		DeviceProvider,
		PhotoServiceProvider,
		TranslateProvider,
		UserInfoProvider,
    	MakerModelProvider,
    	SetanalyticsProvider,
    	ChatsCountProvider,
	]
})
export class AppModule {}
