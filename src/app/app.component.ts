import { Component, ViewChild } from '@angular/core';
import { IonicApp, App, Platform, Nav, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
	templateUrl: 'app.html'
})
export class MyApp {

	@ViewChild(Nav) nav: Nav;
	rootPage:any = 'TabsPage';

	constructor(
		public platform: Platform,
		public app: App, 
		public statusBar: StatusBar, 
		public splashScreen: SplashScreen,
		public ionicApp: IonicApp,
		public alertCtrl: AlertController,
		private ga: GoogleAnalytics,) {

		this.platform.ready().then(() => {

			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			this.hideSplashScreen();			
			this.statusBar.styleDefault();

			// Google Analytics for 1000under
			this.ga.startTrackerWithId('UA-110474265-1')    //Tracking ID
				.then(() => {
					console.log('----------> Google analytics is ready now');
				})
				.catch(e => console.log('==========> Error starting GoogleAnalytics', e));

			// Set statusBar color
			if(this.platform.is('android')){
				// this.statusBar.overlaysWebView(false);
				// set status bar to 1000Under ==> Supported Platforms: iOS, Android 5+, Windows Phone 7+
				this.statusBar.backgroundColorByHexString('#37aafe');	//#37aafe
			}

			// Handle the default mobile's back button with Confirmation Alert
			this.platform.registerBackButtonAction((event) => {
				// check overlayview(ex, alert, actionSheet..)
				const overlayView = this.app._appRoot._overlayPortal._views[0];
				if (overlayView && overlayView.dismiss) {
					overlayView.dismiss();
				} else {
					// check page is loading, modal, toast or overlay
					let activePortal = this.ionicApp._loadingPortal.getActive() ||
										this.ionicApp._modalPortal.getActive() ||
										this.ionicApp._toastPortal.getActive() ||
										this.ionicApp._overlayPortal.getActive();

					//console.log('activePortal ==> '+activePortal);
					//console.log('activePortal.index ==> '+activePortal.index);
					if(activePortal == undefined) {
						let confirm = this.alertCtrl.create({
							title: '종료 하시겠습니까?',
							buttons: [
								{
									text: '취소',
									handler: () => {
										console.log('Disagree clicked');
									}
								},
								{
									text: '확인',
									handler: () => {
										console.log('Agree clicked');
										//this.platform.exitApp();
										navigator['app'].exitApp();
									}
								}
							]
						});
						confirm.present();
					} else if(activePortal && activePortal.index >= 0) {
						activePortal.dismiss(); // closes modal
					} else {
						if(this.nav.getActive().name == 'TabsPage') {    // your homepage
							let confirm = this.alertCtrl.create({
								title: '종료 하시겠습니까?',
								buttons: [
									{
										text: '취소',
										handler: () => {
											console.log('Disagree clicked');
										}
									},
									{
										text: '확인',
										handler: () => {
											console.log('Agree clicked');
											//this.platform.exitApp();
											navigator['app'].exitApp();
										}
									}
								]
							});
							confirm.present();
						} else {
							if(this.nav.canGoBack())
								this.nav.pop();
							//this.nav.setRoot(Homepage);
						}
					}
				}
			},101);
		});
	}

	hideSplashScreen() {
		if (this.splashScreen) {
			setTimeout(() => {
				this.splashScreen.hide();
			}, 100);
		}
	}

}
