import { Component, ViewChild, ElementRef, } from '@angular/core';
import { IonicPage, Platform, ViewController, App, NavController, NavParams, AlertController, LoadingController, ModalController, } from 'ionic-angular';
import { Http } from '@angular/http';  //-------> Test connect to server .php
import { Geolocation } from '@ionic-native/geolocation';
import { ImagePicker } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import numeral from 'numeral';

//-----> Providers
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';

declare var google;

@IonicPage()
@Component({
  selector: 'page-add-my-car',
  templateUrl: 'add-my-car.html',
  providers: [ImagePicker]
})
export class AddMyCarPage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('places') places: ElementRef;
  map: any;

  //-------> for input focus
  @ViewChild('f_owner_name') f_owner_name;
  @ViewChild('f_phone') f_phone;
  @ViewChild('f_str_price') f_str_price;
  @ViewChild('f_model_level') f_model_level;
  @ViewChild('f_car_number') f_car_number;
  @ViewChild('f_str_mileage') f_str_mileage;
  @ViewChild('f_additory_options') f_additory_options;
  @ViewChild('f_introduce') f_introduce;
  
  marketURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";

  searchBrand: any = [];
  showModelFlag: boolean = false;
  
  navigationFlag: boolean = false;
  sunLoopFlag: boolean = false;
  blackBoxFlag: boolean = false;
  warrantyFlag: boolean = false;
  rearSensorFlag: boolean = false;
  startButtonFlag: boolean = false;
  rearCameraFlag: boolean = false;
  heatingSeatFlag: boolean = false;

  normalTypeFlag: boolean = false;
  specialTypeFlag: boolean = false;

  //images;
  rep: any = '';
  registDoc: any = '';
  once: any = '';
  navigation_on: any = '';
  navigation_off: any = '';
  sunLoop_on: any = '';
  sunLoop_off: any = '';
  blackBox_on: any = '';
  blackBox_off: any = '';
  warranty_on: any = '';
  warranty_off: any = '';
  rearSensor_on: any = '';
  rearSensor_off: any = '';
  startButton_on: any = '';
  startButton_off: any = '';
  rearCamera_on: any = '';
  rearCamera_off: any = '';
  heatingSeat_on: any = '';
  heatingSeat_off: any = '';


  getRepImgUrl: any = '';             //--> 대표 이미지 url
  repImgFileName: string = 'noImg';   //--> 대표 이미지 nane
  getRegistImgUrl: any = '';          //--> 등록증 이미지 url
  registImgFileName: string = 'noImg';//--> 등록증 이미지 name
  getOtherImgCnt: number = 0;         //--> 추가 이미지 수
  getOtherImgUrl: any = [7];          //--> 추가 이미지 url
  otherImgFileName: any = [7];        //--> 추가 이미지 name

  photo_main_list : any = [];         //--> Blob file for 대표, 등록증 이미지
  photo_sub_list : any = [];          //--> Blob file for 추가 이미지

  location: string = 'initial';       //--> 차량 위치
  searchPlace: string = '';
  address: string = '';               //--> 차량 소재지 주소
  address_lat: number = 0;            //--> 위도
  address_lon: number = 0;            //--> 경도
  owner_name: string = '';            //--> 소유주 이름
  name: string = '';                  //--> 맴버 이름
  email: string = '';                 //--> 맴버 이메일
  pw: string = '';
  phone: string = '';                 //--> 맴버 전화번호
  member_photo: string = '';          //--> 맴버 사진
  price: number = 0;                  //--> 차량 가격
  str_price: string = '';
  mileage: number = 0;                //--> 주행거리
  str_mileage: string = '';
  maker: string = '';                 //--> 제조사
  maker_key: string = '';
  model: string = '';                 //--> 모델
  model_key: string = '';
  model_sub: string = '';             //--> 세부모델
  model_sub_key: string = '';
  model_level: string = '';           //--> 모델레벨
  regist_type: string = '';                 //--> 등록타입  [regist_type] * 등록타입(0:무료, 1:유료A), 2:유료B
  car_number: string = '';            //--> 차량 등록 번호
  yearMonth: string = '';             //--> 최초등록년월
  year: string = '';                  //--> 최초등록년
  month: string = '';                 //--> 최초등록월
  puer: string = '';                  //--> 연료
  gear: string = '';                  //--> 변속기
  color: string = '';                 //--> 색상
  additory_options: string = '';      //--> 추가옵션
  repair: string = '';                //--> 정비이력
  replacement: string = '';           //--> 교환이력
  options: any = [];                  //--> 주요옵션 [1:블랙박스, 2:열선시트, 3:네비게이션, 4:후방카메라, 5:후방센서, 6:선루프, 7:START버튼, 8:제조사보증]
  introduce: string = '';                  //--> 내차 소개

  specialFee: number = 0;             //--> 스페셜 등록 수수료 
                                      //500만원 이하 차량은 5.5만원(부가세 포함) 
                                      //500만원초과 1000만원 이하 7.7만원
                                      //1000만원 초과 2000만원 이하 11만원
                                      //2000만원 초과 차량은 16.5만원입니다~!

  puers = [
    { id: '1', puer: '디젤'},
    { id: '2', puer: '가솔린'},
    { id: '3', puer: '하이브리드'},
    { id: '4', puer: '전기'},
    { id: '5', puer: '가스'},
    { id: '6', puer: '기타'}
  ];
  gears = [
    { id: '1', gear: '자동'},
    { id: '2', gear: '수동'},
    { id: '3', gear: '기타'},
  ];
  colors = [
    { c_color: '검정색' },
    { c_color: '흰색' },
    { c_color: '은색' },
    { c_color: '은회색' },
    { c_color: '쥐색' },
    { c_color: '청색' },
    { c_color: '하늘색' },
    { c_color: '갈대색' },
    { c_color: '갈색' },
    { c_color: '자주색' },
    { c_color: '빨간색' }
  ];

  constructor(
    public viewCtrl           : ViewController, 
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public alertCtrl          : AlertController,
    public modalCtrl          : ModalController,
    public loadingCtrl        : LoadingController,
    public http               : Http,
    public app                : App,
    public platform           : Platform,
    public userInfoProvider   : UserInfoProvider,
    public msgService         : ResponseMsgServiceProvider,
    private imagePicker       : ImagePicker,
    private transfer          : Transfer, 
    private file              : File, 
    private filePath          : FilePath,
    private geolocation       : Geolocation,
    private ga                : GoogleAnalytics,){
      for (let i = 0; i < 7; i++) {
        this.otherImgFileName[i] = 'noImg';
      }
      this.rep = "img/icon/regist/rep.png";
      this.registDoc = "img/icon/regist/registDoc.png";
      this.once = "img/icon/regist/once.png";
      this.navigation_on = "img/option/navigation_on.png";
      this.navigation_off = "img/option/navigation_off.png";
      this.sunLoop_on = "img/option/sunLoop_on.png";
      this.sunLoop_off = "img/option/sunLoop_off.png";
      this.blackBox_on = "img/option/blackBox_on.png";
      this.blackBox_off = "img/option/blackBox_off.png";
      this.warranty_on = "img/option/warranty_on.png";
      this.warranty_off = "img/option/warranty_off.png";
      this.rearSensor_on = "img/option/rearSensor_on.png";
      this.rearSensor_off = "img/option/rearSensor_off.png";
      this.startButton_on = "img/option/startButton_on.png";
      this.startButton_off = "img/option/startButton_off.png";
      this.rearCamera_on = "img/option/rearCamera_on.png";
      this.rearCamera_off = "img/option/rearCamera_off.png";
      this.heatingSeat_on = "img/option/heatingSeat_on.png";
      this.heatingSeat_off = "img/option/heatingSeat_off.png";
  }
    
  ionViewDidLoad() {  //-----------------------------------------------------------------> ionViewDidLoad()
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    this.email = this.userInfoProvider.userProfile.u_email;
    this.pw = this.userInfoProvider.userProfile.u_pw;
    this.loadMap();
    console.log('ionViewDidLoad AddMyCarPage');
  }

  ionViewDidEnter() {
    this.ga.trackView('내차등록');  //trackView for Google Analytics
  }

  loadMap(){  
    this.geolocation.getCurrentPosition().then((position) => {
      
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      let mapOptions = {
        center: latLng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        fullscreenControl: false
      }
      
      const element: HTMLElement = document.getElementById('amap');
      this.map = new google.maps.Map(element, mapOptions);
      let input_from = /** @type {HTMLInputElement} */ (document.getElementById("place_input"));
      let options = {
          types: [],
          componentRestrictions: {country: "kr"}
      };

      // create the two autocompletes on the place_input fields
      let autocomplete = new google.maps.places.Autocomplete(input_from, options);
      autocomplete.bindTo("bounds", this.map);
      // we need to save a reference to this as we lose it in the callbacks
      let self = this;

      //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      let infowindow = new google.maps.InfoWindow();
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: this.map.getCenter()
      });

      // add the first listener
      autocomplete.addListener("place_changed", function() {
        infowindow.close();
        let place = autocomplete.getPlace();
        if (!place.geometry) {
          return;
        } else {
          self.location = place.name;
          self.address = place.formatted_address;
          self.address_lat = place.geometry.location.lat();
          self.address_lon = place.geometry.location.lng();
        }
        // Set the position of the marker using the place ID and location.
        marker.setPlace( /** @type {!google.maps.Place} */ ({
            placeId: place.place_id,
            location: place.geometry.location
        }));
        marker.setVisible(true);
        infowindow.setContent("<div><strong>" + place.name + "</strong><br>" + "<br>" + place.formatted_address);
        infowindow.open(this.map, marker);
        self.f_introduce.setFocus();
      });

    });
  } 

  addRepImage() {
    let options = {
        maximumImagesCount: 1, width: 800, height: 800, quality: 100
    };
    this.imagePicker.getPictures(options).then((results) => {
      if(results[0] != undefined) {
        this.getRepImgUrl = results[0];
        console.log('this.getRepImgUrl: '+this.getRepImgUrl);
        if (this.platform.is('android')) {
          this.filePath.resolveNativePath(this.getRepImgUrl)
          .then(filePath => {
            //Create a new name for the Image
            let d = new Date();
            let n = d.getTime();
            this.repImgFileName =  "representImage_" +n+ ".jpg";  //한글 이름 않됨 !!!!
            this.CreateMainImage(filePath,0);
          });
        } else {
          //Create a new name for the Image
          let d = new Date();
          let n = d.getTime();
          this.repImgFileName =  "representImage_" +n+ ".jpg";  //한글 이름 않됨 !!!!
          this.CreateMainImage(this.getRepImgUrl,0);
        }
      }
    }, (err) => { 
      console.log('imagePicker.getPictures ERROR: '+err);
    });
  }

  addRegistImage() {
    let options = {
        maximumImagesCount: 1, width: 800, height: 800, quality: 100
    };
    this.imagePicker.getPictures(options).then((results) => {
      if(results[0] != undefined) {
        this.getRegistImgUrl = results[0];
        console.log('this.getRegistImgUrl: '+this.getRegistImgUrl);
        if (this.platform.is('android')) {
          this.filePath.resolveNativePath(this.getRegistImgUrl)
          .then(filePath => {
            //Create a new name for the Image
            let d = new Date();
            let n = d.getTime();
            this.registImgFileName =  "registImage_" +n+ ".jpg";  //한글 이름 않됨 !!!!
            this.CreateMainImage(filePath,1);
          });
        } else {
          //Create a new name for the Image
          let d = new Date();
          let n = d.getTime();
          this.registImgFileName =  "registImage_" +n+ ".jpg";  //한글 이름 않됨 !!!!
          this.CreateMainImage(this.getRegistImgUrl,1);
        }
      }
    }, (err) => { 
      console.log('imagePicker.getPictures ERROR: '+err);
    });
  }

  addOtherImage() {
    //this.getOtherImgCnt = 1;
    let options = {
        maximumImagesCount: 7,
        width: 800,   // proportionally rescale image to this width, default no rescale
        height: 800,  // same for height
        quality: 80   // 0-100, default 100 which is highest quality
    };
    
    for (let i = 0; i < 7; i++) {
          this.otherImgFileName[i] = 'noImg';
    }
    
    this.imagePicker.getPictures(options).then((results) => {
      if(results.length != 0) {
        if(results.length > 7) { 
          this.getOtherImgCnt = 7;
        } else {
          this.getOtherImgCnt = results.length;
        }
        
        for (let i = 0; i < this.getOtherImgCnt; i++) {
            this.getOtherImgUrl[i] = results[i];
            if (this.platform.is('android')) {
              this.filePath.resolveNativePath(this.getOtherImgUrl[i])
              .then(filePath => {
                //Create a new name for the Image
                let d = new Date();
                let n = d.getTime();
                this.otherImgFileName[i] = "subImage_"+[i]+"_"+n+ ".jpg";  //한글 이름 않됨 !!!!
                this.CreateSubImage(filePath,i);
              });
            } else {
              //Create a new name for the Image
              let d = new Date();
              let n = d.getTime();
              this.otherImgFileName[i] = "subImage_"+[i]+"_"+n+ ".jpg";  //한글 이름 않됨 !!!!
              this.CreateSubImage( this.getOtherImgUrl[i],i);
            }
        }
      }
    }, (err) => { 
      console.log('imagePicker.getPictures ERROR: '+err);
    });
  }

  //Main Image파일 생성
  public CreateMainImage(fileName,index)
  {
      
    this.file.resolveLocalFilesystemUrl(fileName).then((fileEntry : any) => {

        fileEntry.file((resFile) => {
          var reader = new FileReader();
          reader.onloadend = (evt: any) => {
            var imgBlob: any = new Blob([evt.target.result], { type: 'image/jpeg' });
            imgBlob.name = fileName;
            this.photo_main_list[index] = imgBlob;
            console.log('Create MainImage: ' +fileName);
          };
          reader.onerror = (e) => {
            console.log("Failed file read: " + e.toString());
          };
          reader.readAsArrayBuffer(resFile);
        });

      }, (err) => {
        console.log(err);
        //alert(JSON.stringify(err))
      });
  }
  
  //Sub Image파일 생성
  public CreateSubImage(fileName,index)
  {
      this.file.resolveLocalFilesystemUrl(fileName).then((fileEntry : any) => {

        fileEntry.file((resFile) => {
          var reader = new FileReader();
          reader.onloadend = (evt: any) => {
            var imgBlob: any = new Blob([evt.target.result], { type: 'image/jpeg' });
            imgBlob.name = fileName;
            this.photo_sub_list[index] = imgBlob;
            console.log('Create SubImage: ' +fileName);
          };
          reader.onerror = (e) => {
            console.log("Failed file read: " + e.toString());
          };
          reader.readAsArrayBuffer(resFile);
        });

      }, (err) => {
        console.log(err);
        //alert(JSON.stringify(err))
      });
  }


  sameOwner() {
    this.owner_name = this.userInfoProvider.userProfile.u_name;
    if(this.userInfoProvider.userProfile.u_phone_num == 'google' || 
       this.userInfoProvider.userProfile.u_phone_num == 'facebook' ||
       this.userInfoProvider.userProfile.u_phone_num == 'kakao'){
      this.phone = '';
    } else {
      this.phone = this.userInfoProvider.userProfile.u_phone_num;
    }
  }
  
  SearchBrand(){ //Modal ------------------------------------------------------->  SearchBrand()
    this.model_sub_key = '0';
    this.model_sub = '';    
    let searchBrandPage_modal = this.modalCtrl.create('SearchBrandPage',{fromPage: 'addMyCarPage'});
    searchBrandPage_modal.onDidDismiss(data => {
      this.searchBrand = [];        
      if (data.maker_name == '전체' || data.model_name == '전체') {
        this.showModelFlag = false;
      } else {
        //{maker_key: "1", maker_name: "현대", model_key: "9", model_name: "i30"}
        //console.log('maker_key + model_key + level_key ===> '+data.maker_key+' + '+ data.model_key+' + '+ data.level_key+' ;')
        this.searchBrand.push(data.maker_name+'-'+data.model_name);
        this.maker_key = data.maker_key;
        this.maker = data.maker_name;
        this.model_key = data.model_key;
        this.model = data.model_name;
        this.model_sub_key = data.level_key;
        this.model_sub = data.level_name;
        this.showModelFlag = true;
      }
      this.f_model_level.setFocus();
    });
    searchBrandPage_modal.present();
  }

  //-------------------------------------------------> for next input focus
  f_owner_name_eventHandler(e) {
    if(e.keyCode == 13) {
      this.f_phone.setFocus();
    }
  }
  f_phone_eventHandler(e) {
    if(e.keyCode == 13) {
      if(!this.chk_phoneNumber(this.phone)) {
        let alert = this.alertCtrl.create({
          title: '알림!',
          subTitle: '잘못된 전화번호입니다. 숫자만 입력해주세요',
          buttons: ['확인']
        });
        alert.present();
        this.phone = '';
      } else {
        this.f_str_price.setFocus();
      }
    }    
  }

  onChangePrice(evt) {
    this.str_price = evt.replace(/[^0-9.]/g, "");
    if (this.str_price) {
      let myNumeral = numeral(this.str_price);
      let value = myNumeral.value();
      this.price = Number(value);
      if(this.price > 0) {
        this.str_price = numeral(this.price).format('0,0');
        console.log("onChangePrice str_price_formatted: " + this.str_price);
        this.specialTypeFlag = true;
        this.regist_type = '1';  //등록타입(0:무료, 1:유료)
        //500만원 이하 차량은 5.5만원(부가세 포함) 
        //500만원초과 1000만원 이하 7.7만원
        //1000만원 초과 2000만원 이하 11만원
        //2000만원 초과 차량은 16.5만원입니다~!
        if(this.price < 500) {
          this.specialFee = 55000;
        } else if(this.price >= 500 && this.price < 1000) {
          this.specialFee = 77000;
        } else if(this.price >= 1000 && this.price < 2000) {
          this.specialFee = 110000;
        } else if(this.price >= 2000) {
          this.specialFee = 165000;
        }
      } else {
        let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: '차량가격을 입력해주세요',
            buttons: ['확인']
          });
        alert.present(); 
      }
    } else {
      this.str_price = '0'; this.price = 0;
    }
  }

  f_model_level_eventHandler(e) {
    if(e.keyCode == 13) {
      this.f_car_number.setFocus();
    }    
  }
  f_car_number_eventHandler(e) {
    if(e.keyCode == 13) {
      this.f_str_mileage.setFocus();
    } 
  }

  onChangeMileage(evt) {
    this.str_mileage = evt.replace(/[^0-9.]/g, "");
    if (this.str_mileage) {
      let myNumeral = numeral(this.str_mileage);
      let value = myNumeral.value();
      this.mileage = Number(value);
      if(this.mileage > 0) {
        this.str_mileage = numeral(value).format('0,0');        
      } else {
        let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: '주행거리를 입력해주세요',
            buttons: ['확인']
          });
        alert.present(); 
      }
    } else {
      this.str_mileage = '0'; this.mileage = 0;
    }
  }
  //-------------------------------------------------> for next input focus

  chk_phoneNumber(phoneNumber) {   
    //console.log('check this.phoneNumber :' +phoneNumber);
    var regExp = /^[0-9]+$/;
    if ( !regExp.test(phoneNumber) ) {
      return false;
    } else {
      return true;
    }
  } 

  selectedColor(e) {  //------------------------------------------------------------------> selectedColor()
    console.log('color:' +this.color);
  }

  selectedPuer(e) {  //-------------------------------------------------------------------> selectedpuer()
    console.log('puer:' +this.puer);
  }

  selectedGear(e) {  //-----------------------------------------------------------> selectedselectedTransmission()
    console.log('gear:' +this.gear);
    //this.f_additory_options.setFocus();
    /*
    console.log('selectedGear change event:' +e);
    if(e != undefined) {
      setTimeout(() => {
        //Keyboard.show() // for android
        this.f_additory_options.setFocus();
      },200);
    }
    */
  }

  // [1:네비게이션, 2:선루프, 3:블랙박스, 4:제조사보증, 5:후방센서, 6:START버튼, 7:후방카메라, 8:열선시트]
  navigationOnOff() { 
    if( this.navigationFlag == false ) { 
      this.navigationFlag = true; 
      this.options.push('1');
    } else { 
      this.navigationFlag = false;
      let index = this.options.indexOf('1');
      if(index > -1){
         this.options.splice(index, 1);
      }
    }
    //console.log('this.options : '+this.options);
  }
  sunLoopOnOff() { 
    if( this.sunLoopFlag == false ) { 
      this.sunLoopFlag = true; 
      this.options.push('2');
    } else { 
      this.sunLoopFlag = false; 
      let index = this.options.indexOf('2');
      if(index > -1){
         this.options.splice(index, 1);
      }
    }
    //console.log('this.options : '+this.options);
  }
  blackBoxOnOff() { 
    if( this.blackBoxFlag == false ) { 
      this.blackBoxFlag = true;
      this.options.push('3'); 
    } else {
      this.blackBoxFlag = false;
      let index = this.options.indexOf('3');
      if(index > -1){
         this.options.splice(index, 1);
      } 
    }
    //console.log('this.options : '+this.options);
  }
  warrantyOnOff() { 
    if( this.warrantyFlag == false ) { 
      this.warrantyFlag = true; 
      this.options.push('4');
    } else { 
      this.warrantyFlag = false; 
      let index = this.options.indexOf('4');
      if(index > -1){
         this.options.splice(index, 1);
      }
    }
    //console.log('this.options : '+this.options);
  }
  rearSensorOnOff() { 
    if( this.rearSensorFlag == false ) { 
      this.rearSensorFlag = true; 
      this.options.push('5');
    } else { 
      this.rearSensorFlag = false; 
      let index = this.options.indexOf('5');
      if(index > -1){
         this.options.splice(index, 1);
      }
    }
    //console.log('this.options : '+this.options);
  }
  startButtonOnOff() { 
    if( this.startButtonFlag == false ) { 
      this.startButtonFlag = true; 
      this.options.push('6');
    } else { 
      this.startButtonFlag = false; 
      let index = this.options.indexOf('6');
      if(index > -1){
         this.options.splice(index, 1);
      }
    }
    //console.log('this.options : '+this.options);
  }
  rearCameraOnOff() { 
    if( this.rearCameraFlag == false ) { 
      this.rearCameraFlag = true; 
      this.options.push('7');
    } else { 
      this.rearCameraFlag = false; 
      let index = this.options.indexOf('7');
      if(index > -1){
         this.options.splice(index, 1);
      }
    }
    //console.log('this.options : '+this.options);
  }
  heatingSeatOnOff() { 
    if( this.heatingSeatFlag == false ) { 
      this.heatingSeatFlag = true;
      this.options.push('8'); 
    } else { 
      this.heatingSeatFlag = false;
      let index = this.options.indexOf('8');
      if(index > -1){
         this.options.splice(index, 1);
      } 
    }
    //console.log('this.options : '+this.options);
  }

  submitRegiste(){  //------------------------------------------------------------------------->  submitRegiste()
    if(this.owner_name == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량 소유주 이름을 입력해주세요',
          buttons: ['확인']
        });
      alert.present();      
    } else if(!this.chk_phoneNumber(this.phone)) {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '잘못된 전화번호입니다. 차량 소유주 연락처를 입력해주세요',
          buttons: ['확인']
        });
      alert.present();      
      this.phone = '';      
    } else if(this.price == 0) {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량가격을 입력해주세요.',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.model_key == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량 모델을 선택해주세요',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.model_level == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량 세부등급을 입력해주세요',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.car_number == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량등록번호를 입력해주세요',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.yearMonth == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량연식을 선택해주세요',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.mileage == 0) {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '주행거리를 입력해주세요.',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.puer == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '연료타입을 선택해주세요.',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.gear == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '변속기 타입을 선택해주세요.',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.color == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '색상을 선택해주세요.',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.address == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '휴대폰의 "위치서비스"를 켜고 차량위치를 입력해주세요.',
          buttons: ['확인']
        });
      alert.present();
    } else if(this.repImgFileName === 'noImg' || this.registImgFileName === 'noImg') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '대표/등록증 이미지를 선택해주세요.',
          buttons: ['확인']
        });
      alert.present();
    } else {
        this.year = this.yearMonth.substr(0, 4);
        this.month = this.yearMonth.substr(5, 2);

        console.log('<------------------ Input Items ---------------->');          
        console.log('model: ' +this.model_key);
        console.log('model_sub: ' +this.model_sub_key);
        console.log('email: ' +this.email);
        console.log('pw: ' +this.pw);
        console.log('phone: ' +this.phone);
        console.log('gear: ' +this.gear );
        console.log('puer: ' +this.puer );
        console.log('car_number: ' +this.car_number );
        console.log('year: ' +this.year );
        console.log('month: ' +this.month );
        console.log('mileage: ' +this.mileage);
        console.log('price: ' +this.price );
        console.log('options: ' +this.options.toString());
        console.log('introduce: ' +this.introduce );
        console.log('model_level: ' +this.model_level);
        console.log('additory_options: ' +this.additory_options );
        console.log('repair: ' +this.repair );
        console.log('replacement: ' +this.replacement );
        console.log('photo_main[0]' +this.photo_main_list[0]+this.repImgFileName);
        console.log('photo_main[1]' +this.photo_main_list[1]+this.registImgFileName);
        for (let i = 0; i < this.photo_sub_list.length ; ++i) {
          console.log('photo_sub[]' +this.photo_sub_list[i]+this.otherImgFileName[i]);
        }
        console.log('address: ' +this.address );
        console.log('address_lat: ' +this.address_lat );
        console.log('address_lon: ' +this.address_lon );
        console.log('owner_name: ' +this.owner_name);
        console.log('color: ' +this.color );
        console.log('regist_type: ' +this.regist_type);

        /*
        console.log('<---------------- Non Input Items -------------->');
        console.log('maker_key: ' +this.maker_key);
        console.log('maker: ' +this.maker );
        console.log('model_sub_key: ' +this.model_sub_key);
        console.log('model_sub: ' +this.model_sub);
        console.log('maker_key: ' +this.maker_key);
        console.log('yearMonth: ' +this.yearMonth ); //2000-11
        console.log('region: ' +this.location );
        console.log('phone: ' +this.phone);
        console.log('repImg : ' +this.repImgFileName);      //대표이미지 
        console.log('registImg: ' +this.registImgFileName); //등록증이미지
        console.log('otherImgCnt: ' +this.getOtherImgCnt);  //추가이미지 수
        */

        let loading = this.loadingCtrl.create({
            spinner: 'bubbles',
            content: '차량 등록중 입니다...'
        });
        loading.present();

        let body = new FormData();
        let xhr = new XMLHttpRequest();
        let env = this;

        ///////////////////////////////////////////////////////////////////////////////////////////////
        body.append('model', this.model_key);
        body.append('model_sub', this.model_sub_key);
        body.append('email', this.email);
        body.append('pw', this.pw);
        body.append('phone', this.phone);
        body.append('gear', this.gear);
        body.append('puer', this.puer);
        body.append('car_number', this.car_number);
        body.append('year', this.year);
        body.append('month', this.month);
        body.append('mileage', String(this.mileage));
        body.append('price', String(this.price));
        body.append('options', this.options.toString());
        body.append('introduce', this.introduce);
        body.append('model_level', this.model_level);      
        body.append('additory_options', this.additory_options );
        body.append('repair', this.repair );
        body.append('replacement', this.replacement );
        //메인 이미지
        body.append('photo_main[]',this.photo_main_list[0],this.repImgFileName);
        //서류 이미지
        body.append('photo_main[]',this.photo_main_list[1],this.registImgFileName);
        //서브 이미지
        for (let i = 0; i < this.photo_sub_list.length ; ++i) {
          body.append('photo_sub[]',this.photo_sub_list[i],this.otherImgFileName[i]);
        }
        body.append('address', this.address);
        body.append('address_lat', String(this.address_lat));
        body.append('address_lon', String(this.address_lon));
        body.append('owner_name', this.owner_name);
        body.append('color', this.color );
        body.append('regist_type', this.regist_type );  // 등록타입(0:무료, 1:유료A)
        ///////////////////////////////////////////////////////////////////////////////////////////////////

        let url_s 	 : any		 = this.marketURI + "set_sale_regist.php";
        xhr.open("POST",url_s,true);
        xhr.send(body);

        xhr.onreadystatechange = function () {
          if (xhr.readyState == 4) {
            if (xhr.status == 200) {  
              loading.dismiss();
              console.log('xhr.responseText'+xhr.responseText);
              let response = JSON.parse(xhr.responseText);  //xhr.responseText{"result":"error_no_parameter"}
              if(response.result == 'ok') {
                  let alert = env.alertCtrl.create({
                    title: '알림',
                    subTitle: '차량 등록이 완료되었습니다.',
                    buttons: ['확인']
                  });
                  alert.present();
                  let result = true;
                  //document.querySelector(".tabbar")['style'].display = 'flex';
                  env.viewCtrl.dismiss(result);
                } else {
                  let alert = env.alertCtrl.create({
                    title: '알림',
                    subTitle: env.msgService.getMessage(response.result),
                    buttons: ['확인']
                  });
                  alert.present();
                }

            } else {
              console.log('Server response result xhr.readyState-xhr.status + : ' +xhr.readyState+' - '+xhr.status); //response result code
              //alert('xhr.status: '+xhr.status);
              //alert('xhr.readyState: '+xhr.readyState);
            }
          } // end of if (xhr.readyState == 4)
        } //--> end of xhr.onreadystatechange = function ()

    } //--> end of check image
  } //--> end of submitRegiste()

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
