import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddMyCarPage } from './add-my-car';

@NgModule({
  declarations: [
    AddMyCarPage,
  ],
  imports: [
    IonicPageModule.forChild(AddMyCarPage),
  ],
  exports: [
    AddMyCarPage
  ]
})
export class AddMyCarPageModule {}
