import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BannerModalPage } from './banner-modal';

@NgModule({
  declarations: [
    BannerModalPage,
  ],
  imports: [
    IonicPageModule.forChild(BannerModalPage),
  ],
  exports: [
    BannerModalPage
  ]
})
export class BannerModalPageModule {}
