import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-banner-modal',
  templateUrl: 'banner-modal.html',
})
export class BannerModalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BannerModalPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
