import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImageViewMorePage } from './image-view-more';

@NgModule({
  declarations: [
    ImageViewMorePage,
  ],
  imports: [
    IonicPageModule.forChild(ImageViewMorePage),
  ],
  exports: [
    ImageViewMorePage
  ]
})
export class ImageViewMorePageModule {}
