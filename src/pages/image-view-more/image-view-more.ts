import { Component, ViewChild, } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, ViewController, Slides, } from 'ionic-angular';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@IonicPage()
@Component({
  selector: 'page-image-view-more',
  templateUrl: 'image-view-more.html',
})
export class ImageViewMorePage {
  
  @ViewChild('mySlider') slides: Slides;

  photo_path: any = [];  //parameter from CarDetailPage
  initialIndex: number;

  constructor( 
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public photoViewer: PhotoViewer,
    private ga: GoogleAnalytics,) 
    {
      this.photo_path = navParams.get("photos");
      this.initialIndex = navParams.get("initialSlide");
      //console.log('this.initialSlide ----> '+this.initialIndex);
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ImageViewMorePage');
  }

  ionViewDidEnter() {
    this.ga.trackView('이미지상세보기');  //trackView for Google Analytics
  }

  zoomImage(imageDaata) {
    this.photoViewer.show(imageDaata);
  }
  
  dismiss() {
    this.viewCtrl.dismiss();
  }

}
