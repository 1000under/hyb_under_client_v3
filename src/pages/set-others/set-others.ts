import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, AlertController, ActionSheetController, Events } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { DeviceProvider } from '../../providers/device/device';
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ChatsCountProvider } from '../../providers/chats-count/chats-count';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import { OuterLinkProvider } from '../../providers/outer-link/outer-link';

@IonicPage()
@Component({
  selector: 'page-set-others',
  templateUrl: 'set-others.html',
})
export class SetOthersPage {

  //-------------------------------------------------> for data query
  queryParams: any;
  private memberURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";
  
  version: any;
  isPush: boolean;
  push_enable: any = '';    //1: YES, 2:NO

  //-------------------------------------------------> loginParams for userInfoProvider
  loginParams: any = [
    { loginApp:       '' },
    { u_name:         '' },
    { u_phone_num:    '' },
    { u_email:        '' },
    { u_pw:           '' },
    { u_photo:        '' },
    { u_type:         '' },
    { cu_flag:        false},
    { fb_flag:        false},
    { kk_flag:        false},
    { gg_flag:        false},
    { login_flag:     false},
    { isMember_flag:  false},
  ];

  constructor(
    public viewCtrl           : ViewController, 
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public modalCtrl          : ModalController,
    public alertCtrl          : AlertController,
    public actionSheetCtrl    : ActionSheetController,
    public http               : Http,
    public events             : Events,
    public deviceProvider     : DeviceProvider,
    public userInfoProvider   : UserInfoProvider,
    public chatsCountProvider: ChatsCountProvider,
    public outerLinkProvider  : OuterLinkProvider,
    public msgService         : ResponseMsgServiceProvider,
    private ga                : GoogleAnalytics,) {
      
      this.queryParams = 'email=' +this.userInfoProvider.userProfile.u_email+ '&pw=' +this.userInfoProvider.userProfile.u_pw;
      let body   : string	 = this.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.memberURI + "get_member.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
        this.version = data.member.version;
        if(data.member.push_enable == 'YES') {
          this.isPush = true;
        } else {
          this.isPush = false;
        }
      });
    }

  ionViewDidLoad() {
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    console.log('ionViewDidLoad SetOthersPage');
  }
  ionViewDidEnter() {
    this.ga.trackView('환경설정');  //trackView for Google Analytics
  }
  closeModal() {
    this.viewCtrl.dismiss(true);
  }

  checkPush() {
    console.log('this.isPush: '+this.isPush);
    if(this.isPush == true) { 
      this.push_enable = 'YES';
    } else {
      this.push_enable = 'NO';
    }
    this.queryParams = 'email=' +this.userInfoProvider.userProfile.u_email+
                        '&pw=' +this.userInfoProvider.userProfile.u_pw+
                        '&push_enable=' +this.push_enable;
    let body   : string	 = this.queryParams,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.memberURI + "set_member_modify.php";
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
        if (data.result != 'ok') {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: this.msgService.getMessage(data.result),
            buttons: ['확인']
          });
          alert.present();
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: '정상 처리되었습니다.',
            buttons: ['확인']
          });
          alert.present();
        } //====> (data.result == 'ok')
    });
    console.log('this.push_enable: '+this.push_enable);
  }

  termsUse() {
    //data.policy_list.type == '이용';
    let termsUsePage_modal = this.modalCtrl.create('TermsUsePage', {termUseContent: this.outerLinkProvider.termUseContent}); 
    termsUsePage_modal.present(); 
  }

  logout() {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: '로그아웃',
          role: 'destructive',
          handler: () => {

            this.queryParams = 'email=' +this.userInfoProvider.userProfile.u_email+
                               '&pw=' +this.userInfoProvider.userProfile.u_pw+
                               '&type=' +this.userInfoProvider.userProfile.u_type;
            let body   : string	 = this.queryParams,
                type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                headers: any		 = new Headers({ 'Content-Type': type}),
                options: any 		 = new RequestOptions({ headers: headers }),
                url 	 : any		 = this.memberURI + 'set_member_logout.php';
            this.http.post(url, body, options)
            .map(res => res.json())
            .subscribe(data => { 
                if (data.result != 'ok') {
                  let alert = this.alertCtrl.create({
                    title: '알림',
                    subTitle: this.msgService.getMessage(data.result),
                    buttons: ['확인']
                  });
                  alert.present();
                } else {
                    this.loginParams.u_name = this.userInfoProvider.userProfile.u_name;
                    this.loginParams.u_phone_num = this.userInfoProvider.userProfile.u_phone_num;
                    this.loginParams.u_email = this.userInfoProvider.userProfile.u_email;
                    this.loginParams.u_pw = this.userInfoProvider.userProfile.u_pw;
                    this.loginParams.u_photo = this.userInfoProvider.userProfile.u_photo;
                    this.loginParams.u_type = this.userInfoProvider.userProfile.u_type;
                    this.loginParams.cu_flag = this.userInfoProvider.userProfile.cu_flag;
                    this.loginParams.fb_flag = this.userInfoProvider.userProfile.fb_flag;
                    this.loginParams.gg_flag = this.userInfoProvider.userProfile.gg_flag;
                    this.loginParams.kk_flag = this.userInfoProvider.userProfile.kk_flag;
                    this.loginParams.login_flag = false;
                    this.loginParams.isMember_flag = true;
                    this.userInfoProvider.setProfile(this.loginParams);
                    this.chatsCountProvider.logoutChatsCount(); //Reset 0 to chats count
                    let alert = this.alertCtrl.create({
                      title: '알림',
                      subTitle: '정상 처리되었습니다.',
                      buttons: ['확인']
                    });
                    alert.present();
                    this.viewCtrl.dismiss(false);
                } 
            }); //====> (subscribe(data => {)
          }
        },{
          text: '취소',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  memberLeave() {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: '회원탈퇴',
          role: 'destructive',
          handler: () => {
            this.queryParams = 'email=' +this.userInfoProvider.userProfile.u_email+
                                '&pw=' +this.userInfoProvider.userProfile.u_pw;

            let body   : string	 = this.queryParams,
                type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                headers: any		 = new Headers({ 'Content-Type': type}),
                options: any 		 = new RequestOptions({ headers: headers }),
                url 	 : any		 = this.memberURI + 'set_member_leave.php';
            this.http.post(url, body, options)
            .map(res => res.json())
            .subscribe(data => { 
                if (data.result != 'ok') {
                  let alert = this.alertCtrl.create({
                    title: '알림',
                    subTitle: this.msgService.getMessage(data.result),
                    buttons: ['확인']
                  });
                  alert.present();
                } else {
                    this.userInfoProvider.clearProfile(); //Clear UserProfile
                    this.chatsCountProvider.logoutChatsCount(); //Reset 0 to chats count
                    let alert = this.alertCtrl.create({
                      title: '알림',
                      subTitle: '정상 처리되었습니다.',
                      buttons: ['확인']
                    });
                    alert.present();
                    this.viewCtrl.dismiss(false);
                } 
            }); //====> (subscribe(data => {)
          }
        },{
          text: '취소',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

}
