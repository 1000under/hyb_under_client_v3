import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetOthersPage } from './set-others';

@NgModule({
  declarations: [
    SetOthersPage,
  ],
  imports: [
    IonicPageModule.forChild(SetOthersPage),
  ],
  exports: [
    SetOthersPage
  ]
})
export class SetOthersPageModule {}
