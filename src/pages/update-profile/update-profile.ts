import { Component } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, ViewController, ModalController, LoadingController, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ImagePicker } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { DeviceProvider } from '../../providers/device/device';
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';

@IonicPage()
@Component({
  selector: 'page-update-profile',
  templateUrl: 'update-profile.html',
  providers: [ImagePicker]
})
export class UpdateProfilePage {

  //-------------------------------------------------> for data query
  queryParams: any;
  private memberURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";

  //-------------------------------------------------> loginParams for userInfoProvider
  loginParams: any = [
    { u_name:         '' },
    { u_phone_num:    '' },
    { u_email:        '' },
    { u_pw:           '' },
    { u_photo:        '' },
    { u_type:         '' },
    { cu_flag:        false},
    { fb_flag:        false},
    { kk_flag:        false},
    { gg_flag:        false},
    { login_flag:     false},
    { isMember_flag:   false},
  ];

  login_flag: boolean;
  isMember_flag: boolean;

  kk_flag: boolean;
  delFlag: boolean = false;
  userImgFlag: boolean = false;
  userImgReplaceFlag: boolean = false;

  getUserImgUrl: any;
  userImgFileName: string = 'noImg';

  snsUser: any;
  isPhotoDelete: boolean;
  isPush: boolean;
  
  name: any = '';
  phoneNumber: any = '';
  email: any = '';
  password: any = '';
  photo_user : any = [];    //--> Blob file for 사용자
  photo_delete: any = '';   //(YES => 1, NO => 2 입니다)

  constructor( 
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public viewCtrl           : ViewController,
    public alertCtrl          : AlertController,
    public modalCtrl          : ModalController,
    public loadingCtrl        : LoadingController,
    public http               : Http,
    public platform           : Platform,
    public deviceProvider     : DeviceProvider,
    public userInfoProvider   : UserInfoProvider,
    public msgService         : ResponseMsgServiceProvider,
    private imagePicker       : ImagePicker,
    private transfer          : Transfer, 
    private file              : File, 
    private filePath          : FilePath,
    private ga                : GoogleAnalytics,) {

  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateProfilePage');
  }

  ionViewDidEnter() {
    this.ga.trackView('프로필수정');  //trackView for Google Analytics
    this.login_flag = this.userInfoProvider.userProfile.login_flag;
    this.isMember_flag = this.userInfoProvider.userProfile.isMember_flag;
    this.kk_flag = this.userInfoProvider.userProfile.kk_flag;

    if(this.userInfoProvider.userProfile.u_type == '1') {  
      this.snsUser = false;
    } else {
      this.snsUser = true;
    }

    this.queryParams = 'email=' +this.userInfoProvider.userProfile.u_email+ 
                    '&pw=' +this.userInfoProvider.userProfile.u_pw;

    let body   : string	 = this.queryParams,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.memberURI + "get_member.php";
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => {
      if(data.result == 'ok') { 
        this.name = data.member.name;
        this.email = data.member.email;
        this.phoneNumber = data.member.phone_num;
        this.password = this.userInfoProvider.userProfile.u_pw;

        if(data.member.photo != "" && data.member.photo != null && (data.member.photo.indexOf('NaN') == -1)) {
          this.getUserImgUrl = data.member.photo;
          this.delFlag = false;         
          this.isPhotoDelete = false;
          this.userImgFlag = true;
          this.photo_delete = '1'; //'NO'
          console.log('data.member.photo: '+data.member.photo);
        } else {
          this.isPhotoDelete = true;
          this.userImgFlag = false;
          this.photo_delete = '2'; //'YES'
        };

      } else {
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: this.msgService.getMessage(data.result),
          buttons: ['확인']
        });
        alert.present();
      }
    });

    console.log('ionViewDidEnter UpdateProfilePage');
  }


  changePassword() {
    let updatePasswordPage_modal = this.modalCtrl.create('UpdatePasswordPage',{ userProfile_pw: this.userInfoProvider.userProfile.u_pw });
    updatePasswordPage_modal.present();
  }

  checkPhoto() {
    console.log('this.isPhotoDelete: '+this.isPhotoDelete);
    if(this.userImgFlag == false && this.isPhotoDelete == false) {
      this.isPhotoDelete = true;
      this.photo_delete = '2';  //'YES'
      this.userImgFlag = false;
    } else {
      if(this.isPhotoDelete == true) { 
        this.photo_delete = '2';  //'YES'
        this.userImgFlag = false;
      } else {
        this.photo_delete = '1';  //'NO'
        this.userImgFlag = true;
      }
    }
    console.log('this.photo_delete: '+this.photo_delete);
  }
  
  updateUserImg() {
    let options = {
        maximumImagesCount: 1, width: 800, height: 800, quality: 100
    };
    this.imagePicker.getPictures(options).then((results) => {
      if(results[0] != undefined) {
        this.getUserImgUrl = results[0];
        console.log('this.getUserImgUrl: '+this.getUserImgUrl);
        this.userImgReplaceFlag = true;
        this.userImgFlag = true;
        this.isPhotoDelete = false;
        if (this.platform.is('android')) {
          this.filePath.resolveNativePath(this.getUserImgUrl)
          .then(filePath => {
            //Create a new name for the Image
            let d = new Date();
            let n = d.getTime();
            this.userImgFileName =  this.userInfoProvider.userProfile.u_email+ "_user_" +n+ ".jpg";  //한글 이름 않됨 !!!!
            this.CreateUserImage(filePath,0);
          });
        } else {
          //Create a new name for the Image
          let d = new Date();
          let n = d.getTime();
          this.userImgFileName =  this.userInfoProvider.userProfile.u_email+ "_user_" +n+ ".jpg";  //한글 이름 않됨 !!!!
          this.CreateUserImage(this.getUserImgUrl,0);
        }
      }
    }, (err) => {
        this.userImgReplaceFlag = false;
        this.userImgFlag = false;
        this.isPhotoDelete = true;
        console.log('imagePicker.getPictures ERROR: '+err);
    });
  }

  //Main Image파일 생성
  public CreateUserImage(fileName,index)
  {
      this.file.resolveLocalFilesystemUrl(fileName).then((fileEntry : any) => {

        fileEntry.file((resFile) => {
          var reader = new FileReader();
          reader.onloadend = (evt: any) => {
            var imgBlob: any = new Blob([evt.target.result], { type: 'image/jpeg' });
            imgBlob.name = fileName;
            this.photo_user[index] = imgBlob;
            console.log('Create UserImage: ' +fileName);
          };
          reader.onerror = (e) => {
            console.log("Failed file read: " + e.toString());
          };
          reader.readAsArrayBuffer(resFile);
        });

      }, (err) => {
        console.log(err);
        alert(JSON.stringify(err))
      });
  }
  
  submitUpdate() {

    console.log('<------------------ Input Items ---------------->');          
    console.log('email: ' +this.userInfoProvider.userProfile.u_email); 
    console.log('pw: ' +this.userInfoProvider.userProfile.u_pw);
    if(this.userImgReplaceFlag == true) {
        console.log('photo: ' +this.userImgFileName);
    }
    console.log('photo_delete: ' +this.photo_delete);
    console.log('name: ' +this.name);
    //console.log('push_enable: ' +this.push_enable);

    if(this.name === '') {
      let alert = this.alertCtrl.create({
        title: '알림!',
        subTitle: '이름을 입력해 주십시요.',
        buttons: ['확인']
      });
      alert.present();
    } else {

      let loading = this.loadingCtrl.create({
          spinner: 'bubbles',
          content: '정보 수정중 입니다...'
      });
      loading.present();
      
      let body = new FormData();
      let xhr = new XMLHttpRequest();
      let env = this;

      ///////////////////////////////////////////////////////////////////////
      body.append('email', this.userInfoProvider.userProfile.u_email);
      body.append('pw', this.userInfoProvider.userProfile.u_pw);
      
      if(this.userImgReplaceFlag == true) {
        body.append('photo', this.photo_user[0], this.userImgFileName);
        this.photo_delete = '1';  //'NO'
      }
      body.append('photo_delete', this.photo_delete);
      body.append('name', this.name);
      //body.append('push_enable', this.push_enable);
      ///////////////////////////////////////////////////////////////////////
      let url_s 	 : any		 = this.memberURI + "set_member_modify.php";

      xhr.open("POST",url_s,true);
      xhr.send(body);

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {  
            loading.dismiss();
            //alert(xhr.responseText); //테스트를 위해 alert으로 바꿨습니다.
            console.log('xhr.responseText'+xhr.responseText);
            let response = JSON.parse(xhr.responseText);  //xhr.responseText{"result":"error_no_parameter"}
            if(response.result == 'ok') {
                //env.loginParams.loginApp = '1000Under';
                env.loginParams.u_name = env.name;
                env.loginParams.u_phone_num = env.phoneNumber;
                env.loginParams.u_email = env.email;
                env.loginParams.u_pw = env.password;
                if(env.photo_delete == '2') {  //'YES'
                  env.loginParams.u_photo = 'NaN';
                }
                env.loginParams.u_type = env.userInfoProvider.userProfile.u_type;            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                if(env.userInfoProvider.userProfile.u_type == '1') {
                  env.loginParams.cu_flag = true;
                } else if(env.userInfoProvider.userProfile.u_type == '2') {
                  env.loginParams.fb_flag = true;
                } else if(env.userInfoProvider.userProfile.u_type == '3') {
                  env.loginParams.kk_flag = true;
                } else if(env.userInfoProvider.userProfile.u_type == '4') {
                  env.loginParams.gg_flag = true;
                }
                env.loginParams.login_flag = true;
                env.loginParams.isMember_flag = true;
                env.userInfoProvider.setProfile(env.loginParams);
                let alert = env.alertCtrl.create({
                  title: '알림',
                  subTitle: '프로필 수정이 완료되었습니다.',
                  buttons: ['확인']
                });
                alert.present();
                env.viewCtrl.dismiss(env.loginParams.login_flag);
              } else {
                let alert = env.alertCtrl.create({
                  title: '알림',
                  subTitle: env.msgService.getMessage(response.result),
                  buttons: ['확인']
                });
                alert.present();
              }
          } else {
            console.log('xhr.status != 200 : '+xhr.status)
          }
        } // end of if (xhr.readyState == 4)
      } //--> end of xhr.onreadystatechange = function ()   
    }      
  }

  dismiss() {
    this.viewCtrl.dismiss(this.login_flag);
  }

}
