import { Component, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IonicPage, Platform, ViewController, NavController, ModalController, AlertController, NavParams, Slides, } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { FCM } from '@ionic-native/fcm';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { DeviceProvider } from '../../providers/device/device';
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ChatsCountProvider } from '../../providers/chats-count/chats-count';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import { OuterLinkProvider } from '../../providers/outer-link/outer-link';
import { MakerModelProvider } from '../../providers/maker-model/maker-model';
import { SetanalyticsProvider } from '../../providers/setanalytics/setanalytics';

declare var google;

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  @ViewChild(Slides) slides: Slides;

  login_flag: boolean;
  isMember_flag: boolean;

  //-------------------------------------------------> for data query
  queryParams: any;
  queryLength: number = 0;
  private boardURI : string  = "http://cjsekfvm.cafe24.com/test_server/app/board/url/";
  private marketURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";
  carDetailParam: any;

  recommend_list: any =[];  //추천매물 data.recommend_list
  recommendFlag: boolean;
  pick_list: any =[];       //모아보기 data.pick_list
  notices: any = [];        //공지시항 data.board_list. "type_board":"공지"
  tips: any = [];           //TIP    data.board_list. "type_board":"팁"
  faqs: any = [];           //Q&A    data.board_list. "type_board":"QA"
  faqsMoreFlag: boolean = false;
  carDetailTabFlag: boolean = false;
  clickUnder1000: boolean = false;

  constructor(
    public platform           : Platform,
    public viewCtrl           : ViewController, 
    public navCtrl            : NavController, 
    public modalCtrl          : ModalController,
    public alertCtrl          : AlertController,
    public http               : Http,
    public navParams          : NavParams,
    private fcm               : FCM,
    private storage           : Storage,
    private ga                : GoogleAnalytics,
    private geolocation       : Geolocation,
    public deviceProvider     : DeviceProvider,
    public userInfoProvider   : UserInfoProvider,
    public chatsCountProvider: ChatsCountProvider,
    public msgService         : ResponseMsgServiceProvider,
    public outerLinkProvider  : OuterLinkProvider,
    public makerModelProvider : MakerModelProvider,
    public setAnalytics       : SetanalyticsProvider,) {

    }

  ionViewDidLoad() {
    this.showIntro();
    setTimeout(() => { this.getBoardList(); }, 400);
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    console.log('ionViewDidLoad HomePage');
  }

  showIntro() {
    this.storage.get('showIntro').then((val) => {
      console.log('----------> showIntro value: '+val);
      if (val != 0) {
        let IntroPage_modal = this.modalCtrl.create('IntroPage');
        IntroPage_modal.present();
      }
      this.checkLocationService();
    });
  }

  ionViewDidEnter() {
    this.ga.trackView('홈');  //trackView for Google Analytics
    console.log('----------> GoogleAnalytics trackView: HomePage');

    if(this.userInfoProvider.userProfile.login_flag) {
      this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
    }
    this.initPushNotification();
    console.log('ionViewDidEnter HomePage');
  }

  checkLocationService() {
    this.geolocation.getCurrentPosition({timeout:7000}).then((success) => {
      console.log('----------> LocationService ON');
    }, (error) => {
      console.log('==========> LocationService OFF');
        if(this.platform.is('ios')) {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: '지도에서 차량의 위치를 확인/등록하려면 휴대폰의 설정->개인정보보호 에서 "위치서비스"를 켜야 합니다.',
            buttons: ['확인']
          });
          alert.present();
        } else if(this.platform.is('android')) {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: '지도에서 차량의 위치를 확인/등록하려면 휴대폰에서 "위치서비스"를 켜야 합니다.',
            buttons: ['확인']
          });
          alert.present();
        } else {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: '지도에서 차량의 위치를 확인/등록하려면 "위치서비스"를 켜야 합니다.',
            buttons: ['확인']
          });
          alert.present();
        }
    });
    
  }

  getBoardList() {
    if(this.userInfoProvider.userProfile.login_flag == true) {
      this.queryParams = 'device=' +this.deviceProvider.d_Type + '&email=' +this.userInfoProvider.userProfile.u_email;
    } else {
      this.queryParams = 'device=' +this.deviceProvider.d_Type;
    }

    let body_s   : string	 = this.queryParams,
        type_s 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers_s: any		 = new Headers({ 'Content-Type': type_s}),
        options_s: any 		 = new RequestOptions({ headers: headers_s }),
        url_s 	 : any		 = this.boardURI + "get_board_list.php";
    this.http.post(url_s, body_s, options_s)
    .map(res => res.json())
    .subscribe(data => { 
        if (data.result != 'ok') {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: this.msgService.getMessage(data.result),
            buttons: ['확인']
          });
          alert.present();
        } else {
          for (let i=0; i < data.recommend_list.length; i++) {
            data.recommend_list[i].maker = this.makerModelProvider.getMakerName(data.recommend_list[i].maker);
            data.recommend_list[i].model = this.makerModelProvider.getModelName(data.recommend_list[i].model);
            this.recommend_list.push(data.recommend_list[i]);
          }
          this.recommend_list = data.recommend_list;
          this.pick_list = data.pick_list;
          let cnt = 0;
          for (let i=0; i < data.board_list.length; i++) {
            if (data.board_list[i].type_board == '공지') {
              this.notices.push(data.board_list[i]);
            } else if (data.board_list[i].type_board == '팁') {
              this.tips.push(data.board_list[i]);
            } else {
              cnt = cnt +1;
              data.board_list[i].title = (cnt).toString()+'. '+data.board_list[i].title;
              this.faqs.push(data.board_list[i]);
            }
          }
          //alert('getBoardList');
        }
    });
  }

  initPushNotification() {
    this.fcm.onNotification().subscribe(data=>{
      if(data.wasTapped){
        if(data.type == 'chat_new') {
          this.userInfoProvider.getProfile();
          console.log('----------> Background notification userProfile.login_flag: '+this.userInfoProvider.userProfile.login_flag);
          if(this.userInfoProvider.userProfile.login_flag) {
            this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
          }
        } //data.type == 'chat_new'
      } else {
        //alert('Received message in Foreground');
        if(data.type == 'chat_new') {
          const dataObj = JSON.stringify(data);
          //console.log('----------> Notification Data: '+dataObj);
          if(this.userInfoProvider.userProfile.login_flag) {
            this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
            let msgObj = JSON.parse(data.message);
            let target_email = msgObj.target_email.replace(/\'/g,''); //특정문자 제거
            let target_name = msgObj.target_name.replace(/\'/g,''); //특정문자 제거
            //console.log('----------> notification target_email: '+target_email);
            let notificationModal_modal = this.modalCtrl.create('NotificationModalPage', 
                {
                  target_email: target_email,
                  target_name: target_name
                });
            let closedByTimeout = false;
            let timeoutHandle = setTimeout(() => { closedByTimeout = true; notificationModal_modal.dismiss(); }, 5000);
      
            notificationModal_modal.onDidDismiss(() => {
              if (closedByTimeout) return;  //timeout 동안 action이 없으면 return
              clearTimeout(timeoutHandle);
            });
            notificationModal_modal.present();
          }
        } else {
          /*
          const dataObj = JSON.stringify(data);
          //console.log('----------> Banner Data: '+dataObj);
          let msgObj = JSON.parse(data.message);
          let target_email = msgObj.target_email.replace(/\'/g,''); //특정문자 제거
          let target_name = msgObj.target_name.replace(/\'/g,''); //특정문자 제거
          let bannerModal_modal = this.modalCtrl.create('BannerModalPage');
          bannerModal_modal.present();
          */
        }//data.type != 'chat_new'
      }
    });
  }

  naviCarDetailPage(selectedCar) {
    this.ga.trackEvent('추천매물', '추천매물: '+selectedCar.maker+'-'+selectedCar.model );  //trackEvent for Google Analytics
    if(!this.carDetailTabFlag) {
      this.carDetailTabFlag = true;
      if(this.userInfoProvider.userProfile.login_flag == true) {
        this.carDetailParam = 'sale=' +selectedCar.key+ '&email=' +this.userInfoProvider.userProfile.u_email;
      } else {
        this.carDetailParam = 'sale=' +selectedCar.key;
      }
      let body   : string	 = this.carDetailParam,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.marketURI + "get_sale_detail.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result != 'ok') {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: this.msgService.getMessage(data.result),
              buttons: ['확인']
            });
            alert.present();
          } else {
            let set_key = selectedCar;
            let set_type = '1'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
            this.setAnalytics.setAnalytics(set_key, set_type);
            let CarDetailPage_modal = this.modalCtrl.create('CarDetailPage', {carParam: data.sale_detail});
            CarDetailPage_modal.onDidDismiss(data => {
              this.carDetailTabFlag = false;
            });
            CarDetailPage_modal.present();
          }
      });
    }
  }

  selectedPickList(selectedPick){
    this.ga.trackEvent('모아보기', '모아보기: '+selectedPick.title );  //trackEvent for Google Analytics
    let categoryViewPage_modal = this.modalCtrl.create('CategoryViewPage',{categoryParam: selectedPick});
    categoryViewPage_modal.present();
  }

  selectedNotice(selectedNotice){ //Modal
    this.ga.trackEvent('공지사항', '공지사항: '+selectedNotice.title );  //trackEvent for Google Analytics
    let noticeModalPage_modal = this.modalCtrl.create('NoticePage',{notice_param: selectedNotice});
    noticeModalPage_modal.present();
  }
  
  selectedTip(selectedTip){ //Modal
    let tipModalPage_modal = this.modalCtrl.create('TipPage',{tip_param: selectedTip});
    tipModalPage_modal.present();
  }

  selectedFAQ(selectedFAQ) {//Modal
    let whatServicePage_modal = this.modalCtrl.create('WhatServicePage', {naviParam: this.faqs});
    whatServicePage_modal.present();
  }

  termsUse() {
    //data.policy_list.type == '이용';
    let termsUsePage_modal = this.modalCtrl.create('TermsUsePage', {termUseContent: this.outerLinkProvider.termUseContent}); 
    termsUsePage_modal.present(); 
  }

  privacyPolicy() {
    //data.policy_list.type == '개인';
    let privacyPolicyPage_modal = this.modalCtrl.create('PrivacyPolicyPage', {privacyPolicyContent: this.outerLinkProvider.privacyPolicyContent}); 
    privacyPolicyPage_modal.present();  
  }
  
  under1000() {
    this.clickUnder1000 = !this.clickUnder1000;
  }
}