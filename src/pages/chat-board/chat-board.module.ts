import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatBoardPage } from './chat-board';

@NgModule({
  declarations: [
    ChatBoardPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatBoardPage),
  ],
  exports: [
    ChatBoardPage
  ]
})
export class ChatBoardPageModule {}
