import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, Content, Events, Platform } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { FCM } from '@ionic-native/fcm';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers & Pipes
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import { ChatsCountProvider } from '../../providers/chats-count/chats-count';

//declare var FCMPlugin;

@IonicPage()
@Component({
  selector: 'page-chat-board',
  templateUrl: 'chat-board.html'
})
export class ChatBoardPage {

  @ViewChild(Content) content: Content;
  
  //-------------------------------------------------> for data query
  queryParams: any;
  private messageURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/message/url/";

  u_email: any;
  u_pw: any;
  u_photo: any;
  target_email: any;
  currentPage: any;

  message_detail_list: any = [];
  message_text: any;

  //-------------------------------------------------> loginParams for userInfoProvider
  loginParams: any = [
    { loginApp:       '' },
    { u_name:         '' },
    { u_phone_num:    '' },
    { u_email:        '' },
    { u_pw:           '' },
    { u_photo:        '' },
    { u_type:         '' },
    { cu_flag:        false},
    { fb_flag:        false},
    { kk_flag:        false},
    { gg_flag:        false},
    { login_flag:     false},
    { isMember_flag:  false},
  ];

  constructor(
    public platform: Platform,
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public params: NavParams,
    public http: Http,
    public events: Events,
    private zone: NgZone,
    public userInfoProvider: UserInfoProvider,
    public msgService: ResponseMsgServiceProvider,
    public chatsCountProvider: ChatsCountProvider,
    private fcm: FCM,
    private ga: GoogleAnalytics,) {
      this.u_email = this.params.get('email');
      this.u_pw = this.params.get('pw');
      this.target_email = this.params.get('target_email');
  }

  ionViewDidLoad() {
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    this.currentPage = 'ChatBoardPage';
    this.getMessage(this.target_email);
    //this.initPushNotification();
    console.log('ionViewDidLoad ChatBoardPage');
  }
  
  ionViewDidEnter() {
    this.ga.trackView('채팅보드');  //trackView for Google Analytics
    this.scrollToBottom();
    this.initPushNotification();
  }

  initPushNotification() {
    this.fcm.onNotification().subscribe(data=>{
      if(!data.wasTapped){
        if(data.type == 'chat_new') {
          //alert('Received message at ChatBoardPage in Foreground');
          if(this.currentPage == 'ChatBoardPage') {
            let msgObj = JSON.parse(data.message);
            let target_email = msgObj.target_email.replace(/\'/g,''); //특정문자 제거
            this.reflashPage(target_email);
          }
        }
      };
    });
  }

  reflashPage(target_email) {
    this.zone.run(() => {
      this.getMessage(target_email);
      console.log('Force update ======> ChatBoardPage');
    });
  }

  getMessage(target_email) {
      this.queryParams = 'email=' +this.u_email+ '&pw=' +this.u_pw+ '&target_email=' +target_email;  
      let body   : string	 = this.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.messageURI + "get_message_detail.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result != 'ok') {
            console.log('Nothing message: '+data.result);
          } else {
            this.message_detail_list = data.message_detail_list;
            this.message_detail_list.sort(function(a,b) {
                if ( a.regist_date < b.regist_date )
                    return -1;
                if ( a.regist_date > b.regist_date )
                    return 1;
                return 0;
            });
            for (let i = 0; i < this.message_detail_list.length; i++) {
              //2017-08-09 11:48:37 --> date: 'MM/dd H:mm'
              let temp_date = this.message_detail_list[i].regist_date.substr(5, 2)+'/'+this.message_detail_list[i].regist_date.substr(8, 2)+' '
                             +this.message_detail_list[i].regist_date.substr(11, 2)+':'+this.message_detail_list[i].regist_date.substr(14, 2);
              this.message_detail_list[i].regist_date = temp_date;
            }
            this.message_text = '';
            console.log('Got messages detail !! ');
            //this.scrollToBottom();
            setTimeout(() => this.scrollToBottom(), 0);
          }
      });
  }
  
  
  scrollToBottom() {
    setTimeout(() => {
        this.content.scrollToBottom();
    });
  }
  
  input_focus(e) {
    setTimeout(() => this.scrollToBottom(), 0);
  }

  chatSend(){
    if(this.message_text != undefined && this.message_text != '') {
      this.queryParams = 'email=' +this.u_email+ '&pw=' +this.u_pw+ '&target_email=' +this.target_email+ '&message_text=' +this.message_text;  
      let body   : string	 = this.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.messageURI + "set_message_add.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result != 'ok') {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: this.msgService.getMessage(data.result),
              buttons: ['확인']
            });
            alert.present();
          } else {
            console.log('Saved messages detail');
            this.reflashPage(this.target_email);
          }
      });
    }
  }

  dismiss() {
    this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
    this.viewCtrl.dismiss();
  }

}
