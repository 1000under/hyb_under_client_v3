import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, AlertController, ActionSheetController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http'; 
import { Facebook,FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { KakaoTalk } from 'ionic-plugin-kakaotalk';
import { LoadingController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { DeviceProvider } from '../../providers/device/device';
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ChatsCountProvider } from '../../providers/chats-count/chats-count';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import CryptoJS from 'crypto-js';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [GooglePlus]
})
export class LoginPage {

  //-------------------------------------------------> for input focus
  @ViewChild('f_email') f_email;
  @ViewChild('f_password') f_password;

  //-------------------------------------------------> for data query
  queryParams: any;
  private memberURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";

  //-------------------------------------------------> loginParams for userInfoProvider
  loginParams: any = [
    { u_name:         '' },
    { u_phone_num:    '' },
    { u_email:        '' },
    { u_pw:           '' },
    { u_photo:        '' },
    { u_type:         '' },
    { cu_flag:        false},
    { fb_flag:        false},
    { kk_flag:        false},
    { gg_flag:        false},
    { login_flag:     false},
    { isMember_flag:  false},
  ];

  FB_APP_ID: number = 412877125738651;

  cu_flag: boolean;
  fb_flag: boolean;
  kk_flag: boolean;
  gg_flag: boolean;
  login_flag: boolean;
  isMember_flag: boolean;

  naviPage: any;

  otherPageFlag: boolean;
  isPhoto:boolean;
  photo: any;
  name: any;
  email: any;
  password: any;
  signUpFlag : boolean = false;


  SECERET_KEY: string = '1000underappadminwebpageaeskey15';


  constructor(
    public viewCtrl           : ViewController, 
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public http               : Http,
    public loadingCtrl        : LoadingController,
    public alertCtrl          : AlertController,
    public modalCtrl          : ModalController,
    public deviceProvider     : DeviceProvider,
    public userInfoProvider   : UserInfoProvider,
    public chatsCountProvider: ChatsCountProvider,
    public msgService         : ResponseMsgServiceProvider,
    public actionSheetCtrl    : ActionSheetController,
    private googlePlus        : GooglePlus,
    private facebook          : Facebook,
    private kakaoTalk         : KakaoTalk,
    private ga                : GoogleAnalytics,) {
      this.naviPage = this.navParams.get('naviPage');
    }

  ionViewDidLoad() {
    if(this.naviPage == 'ProfilePage') {
      this.otherPageFlag = false;
    } else {
      this.otherPageFlag = true;
    }
    this.login_flag = this.userInfoProvider.userProfile.login_flag;
    this.isMember_flag = this.userInfoProvider.userProfile.isMember_flag;
    console.log('ionViewDidLoad LoginPage');
  }

  ionViewDidEnter() {
    this.ga.trackView('로그인');  //trackView for Google Analytics
  }

  //-------------------------------------------------> for next input focus
  f_email_eventHandler(e) {
    if(e.keyCode == 13) {
      this.f_password.setFocus();
    }
  }

  DoMemberLogin(){
    console.log('Login DoMemberLogin ============> deviceProvider.deviceInfo.d_pushKey  ==> '+this.deviceProvider.deviceInfo.d_pushKey);
    if(this.email == undefined || this.password == undefined) {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '이메일, 비밀번호를 입력하세요.',
          buttons: ['확인']
      });
      alert.present();
    } else {
      this.queryParams = 'email='         +this.email+
                        '&pw='            +this.password+
                        '&type='          +'1'+       //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                        '&push_key='      +this.deviceProvider.deviceInfo.d_pushKey+
                        '&photo='         +this.userInfoProvider.userProfile.u_photo+     //사진경로
                        '&push_enable='   +'YES'+       //push enable YES, NO
                        '&device='        +this.deviceProvider.deviceInfo.d_Type+
                        '&device_num='    +this.deviceProvider.deviceInfo.d_UUID+
                        '&version='       +this.deviceProvider.deviceInfo.s_version;
      let body   : string	 = this.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.memberURI + 'set_member_login.php';
          this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
        if (data.result != 'ok') {
          console.log('Failed!! 회원 로그인 ==>' +data.result);
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: this.msgService.getMessage(data.result),
            buttons: ['확인']
          });
          alert.present();
        } else {
         console.log('Success!! 회원 로그인');
          //this.loginParams.loginApp = '1000Under';          
          this.loginParams.u_name = this.userInfoProvider.userProfile.u_name;
          this.loginParams.u_phone_num = this.userInfoProvider.userProfile.u_phone_num;
          this.loginParams.u_email = this.email;
          this.loginParams.u_pw = this.password;
          this.loginParams.u_photo = this.userInfoProvider.userProfile.u_photo;
          this.loginParams.u_type = '1';                  //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
          this.loginParams.cu_flag = true;
          this.loginParams.login_flag = true;
          this.loginParams.isMember_flag = true;
          this.userInfoProvider.setProfile(this.loginParams);
          this.chatsCountProvider.checkChatsCount(this.loginParams.login_flag, this.email, this.password);
          this.viewCtrl.dismiss(this.loginParams.login_flag);
        }
      });
    }

  }

  resetPassword() {
    let resetPasswordPage_modal = this.modalCtrl.create('ResetPasswordPage');
    resetPasswordPage_modal.onDidDismiss(data => {
      this.login_flag = data;
    });
    resetPasswordPage_modal.present(); 
  }
  
  //페이스북 로그인
  DoFbLogin() {
    if(this.userInfoProvider.userProfile.isMember_flag == false || this.userInfoProvider.userProfile.isMember_flag == undefined ||
      this.userInfoProvider.userProfile.login_flag == false || this.userInfoProvider.userProfile.login_flag == undefined){
      let permissions = new Array();
      //the permissions your facebook app needs from the user
      //permissions = ['public_profile', 'user_friends', 'email'];
      permissions = ["public_profile", "email"];
      let env = this;

      env.facebook.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
            env.facebook.api('me?fields=email,name', null).then(
            (profileData) => {
              /*
                if(profileData.email == 'icksykim@naver.com') {
                  profileData.email = 'icksykim2@naver.com';
                }
              */
                console.log('facebook email + ==>'+profileData.email);
                let picture = 'NaN'
                if(env.userInfoProvider.userProfile.u_photo != 'NaN') {
                  picture = "https://graph.facebook.com/" + res.authResponse.userID + "/picture?type=large";
                }
                let facebook_pw = res.authResponse.accessToken;
                
                if(profileData.email == undefined) {
                  let alert = env.alertCtrl.create({
                    title: '알림',
                    subTitle: '확인되지 않은 Facebook email입니다.',
                    buttons: ['확인']
                  });
                  alert.present();
                } else {

                    env.queryParams = 'email='          +profileData.email+
                                      '&pw='            +facebook_pw+
                                      '&type='          +'2'+                   //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                                      '&push_key='      +env.deviceProvider.deviceInfo.d_pushKey+
                                      '&photo='         +picture+ 
                                      '&push_enable='   +'YES'+                                       //push enable 1: YES, 2: NO
                                      '&device='        +env.deviceProvider.deviceInfo.d_Type+
                                      '&device_num='    +env.deviceProvider.deviceInfo.d_UUID+
                                      '&version='       +env.deviceProvider.deviceInfo.s_version; 
                    let body   : string	 = env.queryParams,
                        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                        headers: any		 = new Headers({ 'Content-Type': type}),
                        options: any 		 = new RequestOptions({ headers: headers }),
                        url 	 : any		 = env.memberURI + "set_member_login.php";
                    env.http.post(url, body, options)
                    .map(res => res.json())
                    .subscribe(data => { 
                        if (data.result != 'ok') {
                          let alert = env.alertCtrl.create({
                            title: '알림',
                            subTitle: env.msgService.getMessage(data.result),
                            buttons: ['확인']
                          });
                          alert.present();
                        } else { 
                          //env.loginParams.loginApp = 'Facebook';
                          env.loginParams.u_name = profileData.name;
                          env.loginParams.u_phone_num = 'facebook';
                          env.loginParams.u_email = profileData.email;
                          env.loginParams.u_pw = facebook_pw; 
                          env.loginParams.u_photo = picture;
                          env.loginParams.u_type = '2';            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                          env.loginParams.fb_flag = true;
                          env.loginParams.login_flag = true;
                          env.loginParams.isMember_flag = true;
                          env.userInfoProvider.setProfile(env.loginParams);
                          env.chatsCountProvider.checkChatsCount(this.loginParams.login_flag, profileData.email, facebook_pw);
                          env.viewCtrl.dismiss(env.loginParams.login_flag);
                        }
                    }); 

                } // check profileData.email
                
            },(err) => {
              console.log(JSON.stringify(err));
              let alert = env.alertCtrl.create({
                title: '알림',
                subTitle: 'Facebook 연동에 실패하였습니다.',
                buttons: ['확인']
              });
              alert.present();
            });
      }).catch(e => {
        let alert = env.alertCtrl.create({
          title: '알림',
          subTitle: 'Facebook 연동에 실패하였습니다.',
          buttons: ['확인']
        });
        alert.present();
        console.log('Error logging into Facebook' + e)
      });
    }
  }

  //구글 로그인
  DoGoogleLogin(){
    if(this.userInfoProvider.userProfile.isMember_flag == false || this.userInfoProvider.userProfile.isMember_flag == undefined ||
      this.userInfoProvider.userProfile.login_flag == false || this.userInfoProvider.userProfile.login_flag == undefined){
      let env = this;
      let loading = env.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();
      env.googlePlus.login({
        'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
        'webClientId': '674574852016-ddcao4cj7qgief2auch1u6sh8env2j1d.apps.googleusercontent.com',  // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.r iOS
        'offline': true
      })
      .then(function (user) {
        loading.dismiss();
        if(user.imageUrl == undefined || user.imageUrl.indexOf('photo.jpg') != -1 || env.userInfoProvider.userProfile.u_photo == 'NaN') {
          user.imageUrl = 'NaN';
        }
        console.log('user_data.userId : '+user.userId);
        console.log('user_data.displayName : '+user.displayName);
        console.log('user_data.email : '+user.email);
        console.log('user_data.imageUrl : '+user.imageUrl);
        console.log('user_data.accessToken : '+user.accessToken); //not response
        console.log('user_data.idToken : '+user.idToken);

        let google_pw = user.idToken;
        //let google_pw = user.accessToken;
        console.log('Google pw idToken  ==>'+google_pw);
        
        env.queryParams = 'email='          +user.email+
                          '&pw='            +google_pw+
                          '&type='          +'4'+                   //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                          '&push_key='      +env.deviceProvider.deviceInfo.d_pushKey+
                          '&photo='         +user.imageUrl+ 
                          '&push_enable='   +'YES'+                                       //push enable 1: YES, 2: NO
                          '&device='        +env.deviceProvider.deviceInfo.d_Type+
                          '&device_num='    +env.deviceProvider.deviceInfo.d_UUID+
                          '&version='       +env.deviceProvider.deviceInfo.s_version;

        console.log('env.queryParams ==>'+env.queryParams);

        let body   : string	 = env.queryParams,
            type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
            headers: any		 = new Headers({ 'Content-Type': type}),
            options: any 		 = new RequestOptions({ headers: headers }),
            url 	 : any		 = env.memberURI + "set_member_login.php";
        env.http.post(url, body, options)
        .map(res => res.json())
        .subscribe(data => { 
            console.log('data.result ==>'+data.result);
            if (data.result != 'ok') {
              let alert = env.alertCtrl.create({
                title: '알림',
                subTitle: env.msgService.getMessage(data.result),
                buttons: ['확인']
              });
              alert.present();
            } else { 
              //env.loginParams.loginApp = 'Google';
              env.loginParams.u_name = user.displayName;
              env.loginParams.u_phone_num = 'google';
              env.loginParams.u_email = user.email;
              env.loginParams.u_pw = google_pw; 
              env.loginParams.u_photo = user.imageUrl;
              env.loginParams.u_type = '4';            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
              env.loginParams.gg_flag = true;
              env.loginParams.login_flag = true;
              env.loginParams.isMember_flag = true;
              env.userInfoProvider.setProfile(env.loginParams);
              env.chatsCountProvider.checkChatsCount(env.loginParams.login_flag, user.email, google_pw);
              env.viewCtrl.dismiss(env.loginParams.login_flag);
            } 
        });
    
      }, function (error) {
        let alert = env.alertCtrl.create({
          title: '알림',
          subTitle: 'Google 연동에 실패하였습니다.',
          buttons: ['확인']
        });
        alert.present();
        console.log('Google userInfo return error: ' +error);
        loading.dismiss();
      });
    }
  }

  DoKakaoLogin() {
    this.kakaoTalk.login()
    .then(response => {
      // 로그인 결과와 엑세스 토큰을 promise 형태로 반환한다
      return new Promise(resolve => {
        this.kakaoTalk.getAccessToken().then(accessToken => resolve(Object.assign({}, response, { accessToken })))
      })
    })
    .then(({ accessToken, id, nickname, profile_image }) => {
      let env = this;
      // 유저 정보를 출력
      console.log('kakao_id :' + id);
      console.log('kakao_nickname :' + nickname);
      console.log('kakao_profile_image :' + profile_image);
      console.log('kakao_accessToken :' + accessToken);
      //console.log('kakao_email :' + email);
      if(profile_image == undefined || env.userInfoProvider.userProfile.u_photo == 'NaN') {
        profile_image = 'NaN';
      }
      let kakao_pw = accessToken;        
      env.queryParams = 'email='          +id+
                        '&pw='            +kakao_pw+
                        '&type='          +'3'+                   //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                        '&push_key='      +env.deviceProvider.deviceInfo.d_pushKey+
                        '&photo='         +profile_image+ 
                        '&push_enable='   +'YES'+                                       //push enable 1: YES, 2: NO
                        '&device='        +env.deviceProvider.deviceInfo.d_Type+
                        '&device_num='    +env.deviceProvider.deviceInfo.d_UUID+
                        '&version='       +env.deviceProvider.deviceInfo.s_version;

      let body   : string	 = env.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = env.memberURI + "set_member_login.php";
      env.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          console.log('data.result ==>'+data.result);
          if (data.result != 'ok') {
            let alert = env.alertCtrl.create({
              title: '알림',
              subTitle: env.msgService.getMessage(data.result),
              buttons: ['확인']
            });
            alert.present();
          } else { 
            //env.loginParams.loginApp = 'Kakao';
            env.loginParams.u_name = nickname;
            env.loginParams.u_phone_num = 'kakao';
            env.loginParams.u_email = id;
            env.loginParams.u_pw = kakao_pw; 
            env.loginParams.u_photo = profile_image;
            env.loginParams.u_type = '3';            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
            env.loginParams.kk_flag = true;
            env.loginParams.login_flag = true;
            env.loginParams.isMember_flag = true;
            env.userInfoProvider.setProfile(env.loginParams);
            env.chatsCountProvider.checkChatsCount(env.loginParams.login_flag, id, kakao_pw);
            env.viewCtrl.dismiss(env.loginParams.login_flag);
          } 
      });

    })
    .catch(error => {
      let alert = this.alertCtrl.create({
        title: '알림',
        subTitle: '카카오톡 연동에 실패하였습니다.',
        buttons: ['확인']
      });
      alert.present();
      console.log('Error logging into KakaoTalk' + error);
    });
  }

  public GetPasswordEncrypted(password: string) {
    // Encrypt
    var Encrypttext = String(CryptoJS.AES.encrypt(password, this.SECERET_KEY));
    return Encrypttext;
  }

  signUp() {//Modal
    let signUpPage_modal = this.modalCtrl.create('SignUpPage');
    signUpPage_modal.onDidDismiss(data => {
      this.login_flag = data;
      this.viewCtrl.dismiss(this.login_flag);
    });
    signUpPage_modal.present();
  }  

  closeModal() {
    this.viewCtrl.dismiss(this.login_flag);
  }

}
