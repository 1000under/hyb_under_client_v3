import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';


@IonicPage()
@Component({
  selector: 'page-search-location',
  templateUrl: 'search-location.html',
})
export class SearchLocationPage {

  area: any = '전국';
  viewArea: any = '';
  areaLevel1 = [
    { area: "전국" }, { area: "서울특별시" }, { area: "경기도" }, { area: "강원도" }, { area: "충청북도" }, { area: "충청남도" }, 
    { area: "경상북도" }, { area: "경상남도" }, { area: "전라북도" }, { area: "전라남도" }, { area: "제주특별자치도" }, 
    { area: "인천광역시" }, { area: "부산광역시" }, { area: "대전광역시" }, { area: "울산광역시" }, { area: "대구광역시" }, 
    { area: "광주광역시" }
  ];

  constructor(
    public viewCtrl: ViewController, 
    public navCtrl: NavController, 
    public navParams: NavParams,
    private ga: GoogleAnalytics,) {
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchLocationPage');
  }
  ionViewDidEnter() {
    this.ga.trackView('지역선택');  //trackView for Google Analytics
  }
  selectedArea(e, selectedArea){
    this.area = selectedArea;
    console.log('this.area:' +this.area);
    this.viewCtrl.dismiss(this.area);
  }
  closeModal() {
    this.viewCtrl.dismiss(this.area);
  }
}
