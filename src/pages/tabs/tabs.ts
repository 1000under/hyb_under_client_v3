import { Component } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';


@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'HomePage';
  tab2Root = 'CarsPage';
  tab3Root = 'FavoritePage';
  tab4Root = 'ChatsPage';
  tab5Root = 'ProfilePage';

  public unread_count: number = 0;

  constructor(
    public events: Events,) {
      this.events.subscribe('chat:updated', (count) => {
        this.unread_count = count;
      });
  }

}
