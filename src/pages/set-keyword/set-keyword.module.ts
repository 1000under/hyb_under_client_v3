import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetKeywordPage } from './set-keyword';

@NgModule({
  declarations: [
    SetKeywordPage,
  ],
  imports: [
    IonicPageModule.forChild(SetKeywordPage),
  ],
  exports: [
    SetKeywordPage
  ]
})
export class SetKeywordPageModule {}
