import { Component, NgZone, } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';

@IonicPage()
@Component({
  selector: 'page-set-keyword',
  templateUrl: 'set-keyword.html',
})
export class SetKeywordPage {

  //-------------------------------------------------> for data query
  queryParams: any;
  private memberURI 		: string  = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";

  showKeywordFlag: any = false;
  //keyword_list = [ "중대형차", "SUV", "자동변속기", "이벤트" ];
  keyword_list: any =[];
  keyword: any = '';

  constructor(
    public viewCtrl           : ViewController, 
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public alertCtrl          : AlertController,
    public http               : Http,
    private zone              : NgZone,
    public userInfoProvider   : UserInfoProvider,
    public msgService         : ResponseMsgServiceProvider,
    private ga                : GoogleAnalytics,) {

    }
  
  ionViewDidLoad() {
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    this.getKeyword();
    console.log('ionViewDidLoad SetKeywordPage');
  }
  ionViewDidEnter() {
    this.ga.trackView('관심키워드설정');  //trackView for Google Analytics
  }
  reflashPage() {
    this.zone.run(() => {
      this.getKeyword();
      //console.log('Force update ======> ChatBoardPage');
    });
  }

  getKeyword() {
      this.queryParams = 'email=' +this.userInfoProvider.userProfile.u_email+ 
                         '&pw=' +this.userInfoProvider.userProfile.u_pw;
      let body   : string	 = this.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.memberURI + "get_member.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          if(data.keyword_list != undefined) {
            this.keyword_list = data.keyword_list;
          }
      });
  }

  addKeyword() {   
    //console.log('this.addKeyword: ' +this.keyword);
    let value = this.keyword.replace(/\s/g,'');    //문자열 내의 공백 제거
    if(this.keyword === '' || value === '') {
      let alert = this.alertCtrl.create({
        title: '알림!',
        subTitle: '키워드를 입력해 주세요.',
        buttons: ['확인']
      });
      alert.present();
    } else {
      this.keyword_list.push(value);
      this.submitKeyword();
    }
    this.keyword = '';
  }

  removeKeyWord(selectedKeyword){
    let index = this.keyword_list.indexOf(selectedKeyword);
    if(index > -1){
          this.keyword_list.splice(index, 1);
    }
    this.submitKeyword();
    //console.log('Remove this.keyword_list: ' +this.keyword_list);
    this.keyword = '';
  }

  submitKeyword(){
    if(this.keyword_list == null) {
        let alert = this.alertCtrl.create({
          title: '알림!',
          subTitle: '키워드를 입력해 주십시요.',
          buttons: ['확인']
        });
        alert.present();
    } else {
      this.queryParams = 'email=' +this.userInfoProvider.userProfile.u_email+
                          '&pw=' +this.userInfoProvider.userProfile.u_pw+
                          '&keywords=' +this.keyword_list;
      let body   : string	 = this.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.memberURI + "set_member_keyword.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result != 'ok') {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: this.msgService.getMessage(data.result),
              buttons: ['확인']
            });
            alert.present();
          } else {
            //console.log('Add this.keyword_list to Server' +this.keyword_list);
            this.reflashPage();  
          }
      });
    };  //====> else end.
  }   //====> submit logic end.

  closeModal() {
    this.viewCtrl.dismiss();
  }

}
