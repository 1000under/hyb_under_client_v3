import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import { SetanalyticsProvider } from '../../providers/setanalytics/setanalytics';

@IonicPage()
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {

  queryParams: any;
  private marketURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";

  sale: any;
  email: any;
  pw: any;
  type: any = '';
  memo: any = '';

  reportTypes = [
    { id: "01", type: "판매완료" },
    { id: "02", type: "딜러매물" },
    { id: "03", type: "정보오류" },
    { id: "04", type: "허위매물" },
    { id: "05", type: "기타" },
  ]

  constructor(
    public navCtrl:     NavController, 
    public navParams:   NavParams,
    public viewCtrl:    ViewController,
    public alertCtrl:   AlertController,
    public http:        Http,
    public msgService:  ResponseMsgServiceProvider,
    public setAnalytics: SetanalyticsProvider,
    private ga: GoogleAnalytics,) {

      this.sale = navParams.get("sale");
      this.email = navParams.get("email");
      this.pw = navParams.get("pw");
      //console.log('navParams.get: '+this.sale+' - '+this.email+' - '+this.pw);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportPage');
  }
  ionViewDidEnter() {
    this.ga.trackView('차량신고');  //trackView for Google Analytics
  }
  submitReport(){
    if(this.type == undefined || this.type == ''){
      let alert = this.alertCtrl.create({
        title: '알림',
        subTitle: '신고 타입을 선택해 주세요.',
        buttons: ['확인']
      });
      alert.present();      
    } else if(this.type == '기타' && (this.memo == undefined || this.memo == '')){
      let alert = this.alertCtrl.create({
        title: '알림',
        subTitle: '신고 내용을 입력해 주세요.',
        buttons: ['확인']
      });
      alert.present();      
    } else {
      console.log('this.type:' +this.type);
      this.queryParams = 'sale='+this.sale+
                        '&type='+this.type+
                        '&email='+this.email+
                        '&pw='+this.pw+
                        '&memo='+this.memo;
      //----------------------------------------------------------------> get Similar cars
      let body_s   : string	 = this.queryParams,
          type_s 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers_s: any		 = new Headers({ 'Content-Type': type_s}),
          options_s: any 		 = new RequestOptions({ headers: headers_s }),
          url_s 	 : any		 = this.marketURI + "set_sale_report.php";
      this.http.post(url_s, body_s, options_s)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result != 'ok') {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: this.msgService.getMessage(data.result),
              buttons: ['확인']
            });
            alert.present();
          } else {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: '정상 처리 되었습니다.',
              buttons: ['확인']
            });
            alert.present();

            let set_key = this.sale;
            let set_type = '4'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
            this.setAnalytics.setAnalytics(set_key, set_type);
            
            this.viewCtrl.dismiss('1'); //reported
          }
      });
    }
  }

  closeModal() {
    this.viewCtrl.dismiss('0'); //cancel
  }

}
