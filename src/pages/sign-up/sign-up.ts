import { Component, ViewChild } from '@angular/core';
import { IonicPage, Platform, ViewController, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Facebook,FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { KakaoTalk } from 'ionic-plugin-kakaotalk';
import { LoadingController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { DeviceProvider } from '../../providers/device/device';
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import { OuterLinkProvider } from '../../providers/outer-link/outer-link';
import CryptoJS from 'crypto-js';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
  providers: [GooglePlus]
})
export class SignUpPage {
  //-------------------------------------------------> for input focus
  @ViewChild('f_name') f_name;
  @ViewChild('f_phoneNumber') f_phoneNumber;
  @ViewChild('f_email') f_email;
  @ViewChild('f_password') f_password;
  @ViewChild('f_confirmPassword') f_confirmPassword;

  //-------------------------------------------------> for data query
  queryParams: any;
  private memberURI 		: string  = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";

  loginParams: any = [
    { u_name:         '' },
    { u_phone_num:    '' },
    { u_email:        '' },
    { u_pw:           '' },
    { u_photo:        '' },
    { u_type:         '' },
    { cu_flag:        false},
    { fb_flag:        false},
    { kk_flag:        false},
    { gg_flag:        false},
    { login_flag:     false},
    { isMember_flag:  false},
  ];
  
  FB_APP_ID: number = 412877125738651;
  SECERET_KEY: string = '1000underappadminwebpageaeskey15';

  name: any = '';
  phoneNumber: any = '';
  email: any = '';
  password: any = '';
  confirmPassword: any = '';

  termsUseFlag: boolean = false;
  privacyPolicyFlag: boolean = false;

  constructor(
    public viewCtrl           : ViewController, 
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public modalCtrl          : ModalController,
    public alertCtrl          : AlertController,
    public loadingCtrl        : LoadingController,
    public http               : Http,
    public platform           : Platform,
    public deviceProvider     : DeviceProvider,
    public userInfoProvider   : UserInfoProvider,
    public msgService         : ResponseMsgServiceProvider,
    public outerLinkProvider  : OuterLinkProvider,
    private googlePlus        : GooglePlus,
    private facebook          : Facebook,
    private kakaoTalk         : KakaoTalk,
    private ga                : GoogleAnalytics,) {

    }

  ionViewDidLoad() {
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    //this.f_name.setFocus();
    console.log('ionViewDidLoad SignUpPage');
  }
  ionViewDidEnter() {
    this.ga.trackView('회원가입');  //trackView for Google Analytics
  }
  //-------------------------------------------------> for next input focus
  f_name_eventHandler(e) {
    if(e.keyCode == 13) {
      this.f_phoneNumber.setFocus();
    }
  }
  f_phoneNumber_eventHandler(e) {
    if(e.keyCode == 13) {
      if(!this.chk_phoneNumber(this.phoneNumber)) {
        let alert = this.alertCtrl.create({
          title: '알림!',
          subTitle: '잘못된 전화번호입니다. 숫자만 입력해주세요',
          buttons: ['확인']
        });
        alert.present();
        this.phoneNumber = '';
      } else {
        this.f_email.setFocus();
      }
    }
  }
  f_email_eventHandler(e) {
    if(e.keyCode == 13) {
      if(!this.chk_email(this.email)) {
        let alert = this.alertCtrl.create({
          title: '알림!',
          subTitle: '잘못된 이메일 주소입니다.',
          buttons: ['확인']
        });
        alert.present();
        this.email = '';
      } else {
        this.f_password.setFocus();
      }
    }
  }
  f_password_eventHandler(e) {
    if(e.keyCode == 13) {
      this.f_confirmPassword.setFocus();
    }
  }
  //-------------------------------------------------> for next input focus

  chk_phoneNumber(phoneNumber) {   
    console.log('check this.phoneNumber :' +phoneNumber);
    var regExp = /^[0-9]+$/;
    if ( !regExp.test(phoneNumber) ) {
      return false;
    } else {
      return true;
    }
  }  

  chk_email(email) {   
    console.log('check this.email :' +email);
    var regExp =  /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
    if ( !regExp.test(this.email) ) {
      return false;
    } else {
      return true;
    } 
  }

  termsUseCheck(){
    this.termsUseFlag = !this.termsUseFlag;
  } 
  termsUse(){ //Modal
    //data.policy_list.type == '이용';
    let termsUsePage_modal = this.modalCtrl.create('TermsUsePage', {termUseContent: this.outerLinkProvider.termUseContent}); 
    termsUsePage_modal.present(); 
  }

  privacyPolicyCheck(){
    this.privacyPolicyFlag = !this.privacyPolicyFlag;
  }  
  privacyPolicy(){ //Modal
    //data.policy_list.type == '개인';
    let privacyPolicyPage_modal = this.modalCtrl.create('PrivacyPolicyPage', {privacyPolicyContent: this.outerLinkProvider.privacyPolicyContent}); 
    privacyPolicyPage_modal.present(); 
  }  

  submitRegiste(){
    if(!this.termsUseFlag || !this.privacyPolicyFlag) {
      let alert = this.alertCtrl.create({
        title: '알림!',
        subTitle: '천언더 이용약관 및 개인정보 수집,이용에 대해 동의하셔야 합니다.',
        buttons: ['확인']
      });
      alert.present();
    } else if(this.name === '' || this.phoneNumber === '' || this.email === '' ||
        this.password === '' || this.confirmPassword === '') {
        let alert = this.alertCtrl.create({
          title: '알림!',
          subTitle: '모든 내용을 입력해 주십시요.',
          buttons: ['확인']
        });
        alert.present();
    } else if(!this.chk_phoneNumber(this.phoneNumber)) {
      let alert = this.alertCtrl.create({
        title: '알림!',
        subTitle: '잘못된 전화번호입니다. 숫자만 입력해주세요',
        buttons: ['확인']
      });
      alert.present();
      this.phoneNumber = '';
    } else if(!this.chk_email(this.email)) {
      let alert = this.alertCtrl.create({
        title: '알림!',
        subTitle: '잘못된 이메일 주소입니다.',
        buttons: ['확인']
      });
      alert.present();
      this.email = '';
    } else if(this.password != this.confirmPassword) {
        let alert = this.alertCtrl.create({
          title: '알림!',
          subTitle: '"확인 비밀번호"가 올바르지 않습니다.',
          buttons: ['확인']
        });
        alert.present();
        this.confirmPassword = '';
    } else {
      let chkPwdResualt = chkPwd(this.password);
      if(chkPwdResualt != 'good'){
        let alert = this.alertCtrl.create({
          title: '알림!',
          subTitle: chkPwdResualt,
          buttons: ['확인']
        });
        alert.present();
        this.f_password.setFocus();
      } else {  //====> submit logic start.

            this.queryParams = 'email=' +this.email+
                              '&pw=' +this.password+
                              '&push_key=' +this.deviceProvider.deviceInfo.d_pushKey+  //this.deviceProvider.deviceInfo.d_pushKey
                              '&type=' +'1'+ //1:차체, 2:페이스북, 3:구글, 4:카카오, 5:기타
                              '&name=' +this.name+
                              '&photo=' +'NaN'+
                              '&device=' +this.deviceProvider.deviceInfo.d_Type+  //this.deviceProvider.deviceInfo.d_Type
                              '&device_num=' +this.deviceProvider.deviceInfo.d_UUID+  //this.deviceProvider.deviceInfo.d_UUID
                              '&phone_num=' +this.phoneNumber+
                              '&version=' +this.deviceProvider.deviceInfo.s_version;

            let body   : string	 = this.queryParams,
                type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                headers: any		 = new Headers({ 'Content-Type': type}),
                options: any 		 = new RequestOptions({ headers: headers }),
                url 	 : any		 = this.memberURI + "set_member_join.php";
            this.http.post(url, body, options)
            .map(res => res.json())
            .subscribe(data => { 
                if (data.result != 'ok') {
                  let alert = this.alertCtrl.create({
                    title: '알림',
                    subTitle: this.msgService.getMessage(data.result),
                    buttons: ['확인']
                  });
                  alert.present();
                  this.viewCtrl.dismiss(this.loginParams.cu_flag);
                } else {
                  //this.loginParams.loginApp = '1000Under';
                  this.loginParams.u_name = this.name;
                  this.loginParams.u_phone_num = this.phoneNumber;
                  this.loginParams.u_email = this.email;
                  this.loginParams.u_pw = this.password; 
                  this.loginParams.u_photo = 'NaN';
                  this.loginParams.u_type = '1';            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                  this.loginParams.cu_flag = true;
                  this.loginParams.login_flag = true;
                  this.loginParams.isMember_flag = true;
                  this.userInfoProvider.setProfile(this.loginParams);
                  let alert = this.alertCtrl.create({
                    title: '알림',
                    subTitle: '회원 등록이 완료되었습니다.',
                    buttons: ['확인']
                  });
                  alert.present();
                  this.viewCtrl.dismiss(this.loginParams.login_flag);  //---> Closed Modal SignUpPage and goto ProfilePage
                } //====> (data.result == 'ok')
            });
          };  //====> submit logic end.
          
      }

      function chkPwd(str){
        let pw = str;
        let num = pw.search(/[0-9]/g);
        let eng = pw.search(/[a-z]/ig);
        //let spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

        if(pw.length < 6 || pw.length > 15){
          return '비밀번호는 영.숫자 포함 6자리 ~ 15자리 이내로 입력해주세요.';
        }
        if(pw.search(/₩s/) != -1){
          return '비밀번호는 공백없이 입력해주세요.';
        } 
        if(num < 0 || eng < 0){
          return '비밀번호는 영문.숫자를 혼합하여 입력해주세요.';
        }
        return 'good';
      };
  }

  DoFbSignup() {
    if(!this.termsUseFlag || !this.privacyPolicyFlag) {
      let alert = this.alertCtrl.create({
        title: '알림!',
        subTitle: '천언더 이용약관 및 개인정보 수집,이용에 대해 동의하셔야 합니다.',
        buttons: ['확인']
      });
      alert.present();
    } else {
      let permissions = new Array();
      //the permissions your facebook app needs from the user
      //permissions = ['public_profile', 'user_friends', 'email'];
      permissions = ["public_profile", "email"];
      let env = this;
      env.facebook.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
        env.facebook.api('me?fields=email,name', null).then(
        (profileData) => {
            console.log('facebook email + ==>'+profileData.email);
            let picture = "https://graph.facebook.com/" + res.authResponse.userID + "/picture?type=large";
            let facebook_pw = res.authResponse.accessToken;                
            if(profileData.email == undefined) {
              let alert = env.alertCtrl.create({
                title: '알림',
                subTitle: '확인되지 않은 Facebook email입니다.',
                buttons: ['확인']
              });
              alert.present();
            } else {

              env.queryParams = 'email=' + profileData.email +  //user.email
                    '&pw=' + facebook_pw + 
                    '&push_key=' +env.deviceProvider.deviceInfo.d_pushKey+  //env.deviceProvider.deviceInfo.d_pushKey
                    '&type=' +'2'+ //1:차체, 2:페이스북, 3:구글, 4:카카오, 5:기타
                    '&name=' + profileData.name+
                    '&photo=' + picture +
                    '&device=' +env.deviceProvider.deviceInfo.d_Type+  //env.deviceProvider.deviceInfo.d_Type
                    '&device_num=' +env.deviceProvider.deviceInfo.d_UUID+  //env.deviceProvider.deviceInfo.d_UUID
                    '&phone_num=' +'facebook'+
                    '&version=' +env.deviceProvider.deviceInfo.s_version;

              let body   : string	 = env.queryParams,
                  type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                  headers: any		 = new Headers({ 'Content-Type': type}),
                  options: any 		 = new RequestOptions({ headers: headers }),
                  url 	 : any		 = env.memberURI + "set_member_join.php";
              env.http.post(url, body, options)
              .map(res => res.json())
              .subscribe(data => { 
                  if (data.result != 'ok') {
                    let alert = env.alertCtrl.create({
                      title: '알림',
                      subTitle: env.msgService.getMessage(data.result),
                      buttons: ['확인']
                    });
                    alert.present();
                  } else { 
                    //now we have the users info, let's save it in the ionicStorage thru userInfoProvider
                    //env.loginParams.loginApp = 'Facebook';
                    env.loginParams.u_name = profileData.name;
                    env.loginParams.u_phone_num = 'facebook';
                    env.loginParams.u_email = profileData.email;
                    env.loginParams.u_pw = facebook_pw; 
                    env.loginParams.u_photo = picture;
                    env.loginParams.u_type = '2';            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                    env.loginParams.fb_flag = true;
                    env.loginParams.login_flag = true;
                    env.loginParams.isMember_flag = true;
                    env.userInfoProvider.setProfile(env.loginParams);
                    env.viewCtrl.dismiss(env.loginParams.login_flag);
                  } 
              });

            } // check profileData.email
            
        },(err) => {
          console.log(JSON.stringify(err));
          let alert = env.alertCtrl.create({
            title: '알림',
            subTitle: 'Facebook 연동에 실패하였습니다.',
            buttons: ['확인']
          });
          alert.present();
        });
      }).catch(e => {
        let alert = env.alertCtrl.create({
          title: '알림',
          subTitle: 'Facebook 연동에 실패하였습니다.',
          buttons: ['확인']
        });
        alert.present();
        console.log('Error logging into Facebook' + e)
      });
    }
  }
  //구글 로그인
  DoGoogleSignup(){
    if(!this.termsUseFlag || !this.privacyPolicyFlag) {
      let alert = this.alertCtrl.create({
        title: '알림!',
        subTitle: '천언더 이용약관 및 개인정보 수집,이용에 대해 동의하셔야 합니다.',
        buttons: ['확인']
      });
      alert.present();
    } else {
      let env = this;
      let loading = env.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();
      env.googlePlus.login({
        'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
        'webClientId': '674574852016-ddcao4cj7qgief2auch1u6sh8env2j1d.apps.googleusercontent.com',  // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.r iOS
        'offline': true
      })
      .then(function (user) {
        loading.dismiss();
        if(user.imageUrl == undefined || user.imageUrl.indexOf('photo.jpg') != -1) {
          user.imageUrl = 'NaN';
        }
        console.log('user_data.userId : '+user.userId);
        console.log('user_data.displayName : '+user.displayName);
        console.log('user_data.email : '+user.email);
        console.log('user_data.imageUrl : '+user.imageUrl);
        console.log('user_data.accessToken : '+user.accessToken);
        console.log('user_data.idToken : '+user.idToken);

        let google_pw = user.idToken;
        //let google_pw = user.accessToken;
        console.log('Google pw idToken ======>: ' +google_pw);
        env.queryParams = 'email=' +user.email+
                          '&pw=' +google_pw+
                          '&push_key=' +env.deviceProvider.deviceInfo.d_pushKey+  //env.deviceProvider.deviceInfo.d_pushKey
                          '&type=' +'4'+              //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                          '&name=' +user.displayName+
                          '&photo=' +user.imageUrl+
                          '&device=' +env.deviceProvider.deviceInfo.d_Type+  //env.deviceProvider.deviceInfo.d_Type
                          '&device_num=' +env.deviceProvider.deviceInfo.d_UUID+  //env.deviceProvider.deviceInfo.d_UUID
                          '&phone_num=' +'google'+
                          '&version=' +env.deviceProvider.deviceInfo.s_version;
        let body   : string	 = env.queryParams,
            type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
            headers: any		 = new Headers({ 'Content-Type': type}),
            options: any 		 = new RequestOptions({ headers: headers }),
            url 	 : any		 = env.memberURI + "set_member_join.php";
        env.http.post(url, body, options)
        .map(res => res.json())
        .subscribe(data => { 
            if (data.result != 'ok') {
              let alert = env.alertCtrl.create({
                title: '알림',
                subTitle: env.msgService.getMessage(data.result),
                buttons: ['확인']
              });
              alert.present();
            } else {
              //now we have the users info, let's save it in the ionicStorage thru userInfoProvider
              //env.loginParams.loginApp = 'Google';
              env.loginParams.u_name = user.displayName;
              env.loginParams.u_phone_num = 'google';
              env.loginParams.u_email = user.email;
              env.loginParams.u_pw = google_pw; 
              env.loginParams.u_photo = user.imageUrl;
              env.loginParams.u_type = '4';            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
              env.loginParams.gg_flag = true;
              env.loginParams.login_flag = true;
              env.loginParams.isMember_flag = true;
              env.userInfoProvider.setProfile(env.loginParams);
              env.viewCtrl.dismiss(env.loginParams.login_flag);
            } 
        }); 
    
      }, function (error) {
        let alert = env.alertCtrl.create({
          title: '알림',
          subTitle: 'Google 연동에 실패하였습니다.',
          buttons: ['확인']
        });
        alert.present();
        console.log('Google userInfo return error: ' +error);
        loading.dismiss();
      });
    }
  }

  DoKakaoSignup() {
    if(!this.termsUseFlag || !this.privacyPolicyFlag) {
      let alert = this.alertCtrl.create({
        title: '알림!',
        subTitle: '천언더 이용약관 및 개인정보 수집,이용에 대해 동의하셔야 합니다.',
        buttons: ['확인']
      });
      alert.present();
    } else {
      this.kakaoTalk.login()
      .then(response => {
        // 로그인 결과와 엑세스 토큰을 promise 형태로 반환한다
        return new Promise(resolve => {
          this.kakaoTalk.getAccessToken().then(accessToken => resolve(Object.assign({}, response, { accessToken })))
        })
      })
      .then(({ accessToken, id, nickname, profile_image }) => {
        let env = this;
        // 유저 정보를 출력
        console.log('kakao_id :' + id);
        console.log('kakao_nickname :' + nickname);
        console.log('kakao_profile_image :' + profile_image);
        console.log('kakao_accessToken :' + accessToken);
        //console.log('kakao_email :' + email);
        if(profile_image == undefined) {
          profile_image = 'NaN';
        }
        let kakao_pw = accessToken;        
        env.queryParams = 'email='          +id+
                          '&pw='            +kakao_pw+
                          '&push_key='      +env.deviceProvider.deviceInfo.d_pushKey+
                          '&type='          +'3'+                   //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                          '&name='          +nickname+
                          '&photo='         +profile_image+ 
                          '&device='        +env.deviceProvider.deviceInfo.d_Type+
                          '&device_num='    +env.deviceProvider.deviceInfo.d_UUID+
                          '&phone_num='     +'kakao'+
                          '&version='       +env.deviceProvider.deviceInfo.s_version;
        let body   : string	 = env.queryParams,
            type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
            headers: any		 = new Headers({ 'Content-Type': type}),
            options: any 		 = new RequestOptions({ headers: headers }),
            url 	 : any		 = env.memberURI + "set_member_join.php";
        env.http.post(url, body, options)
        .map(res => res.json())
        .subscribe(data => { 
            console.log('data.result ==>'+data.result);
            if (data.result != 'ok') {
              let alert = env.alertCtrl.create({
                title: '알림',
                subTitle: env.msgService.getMessage(data.result),
                buttons: ['확인']
              });
              alert.present();
            } else { 
              //env.loginParams.loginApp = 'Kakao';
              env.loginParams.u_name = nickname;
              env.loginParams.u_phone_num = 'kakao';
              env.loginParams.u_email = id;
              env.loginParams.u_pw = kakao_pw; 
              env.loginParams.u_photo = profile_image;
              env.loginParams.u_type = '3';            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
              env.loginParams.kk_flag = true;
              env.loginParams.login_flag = true;
              env.loginParams.isMember_flag = true;
              env.userInfoProvider.setProfile(env.loginParams);
              env.viewCtrl.dismiss(env.loginParams.login_flag);
            } 
        });

      })
      .catch(error => {
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '카카오톡 연동에 실패하였습니다.',
          buttons: ['확인']
        });
        alert.present();
        console.log('Error logging into KakaoTalk' + error);
      });
    }    
  }

  public GetPasswordEncrypted(password: string) {
    // Encrypt
    var Encrypttext = String(CryptoJS.AES.encrypt(password, this.SECERET_KEY));
    return Encrypttext;
  }

  closeModal() {
    this.viewCtrl.dismiss(this.loginParams.login_flag);
  }
}
