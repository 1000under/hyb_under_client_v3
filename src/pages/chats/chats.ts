import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, App, NavParams, Platform, ViewController, ModalController, AlertController, } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { FCM } from '@ionic-native/fcm';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ChatsCountProvider } from '../../providers/chats-count/chats-count';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';

//declare var FCMPlugin;

@IonicPage()
@Component({
  selector: 'page-chats',
  templateUrl: 'chats.html',
})
export class ChatsPage { 

  login_flag: boolean;
  isMember_flag: boolean;
  u_email: any;
  u_pw: any;

  msgListFlag: boolean = false;

  //-------------------------------------------------> for data query
  queryParams: any;
  private messageURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/message/url/";

  photo: any;
  name: any;
  message_list: any = [];
  target_email: any;
  currentPage: any;

  constructor(
    public platform: Platform,
    public viewCtrl:          ViewController, 
    public navCtrl:           NavController,
    public app:               App, 
    public params:            NavParams,
    public modalCtrl:         ModalController,
    public alertCtrl:         AlertController,
    public http:              Http,
    public userInfoProvider:  UserInfoProvider,
    public chatsCountProvider: ChatsCountProvider,
    public msgService:        ResponseMsgServiceProvider,
    private zone:             NgZone,
    private fcm:              FCM,
    private ga:               GoogleAnalytics,) {
      
  }

  ionViewDidLoad() {
    this.login_flag = this.userInfoProvider.userProfile.login_flag;
    this.isMember_flag = this.userInfoProvider.userProfile.isMember_flag;
    this.u_email = this.userInfoProvider.userProfile.u_email;
    this.u_pw = this.userInfoProvider.userProfile.u_pw;
    this.currentPage = 'ChatsPage';
    console.log('ionViewDidLoad ChatsPage');
  }

  ionViewDidEnter() {
    this.ga.trackView('채팅');  //trackView for Google Analytics
    console.log('----------> GoogleAnalytics trackView: ChatsPage');

    this.login_flag = this.userInfoProvider.userProfile.login_flag;
    this.isMember_flag = this.userInfoProvider.userProfile.isMember_flag;
    this.u_email = this.userInfoProvider.userProfile.u_email;
    this.u_pw = this.userInfoProvider.userProfile.u_pw;
    this.initPushNotification();
    this.getMessageList();
    console.log('ionViewDidEnter ChatsPage');
  }

  initPushNotification() {
    this.fcm.onNotification().subscribe(data=>{
      if(this.login_flag == true) {
        if(!data.wasTapped){
          if(data.type == 'chat_new') {
            //alert('Received message at ChatsPage in Foreground');
            this.reflashPage();
            let msgObj = JSON.parse(data.message);
            let target_email = msgObj.target_email.replace(/\'/g,''); //특정문자 제거
            let target_name = msgObj.target_name.replace(/\'/g,''); //특정문자 제거
            //console.log('notification target_email====> '+target_email);
            let notificationModal_modal = this.modalCtrl.create('NotificationModalPage', 
                {
                  target_email: 'ChatsPage',
                  target_name: target_name
                });
            let closedByTimeout = false;
            let timeoutHandle = setTimeout(() => { closedByTimeout = true; notificationModal_modal.dismiss(); }, 5000);
      
            notificationModal_modal.onDidDismiss(() => {
              if (closedByTimeout) return;  //timeout 동안 action이 없으면 return
              clearTimeout(timeoutHandle);
              this.reflashPage();
            });
            notificationModal_modal.present();
          }
        };
      }
    });
  }

  reflashPage() {
    this.zone.run(() => {
      this.getMessageList();
      console.log('Force update ======> ChatsPage');
    });
  }  
  
  getMessageList() {
    this.queryParams = 'email='+this.u_email+
                        '&pw='+this.u_pw;  
    let body   : string	 = this.queryParams,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.messageURI + "get_message_list.php";
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
      if (data.result != 'ok') {
        this.msgListFlag = false;
      } else {
        this.message_list = data.message_list;
        this.msgListFlag = true;
        this.message_list.sort(function(a,b) {
            if ( a.regist_date < b.regist_date )
                return 1;
            if ( a.regist_date > b.regist_date )
                return -1;
            return 0;
        });
        for (let i = 0; i < this.message_list.length; i++) {
          //2017-08-09 11:48:37 --> date: 'MM/dd H:mm'
          let temp_date = this.message_list[i].regist_date.substr(5, 2)+'/'+this.message_list[i].regist_date.substr(8, 2)+' '
                        +this.message_list[i].regist_date.substr(11, 2)+':'+this.message_list[i].regist_date.substr(14, 2);
          this.message_list[i].regist_date = temp_date;
          //console.log('ChatsPage unread_count ==============>'+this.message_list[i].target_name+': '+this.message_list[i].unread_count);
        }
        this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
      }
      //console.log('ChatsPage msgListFlag ==============>'+this.msgListFlag);
    });  
  }

  checkLogin() {    
    if(this.login_flag == false) {
      let LoginPage_modal = this.modalCtrl.create('LoginPage', {naviPage: 'ChatsPage'});
      LoginPage_modal.onDidDismiss(data => {
        this.login_flag = data;
        if(this.login_flag == true) {
          this.navCtrl.setRoot(this.navCtrl.getActive().component);
        }
      });
      LoginPage_modal.present();
    }
  }

  chatBoard(selectedChat){
    let ChatBoardPage_modal = this.modalCtrl.create('ChatBoardPage',{
      email: this.u_email,
      pw: this.u_pw,
      target_email: selectedChat.target_email,
      unread_count: selectedChat.unread_count
    });
    ChatBoardPage_modal.onDidDismiss(data => {
      console.log('ChatBoardPage Dismiss current page =====>'+ this.currentPage);
      this.initPushNotification();
      this.reflashPage();
      //this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
    });
    ChatBoardPage_modal.present();
  }

  chatBoardInit(){
    let ChatBoardPage_modal = this.modalCtrl.create('ChatBoardPage',{
      email: this.u_email,
      pw: this.u_pw,
      target_email: 'admin@1000under.com'
    });
    ChatBoardPage_modal.onDidDismiss(data => {
      this.initPushNotification();
      this.reflashPage();
      //this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
    });
    ChatBoardPage_modal.present();
  }

  deleteChat(selectedChat) {
    console.log('deleteChat(target_email): ' +selectedChat.target_email);
    this.queryParams = 'email=' +this.u_email+ '&pw=' +this.u_pw+ '&target_email=' +selectedChat.target_email;  
    let body   : string	 = this.queryParams,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.messageURI + "set_message_delete.php";
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
        if (data.result != 'ok') {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: this.msgService.getMessage(data.result),
            buttons: ['확인']
          });
          alert.present();
        } else {
          //this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
          this.getMessageList();
        }
    });

  }

}
