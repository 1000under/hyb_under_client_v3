import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@IonicPage()
@Component({
  selector: 'page-privacy-policy',
  templateUrl: 'privacy-policy.html',
})
export class PrivacyPolicyPage {

  privacyPolicyContent: any = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private ga: GoogleAnalytics,) {
      this.privacyPolicyContent = navParams.get("privacyPolicyContent");
  }

  ionViewDidLoad() {
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    console.log('ionViewDidLoad PrivacyPolicyPage');
  }
  ionViewDidEnter() {
    this.ga.trackView('개인정보취급방침');  //trackView for Google Analytics
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }
}