import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {

  tutorials = [
    { id: '01', imgPath: "img/intro/tutorial1.png"},
    { id: '02', imgPath: "img/intro/tutorial2.png"},
    { id: '03', imgPath: "img/intro/tutorial3.png"},
    { id: '04', imgPath: "img/intro/tutorial4.png"},
    { id: '05', imgPath: "img/intro/tutorial5.png"},
    { id: '06', imgPath: "img/intro/tutorial6.png"},
    { id: '07', imgPath: "img/intro/tutorial7.png"},
    { id: '08', imgPath: "img/intro/tutorial8.png"}
  ]

    public options;
    constructor(
        private navCtrl: NavController,
        public viewCtrl: ViewController,
        private storage: Storage,) {
        this.options = {
            show: false
        };
    }

    doNotShow() {
        if (!this.options.show) {
            //Save flag if don't need to show intro slider on next app startup
            this.storage.set('showIntro', 0).then(() => {
                // we don't actually need this callbacks in such simple example,
                // but in real apps it will be useful
            });
        }
        this.viewCtrl.dismiss();
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

}