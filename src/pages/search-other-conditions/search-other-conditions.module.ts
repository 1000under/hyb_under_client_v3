import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchOtherConditionsPage } from './search-other-conditions';

@NgModule({
  declarations: [
    SearchOtherConditionsPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchOtherConditionsPage),
  ],
  exports: [
    SearchOtherConditionsPage
  ]
})
export class SearchOtherConditionsPageModule {}
