import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController, Slides, AlertController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import numeral from 'numeral';

@IonicPage()
@Component({
  selector: 'page-search-other-conditions',
  templateUrl: 'search-other-conditions.html',
})
export class SearchOtherConditionsPage {
  
  @ViewChild(Slides) slides: Slides;

  selectedType: any = '1';          //------> 전체: 1 Defualt selected
  largeFlag: boolean = false;       //------> 대형: 3
  mediumFlag: boolean = false;      //------> 중형: 3
  semiMediumFlag: boolean = false;  //------> 준중형: 3
  smallFlag: boolean = false;       //------> 소형: 2
  extraSmallFlag: boolean = false;  //------> 경차: 2
  suvFlag: boolean = false;         //------> SUV: 4
  importFlag: boolean = false;      //------> 수입차: 5
  etcFlag: boolean = false;         //------> 기타: 5
  
  priceFlag: boolean = false;
  directPriceFlag: boolean = false;
  inputMinPrice: number;
  inputMaxPrice: number;
  carPrice: any;
  minPrice: number = 0;
  maxPrice: number = 5000;
  viewMinPrice: any;
  viewMaxPrice: any;

  yearFlag: boolean = false;
  directYearFlag: boolean = false;
  inputMinYear: number; 
  inputMaxYear: number; 
  thisYear: any;
  carYear: any;
  minYear: any;
  maxYear: any;
  viewMinYear: any;
  viewMaxYear: any;

  kmFlag: boolean = false;
  directKmFlag: boolean = false;
  inputMinKm: number; 
  inputMaxKm: number; 
  carKm: any;
  minKm: number = 0;
  maxKm: number = 300000;
  viewMinKm: any;
  viewMaxKm: any;

  otherParams = { 
      minPrice: 0, 
      maxPrice: 0, 
      minYear: '', 
      maxYear: '', 
      minKm: 0, 
      maxKm: 0, 
      type: '1',
      puer: '1',
      gear: '1'
  };

  puer: any = '1';
  puers = [
    { id: '1', puer: '전체'},
    { id: '2', puer: '디젤'},
    { id: '3', puer: '가솔린'},
    { id: '4', puer: '하이브리드'},
    { id: '5', puer: '전기'},
    { id: '6', puer: '가스'},
    { id: '7', puer: '기타'}
  ];
  gear: any = '1';
  gears = [
    { id: '1', gear: '전체'},
    { id: '2', gear: '수동'},
    { id: '3', gear: '오토'},
    { id: '4', gear: '기타'}
  ];

  constructor(
    public viewCtrl: ViewController, 
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private ga: GoogleAnalytics,) {
      
      this.carPrice = {
        lower: this.minPrice,
        upper: this.maxPrice
      }
      this.viewMinPrice =this.minPrice;
      this.viewMaxPrice =this.maxPrice;

      this.thisYear = new Date();
      this.thisYear = dateToYYYYMMDD(this.thisYear);
      //----- Date format function-----//
      function dateToYYYYMMDD(date){
        return date.getFullYear();
      }
      this.maxYear = this.thisYear;
      this.minYear = this.thisYear - 20;
      this.carYear = {
        lower: this.minYear,
        upper: this.maxYear
      }
      this.viewMaxYear = this.maxYear;
      this.viewMinYear = this.minYear;

      this.carKm = {
        lower: this.minKm,
        upper: this.maxKm
      }
      this.viewMaxKm = this.maxKm;
      this.viewMinKm = this.minKm;
    }

  ionViewDidEnter() {
    this.ga.trackView('차량검색조건');  //trackView for Google Analytics
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchOtherConditionsPage');
  }

  //차량타입 ---> 1:전체, 2:소형, 3:중대형, 4:SUV, 5:수입차
  largeOnOff() { 
    if( this.largeFlag == false ) { 
      this.largeFlag = true; this.selectedType = '3';
      this.mediumFlag = false; this.semiMediumFlag = false; this.smallFlag = false; this.extraSmallFlag = false;
      this.suvFlag = false; this.importFlag = false; this.etcFlag = false;
      //console.log(this.largeFlag, this.mediumFlag, this.semiMediumFlag, this.smallFlag, this.suvFlag, this.importFlag, this.etcFlag);
    } else { 
      this.largeFlag = false; 
    } 
  }
  mediumOnOff() { 
    if( this.mediumFlag == false ) { 
      this.mediumFlag = true; this.selectedType = '3';
      this.largeFlag = false; this.semiMediumFlag = false; this.smallFlag = false; this.extraSmallFlag = false;
      this.suvFlag = false; this.importFlag = false; this.etcFlag = false;
    } else { 
      this.mediumFlag = false; 
    } 
  }
  semiMediumOnOff() { 
    if( this.semiMediumFlag == false ) { 
      this.semiMediumFlag = true; this.selectedType = '3';
      this.largeFlag = false; this.mediumFlag = false; this.smallFlag = false; this.extraSmallFlag = false;
      this.suvFlag = false; this.importFlag = false; this.etcFlag = false; 
    } else { 
      this.semiMediumFlag = false; 
    } 
  }
  smallOnOff() { 
    if( this.smallFlag == false ) { 
      this.smallFlag = true; this.selectedType = '2'; 
      this.largeFlag = false; this.mediumFlag = false; this.semiMediumFlag = false; this.extraSmallFlag = false;
      this.suvFlag = false; this.importFlag = false; this.etcFlag = false;
    } else { 
      this.smallFlag = false; 
    } 
  }
  extraSmallOnOff() { 
    if( this.extraSmallFlag == false ) { 
      this.extraSmallFlag = true; this.selectedType = '2'; 
      this.largeFlag = false; this.mediumFlag = false; this.semiMediumFlag = false; this.smallFlag = false; 
      this.suvFlag = false; this.importFlag = false; this.etcFlag = false;
    } else { 
      this.extraSmallFlag = false; 
    } 
  }
  suvOnOff() { 
    if( this.suvFlag == false ) { 
      this.suvFlag = true; this.selectedType = '4'; 
      this.largeFlag = false; this.mediumFlag = false; this.semiMediumFlag = false; this.smallFlag = false; 
      this.extraSmallFlag = false; this.importFlag = false; this.etcFlag = false;
    } else { 
      this.suvFlag = false;
    } 
  }
  importOnOff() { 
    if( this.importFlag == false ) { 
      this.importFlag = true; this.selectedType = '5'; 
      this.largeFlag = false; this.mediumFlag = false; this.semiMediumFlag = false; this.smallFlag = false; 
      this.extraSmallFlag = false; this.suvFlag = false; this.etcFlag = false;
    } else { 
      this.importFlag = false; 
    } 
  }
  etcOnOff() { 
    if( this.etcFlag == false ) { 
      this.etcFlag = true; this.selectedType = '5'; 
      this.largeFlag = false; this.mediumFlag = false; this.semiMediumFlag = false; this.smallFlag = false; 
      this.extraSmallFlag = false; this.suvFlag = false; this.importFlag = false;
    } else { 
      this.etcFlag = false; 
    } 
  }

  directPrice() {
    this.directPriceFlag = true;
    let loginPrompt = this.alertCtrl.create({
      title: '차량 가격(만원)',
      message: '원하시는 최소/최대 값을 입력하세요.',
      inputs: [
        {
          name: 'minPrice',
          placeholder: '최소 가격',
          type: 'number'
        },
        {
          name: 'maxPrice',
          placeholder: '최대 가격',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: '취소',
          handler: promptData => {
            console.log('Cancel clicked');
          }
        },  // End of '취소'
        {
          text: '확인',
          handler: promptData => {
            let myNumeral = numeral(promptData.minPrice);
            let value = myNumeral.value();
            console.log('this.myNumeral minPrice:' +value);
            if(value == null || value == 'NaN') {
              this.inputMinPrice = 0;
            } else {
              this.inputMinPrice = Number(value);
            }

            myNumeral = numeral(promptData.maxPrice);
            value = myNumeral.value();
            console.log('this.myNumeral maxPrice:' +value);
            if(value == null || value == 'NaN') {
              this.inputMaxPrice = 0;
            } else {
              this.inputMaxPrice = Number(value);
            }
            if (this.inputMinPrice > this.inputMaxPrice) {
              let alert = this.alertCtrl.create({
                title: '알림',
                subTitle: '최소값/최대값을 확인하세요.',
                buttons: ['확인']
              });
              alert.present();
            } else {
              this.carPrice = {
                lower: this.inputMinPrice,
                upper: this.inputMaxPrice
              }            
              this.updatePrice(this.inputMinPrice, this.inputMaxPrice);
            }
          }
        }
      ]
    });
    loginPrompt.present();
  }
  updatePrice(lower, upper){
    this.viewMinPrice = lower;
    this.viewMaxPrice = upper;
    this.carPrice.lower = lower;
    this.carPrice.upper = upper;
    if(this.viewMinPrice == this.minPrice && this.viewMaxPrice == this.maxPrice) {
      this.priceFlag = false;
    } else {
      this.priceFlag = true;
    }
  }

  directYear() {
    this.directYearFlag = true;
    let loginPrompt = this.alertCtrl.create({
      title: '연식(연도)',
      message: '원하시는 최소/최대 값을 입력하세요.',
      inputs: [
        {
          name: 'minYear',
          placeholder: '최소 연식',
          type: 'number'
        },
        {
          name: 'maxYear',
          placeholder: '최대 연식',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: '취소',
          handler: promptData => {
            console.log('Cancel clicked');
          }
        },  // End of '취소'
        {
          text: '확인',
          handler: promptData => {
            let myNumeral = numeral(promptData.minYear);
            let value = myNumeral.value();
            console.log('this.myNumeral minYear:' +value);
            if(value == null || value == 'NaN') {
              this.inputMinYear = 0;
            } else {
              this.inputMinYear = Number(value);
            }

            myNumeral = numeral(promptData.maxYear);
            value = myNumeral.value();
            console.log('this.myNumeral maxPrice:' +value);
            if(value == null || value == 'NaN') {
              this.inputMaxYear = 0;
            } else {
              this.inputMaxYear = Number(value);
            }
            if (this.inputMinYear > this.inputMaxYear) {
              let alert = this.alertCtrl.create({
                title: '알림',
                subTitle: '최소값/최대값을 확인하세요.',
                buttons: ['확인']
              });
              alert.present();
            } else {            
              this.carYear = {
                lower: this.inputMinYear,
                upper: this.inputMaxYear
              }               
              this.updateYear(this.inputMinYear, this.inputMaxYear);
            }
          }
        }
      ]
    });
    loginPrompt.present();
  }
  updateYear(lower, upper){
    this.viewMinYear = lower;
    this.viewMaxYear = upper;
    this.carYear.lower = lower;
    this.carYear.upper = upper;
    if(this.viewMinYear == this.minYear && this.viewMaxYear == this.maxYear) {
      this.yearFlag = false;
    } else {
      this.yearFlag = true;
    }
  }

  directKm(){
    this.directKmFlag = true;
    let loginPrompt = this.alertCtrl.create({
      title: '주행 거리(Km)',
      message: '원하시는 최소/최대 값을 입력하세요.',
      inputs: [
        {
          name: 'minKm',
          placeholder: '최소 주행 거리',
          type: 'number'
        },
        {
          name: 'maxKm',
          placeholder: '최대 주행 거리',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: '취소',
          handler: promptData => {
            console.log('Cancel clicked');
          }
        },  // End of '취소'
        {
          text: '확인',
          handler: promptData => {
            let myNumeral = numeral(promptData.minKm);
            let value = myNumeral.value();
            console.log('this.myNumeral minKm:' +value);
            if(value == null || value == 'NaN') {
              this.inputMinKm = 0;
            } else {
              this.inputMinKm = Number(value);
            }

            myNumeral = numeral(promptData.maxKm);
            value = myNumeral.value();
            console.log('this.myNumeral maxKm:' +value);
            if(value == null || value == 'NaN') {
              this.inputMaxKm = 0;
            } else {
              this.inputMaxKm = Number(value);
            }
            if (this.inputMinKm > this.inputMaxKm) {
              let alert = this.alertCtrl.create({
                title: '알림',
                subTitle: '최소값/최대값을 확인하세요.',
                buttons: ['확인']
              });
              alert.present();
            } else {
              this.carKm = {
                lower: this.inputMinKm,
                upper: this.inputMaxKm
              }
              this.updateKm(this.inputMinKm, this.inputMaxKm);
            }
          }
        }
      ]
    });
    loginPrompt.present();
  }
  updateKm(lower, upper){
    this.viewMinKm = lower;
    this.viewMaxKm = upper; 
    this.carKm.lower = lower;
    this.carKm.upper = upper;
    if(this.viewMinKm == this.minKm && this.viewMaxKm == this.maxKm) {
      this.kmFlag = false;
    } else {
      this.kmFlag = true;
    }
  }

  selectedPuer(e) {  //-------------------------------------------------------------------> selectedpuer()
    console.log('car_puer:' +this.puer);
  }
  selectedGear() {  //-----------------------------------------------------------> selectedselectedgear()
    console.log('car_gear:' +this.gear);
  }

  selected() {
    if((this.inputMinPrice > this.inputMaxPrice) || (this.inputMinYear > this.inputMaxYear) || (this.inputMinKm > this.inputMaxKm)) {
      let alert = this.alertCtrl.create({
        title: '알림',
        subTitle: '최소값/최대값을 확인하세요.',
        buttons: ['확인']
      });
      alert.present();
    } else {
      if(this.inputMinPrice > 0) { this.viewMinPrice = this.inputMinPrice; }
      if(this.inputMaxPrice > 0) { this.viewMaxPrice = this.inputMaxPrice; }
      if(this.inputMinKm > 0) { this.viewMinKm = this.inputMinKm; }
      if(this.inputMaxKm > 0) { this.viewMaxKm = this.inputMaxKm; }
      if(this.inputMinYear > 0) { this.viewMinYear = this.inputMinYear; }
      if(this.inputMaxYear > 0) { this.viewMaxYear = this.inputMaxYear; }
      this.otherParams = { 
        minPrice: this.viewMinPrice, 
        maxPrice: this.viewMaxPrice, 
        minYear: this.viewMinYear, 
        maxYear: this.viewMaxYear, 
        minKm: this.viewMinKm, 
        maxKm: this.viewMaxKm,
        type: this.selectedType,
        puer: this.puer,
        gear: this.gear
      }
      console.log('Price: ' +this.viewMinPrice+' - '+this.viewMaxPrice);
      console.log('Year: ' +this.viewMinYear+' - '+this.viewMaxYear);
      console.log('Km: ' +this.viewMinKm+' - '+this.viewMaxKm);
      this.viewCtrl.dismiss(this.otherParams);
    }
  }

  closeModal() {
    this.viewCtrl.dismiss('더보기');
  }

}
