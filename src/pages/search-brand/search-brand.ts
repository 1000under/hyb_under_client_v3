import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@IonicPage()
@Component({
  selector: 'page-search-brand',
  templateUrl: 'search-brand.html',
})
export class SearchBrandPage {

  fromPage: any;

  //-------------------------------------------------> for data query
  cars: any =[];
  private baseURI 		: string  = "http://cjsekfvm.cafe24.com/test_server/app/car/url/";

  makersFlag: boolean;
  modelFlag: boolean = false;
  levelFlag: boolean = false;
  showLevelButton: boolean = false;

  brandParams = { 
      maker_key: '0',
      maker_name: '전체',
      model_key: '0',
      model_name: '전체',
      level_key: '0',
      level_name: '전체'      
  }

  model_list: any = [];
  /*
  maker_key:"1"
  maker_name:"현대"
  model_key:"1"
  model_name:"쏘나타"
  model_sub_key:"51"
  model_sub_name:"쏘나타 뉴 라이즈(17년~현재)"
  */
  makers: any = [];
  models: any = [];
  levels: any = [];

  constructor(
    public alertCtrl    : AlertController,
    public viewCtrl     : ViewController, 
    public navCtrl      : NavController, 
    public navParams    : NavParams,
    public http         : Http,
    private ga          : GoogleAnalytics,) { 

      this.fromPage = navParams.get("fromPage");
      console.log('this.fromPage: ' +this.fromPage);
      if(this.fromPage == 'carsPage') {
        this.showLevelButton = false;
      } else {
        this.showLevelButton = true;
      }
      let body   : string	 = '',
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.baseURI + "get_car.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result != 'ok') {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: data.result,
              buttons: ['확인']
            });
            alert.present();
          } else {
            //--> this.makers.push({maker_key: '0', maker_name: '전체'});
            this.model_list = data.model_list;
            let temp_maker_key = '';
            for (let i = 0; i < this.model_list.length; i++) {
              if (this.model_list[i].maker_key != temp_maker_key) {
                temp_maker_key = this.model_list[i].maker_key;
                this.makers.push(this.model_list[i]);
                //console.log('maker_key_name: '+this.model_list[i].maker_key+'-'+this.model_list[i].maker_name);
              }
            }
          }
      });

    }

  
  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchModelModelPage');
  }
  ionViewDidEnter() {
    this.ga.trackView('제조사/모델선택');  //trackView for Google Analytics
  }
  showMaker(){
    this.makersFlag = true;
    this.models = [];
    this.levels = [];
    this.brandParams.model_key = '0';
    this.brandParams.model_name = '';
    this.brandParams.level_key = '0';
    this.brandParams.level_name = '';
  }
  selectedMaker(e, selectedMaker){
    console.log('selectedMaker: ' +selectedMaker.maker_key+': '+selectedMaker.maker_name);
    if(this.showLevelButton == false) {
      this.models.push({model_key: '0', model_name: '전체'});
    }
    this.brandParams.maker_key = selectedMaker.maker_key;
    this.brandParams.maker_name = selectedMaker.maker_name;
    let temp_model_key = '';
    for (let i = 0; i < this.model_list.length; i++) {
      if(this.model_list[i].maker_key == this.brandParams.maker_key && 
         this.model_list[i].model_key != temp_model_key) {
          temp_model_key = this.model_list[i].model_key;
          this.models.push(this.model_list[i]);
          //console.log('model_key_name: '+this.model_list[i].model_key+'-'+this.model_list[i].model_name);
      }
    }
    this.makersFlag = false;
    this.modelFlag = true;
  }

  showModel(){
    this.modelFlag = !this.modelFlag;
    this.levels = [];
    this.brandParams.level_key = '0';
    this.brandParams.level_name = '전체';
  }
  selectedModel(e, selectedModel){
    console.log('selectedModel: ' +selectedModel.model_key+': '+selectedModel.model_name);
    // console.log('selectedModel name: ' +selectedModel.model_name);
    this.brandParams.model_key = selectedModel.model_key;
    this.brandParams.model_name = selectedModel.model_name;
    if(this.showLevelButton == false) {
      this.viewCtrl.dismiss(this.brandParams);
    } else {
      let temp_level_key = ''; 
      for (let i = 0; i < this.models.length; i++) {
        if(this.model_list[i].maker_key == this.brandParams.maker_key && 
           this.model_list[i].model_key == this.brandParams.model_key &&
           this.model_list[i].model_sub_key != temp_level_key) {
            temp_level_key = this.model_list[i].model_sub_key;
            this.levels.push(this.model_list[i]);
            //console.log('level_key_name: '+this.model_list[i].model_sub_key+'-'+this.model_list[i].model_sub_name);
        }
      }
      if(this.levels.length == 0) {
        this.brandParams.level_key = '0';
        this.brandParams.level_name = 'NaN';
        this.viewCtrl.dismiss(this.brandParams);
      } else {
        this.levelFlag = true;
      }
    }
    this.makersFlag = false;
    this.modelFlag = false;
  }

  showLevel(){
    this.levelFlag = !this.levelFlag;
  }
  selectedLevel(e, selectedLevel){
    console.log('selectedLevel: ' +selectedLevel.model_sub_key+': '+selectedLevel.model_sub_name);
    this.brandParams.level_key = selectedLevel.model_sub_key;
    this.brandParams.level_name = selectedLevel.model_sub_name;
    this.viewCtrl.dismiss(this.brandParams);
  }

  selected() {
    //console.log('brandParams: ' +this.brandParams);
    this.viewCtrl.dismiss(this.brandParams);
  }
  closeModal() {
    if (this.levelFlag == true) {
      let alert = this.alertCtrl.create({
        title: '알림',
        subTitle: '세부모델을 선택해주세요.',
        buttons: ['확인']
      });
      alert.present();
    } else {
      this.brandParams.maker_key = '0';
      this.brandParams.maker_name = '전체';
      this.brandParams.model_key = '0';
      this.brandParams.model_name = '전체';
      this.brandParams.level_key = '0';
      this.brandParams.level_name = '전체';
      this.viewCtrl.dismiss(this.brandParams);
    }
  }

}