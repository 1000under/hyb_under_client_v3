import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchBrandPage } from './search-brand';

@NgModule({
  declarations: [
    SearchBrandPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchBrandPage),
  ],
  exports: [
    SearchBrandPage
  ]
})
export class SearchBrandPageModule {}
