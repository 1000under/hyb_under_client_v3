import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SimilarCarPage } from './similar-car';

@NgModule({
  declarations: [
    SimilarCarPage,
  ],
  imports: [
    IonicPageModule.forChild(SimilarCarPage),
  ],
  exports: [
    SimilarCarPage
  ]
})
export class SimilarCarPageModule {}
