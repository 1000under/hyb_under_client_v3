import { Component, ViewChild, ElementRef  } from '@angular/core';
import { IonicPage, Platform, ViewController, NavController, NavParams, Slides, AlertController, ModalController, Content, Events } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';  //-------> Connect to server .php
import { SocialSharing } from '@ionic-native/social-sharing';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import { DeviceProvider } from '../../providers/device/device';
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { OuterLinkProvider } from '../../providers/outer-link/outer-link';
import { MakerModelProvider } from '../../providers/maker-model/maker-model';
import { SetanalyticsProvider } from '../../providers/setanalytics/setanalytics';

declare var google;
declare var window;

@IonicPage()
@Component({
  selector: 'page-similar-car',
  templateUrl: 'similar-car.html',
})
export class SimilarCarPage {

  @ViewChild(Content) content: Content;
  @ViewChild("contentRef") contentHandle: Content;

  selectedCar: any;
  marketURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";
  carDetailParam: any;

  private tabBarHeight;
  private topOrBottom:string;
  private contentBox;

  carsReflashFlag: boolean;
  imageViewMoreTabFlag: boolean = false;

  contactUsFlag: boolean = false;
  reportFlag: boolean = false;
  navigationFlag: boolean = false;
  sunLoopFlag: boolean = false;
  blackBoxFlag: boolean = false;
  warrantyFlag: boolean = false;
  rearSensorFlag: boolean = false;
  startButtonFlag: boolean = false;
  rearCameraFlag: boolean = false;
  heatingSeatFlag: boolean = false;

  private photos: any = [];   // for Image more view
  repairImgPath: any;         //photo_list.type: 7(사고이력)
  registImgPath: any;         //photo_list.type: 6(차량원부)
  repPhoto: any;              //대표이미지 for SNS share
  photo_path: any =[];        //*ngFor="let pp of photo_path"

  key: any;
  car_number: any;
  mileage: any;
  repair: any;
  lookUpDoc: any = '조회가능';
  transfer_ownership: any;
  seizure: any;
  accident_count: any;
  is_favorite: any;
  is_report: any;
  price: any;
  d_price: any;
  under_price: any;
  regist_type: any;
  state: any;  
  maker: any;
  model: any;
  regist_date: any;
  view_count: any;
  favorite_count: any;
  model_level: any;
  year: any;
  month: any;
  puer: any;
  gear: any;
  model_type: any;
  color: any;
  options: any;
  additory_options: any;
  replacement: any;
  location: any;
  address: any;
  address_lat: number = 0;
  address_lon: number = 0;
  //u_link: any;
  memo: any;
  email: any;
  phone: any;

  outerLink_loan: any = [];
  outerLink_ins: any = [];

  @ViewChild(Slides) slides: Slides;
  @ViewChild('map') mapElement: ElementRef;
  map: any;

  
  carPrice: any;
  minPrice: any;
  maxPrice: any;

  constructor(
    public platform           : Platform,
    public viewCtrl           : ViewController, 
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public alertCtrl          : AlertController,
    public modalCtrl          : ModalController, 
    public http               : Http,
    public events             : Events,
    public msgService         : ResponseMsgServiceProvider,
    public deviceProvider     : DeviceProvider,
    public userInfoProvider   : UserInfoProvider,
    public makerModelProvider : MakerModelProvider,    
    private socialSharing     : SocialSharing,
    private geolocation       : Geolocation,
    private ga                : GoogleAnalytics,
    public outerLinkProvider  : OuterLinkProvider,
    public setAnalytics       : SetanalyticsProvider) {

      this.selectedCar = this.navParams.get("carParam");
      
  }

  ionViewDidLoad() {
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    console.log('ionViewDidLoad SimilarCarPage');
  }

  ionViewDidEnter() {
    this.carsReflashFlag = false;

    this.topOrBottom = this.contentHandle._tabsPlacement;
    this.contentBox = document.querySelector(".scroll-content")['style'];
  
    if (this.topOrBottom == "top") {
      this.tabBarHeight = this.contentBox.marginTop;
    } else if (this.topOrBottom == "bottom") {
      this.tabBarHeight = this.contentBox.marginBottom;
    }

    let set_key = this.selectedCar.key;
    let set_type = '1'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
    this.setAnalytics.setAnalytics(set_key, set_type);
    
    for(let i=0; i<this.selectedCar.photo_list.length; i++) {
      if(this.selectedCar.photo_list[i].type == '9') {
        this.registImgPath = this.selectedCar.photo_list[i].path; //photo_list.type: 9(차량원부)
      } else if(this.selectedCar.photo_list[i].type == '10') {
        this.repairImgPath = this.selectedCar.photo_list[i].path; //photo_list.type: 10(사고이력)
      } else {
        this.photo_path.push(this.selectedCar.photo_list[i]);
      }
    }
    this.car_number = this.selectedCar.car_number;
    this.repPhoto = this.selectedCar.photo_list[0].path;
    this.mileage = this.selectedCar.mileage;
    this.repair = this.selectedCar.repair;
    if(this.selectedCar.seizure == '0') {
      this.seizure = '무';
    } else {
      this.seizure = '유';
    }
    this.accident_count = this.selectedCar.accident_count;
    this.transfer_ownership = this.selectedCar.transfer_ownership;
    this.price = this.selectedCar.price;
    this.under_price = this.selectedCar.d_price - this.selectedCar.price;
    this.regist_type = this.selectedCar.regist_type;
    this.state = this.selectedCar.state;
    this.maker = this.selectedCar.maker;
    this.model = this.selectedCar.model;
    this.regist_date = this.selectedCar.regist_date.substr(0,4)+'년'+this.selectedCar.regist_date.substr(5,2)+'월'+this.selectedCar.regist_date.substr(8,2)+'일';
    this.view_count = this.selectedCar.view_count;
    this.favorite_count = Number(this.selectedCar.favorite_count);
    this.model_level = this.selectedCar.model_level;
    this.is_favorite = this.selectedCar.is_favorite;
    this.is_report = this.selectedCar.is_report;
    this.year = this.selectedCar.year;
    this.month = this.selectedCar.month;
    this.puer = this.selectedCar.puer;
    this.gear = this.selectedCar.gear;
    this.model_type = this.selectedCar.model_type;
    this.color = this.selectedCar.color;
    if(this.selectedCar.options != null) {
      this.options = this.selectedCar.options; //--> 주요옵션 [1:네비게이션, 2:선루프, 3:블랙박스, 4:제조사보증, 5:후방센서, 6:START버튼, 7:후방카메라, 8:열선시트]
      let optionsArray = this.selectedCar.options.split(",");
      for(let i=0; i<=optionsArray.length; i++) {
        switch(optionsArray[i]) { 
          case "1": { this.navigationFlag = true; break; }
          case "2": { this.sunLoopFlag = true; break; } 
          case "3": { this.blackBoxFlag = true; break; }
          case "4": { this.warrantyFlag = true; break; }
          case "5": { this.rearSensorFlag = true; break; }
          case "6": { this.startButtonFlag = true; break; } 
          case "7": { this.rearCameraFlag = true; break; }
          case "8": { this.heatingSeatFlag = true; break; } 
          default: { break; } 
        } 
      }
    }
    this.additory_options = this.selectedCar.additory_options;
    this.repair = this.selectedCar.repair;
    this.replacement = this.selectedCar.replacement;

    if(this.selectedCar.address != null) {
      this.address = this.selectedCar.address.replace("대한민국", "");
      let strArray = this.selectedCar.address.split(' ');
      this.location = strArray[2];
      this.address_lat = this.selectedCar.address_lat;
      this.address_lon = this.selectedCar.address_lon;
    } else {
      this.address = "차량의 위치 확인이 필요합니다.";
      this.location = "수내동"; //lat: 37.3713177, lng: 127.1223533
      this.address_lat = 37.3713177;
      this.address_lon = 127.1223533;
    }

    this.memo = this.selectedCar.memo;
    this.email = this.selectedCar.email;
    this.phone = this.selectedCar.phone;

    this.minPrice = 1000;
    this.maxPrice = 2000;
    this.carPrice = {
      lower: 1400,
      upper: 1700
    }

    this.outerLink_loan = this.outerLinkProvider.outerLinkList[0].link;
    this.outerLink_ins = this.outerLinkProvider.outerLinkList[1].link;
    //console.log('this.outerLink_ins: '+this.outerLink_ins);

    this.maker = this.makerModelProvider.getMakerName(this.selectedCar.maker);  //--> 제조사명
    this.model = this.makerModelProvider.getModelName(this.selectedCar.model);  //--> 모델명
    this.loadMap();

    this.ga.trackView('유사차량상세: '+this.maker+'-'+this.model+' '+this.car_number);  //trackView for Google Analytics
  }

  loadMap(){ 
    this.geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(this.address_lat, this.address_lon);
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        fullscreenControl: false
      }
      const element: HTMLElement = document.getElementById('smap');
      this.map = new google.maps.Map(element, mapOptions);
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: this.map.getCenter()
      });
      marker.setVisible(true);
    }).catch((error) => {
      console.log('Error getting location'+ error);
    });
  }

  selectFavoriteCar(){
    if(this.userInfoProvider.userProfile.login_flag == false) {
      let LoginPage_modal = this.modalCtrl.create('LoginPage', {naviPage: 'SimilarCarPage'});
      LoginPage_modal.present();
      } else {
        if(this.is_favorite == '0') {
            let favoriteQueryParams = 'sale=' +this.selectedCar.key+
                                      '&email=' +this.userInfoProvider.userProfile.u_email+
                                      '&pw=' +this.userInfoProvider.userProfile.u_pw+
                                      '&favorite=' +'1'; //'1'=찜등록, '2'=찜취소
            let body   : string	 = favoriteQueryParams,  
                type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                headers: any		 = new Headers({ 'Content-Type': type}),
                options: any 		 = new RequestOptions({ headers: headers }),
                url 	 : any		 = this.marketURI + "set_sale_favorite.php";
            this.http.post(url, body, options)
            .map(res => res.json())
            .subscribe(data => { 
                if (data.result != 'ok') {
                  let alert = this.alertCtrl.create({
                    title: '알림',
                    subTitle: this.msgService.getMessage(data.result),
                    buttons: ['확인']
                  });
                  alert.present();
                } else {
                  this.ga.trackEvent('유사차량상세', '찜: '+this.maker+'-'+this.model+' '+this.car_number );  //trackEvent for Google Analytics
                  let alert = this.alertCtrl.create({
                    title: '알림',
                    subTitle: '선택하신 차량이 찜 목록에 저장되었습니다.',
                    buttons: ['확인']
                  });
                  alert.present();
                  this.is_favorite = '1';
                  this.favorite_count = this.favorite_count + 1;
                  let set_key = this.selectedCar.key;
                  let set_type = '2'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                  this.setAnalytics.setAnalytics(set_key, set_type);
                  this.carsReflashFlag = true;
                  this.events.publish('cars:refrash', this.carsReflashFlag);
                }
            });
        } else {// for 찜취소
          //console.log('car.key car.is_favorite: '+this.cars[i].key+'-'+this.cars[i].is_favorite);
          let favoriteQueryParams = 'sale=' +this.selectedCar.key+
                                    '&email=' +this.userInfoProvider.userProfile.u_email+
                                    '&pw=' +this.userInfoProvider.userProfile.u_pw+
                                    '&favorite=' +'2'; //'1'=찜등록, '2'=찜취소
          let body   : string	 = favoriteQueryParams,  
              type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
              headers: any		 = new Headers({ 'Content-Type': type}),
              options: any 		 = new RequestOptions({ headers: headers }),
              url 	 : any		 = this.marketURI + "set_sale_favorite.php";
          this.http.post(url, body, options)
          .map(res => res.json())
          .subscribe(data => { 
              if (data.result != 'ok') {
                let alert = this.alertCtrl.create({
                  title: '알림',
                  subTitle: this.msgService.getMessage(data.result),
                  buttons: ['확인']
                });
                alert.present();
              } else {
                let alert = this.alertCtrl.create({
                  title: '알림',
                  subTitle: '선택하신 차량이 찜 목록에서 삭제되었습니다.',
                  buttons: ['확인']
                });
                alert.present();
                this.is_favorite = '0';
                this.favorite_count = this.favorite_count - 1;
                let set_key = this.selectedCar.key;
                let set_type = '3'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                this.setAnalytics.setAnalytics(set_key, set_type);
                this.carsReflashFlag = true;
                this.events.publish('cars:refrash', this.carsReflashFlag);
              }
          }); 
        }
      }
  }

  socialShare() {
    let image = this.repPhoto;
    let app_link = this.deviceProvider.deviceInfo.s_link;
    /**
     * Shares using the share sheet
     * @param message {string} The message you would like to share.
     * @param subject {string} The subject
     * @param file {string|string[]} URL(s) to file(s) or image(s), local path(s) to file(s) or image(s), or base64 data of an image. Only the first file/image will be used on Windows Phone.
     * @param url {string} A URL to share
     * @returns {Promise<any>}
     * share(message?: string, subject?: string, file?: string | string[], url?: string): Promise<any>;
     */

    this.socialSharing.share(
      '믿고 맡기는 중고차 안심 직거래 천언더!!!', '믿고 맡기는 중고차 안심 직거래 천언더!!!', image, app_link).then(() => {
      this.ga.trackEvent('유사차량상세', '공유: '+this.maker+'-'+this.model+' '+this.car_number );  //trackEvent for Google Analytics
      //console.log('Shared via any App !!!');
      }).catch(() => {
        console.log('Sharing via any App is impossible');
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '공유에 실패했습니다.',
          buttons: ['확인']
        });
        alert.present();
    });
  }

  imageViewMore(eachImgPath){ //Modal
    if(!this.imageViewMoreTabFlag) {
      this.imageViewMoreTabFlag = true;
      let initialSlide = 0;
      for(let i=0; i<this.photo_path.length; i++) {
        if(this.photo_path[i].path == eachImgPath) {
          initialSlide = i;
        }
      }
      let ImageViewMore_modal = this.modalCtrl.create('ImageViewMorePage',{
        photos: this.photo_path,
        initialSlide: initialSlide, // The first image
      });
      ImageViewMore_modal.onDidDismiss(data => {
        this.imageViewMoreTabFlag = false;
        this.photo_path = [];
        for(let i=0; i<this.selectedCar.photo_list.length; i++) {
          if(this.selectedCar.photo_list[i].type == '9') {
            this.registImgPath = this.selectedCar.photo_list[i].path; //photo_list.type: 9(차량원부)
          } else if(this.selectedCar.photo_list[i].type == '10') {
            this.repairImgPath = this.selectedCar.photo_list[i].path; //photo_list.type: 10(사고이력)
          } else {
            this.photo_path.push(this.selectedCar.photo_list[i]);
          }
        }
      });
      ImageViewMore_modal.present();
    }
  }

  repairImgView(repairImg) {
    if(repairImg == undefined) {
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량 수리이력 정보가 입력되지 않았습니다.',
          buttons: ['확인']
        });
        alert.present();
    } else {
      if(!this.imageViewMoreTabFlag) {
        this.imageViewMoreTabFlag = true;
        this.ga.trackEvent('유사차량상세', '수리이력조회: '+this.maker+'-'+this.model+' '+this.car_number );  //trackEvent for Google Analytics
        let ImageViewMore_modal = this.modalCtrl.create('ImageViewMorePage',{
          photos: repairImg,
          initialSlide: 0, // The first image
        });
        ImageViewMore_modal.onDidDismiss(data => {
          this.imageViewMoreTabFlag = false;
        });
        ImageViewMore_modal.present();
      }
    }
  }
  registImgView(registImg) {
    if(registImg == undefined) {
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량원부 정보가 입력되지 않았습니다.',
          buttons: ['확인']
        });
        alert.present();
    } else {
      if(!this.imageViewMoreTabFlag) {
        this.imageViewMoreTabFlag = true;
        this.ga.trackEvent('유사차량상세', '원부조회: '+this.maker+'-'+this.model+' '+this.car_number );  //trackEvent for Google Analytics
        let ImageViewMore_modal = this.modalCtrl.create('ImageViewMorePage',{
          photos: registImg,
          initialSlide: 0, // The first image
        });
        ImageViewMore_modal.onDidDismiss(data => {
          this.imageViewMoreTabFlag = false;
        });
        ImageViewMore_modal.present();
      }
    }
  }

  report(){
    if(this.userInfoProvider.userProfile.login_flag == false) {
      let LoginPage_modal = this.modalCtrl.create('LoginPage', {naviPage: 'SimilarCarPage'});
      LoginPage_modal.present();
      } else {
        let ReportPage_modal = this.modalCtrl.create('ReportPage', {
          sale: this.selectedCar.key,
          email: this.userInfoProvider.userProfile.u_email,
          pw: this.userInfoProvider.userProfile.u_pw
        });
        ReportPage_modal.onDidDismiss(data => {
          this.is_report = data;
        });
        ReportPage_modal.present();
      }
  }

  loanLimit() {
    let confirm = this.alertCtrl.create({
      title: '대출 한도를 알고싶으세요?',
      message: '귀하의 신용도에 따른 대출금 한도를 알려드립니다.',
      buttons: [
        {
          text: '취소',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: '문의하기',
          handler: () => {
            if(this.userInfoProvider.userProfile.login_flag == false) {
              let LoginPage_modal = this.modalCtrl.create('LoginPage', {naviPage: 'SimilarCarPage'});
              LoginPage_modal.present();
            } else {
              this.ga.trackEvent('유사차량상세', '대출한도문의: '+this.maker+'-'+this.model+' '+this.car_number );  //trackEvent for Google Analytics
              let ChatBoardPage_modal = this.modalCtrl.create('ChatBoardPage',{
                email: this.userInfoProvider.userProfile.u_email,
                pw: this.userInfoProvider.userProfile.u_pw,
                target_email: this.email  //천언더 담당자 email
              });
              ChatBoardPage_modal.present();
            } 
          }
        }
      ]
    });
    confirm.present();
  }

  insuranceQustion() {
    this.ga.trackEvent('유사차량상세', '보험문의: '+this.maker+'-'+this.model+' '+this.car_number );  //trackEvent for Google Analytics
  }

  chatWithUnder() {
    if(this.userInfoProvider.userProfile.login_flag == false) {
      let LoginPage_modal = this.modalCtrl.create('LoginPage', {naviPage: 'SimilarCarPage'});
      LoginPage_modal.present();
      } else {

          let set_key = this.selectedCar.key;
          let set_type = '5'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
          this.setAnalytics.setAnalytics(set_key, set_type);
          this.ga.trackEvent('유사차량상세', '채팅하기: '+this.maker+'-'+this.model+' '+this.car_number );  //trackEvent for Google Analytics
          let ChatBoardPage_modal = this.modalCtrl.create('ChatBoardPage',{
            email: this.userInfoProvider.userProfile.u_email,
            pw: this.userInfoProvider.userProfile.u_pw,
            target_email: this.email  //천언더 담당자 email
          });
          ChatBoardPage_modal.present();
      }
  }

  callToUnder() {
    let set_key = this.selectedCar.key;
    let set_type = '6'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
    this.setAnalytics.setAnalytics(set_key, set_type);
    this.ga.trackEvent('유사차량상세', '전화하기: '+this.maker+'-'+this.model+' '+this.car_number );  //trackEvent for Google Analytics
    //this.phone = '220886460';
    //let temp_phone = this.phone.split('-');
    //let temp_phone_length = temp_phone.length;
    //let temp_phone_lastStart = temp_phone_length - 4;
    console.log('this.phone: '+this.phone);
    let temp_phone = 'tel:'+this.phone;
    //let temp_phone = 'tel:+82-'+this.phone.substr(1, 1)+'-'+this.phone.substr(2, 4)+'-'+this.phone.substr(6, 4);
    console.log('temp_phone: '+temp_phone);
    window.location = temp_phone;
  }

  scrollingCotroll(e) {
    if (e.scrollTop > this.contentHandle.getContentDimensions().contentHeight) {
      if (this.topOrBottom == "top") {
        this.contentBox.marginTop = 0;
      } else if (this.topOrBottom == "bottom") {
        this.contentBox.marginBottom = 0;
      }
 
    } else {
      if (this.topOrBottom == "top") {
        this.contentBox.marginTop = this.tabBarHeight;
      } else if (this.topOrBottom == "bottom") {
        this.contentBox.marginBottom = this.tabBarHeight;
      }
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
