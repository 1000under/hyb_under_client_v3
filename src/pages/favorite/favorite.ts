import { Component, ViewChild } from '@angular/core';
import { IonicPage,Platform, NavController, App, NavParams, LoadingController, AlertController, Content, ModalController, Events } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { FCM } from '@ionic-native/fcm';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ChatsCountProvider } from '../../providers/chats-count/chats-count';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import { MakerModelProvider } from '../../providers/maker-model/maker-model';
import { SetanalyticsProvider } from '../../providers/setanalytics/setanalytics';


@IonicPage()
@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html',
})
export class FavoritePage {

  login_flag: boolean;
  showFavorite: boolean = false;
  todayDate: any;
  carDetailTabFlag: boolean = false;
  carsReflashFlag: boolean;
  //-------------------------------------------------> for data query
  cars: any =[];
  queryParams: any;
  queryLength: number = 0;
  doInfiniteFlag:boolean = true;
  private marketURI 		: string  = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";

  @ViewChild(Content) content: Content;
  
  /*
  private tabBarHeight;
  private topOrBottom:string;
  private contentBox;
  */
  constructor(
    public platform           : Platform,
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public loadingCtrl        : LoadingController,
    public alertCtrl          : AlertController,
    public modalCtrl          : ModalController,
    public http               : Http,
    public app                : App,
    public events             : Events,
    public userInfoProvider   : UserInfoProvider,
    public chatsCountProvider: ChatsCountProvider,
    public makerModelProvider : MakerModelProvider,
    public msgService         : ResponseMsgServiceProvider,
    public setAnalytics       : SetanalyticsProvider,
    private fcm               : FCM,
    private ga                : GoogleAnalytics,) {

    }

  ionViewWillEnter() {
    this.events.subscribe('cars:refrash', (reflashFlag) => {
      if(reflashFlag == true) {
        this.getFavoriteCars();
      }
    });
  }

  ionViewDidLoad() {
    this.ga.trackView('찜');  //trackView for Google Analytics
    console.log('----------> GoogleAnalytics trackView: FavoritePage');

    this.login_flag = this.userInfoProvider.userProfile.login_flag;
    this.todayDate = new Date().toISOString().slice(0,10); //dateFormat --> '2017-08-31'
    this.getFavoriteCars();
    console.log('ionViewDidLoad FavoriteCarsPage');
  }

  ionViewDidEnter() {
    this.login_flag = this.userInfoProvider.userProfile.login_flag;
    this.carsReflashFlag = false;
    this.initPushNotification();
    this.getFavoriteCars();
    console.log('ionViewDidEnter FavoriteCarsPage');
  }

  initPushNotification() {
    this.fcm.onNotification().subscribe(data=>{
      if(this.login_flag == true) {
        if(!data.wasTapped){
          if(data.type == 'chat_new') {
            const dataObj = JSON.stringify(data);
            //console.log('==============> Notification Data: '+dataObj);
            if(this.userInfoProvider.userProfile.login_flag) {
              this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
              let msgObj = JSON.parse(data.message);
              let target_email = msgObj.target_email.replace(/\'/g,''); //특정문자 제거
              let target_name = msgObj.target_name.replace(/\'/g,''); //특정문자 제거
              //console.log('notification target_email====> '+target_email);
              let notificationModal_modal = this.modalCtrl.create('NotificationModalPage', 
                  {
                    target_email: target_email,
                    target_name: target_name
                  });
              let closedByTimeout = false;
              let timeoutHandle = setTimeout(() => { closedByTimeout = true; notificationModal_modal.dismiss(); }, 5000);
        
              notificationModal_modal.onDidDismiss(() => {
                if (closedByTimeout) return;  //timeout 동안 action이 없으면 return
                clearTimeout(timeoutHandle);
              });
              notificationModal_modal.present();
            }
          } //data.type == 'chat_new'
        };
      }
    });
  }

  getFavoriteCars() {
    this.queryParams = 'order=1'+'&email=' +this.userInfoProvider.userProfile.u_email;  // order=최신&email=zinns58@gmail.com
    let body   : string	 = this.queryParams,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.marketURI + "get_sale_favorite.php";
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
        if (data.result != 'ok') {
          this.showFavorite = false;
        } else {
          this.cars = [];
          for (let i = 0; i < Object.keys(data.sale_list).length; i++) {
            data.sale_list[i].maker = this.makerModelProvider.getMakerName(data.sale_list[i].maker);
            data.sale_list[i].model = this.makerModelProvider.getModelName(data.sale_list[i].model);

            // 거래중 blind_date:"2017-08-31 00:00:00" >= this.todayDate
            // console.log('blind_date: '+data.sale_list[i].blind_date.substr(0, 10));
            if(data.sale_list[i].blind_date.substr(0, 10) >= this.todayDate) {
              data.sale_list[i].state = '거래중';
            }

            let strArray = data.sale_list[i].address.split(' ');
            data.sale_list[i].address = strArray[2];
            this.cars.push(data.sale_list[i]);
          }
          this.queryLength = Object.keys(data.sale_list).length;
          console.log('getFavoriteCars() ===> this.queryLength: '+this.queryLength);
          if(this.queryLength >= 20) {
            this.doInfiniteFlag = true;
          } else {
            this.doInfiniteFlag = false;
          }
          this.showFavorite = true;
        }       
    });
  }

  checkLogin() {    
    if(this.userInfoProvider.userProfile.login_flag == false) {
      let LoginPage_modal = this.modalCtrl.create('LoginPage', {naviPage: 'FavoriteCarsPage'});
      LoginPage_modal.onDidDismiss(data => {
        this.login_flag = data;
        /*
        if(this.login_flag == true) {
          //this.app.getRootNav().getActiveChildNav().select(2);  // goto tab3Root - FavoritePage
          this.navCtrl.setRoot(this.navCtrl.getActive().component);
        }
        */
      });
      LoginPage_modal.present();
    }
  }

  naviCarDetailPage(selectedCar) {
    if(!this.carDetailTabFlag) {
      this.carDetailTabFlag = true;
      if(this.userInfoProvider.userProfile.login_flag == true) {
        this.queryParams = 'sale=' +selectedCar+ '&email=' +this.userInfoProvider.userProfile.u_email;
      } else {
        this.queryParams = 'sale=' +selectedCar;
      }
      let body   : string	 = this.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.marketURI + "get_sale_detail.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result != 'ok') {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: this.msgService.getMessage(data.result),
              buttons: ['확인']
            });
            alert.present();
          } else {
            let CarDetailPage_modal = this.modalCtrl.create('CarDetailPage',{ carParam: data.sale_detail });
            CarDetailPage_modal.onDidDismiss(data => {
              this.carDetailTabFlag = false;
            });
            CarDetailPage_modal.present();
          }
      });
    }
  }

  deleteMyFavoriteCar(carKey){
    let confirm = this.alertCtrl.create({
      title: '찜목록에서 삭제하시겠습니까?',
      message: '"삭제"를 선택하면 찜목록에서 데이터가 삭제됩니다.',
      buttons: [
        {
          text: '취소',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: '삭제',
          handler: () => {

            let favoriteQueryParams = 'sale=' +carKey+
                                      '&email=' +this.userInfoProvider.userProfile.u_email+
                                      '&pw=' +this.userInfoProvider.userProfile.u_pw+
                                      '&favorite=' +'2'; //'1'=찜등록, '2'=찜취소
            let body   : string	 = favoriteQueryParams,  
                type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                headers: any		 = new Headers({ 'Content-Type': type}),
                options: any 		 = new RequestOptions({ headers: headers }),
                url 	 : any		 = this.marketURI + "set_sale_favorite.php";
            this.http.post(url, body, options)
            .map(res => res.json())
            .subscribe(data => { 
                if (data.result != 'ok') {
                  let alert = this.alertCtrl.create({
                    title: '알림',
                    subTitle: this.msgService.getMessage(data.result),
                    buttons: ['확인']
                  });
                  alert.present();
                } else {
                  let set_key = carKey;
                  let set_type = '3'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                  this.setAnalytics.setAnalytics(set_key, set_type);
                  this.carsReflashFlag = true;
                  this.events.publish('cars:refrash', this.carsReflashFlag);
                  //this.navCtrl.setRoot(this.navCtrl.getActive().component);
                }
            });

          }
        }
      ]
    });
    confirm.present();
  }

  doInfinite(infiniteScroll) {
    if(this.doInfiniteFlag == true) {
      let body   : string	 = this.queryParams+'&key=' +this.queryLength,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.marketURI + "get_sale_favorite.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
        if (data.result != 'ok') {
          this.doInfiniteFlag = false;
        } else {
          for (let i = 0; i < Object.keys(data.sale_list).length; i++) {
              data.sale_list[i].maker = this.makerModelProvider.getMakerName(data.sale_list[i].maker);
              data.sale_list[i].model = this.makerModelProvider.getModelName(data.sale_list[i].model);

              // 거래중 blind_date:"2017-08-31 00:00:00" >= this.todayDate
              // console.log('blind_date: '+data.sale_list[i].blind_date.substr(0, 10));
              if(data.sale_list[i].blind_date.substr(0, 10) >= this.todayDate) {
                data.sale_list[i].state = '거래중';
              }

              let strArray = data.sale_list[i].address.split(' ');
              data.sale_list[i].address = strArray[2];
              this.cars.push(data.sale_list[i]);
          }
          if( Object.keys(data.pick_detail_list).length < 20) {
            this.doInfiniteFlag = false;
          }
          this.queryLength = this.queryLength + Object.keys(data.sale_list).length;
          console.log('this.queryLength: ' +this.queryLength);
        }
        //console.log('Next operation has ended');
        infiniteScroll.complete();
      });
    }
  }
}
