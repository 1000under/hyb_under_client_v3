import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ModalController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { UserInfoProvider } from '../../providers/user-info/user-info';

declare var window;

@IonicPage()
@Component({
  selector: 'page-what-service',
  templateUrl: 'what-service.html',
})
export class WhatServicePage {

  login_flag: boolean;
  isMember_flag: boolean;
  selectFlag: boolean = false;
  faqs: any =[];  //parameter from SubHomePage
  memo: any;
  faq_index: any;

  constructor(
    public viewCtrl         : ViewController, 
    public navCtrl          : NavController, 
    public navParams        : NavParams,
    public alertCtrl        : AlertController,
    public modalCtrl        : ModalController,
    public userInfoProvider : UserInfoProvider,
    private ga              : GoogleAnalytics,) {
      this.faqs = this.navParams.get("naviParam");
    }

  ionViewDidLoad() {
    this.login_flag = this.userInfoProvider.userProfile.login_flag;
    this.isMember_flag = this.userInfoProvider.userProfile.isMember_flag;
    console.log('ionViewDidLoad WhatServicePage');
  }

  ionViewDidEnter() {
    this.ga.trackView('자주하는질문');  //trackView for Google Analytics
  }

  selectedFAQ(title, index) {
    //console.log('title: '+title+'- index: '+index);
    this.selectFlag = !this.selectFlag;
    this.faq_index = index;
  }

  chatWithUnder() {
    if(this.userInfoProvider.userProfile.login_flag == false) {
      let LoginPage_modal = this.modalCtrl.create('LoginPage', {naviPage: 'WhatServicePage'});
      LoginPage_modal.onDidDismiss(data => {
        this.login_flag = data;
      });
      LoginPage_modal.present();
    } else {
      this.ga.trackEvent('자주하는질문', '채팅하기');  //trackEvent for Google Analytics
      let ChatBoardPage_modal = this.modalCtrl.create('ChatBoardPage',{
        email: this.userInfoProvider.userProfile.u_email,
        pw: this.userInfoProvider.userProfile.u_pw,
        target_email: 'admin@1000under.com'  //천언더 담당자 email
      });
      ChatBoardPage_modal.present();
    }

  }

  callToUnder() {
    //window.location = 'tel:+82-2-2088-6460';
    this.ga.trackEvent('자주하는질문', '전화하기');  //trackEvent for Google Analytics
    window.location = 'tel:02-2088-6460';
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
