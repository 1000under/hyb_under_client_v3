import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WhatServicePage } from './what-service';

@NgModule({
  declarations: [
    WhatServicePage,
  ],
  imports: [
    IonicPageModule.forChild(WhatServicePage),
  ],
  exports: [
    WhatServicePage
  ]
})
export class WhatServicePageModule {}
