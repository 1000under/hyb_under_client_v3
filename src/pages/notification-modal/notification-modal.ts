import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, } from 'ionic-angular';

import { UserInfoProvider } from '../../providers/user-info/user-info';


@IonicPage()
@Component({
  selector: 'page-notification-modal',
  templateUrl: 'notification-modal.html',
})
export class NotificationModalPage {

  target_email: any;
  target_name: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController, 
    public modalCtrl: ModalController,
    public userInfoProvider: UserInfoProvider,) {
      this.target_email = this.navParams.get('target_email');
      this.target_name = this.navParams.get('target_name');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationModalPage');
  }
  goChatBoard() {
    let ChatBoardPage_modal = this.modalCtrl.create('ChatBoardPage',{
      email: this.userInfoProvider.userProfile.u_email,
      pw: this.userInfoProvider.userProfile.u_pw,
      target_email: this.target_email
    });
    ChatBoardPage_modal.present();
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
