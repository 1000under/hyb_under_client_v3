import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';


@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {

  //-------------------------------------------------> for data query
  queryParams: any;
  private memberURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";

  //-------------------------------------------------> loginParams for userInfoProvider
  loginParams: any = [
    { loginApp:       '' },
    { u_name:         '' },
    { u_phone_num:    '' },
    { u_email:        '' },
    { u_pw:           '' },
    { u_photo:        '' },
    { u_type:         '' },
    { cu_flag:        false},
    { fb_flag:        false},
    { kk_flag:        false},
    { gg_flag:        false},
    { login_flag:     false},
    { isMember_flag:   false},
  ];

  userProfile_pw: any ='';

  email
  auth_code
  currentPassword: any ='';
  newPassword: any = '';
  confirmNewPassword: any = '';
  
  isSuccess: boolean = false;

  constructor( 
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public viewCtrl           : ViewController,
    public alertCtrl          : AlertController,
    public modalCtrl          : ModalController,
    public http               : Http,
    public userInfoProvider   : UserInfoProvider,
    public msgService         : ResponseMsgServiceProvider,
    private ga                : GoogleAnalytics,) {

  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordPage');
  }
  ionViewDidEnter() {
    this.ga.trackView('비밀번호재설정');  //trackView for Google Analytics
  }
  requestAuthCode() {
    if(this.email == '') {
      let alert = this.alertCtrl.create({
        title: '알림!',
        subTitle: '"이메일"을 입력해 주세요.',
        buttons: ['확인']
      });
      alert.present();
    } else {
        this.queryParams = 'email='+this.email;
        //----------------------------------------------------------------> get Similar cars
        let body_s   : string	 = this.queryParams,
            type_s 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
            headers_s: any		 = new Headers({ 'Content-Type': type_s}),
            options_s: any 		 = new RequestOptions({ headers: headers_s }),
            url_s 	 : any		 = this.memberURI + "set_member_auth_code.php";
        this.http.post(url_s, body_s, options_s)
        .map(res => res.json())
        .subscribe(data => { 
            if (data.result != 'ok') {
              let alert = this.alertCtrl.create({
                title: '알림',
                subTitle: this.msgService.getMessage(data.result),
                buttons: ['확인']
              });
              alert.present();
            } else {
              let alert = this.alertCtrl.create({
                title: '알림',
                subTitle: '인증코드가 이메일로 발급되었습니다.',
                buttons: ['확인']
              });
              alert.present();
            }
        });
    }    
  }

  confirm() {
      let chkPwdResualt = chkPwd(this.newPassword);

      if(this.email == '') {
        let alert = this.alertCtrl.create({
          title: '알림!',
          subTitle: '"이메일"을 입력해 주세요.',
          buttons: ['확인']
        });
        alert.present();
      } else if(this.auth_code == '') {
        let alert = this.alertCtrl.create({
          title: '알림!',
          subTitle: '"인증코드"를 입력해 주세요.',
          buttons: ['확인']
        });
        alert.present();
      } else if(this.newPassword == '' || this.confirmNewPassword == '') {
          let alert = this.alertCtrl.create({
            title: '알림!',
            subTitle: '"새 비밀번호" 및 "새 비밀번호 확인"을 입력해 주세요.',
            buttons: ['확인']
          });
          alert.present();
      } else if(this.newPassword != this.confirmNewPassword) {
          let alert = this.alertCtrl.create({
            title: '알림!',
            subTitle: '"비밀번호 확인"이 올바르지 않습니다.',
            buttons: ['확인']
          });
          alert.present();
      } else if(chkPwdResualt != 'good'){
          let alert = this.alertCtrl.create({
            title: '알림!',
            subTitle: chkPwdResualt,
            buttons: ['확인']
          });
          alert.present();
      } else {

        this.queryParams = 'email='+this.email+
                           '&auth_code='+this.auth_code+
                           '&pw='+this.newPassword;
        //----------------------------------------------------------------> get Similar cars
        let body_s   : string	 = this.queryParams,
            type_s 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
            headers_s: any		 = new Headers({ 'Content-Type': type_s}),
            options_s: any 		 = new RequestOptions({ headers: headers_s }),
            url_s 	 : any		 = this.memberURI + "set_member_pw_reset.php";
        this.http.post(url_s, body_s, options_s)
        .map(res => res.json())
        .subscribe(data => { 
            if (data.result != 'ok') {
              let alert = this.alertCtrl.create({
                title: '알림',
                subTitle: this.msgService.getMessage(data.result),
                buttons: ['확인']
              });
              alert.present();
            } else {
              this.loginParams.loginApp = '1000Under';
              this.loginParams.u_name = this.userInfoProvider.userProfile.u_name;
              this.loginParams.u_phone_num = this.userInfoProvider.userProfile.u_phone_num;
              this.loginParams.u_email = this.email;
              this.loginParams.u_pw = this.newPassword;
              this.loginParams.u_type = '1';            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
              this.loginParams.cu_flag = true;
              this.loginParams.login_flag = true;
              this.loginParams.isMember_flag = true;
              this.userInfoProvider.setProfile(this.loginParams);
              this.isSuccess = true;
              this.viewCtrl.dismiss(this.isSuccess);
            }
        });

      }

      function chkPwd(str){
        let pw = str;
        let num = pw.search(/[0-9]/g);
        let eng = pw.search(/[a-z]/ig);
        //let spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

        if(pw.length < 6 || pw.length > 15){
          return '영.숫자 포함 6자리 ~ 15자리 이내로 입력해주세요.';
        }
        if(pw.search(/₩s/) != -1){
          return '비밀번호는 공백없이 입력해주세요.';
        } 
        if(num < 0 || eng < 0){
          return '영문.숫자를 혼합하여 입력해주세요.';
        }
        return 'good';
      };
  }

  dismiss() {
    this.viewCtrl.dismiss(false);
  }

}