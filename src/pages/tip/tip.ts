import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ModalController  } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { UserInfoProvider } from '../../providers/user-info/user-info';

@IonicPage()
@Component({
  selector: 'page-tip',
  templateUrl: 'tip.html',
})
export class TipPage {

  tip: any =[];  //parameter from SubHomePage

  constructor(
    public viewCtrl         : ViewController, 
    public navCtrl          : NavController, 
    public navParams        : NavParams,
    public alertCtrl        : AlertController,
    public modalCtrl        : ModalController,
    public userInfoProvider : UserInfoProvider,
    private ga              : GoogleAnalytics,) 
    { 
      this.tip = navParams.get("tip_param");
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TipModalPage');
  }
  ionViewDidEnter() {
    this.ga.trackView('Tip');  //trackView for Google Analytics
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
