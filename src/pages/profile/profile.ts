import { Component } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, ViewController, ModalController, AlertController, ActionSheetController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http'; 
import { Facebook,FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { FCM } from '@ionic-native/fcm';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { KakaoTalk } from 'ionic-plugin-kakaotalk';
import { LoadingController } from 'ionic-angular';

//-----> Providers
import { DeviceProvider } from '../../providers/device/device';
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ChatsCountProvider } from '../../providers/chats-count/chats-count';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import CryptoJS from 'crypto-js';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [GooglePlus]
})
export class ProfilePage {

  //-------------------------------------------------> for data query
  queryParams: any;
  private memberURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";

  //-------------------------------------------------> loginParams for userInfoProvider
  loginParams: any = [
    { u_name:         '' },
    { u_phone_num:    '' },
    { u_email:        '' },
    { u_pw:           '' },
    { u_photo:        '' },
    { u_type:         '' },     //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
    { cu_flag:        false},
    { fb_flag:        false},
    { kk_flag:        false},
    { gg_flag:        false},
    { login_flag:     false},
    { isMember_flag:  false},
  ];

  FB_APP_ID: number = 412877125738651;

  cu_flag: boolean;
  fb_flag: boolean;
  kk_flag: boolean;
  gg_flag: boolean;
  login_flag: boolean;
  isMember_flag: boolean;

  kakao_id: any;
  kakao_nickname: any;
  kakao_profile_image: any;
  kakao_accessToken: any;
  kakao_email: any;

  isPhoto:boolean;
  photo: any;
  name: any;
  email: any;
  password: any;

  SECERET_KEY: string = '1000underappadminwebpageaeskey15';

  constructor(
    public platform           : Platform,
    public viewCtrl           : ViewController, 
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public http               : Http,
    public loadingCtrl        : LoadingController,
    public alertCtrl          : AlertController,
    public modalCtrl          : ModalController,
    public deviceProvider     : DeviceProvider,
    public userInfoProvider   : UserInfoProvider,
    public chatsCountProvider: ChatsCountProvider,
    public msgService         : ResponseMsgServiceProvider,
    public actionSheetCtrl    : ActionSheetController,
    private googlePlus        : GooglePlus,
    private facebook          : Facebook,
    private kakaoTalk         : KakaoTalk,
    private fcm               : FCM,
    private ga                : GoogleAnalytics,) {
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  ionViewDidEnter() {
    this.ga.trackView('더보기');  //trackView for Google Analytics
    console.log('----------> GoogleAnalytics trackView: ProfilePage');

    this.getMember();
    this.initPushNotification();
    console.log('ionViewDidEnter ProfilePage');
  }

  initPushNotification() {
    this.fcm.onNotification().subscribe(data=>{
      if(this.login_flag == true) {
        if(!data.wasTapped){
          if(data.type == 'chat_new') {
            const dataObj = JSON.stringify(data);
            //console.log('==============> Notification Data: '+dataObj);
            if(this.userInfoProvider.userProfile.login_flag) {
              this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
              let msgObj = JSON.parse(data.message);
              let target_email = msgObj.target_email.replace(/\'/g,''); //특정문자 제거
              let target_name = msgObj.target_name.replace(/\'/g,''); //특정문자 제거
              //console.log('notification target_email====> '+target_email);
              let notificationModal_modal = this.modalCtrl.create('NotificationModalPage', 
                  {
                    target_email: target_email,
                    target_name: target_name
                  });
              let closedByTimeout = false;
              let timeoutHandle = setTimeout(() => { closedByTimeout = true; notificationModal_modal.dismiss(); }, 5000);
        
              notificationModal_modal.onDidDismiss(() => {
                if (closedByTimeout) return;  //timeout 동안 action이 없으면 return
                clearTimeout(timeoutHandle);
              });
              notificationModal_modal.present();
            }
          } //data.type == 'chat_new'
        };
      }
    });
  }

  getMember() {
    this.login_flag = this.userInfoProvider.userProfile.login_flag;
    this.isMember_flag = this.userInfoProvider.userProfile.isMember_flag;
    this.kk_flag = this.userInfoProvider.userProfile.kk_flag;

    if(this.login_flag == true) {
      //console.log('getMember email =>'+this.userInfoProvider.userProfile.u_email);
      //console.log('getMember password =>'+this.userInfoProvider.userProfile.u_pw);

      this.queryParams = 'email=' +this.userInfoProvider.userProfile.u_email+
                          '&pw='  +this.userInfoProvider.userProfile.u_pw; 
      let body_1   : string	 = this.queryParams,
          type_1 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers_1: any		 = new Headers({ 'Content-Type': type_1}),
          options_1: any 		 = new RequestOptions({ headers: headers_1 }),
          url_1 	 : any		 = this.memberURI + 'get_member.php';
      this.http.post(url_1, body_1, options_1)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result != 'ok') {
            console.log('Failed!! get_member.php' +data.result);
          } else {
            console.log('Success!! get_member.php');
            //this.loginParams.loginApp = '1000Under';
            this.loginParams.u_name = data.member.name;
            this.loginParams.u_phone_num = data.member.phone_num;
            this.loginParams.u_email = data.member.email;
            this.loginParams.u_pw = this.userInfoProvider.userProfile.u_pw;
            this.loginParams.u_photo = this.userInfoProvider.userProfile.u_photo;
            this.loginParams.u_type = this.userInfoProvider.userProfile.u_type;   //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
            this.loginParams.cu_flag = true;
            this.loginParams.login_flag = true;
            this.loginParams.isMember_flag = true;
            this.userInfoProvider.setProfile(this.loginParams);
            
            this.name =  data.member.name;
            this.email =  data.member.email;
            if(data.member.photo == "" || data.member.photo == null || (data.member.photo.indexOf('NaN') != -1)) {
              this.isPhoto = false;
            } else {
              this.isPhoto = true; 
              this.photo =  data.member.photo;
            }
          } // 회원 정보 (data.result == 'ok')
      });
    }
  }


  DoMemberLogin(){
    let LoginPage_modal = this.modalCtrl.create('LoginPage',{ fromPage: 'ProfilePage' });
    LoginPage_modal.onDidDismiss(data => {
      this.getMember();
    });
    LoginPage_modal.present();
  }


  //페이스북 로그인
  DoFbLogin() {
    if(this.userInfoProvider.userProfile.isMember_flag == false || this.userInfoProvider.userProfile.isMember_flag == undefined ||
      this.userInfoProvider.userProfile.login_flag == false || this.userInfoProvider.userProfile.login_flag == undefined){
      let permissions = new Array();
      //the permissions your facebook app needs from the user
      //permissions = ['public_profile', 'user_friends', 'email'];
      permissions = ["public_profile", "email"];
      let env = this;

      env.facebook.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
            env.facebook.api('me?fields=email,name', null).then(
            (profileData) => {
              let picture = 'NaN'
              if(env.userInfoProvider.userProfile.u_photo != 'NaN') {
                picture = "https://graph.facebook.com/" + res.authResponse.userID + "/picture?type=large";
              }
              let facebook_pw = res.authResponse.accessToken;                
              if(profileData.email == undefined) {
                let alert = env.alertCtrl.create({
                  title: '알림',
                  subTitle: '확인되지 않은 Facebook email입니다.',
                  buttons: ['확인']
                });
                alert.present();
              } else {
                  console.log('Login email =>'+profileData.email);
                  console.log('Login password =>'+facebook_pw);
                  env.queryParams = 'email='          +profileData.email+
                                    '&pw='            +facebook_pw+
                                    '&type='          +'2'+                   //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                                    '&push_key='      +env.deviceProvider.deviceInfo.d_pushKey+
                                    '&photo='         +picture+ 
                                    '&push_enable='   +'YES'+                                       //push enable 1: YES, 2: NO
                                    '&device='        +env.deviceProvider.deviceInfo.d_Type+
                                    '&device_num='    +env.deviceProvider.deviceInfo.d_UUID+
                                    '&version='       +env.deviceProvider.deviceInfo.s_version; 
                  let body   : string	 = env.queryParams,
                      type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                      headers: any		 = new Headers({ 'Content-Type': type}),
                      options: any 		 = new RequestOptions({ headers: headers }),
                      url 	 : any		 = env.memberURI + "set_member_login.php";
                  env.http.post(url, body, options)
                  .map(res => res.json())
                  .subscribe(data => { 
                      if (data.result != 'ok') {
                        
                        let alert = env.alertCtrl.create({
                          title: '알림',
                          subTitle: env.msgService.getMessage(data.result),
                          buttons: ['확인']
                        });
                        alert.present();

                      } else { 
                        //env.loginParams.loginApp = 'Facebook';
                        env.loginParams.u_name = profileData.name;
                        env.loginParams.u_phone_num = 'facebook';
                        env.loginParams.u_email = profileData.email;
                        env.loginParams.u_pw = facebook_pw; 
                        env.loginParams.u_photo = picture;
                        env.loginParams.u_type = '2';            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                        env.loginParams.fb_flag = true;
                        env.loginParams.login_flag = true;
                        env.loginParams.isMember_flag = true;
                        env.userInfoProvider.setProfile(env.loginParams);
                        env.chatsCountProvider.checkChatsCount(env.loginParams.login_flag, profileData.email, facebook_pw);
                        env.isMember_flag = true;
                        env.getMember();
                        //env.navCtrl.setRoot(env.navCtrl.getActive().component);
                      }
                  }); 

              } // check profileData.email
                
            },(err) => {
              console.log(JSON.stringify(err));
              let alert = env.alertCtrl.create({
                title: '알림',
                subTitle: 'Facebook 연동에 실패하였습니다.',
                buttons: ['확인']
              });
              alert.present();
            });
      }).catch(e => {
        let alert = env.alertCtrl.create({
          title: '알림',
          subTitle: 'Facebook 연동에 실패하였습니다.',
          buttons: ['확인']
        });
        alert.present();
        console.log('Error logging into Facebook' + e)
      });
    }
  }


  //구글 로그인
  DoGoogleLogin(){
    if(this.userInfoProvider.userProfile.isMember_flag == false || this.userInfoProvider.userProfile.isMember_flag == undefined ||
       this.userInfoProvider.userProfile.login_flag == false || this.userInfoProvider.userProfile.login_flag == undefined){
      let env = this;
      let loading = env.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();
      env.googlePlus.login({
        'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
        'webClientId': '674574852016-ddcao4cj7qgief2auch1u6sh8env2j1d.apps.googleusercontent.com',  // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.r iOS
        'offline': true
      })
      .then(function (user) {
        loading.dismiss();
        if(user.imageUrl == undefined || user.imageUrl.indexOf('photo.jpg') != -1 || env.userInfoProvider.userProfile.u_photo == 'NaN') {
          user.imageUrl = 'NaN';
        }
        console.log('user_data.userId : '+user.userId);
        console.log('user_data.displayName : '+user.displayName);
        console.log('user_data.email : '+user.email);
        console.log('user_data.imageUrl : '+user.imageUrl);
        console.log('user_data.accessToken : '+user.accessToken);
        console.log('user_data.idToken : '+user.idToken);

        let google_pw = user.idToken;
        //let google_pw = user.accessToken;        
        env.queryParams = 'email='          +user.email+
                          '&pw='            +google_pw+
                          '&type='          +'4'+                   //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                          '&push_key='      +env.deviceProvider.deviceInfo.d_pushKey+
                          '&photo='         +user.imageUrl+ 
                          '&push_enable='   +'YES'+                                       //push enable 1: YES, 2: NO
                          '&device='        +env.deviceProvider.deviceInfo.d_Type+
                          '&device_num='    +env.deviceProvider.deviceInfo.d_UUID+
                          '&version='       +env.deviceProvider.deviceInfo.s_version;

        console.log('env.queryParams ==>'+env.queryParams);

        let body   : string	 = env.queryParams,
            type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
            headers: any		 = new Headers({ 'Content-Type': type}),
            options: any 		 = new RequestOptions({ headers: headers }),
            url 	 : any		 = env.memberURI + "set_member_login.php";
        env.http.post(url, body, options)
        .map(res => res.json())
        .subscribe(data => { 
            console.log('data.result ==>'+data.result);
            if (data.result != 'ok') {
              let alert = env.alertCtrl.create({
                title: '알림',
                subTitle: env.msgService.getMessage(data.result),
                buttons: ['확인']
              });
              alert.present();
            } else { 
              //env.loginParams.loginApp = 'Google';
              env.loginParams.u_name = user.displayName;
              env.loginParams.u_phone_num = 'google';
              env.loginParams.u_email = user.email;
              env.loginParams.u_pw = google_pw; 
              env.loginParams.u_photo = user.imageUrl;
              env.loginParams.u_type = '4';            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
              env.loginParams.gg_flag = true;
              env.loginParams.login_flag = true;
              env.loginParams.isMember_flag = true;
              env.userInfoProvider.setProfile(env.loginParams);
              env.chatsCountProvider.checkChatsCount(env.loginParams.login_flag, user.email, google_pw);
              env.isMember_flag = true;
              env.getMember();
              //env.navCtrl.setRoot(env.navCtrl.getActive().component);
            } 
        });
    
      }, function (error) {
        let alert = env.alertCtrl.create({
          title: '알림',
          subTitle: 'Google 연동에 실패하였습니다.',
          buttons: ['확인']
        });
        alert.present();
        console.log('Google userInfo return error: ' +error);
        loading.dismiss();
      });
    }
  }

  DoKakaoLogin() {
    this.kakaoTalk.login()
    .then(response => {
      // 로그인 결과와 엑세스 토큰을 promise 형태로 반환한다
      return new Promise(resolve => {
        this.kakaoTalk.getAccessToken().then(accessToken => resolve(Object.assign({}, response, { accessToken })))
      })
    })
    .then(({ accessToken, id, nickname, profile_image }) => {
      let env = this;
      // 유저 정보를 출력
      console.log('kakao_id :' + id);
      console.log('kakao_nickname :' + nickname);
      console.log('kakao_profile_image :' + profile_image);
      console.log('kakao_accessToken :' + accessToken);
      let kakao_pw = accessToken;        
      
      //console.log('kakao_email :' + email);
      if(profile_image == undefined || env.userInfoProvider.userProfile.u_photo == 'NaN') {
        profile_image = 'NaN';
      }

      env.queryParams = 'email='          +id+
                        '&pw='            +kakao_pw+
                        '&type='          +'3'+                   //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
                        '&push_key='      +env.deviceProvider.deviceInfo.d_pushKey+
                        '&photo='         +profile_image+ 
                        '&push_enable='   +'YES'+                                       //push enable 1: YES, 2: NO
                        '&device='        +env.deviceProvider.deviceInfo.d_Type+
                        '&device_num='    +env.deviceProvider.deviceInfo.d_UUID+
                        '&version='       +env.deviceProvider.deviceInfo.s_version;

      let body   : string	 = env.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = env.memberURI + "set_member_login.php";
      env.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          console.log('data.result ==>'+data.result);
          if (data.result != 'ok') {
            let alert = env.alertCtrl.create({
              title: '알림',
              subTitle: env.msgService.getMessage(data.result),
              buttons: ['확인']
            });
            alert.present();
          } else { 
            //env.loginParams.loginApp = 'Kakao';
            env.loginParams.u_name = nickname;
            env.loginParams.u_phone_num = 'kakao';
            env.loginParams.u_email = id;
            env.loginParams.u_pw = kakao_pw; 
            env.loginParams.u_photo = profile_image;
            env.loginParams.u_type = '3';            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
            env.loginParams.kk_flag = true;
            env.loginParams.login_flag = true;
            env.loginParams.isMember_flag = true;
            env.userInfoProvider.setProfile(env.loginParams);
            env.chatsCountProvider.checkChatsCount(env.loginParams.login_flag, id, kakao_pw);
            env.isMember_flag = true;
            env.getMember();
            //env.navCtrl.setRoot(env.navCtrl.getActive().component);
          }
      });

    })
    .catch(error => {
      let alert = this.alertCtrl.create({
        title: '알림',
        subTitle: '카카오톡 연동에 실패하였습니다.',
        buttons: ['확인']
      });
      alert.present();
      console.log('Error logging into KakaoTalk' + error);
    });
  }

  signUp() {//Modal
    console.log('click signUp <======>')
    let signUpPage_modal = this.modalCtrl.create('SignUpPage');
    signUpPage_modal.onDidDismiss(data => {
      this.getMember();
    });
    signUpPage_modal.present();
  }

  updateProfile(){
    let updateProfilePage_modal = this.modalCtrl.create('UpdateProfilePage');
    updateProfilePage_modal.onDidDismiss(data => {
      this.getMember();
    });
    updateProfilePage_modal.present();
  }

  myCars(){ //Modal
    let myCarsPage_modal = this.modalCtrl.create('MyCarsPage');
    myCarsPage_modal.onDidDismiss(data => {
      document.querySelector(".tabbar")['style'].display = 'flex';
    });
    myCarsPage_modal.present();
  }

  setKeyword(){
    let setKeywordPage_modal = this.modalCtrl.create('SetKeywordPage');
    setKeywordPage_modal.present();
  }

  setOthers() { //Modal
    let setOthersPage_modal = this.modalCtrl.create('SetOthersPage');
    setOthersPage_modal.onDidDismiss(data => {
      this.getMember();
    });
    setOthersPage_modal.present();
  }
  
  public GetPasswordEncrypted(password: string) {
    // Encrypt
    var Encrypttext = String(CryptoJS.AES.encrypt(password, this.SECERET_KEY));
    return Encrypttext;
  }
  
}
