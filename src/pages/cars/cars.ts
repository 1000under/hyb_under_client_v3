import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Platform, IonicApp, ViewController, ModalController, LoadingController, AlertController, Content, Events } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FCM } from '@ionic-native/fcm';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ChatsCountProvider } from '../../providers/chats-count/chats-count';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import { MakerModelProvider } from '../../providers/maker-model/maker-model';
import { TranslateProvider } from '../../providers/translate/translate';
import { SetanalyticsProvider } from '../../providers/setanalytics/setanalytics';

@IonicPage()
@Component({
  selector: 'page-cars',
  templateUrl: 'cars.html',
})
export class CarsPage {

  @ViewChild(Content) content: Content;
  
  carDetailTabFlag: boolean = false;
  //-------------------------------------------------> for hide and show Header
  start = 0;
  threshold = 100;  //100, 200
  slideHeaderPrevious = 0;
  ionScroll:any;
  showheader:boolean;
  hideheader:boolean;
  headercontent:any;
  todayDate: any;

  //-------------------------------------------------> for data query
  cars: any =[];
  orderParam: any;
  queryParams: any;
  queryLength: number = 0;
  doInfiniteFlag:boolean = true;
  private marketURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";
  carListParam: any;

  //-------------------------------------------------> For Animated Ionic Splash Screen
  tabBarElement: any;
  splash = true;
  
  //-------------------------------------------------> for hide & show Header-bar
  //ionScroll:any;
  showTop: boolean; //---------> for hide & show search-list
  initSearchFlag: boolean;  //---------> for initialize search

  searchBrand: any = [];
  searchBrandFlag: boolean = false;
  brandParams: any;

  searchArea: any;
  searchAreaFlag: boolean = false;
  areaParams: any;

  searchResult: any = '모든 모델 ・ 모든 지역 ・ 추가 조건 검색';  //searchBrand + searchArea

  searchCondition: any =[];
  searchConditionFlag: boolean = false;
  otherParams: any;

  popularActive: boolean = false;
  newActive: boolean = false;

  constructor(
    public platform           : Platform,
    public ionicApp           : IonicApp,
    public viewCtrl           : ViewController,
    public navCtrl            : NavController, 
    public modalCtrl          : ModalController,
    public loadingCtrl        : LoadingController,
    public alertCtrl          : AlertController,
    private fcm               : FCM,
    public http               : Http,
    public events             : Events,
    private socialSharing     : SocialSharing,
    private ga                : GoogleAnalytics,
    public translateProvider  : TranslateProvider,
    public userInfoProvider   : UserInfoProvider,
    public chatsCountProvider: ChatsCountProvider,
    public msgService         : ResponseMsgServiceProvider,
    public makerModelProvider : MakerModelProvider,
    public setAnalytics       : SetanalyticsProvider,
    public myElement          : ElementRef) {
  }

  ionViewWillEnter() {
    this.events.subscribe('cars:refrash', (reflashFlag) => {
      if(reflashFlag == true) {
        this.getCars(this.queryParams);
      }
    });
  }

  ionViewDidLoad() {
    this.presentLoading();
    this.translateProvider.SetTranslateService('kor');
    this.todayDate = new Date().toISOString().slice(0,10); //dateFormat --> '2017-08-31'
    this.searchBrand = '제조사/모델명';
    this.searchArea = '지역';
    //this.searchResult = '모든 모델 ・ 모든 지역 ・ 추가 조건 검색';
    this.searchCondition = ['더보기'];
    this.initSearchFlag = false;
    this.searchBrandFlag = false;
    this.searchAreaFlag = false;
    this.searchConditionFlag = false;
    this.newActive = true;     // button incative
    this.popularActive = false; // button incative

    this.orderParam = 'order=1';   // 'order=1:최신, 2:인기
    this.queryParams = this.orderParam;
    this.getCars(this.queryParams);
    //this.showTop = false;
    //this.ngOnInit();
    console.log('ionViewDidLoad CarsPage');
  }

  ionViewDidEnter() {
    this.ga.trackView('차량');  //trackView for Google Analytics
    console.log('----------> GoogleAnalytics trackView: CarsPage');

    this.initPushNotification();
    this.showTop = false;
    this.ngOnInit();
    console.log('ionViewDidEnter CarsPage');
  }

  initPushNotification() {
    this.fcm.onNotification().subscribe(data=>{
      if(this.userInfoProvider.userProfile.login_flag == true) {
        if(!data.wasTapped){
          if(data.type == 'chat_new') {
            const dataObj = JSON.stringify(data);
            //console.log('==============> Notification Data: '+dataObj);
            if(this.userInfoProvider.userProfile.login_flag) {
              this.chatsCountProvider.checkChatsCount(this.userInfoProvider.userProfile.login_flag, this.userInfoProvider.userProfile.u_email, this.userInfoProvider.userProfile.u_pw);
              let msgObj = JSON.parse(data.message);
              let target_email = msgObj.target_email.replace(/\'/g,''); //특정문자 제거
              let target_name = msgObj.target_name.replace(/\'/g,''); //특정문자 제거
              //console.log('notification target_email====> '+target_email);
              let notificationModal_modal = this.modalCtrl.create('NotificationModalPage', 
                  {
                    target_email: target_email,
                    target_name: target_name
                  });
              let closedByTimeout = false;
              let timeoutHandle = setTimeout(() => { closedByTimeout = true; notificationModal_modal.dismiss(); }, 5000);
        
              notificationModal_modal.onDidDismiss(() => {
                if (closedByTimeout) return;  //timeout 동안 action이 없으면 return
                clearTimeout(timeoutHandle);
              });
              notificationModal_modal.present();
            }
          } //data.type == 'chat_new'
        };
      }
    });
  }

  ngOnInit() {
    // Ionic scroll element
    this.ionScroll = this.myElement.nativeElement.getElementsByClassName('scroll-content')[0];
    this.start = -100;  //-100
    this.ionScroll.addEventListener("scroll", () => {
      if(this.ionScroll.scrollTop - (this.start) > this.threshold) {
        this.showheader =false;
        this.showTop = false;
        this.hideheader = false;
      } else {
        this.showheader =false;
        this.showTop = true;
        this.hideheader = true;
      }
    });
  }

  showTopSwitch() {
    if(this.showTop == true) {
      this.showTop = false;
      this.ionScroll.scrollTop = 155;
      this.ngOnInit();
    } else {
      this.showTop = true;
      this.ionScroll.scrollTop = 0;
      this.ngOnInit();
    }
  }

  getCars(param) {
    if(this.userInfoProvider.userProfile.login_flag == true) {
      this.carListParam = param+ '&email=' +this.userInfoProvider.userProfile.u_email;
    } else {
      this.carListParam = param;
    }
    //console.log('==============> getCars(param) ======> '+this.carListParam);
    let body   : string	 = this.carListParam,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.marketURI + "get_sale_list.php";
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
        if (data.result != 'ok') {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: this.msgService.getMessage(data.result),
            buttons: ['확인']
          });
          alert.present();
        } else {

          for (let i = 0; i < Object.keys(data.sale_list).length; i++) {
            data.sale_list[i].maker = this.makerModelProvider.getMakerName(data.sale_list[i].maker);
            data.sale_list[i].model = this.makerModelProvider.getModelName(data.sale_list[i].model);

            // for 거래중 blind_date:"2017-08-31 00:00:00" >= this.todayDate
            if(data.sale_list[i].blind_date.substr(0, 10) >= this.todayDate) {
              data.sale_list[i].state = '거래중';
            }
            let strArray = data.sale_list[i].address.split(' ');
            data.sale_list[i].address = strArray[2];  
          }
          this.cars = data.sale_list;

          this.queryLength = Object.keys(data.sale_list).length;
          console.log('getCars() ===> this.queryLength: '+this.queryLength);
          if(this.queryLength >= 20) {
            this.doInfiniteFlag = true;
          } else {
            this.doInfiniteFlag = false;
          }
        }
    });
  }

  resetSearch(){
    this.ionScroll.scrollTop = 0;
    this.searchBrand = '제조사/모델명';
    this.searchArea = '지역';
    this.searchResult = '모든 모델 ・ 모든 지역 ・ 추가 조건 검색';
    this.searchCondition = ['더보기'];
    this.initSearchFlag = false;
    this.searchBrandFlag = false;
    this.searchAreaFlag = false;
    this.searchConditionFlag = false;
    this.newActive = true;     // button incative
    this.popularActive = false; // button incative

    this.cars = [];
    this.doInfiniteFlag = false;
    this.orderParam = 'order=1'; //order=최신
    this.queryParams = this.orderParam;
    this.getCars(this.queryParams);
  }


  searchNew(){
    this.newActive = true;     // button incative
    this.popularActive = false; // button incative
    this.cars = [];
    this.queryLength = 0;
    this.doInfiniteFlag = false;
    this.orderParam = 'order=1'; //order=최신
    if(this.searchBrandFlag && this.searchAreaFlag && this.searchConditionFlag) {
      this.queryParams = this.orderParam + this.brandParams + this.areaParams + this.otherParams;
    } else if(this.searchBrandFlag && this.searchAreaFlag) {
      this.queryParams = this.orderParam + this.brandParams + this.areaParams;
    } else if(this.searchBrandFlag && this.searchConditionFlag) {
      this.queryParams = this.orderParam + this.brandParams + this.otherParams;
    } else if(this.searchAreaFlag && this.searchConditionFlag) {
      this.queryParams = this.orderParam + this.areaParams + this.otherParams;
    } else if(this.searchBrandFlag) {
      this.queryParams = this.orderParam + this.brandParams;
    } else if(this.searchAreaFlag) {
      this.queryParams = this.orderParam + this.areaParams;
    } else if(this.searchConditionFlag) {
      this.queryParams = this.orderParam + this.otherParams;
    } else {
      this.queryParams = this.orderParam;
    }
    this.getCars(this.queryParams);
  }

  searchPopular(){
    this.newActive = false;     // button incative
    this.popularActive = true; // button incative
    this.cars = [];
    this.queryLength = 0;
    this.doInfiniteFlag = false;
    this.orderParam = 'order=2'; //order=인기
    if(this.searchBrandFlag && this.searchAreaFlag && this.searchConditionFlag) {
      this.queryParams = this.orderParam + this.brandParams + this.areaParams + this.otherParams;
    } else if(this.searchBrandFlag && this.searchAreaFlag) {
      this.queryParams = this.orderParam + this.brandParams + this.areaParams;
    } else if(this.searchBrandFlag && this.searchConditionFlag) {
      this.queryParams = this.orderParam + this.brandParams + this.otherParams;
    } else if(this.searchAreaFlag && this.searchConditionFlag) {
      this.queryParams = this.orderParam + this.areaParams + this.otherParams;
    } else if(this.searchBrandFlag) {
      this.queryParams = this.orderParam + this.brandParams;
    } else if(this.searchAreaFlag) {
      this.queryParams = this.orderParam + this.areaParams;
    } else if(this.searchConditionFlag) {
      this.queryParams = this.orderParam + this.otherParams;
    } else {
      this.queryParams = this.orderParam;
    }
    this.getCars(this.queryParams);
  }

  naviCarDetailPage(selectedCar) {
    if(!this.carDetailTabFlag) {
      this.carDetailTabFlag = true;
      if(this.userInfoProvider.userProfile.login_flag == true) {
        this.carListParam = 'sale=' +selectedCar+ '&email=' +this.userInfoProvider.userProfile.u_email;
      } else {
        this.carListParam = 'sale=' +selectedCar;
      }
      let body   : string	 = this.carListParam,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.marketURI + "get_sale_detail.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result != 'ok') {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: this.msgService.getMessage(data.result),
              buttons: ['확인']
            });
            alert.present();
          } else {
            let set_key = selectedCar;
            let set_type = '1'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
            this.setAnalytics.setAnalytics(set_key, set_type);
            let CarDetailPage_modal = this.modalCtrl.create('CarDetailPage', {carParam: data.sale_detail});
            CarDetailPage_modal.onDidDismiss(data => {
              this.carDetailTabFlag = false;
            });
            CarDetailPage_modal.present();
          }
      });
    }
  }
  
  SearchBrand(){
    let SearchBrandPage_modal = this.modalCtrl.create('SearchBrandPage',{fromPage: 'carsPage'});
    SearchBrandPage_modal.onDidDismiss(data => {
      //console.log('this.ionicApp._modalPortal.getActive().index: '+this.ionicApp._modalPortal.getActive().index);
      if(this.ionicApp._modalPortal.getActive().index == 0 && data != undefined) {
        this.searchBrand = [];
        console.log('data.maker_name, data.model_name: '+data.maker_name+'-'+data.model_name);   
        if (data.maker_name == '전체' && data.model_name  == '전체') {
          this.searchBrand = '제조사/모델명';
          this.brandParams = '';
          this.searchBrandFlag = false;
          if(this.searchAreaFlag == true) {
            this.searchResult = '제조사:전체, 지역:' +this.searchArea;
          } else {
            this.searchResult = '제조사:전체, 지역:전국';
          }

          if(this.searchAreaFlag && this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.areaParams + this.otherParams;
          } else if(this.searchAreaFlag) {
            this.queryParams = this.orderParam + this.areaParams;
          } else if(this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.otherParams;
          } else {
            this.queryParams = this.orderParam;
          }
          this.getCars(this.queryParams);
          
        } else {
          //{maker_key: "1", maker_name: "현대", model_key: "9", model_name: "i30"}
          this.searchBrand.push(data.maker_name+'-'+data.model_name);
          this.searchBrandFlag = true;
          this.initSearchFlag = true;
          if(this.searchAreaFlag == true) {
            this.searchResult = this.searchBrand+', 지역:' + this.searchArea;
          } else {
            this.searchResult = this.searchBrand+', 지역:전국';
          }
          // Initialized cars[]
          this.cars = [];
          this.queryLength = 0;
          this.doInfiniteFlag = false;

          if (data.model_name  == '전체') {
            this.brandParams = '&maker='+data.maker_key;
          } else {
            this.brandParams = '&model='+data.model_key;
          }
          this.queryParams = this.orderParam + this.brandParams;

          if(this.searchAreaFlag && this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.brandParams + this.areaParams + this.otherParams;
          } else if(this.searchAreaFlag) {
            this.queryParams = this.orderParam + this.brandParams + this.areaParams;
          } else if(this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.brandParams + this.otherParams;
          }
          this.getCars(this.queryParams);
          //console.log('제조사/모델명 queryParams: '+this.queryParams);
        }
      } // check registerBackButtonAction for android
    });
    SearchBrandPage_modal.present();
  }

  SearchLocation(){ //Modal
    let searchLocationPage_modal = this.modalCtrl.create('SearchLocationPage');
    searchLocationPage_modal.onDidDismiss(data => {
      if(this.ionicApp._modalPortal.getActive().index == 0 && data != undefined) {
        //console.log('Got area parameter ==>' +data);
        if (data == '전국') {
          this.searchArea = data;
          this.searchAreaFlag = false;
          if(this.searchBrandFlag == true) {
            this.searchResult = this.searchBrand+', 지역:'+data;
          } else {
            this.searchResult = '제조사:전체, 지역:'+data;
          }

          if(this.searchBrandFlag && this.searchConditionFlag) {
            this.queryParams = this.orderParam+ this.brandParams + this.otherParams;
          } else if(this.searchBrandFlag) {
              this.queryParams = this.orderParam + this.brandParams;
          } else if(this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.otherParams;
          } else {
            this.queryParams = this.orderParam
          }
          this.getCars(this.queryParams);
        } else {
          this.searchAreaFlag = true;
          this.initSearchFlag = true;
          this.searchArea = data;
          if(this.searchBrandFlag == true) {
            this.searchResult = this.searchBrand+', 지역:'+data;
          } else {
            this.searchResult = '제조사:전체, 지역:'+data;
          }

          // Initialized cars[]
          this.cars = [];
          this.queryLength = 0;
          this.doInfiniteFlag = false;

          this.areaParams = '&address=' + data;
          this.queryParams = this.orderParam + this.areaParams;

          if(this.searchBrandFlag && this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.areaParams + this.brandParams + this.otherParams;
          } else if(this.searchBrandFlag) {
              this.queryParams = this.orderParam + this.areaParams + this.brandParams;
          } else if(this.searchConditionFlag) {
            this.queryParams = this.orderParam + this.areaParams + this.otherParams;
          }
          //console.log('지역 queryParams: '+this.queryParams);
          this.getCars(this.queryParams);          
        }
      } // check registerBackButtonAction for android
    });
    searchLocationPage_modal.present();
  }

  SearchOtherConditions(){  //Modal
    let searchOtherConditionsPage_modal = this.modalCtrl.create('SearchOtherConditionsPage');
    searchOtherConditionsPage_modal.onDidDismiss(data => {
      console.log('Other condition return value: '+data);
      if(this.ionicApp._modalPortal.getActive().index == 0 && data != undefined) {
        if(data == "더보기") {
          this.searchCondition = data;
          this.otherParams = '';
          this.searchConditionFlag = false;

          if(this.searchBrandFlag && this.searchAreaFlag) {
            this.queryParams = this.orderParam + this.brandParams + this.areaParams;
          } else if(this.searchBrandFlag) {
              this.queryParams = this.orderParam + this.brandParams;
          } else if(this.searchAreaFlag) {
            this.queryParams = this.orderParam + this.areaParams;
          } else {
            this.queryParams = this.orderParam;
          }
          this.getCars(this.queryParams);
        } else {
          this.searchCondition = [];
          this.searchConditionFlag = true;
          this.initSearchFlag = true;

          // Initialized cars[]
          this.cars = [];
          this.queryLength = 0;
          this.doInfiniteFlag = false;

          //this.searchCondition.push(data.type);
          switch(data.type) {
            case'1':  this.searchCondition.push('전체');  break;
            case'2':  this.searchCondition.push('소형');  break;
            case'3':  this.searchCondition.push('중대형');  break;
            case'4':  this.searchCondition.push('SUV');  break;
            case'5':  this.searchCondition.push('수입차');  break;
            default:  this.searchCondition.push('전체');  break;
          }      
          //this.searchCondition.push(data.puer);
          switch(data.puer) {
            case'1':  this.searchCondition.push('전체');  break;
            case'2':  this.searchCondition.push('디젤');  break;
            case'3':  this.searchCondition.push('가솔린');  break;
            case'4':  this.searchCondition.push('하이브리드');  break;
            case'5':  this.searchCondition.push('전기');  break;
            case'6':  this.searchCondition.push('가스');  break;
            case'7':  this.searchCondition.push('기타');  break;
            default:  this.searchCondition.push('전체');  break;
          }
          //this.searchCondition.push(data.gear);
          switch(data.gear) {
            case'1':  this.searchCondition.push('전체');  break;
            case'2':  this.searchCondition.push('수동');  break;
            case'3':  this.searchCondition.push('자동');  break;
            case'4':  this.searchCondition.push('기타');  break;
            default:  this.searchCondition.push('전체');  break;
          } 
          this.searchCondition.push(data.minPrice);
          this.searchCondition.push(data.maxPrice);
          this.searchCondition.push(data.minYear);
          this.searchCondition.push(data.maxYear);
          this.searchCondition.push(data.minKm);
          this.searchCondition.push(data.maxKm);

          this.otherParams = '&type='+data.type+'&puer='+data.puer+'&gear='+data.gear+'&price='+data.minPrice+','+data.maxPrice+
                            '&year='+data.minYear+','+data.maxYear+'&mileage='+data.minKm+','+data.maxKm;

          this.queryParams = this.orderParam + this.otherParams;

          if(this.searchBrandFlag && this.searchAreaFlag) {
            this.queryParams = this.orderParam + this.otherParams + this.brandParams + this.areaParams;
          } else if(this.searchBrandFlag) {
              this.queryParams = this.orderParam + this.otherParams + this.brandParams;
          } else if(this.searchAreaFlag) {
            this.queryParams = this.orderParam + this.otherParams + this.areaParams;
          }
          this.getCars(this.queryParams);
          //console.log('조건 queryParams: '+this.queryParams);
        }
      }  // check registerBackButtonAction for android
    });
    searchOtherConditionsPage_modal.present();
  }

  selectFavoriteCar(selectedCar){
    if(this.userInfoProvider.userProfile.login_flag == false) {
      let LoginPage_modal = this.modalCtrl.create('LoginPage', {naviPage: 'CarsPage'});
      LoginPage_modal.onDidDismiss(data => {
        if(data == true) {
          this.navCtrl.setRoot(this.navCtrl.getActive().component);
        }
      });
      LoginPage_modal.present();
    } else {
        for (let i = 0; i < this.cars.length; i++) {
          if(this.cars[i].key == selectedCar.key) {
            if(this.cars[i].is_favorite == '0') { // for 찜등록
                //console.log('car.key car.is_favorite: '+this.cars[i].key+'-'+this.cars[i].is_favorite);
                let favoriteQueryParams = 'sale=' +selectedCar.key+
                                          '&email=' +this.userInfoProvider.userProfile.u_email+
                                          '&pw=' +this.userInfoProvider.userProfile.u_pw+
                                          '&favorite=' +'1'; //'1'=찜등록, '2'=찜취소
                let body   : string	 = favoriteQueryParams,  
                    type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                    headers: any		 = new Headers({ 'Content-Type': type}),
                    options: any 		 = new RequestOptions({ headers: headers }),
                    url 	 : any		 = this.marketURI + "set_sale_favorite.php";
                this.http.post(url, body, options)
                .map(res => res.json())
                .subscribe(data => { 
                    if (data.result != 'ok') {
                      let alert = this.alertCtrl.create({
                        title: '알림',
                        subTitle: this.msgService.getMessage(data.result),
                        buttons: ['확인']
                      });
                      alert.present();
                    } else {
                      this.ga.trackEvent('차량상세', '찜: '+selectedCar.maker+'-'+selectedCar.model );  //trackEvent for Google Analytics
                      let alert = this.alertCtrl.create({
                        title: '알림',
                        subTitle: '선택하신 차량이 찜 목록에 저장되었습니다.',
                        buttons: ['확인']
                      });
                      alert.present();
                      this.cars[i].is_favorite = '1';
                      let set_key = selectedCar.key;
                      let set_type = '2'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                      this.setAnalytics.setAnalytics(set_key, set_type);
                    }
                });
            } else {  // for 찜취소
                //console.log('car.key car.is_favorite: '+this.cars[i].key+'-'+this.cars[i].is_favorite);
                let favoriteQueryParams = 'sale=' +selectedCar.key+
                                          '&email=' +this.userInfoProvider.userProfile.u_email+
                                          '&pw=' +this.userInfoProvider.userProfile.u_pw+
                                          '&favorite=' +'2'; //'1'=찜등록, '2'=찜취소
                let body   : string	 = favoriteQueryParams,  
                    type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                    headers: any		 = new Headers({ 'Content-Type': type}),
                    options: any 		 = new RequestOptions({ headers: headers }),
                    url 	 : any		 = this.marketURI + "set_sale_favorite.php";
                this.http.post(url, body, options)
                .map(res => res.json())
                .subscribe(data => { 
                    if (data.result != 'ok') {
                      let alert = this.alertCtrl.create({
                        title: '알림',
                        subTitle: this.msgService.getMessage(data.result),
                        buttons: ['확인']
                      });
                      alert.present();
                    } else {
                      let alert = this.alertCtrl.create({
                        title: '알림',
                        subTitle: '선택하신 차량이 찜 목록에서 삭제되었습니다.',
                        buttons: ['확인']
                      });
                      alert.present();
                      this.cars[i].is_favorite = '0';
                      let set_key = selectedCar.key;
                      let set_type = '3'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                      this.setAnalytics.setAnalytics(set_key, set_type);
                    }
                });              
            }
          }
        }
    }
  }

  addMyCar() {
    if(this.userInfoProvider.userProfile.login_flag == false) {
      let LoginPage_modal = this.modalCtrl.create('LoginPage', {naviPage: 'CarsPage'});
      LoginPage_modal.onDidDismiss(data => {
        if(data == true) {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: '정상적으로 로그인 되었습니다.',
            buttons: ['확인']
          });
          alert.present();
        }
      });
      LoginPage_modal.present();
    } else {
      this.ga.trackEvent('차량상세', '내차팔기');  //trackEvent for Google Analytics
      let RegistAgreePage_modal = this.modalCtrl.create('RegistAgreePage');
      RegistAgreePage_modal.present();
    }
  }

  doInfinite(infiniteScroll) {
    let param_string = this.queryParams;
    //console.log('this.doInfiniteFlag & param_string & this.queryLength =>'+this.doInfiniteFlag+'-'+param_string+'-'+this.queryLength);

    if(this.doInfiniteFlag == true) {
      if(this.userInfoProvider.userProfile.login_flag == true) {
        this.carListParam = param_string+ '&email=' +this.userInfoProvider.userProfile.u_email;
      } else {
        this.carListParam = param_string;
      }
      let body   : string	 = this.carListParam+'&key='+this.queryLength,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.marketURI + "get_sale_list.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
        if (data.result != 'ok') {
          this.doInfiniteFlag = false;
        } else {
          //this.presentLoading();
          for (let i = 0; i < Object.keys(data.sale_list).length; i++) {
            data.sale_list[i].maker = this.makerModelProvider.getMakerName(data.sale_list[i].maker);
            data.sale_list[i].model = this.makerModelProvider.getModelName(data.sale_list[i].model);

            // 거래중 blind_date:"2017-08-31 00:00:00" >= this.todayDate
            if(data.sale_list[i].blind_date.substr(0, 10) >= this.todayDate) {
              data.sale_list[i].state = '거래중';
            }

            // for display area
            let strArray = data.sale_list[i].address.split(' ');
            if(this.searchArea != '지역') {
              if(strArray[1] == this.searchArea) {
                data.sale_list[i].address = strArray[2];  
                this.cars.push(data.sale_list[i]);
              }
            } else { 
              data.sale_list[i].address = strArray[2];
              //console.log('adderss place name: '+data.sale_list[i].address);
              this.cars.push(data.sale_list[i]);
            }
          }
          if( Object.keys(data.sale_list).length < 20) {
            this.doInfiniteFlag = false;
          }
          this.queryLength = this.queryLength + Object.keys(data.sale_list).length;
          console.log('doInfinite.queryLength ===> '+Object.keys(data.sale_list).length);
          console.log('total queryLength ===> '+this.queryLength);
        }
        // console.log('Next operation has ended');
        infiniteScroll.complete();
      });
    }
  }

  presentLoading() {
    this.loadingCtrl.create({
      content: 'Loading Data...', 
      duration: 1000,
      showBackdrop: false,
      dismissOnPageChange: true
    }).present();
  }
  
}