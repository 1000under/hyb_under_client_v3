import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, LoadingController, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';  
import { SocialSharing } from '@ionic-native/social-sharing';   
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import { MakerModelProvider } from '../../providers/maker-model/maker-model';
import { SetanalyticsProvider } from '../../providers/setanalytics/setanalytics';

@IonicPage()
@Component({
  selector: 'page-category-view',
  templateUrl: 'category-view.html',
})
export class CategoryViewPage {

  login_flag: boolean;
  isMember_flag: boolean;
  todayDate: any;

  //-------------------------------------------------> for data query
  pickCars: any =[];
  carDetailParam: any;
  myFavoriteCars: any =[];
  queryParams: any;
  queryLength: number = 0;
  doInfiniteFlag:boolean = true;
  private marketURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";
  private boardURI : string  = "http://cjsekfvm.cafe24.com/test_server/app/board/url/";

  public selectedPick: any;  //parameter from SubHomePage
  carDetailTabFlag: boolean = false;

  constructor(
    public viewCtrl           : ViewController,
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public modalCtrl          : ModalController,
    public loadingCtrl        : LoadingController,
    public alertCtrl          : AlertController,
    public http               : Http,
    public userInfoProvider   : UserInfoProvider,
    public makerModelProvider : MakerModelProvider,
    public msgService         : ResponseMsgServiceProvider,
    public setAnalytics       : SetanalyticsProvider,
    private socialSharing     : SocialSharing,
    private ga                : GoogleAnalytics,) 
  {
      this.selectedPick = navParams.get("categoryParam"); // categoryParam: type: "5", image: "img/category/import.png"
      this.queryParams = 'pick='+this.selectedPick.key;

  }

  ionViewDidLoad() {
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    this.login_flag = this.userInfoProvider.userProfile.login_flag;
    this.isMember_flag = this.userInfoProvider.userProfile.isMember_flag;
    this.getCars();
    console.log('ionViewDidLoad CategoryViewPage');
  }

  ionViewDidEnter() {
    this.ga.trackView('모아보기: '+this.selectedPick.title);  //trackView for Google Analytics
    console.log('ionViewDidEnter CategoryViewPage');
  }

  getCars() {
    this.getMyFavoriteCars();
    if(this.login_flag == true) {
      this.carDetailParam = this.queryParams+ '&email=' +this.userInfoProvider.userProfile.u_email;
    } else {
      this.carDetailParam = this.queryParams;
    }
    this.pickCars =[];
    let body   : string	 = this.carDetailParam,
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.boardURI + "get_board_pick_detail_list.php";
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => {
      if (data.pick_detail_list == undefined) {
        let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '검색결과가 없습니다.',
          buttons: ['확인']
        });
        alert.present();
        this.viewCtrl.dismiss();
      } else {
        for (let i = 0; i < Object.keys(data.pick_detail_list).length; i++) {
          data.pick_detail_list[i].maker = this.makerModelProvider.getMakerName(data.pick_detail_list[i].maker);
          data.pick_detail_list[i].model = this.makerModelProvider.getModelName(data.pick_detail_list[i].model);

          // for 거래중 date_blind:"2017-08-31 00:00:00" >= this.todayDate
          if(data.pick_detail_list[i].date_blind.substr(0, 10) >= this.todayDate) {
            data.pick_detail_list[i].state = '거래중';
          }

          // for find my favorite cars
          for (let j = 0; j < this.myFavoriteCars.length; j++) {
            if(data.pick_detail_list[i].key == this.myFavoriteCars[j].key) {
              data.pick_detail_list[i].is_favorite = '1';
            }
          }

          this.pickCars.push(data.pick_detail_list[i]);
        }
        this.queryLength =Object.keys(data.pick_detail_list).length;
        console.log('getCars() ===> this.queryLength: '+this.queryLength);
        if(this.queryLength >= 20) {
          this.doInfiniteFlag = true;
        } else {
          this.doInfiniteFlag = false;
        }
      }
    });

  }

   getMyFavoriteCars() {
    this.myFavoriteCars =[];
    if(this.userInfoProvider.userProfile.login_flag == true) {
      let body   : string	 = 'order=1'+'&email=' +this.userInfoProvider.userProfile.u_email,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.marketURI + "get_sale_favorite.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => {
        if (data.result == 'ok') {
          for (let i = 0; i < Object.keys(data.sale_list).length; i++) {
            this.myFavoriteCars.push(data.sale_list[i]);
          }
        }      
      });
    }
  } 

  naviCarDetailPage(selectedCar) {
    if(!this.carDetailTabFlag) {
      this.carDetailTabFlag = true;
      if(this.login_flag == true) {
        this.carDetailParam = 'sale=' +selectedCar+ '&email=' +this.userInfoProvider.userProfile.u_email;
      } else {
        this.carDetailParam = 'sale=' +selectedCar;
      }
      let body   : string	 = this.carDetailParam,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.marketURI + "get_sale_detail.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result != 'ok') {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: this.msgService.getMessage(data.result),
              buttons: ['확인']
            });
            alert.present();
          } else {
            let CarDetailPage_modal = this.modalCtrl.create('CarDetailPage', {carParam: data.sale_detail});
            CarDetailPage_modal.onDidDismiss(data => {
              this.carDetailTabFlag = false;
            });
            CarDetailPage_modal.present();
          }
      });
    }
  }

  selectFavoriteCar(selectedCar){
    if(this.userInfoProvider.userProfile.login_flag == false) {
      let LoginPage_modal = this.modalCtrl.create('LoginPage');
      LoginPage_modal.onDidDismiss(data => {
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
      });
      LoginPage_modal.present();
    } else {
        for (let i = 0; i < this.pickCars.length; i++) {
          if(this.pickCars[i].key == selectedCar.key) {
            if(this.pickCars[i].is_favorite == '0') { // for 찜등록
                //console.log('car.key car.is_favorite: '+this.cars[i].key+'-'+this.cars[i].is_favorite);
                let favoriteQueryParams = 'sale=' +selectedCar.key+
                                          '&email=' +this.userInfoProvider.userProfile.u_email+
                                          '&pw=' +this.userInfoProvider.userProfile.u_pw+
                                          '&favorite=' +'1'; //'1'=찜등록, '2'=찜취소
                let body   : string	 = favoriteQueryParams,  
                    type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                    headers: any		 = new Headers({ 'Content-Type': type}),
                    options: any 		 = new RequestOptions({ headers: headers }),
                    url 	 : any		 = this.marketURI + "set_sale_favorite.php";
                this.http.post(url, body, options)
                .map(res => res.json())
                .subscribe(data => { 
                    if (data.result != 'ok') {
                      let alert = this.alertCtrl.create({
                        title: '알림',
                        subTitle: this.msgService.getMessage(data.result),
                        buttons: ['확인']
                      });
                      alert.present();
                    } else {
                      this.ga.trackEvent('찜', '찜: '+selectedCar.maker+'-'+selectedCar.model );  //trackEvent for Google Analytics
                      let alert = this.alertCtrl.create({
                        title: '알림',
                        subTitle: '선택하신 차량이 찜 목록에 저장되었습니다.',
                        buttons: ['확인']
                      });
                      alert.present();
                      this.pickCars[i].is_favorite = '1';
                      let set_key = selectedCar.key;
                      let set_type = '2'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                      this.setAnalytics.setAnalytics(set_key, set_type);
                    }
                });
            } else {  // for 찜취소
                //console.log('car.key car.is_favorite: '+this.cars[i].key+'-'+this.cars[i].is_favorite);
                let favoriteQueryParams = 'sale=' +selectedCar.key+
                                          '&email=' +this.userInfoProvider.userProfile.u_email+
                                          '&pw=' +this.userInfoProvider.userProfile.u_pw+
                                          '&favorite=' +'2'; //'1'=찜등록, '2'=찜취소
                let body   : string	 = favoriteQueryParams,  
                    type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                    headers: any		 = new Headers({ 'Content-Type': type}),
                    options: any 		 = new RequestOptions({ headers: headers }),
                    url 	 : any		 = this.marketURI + "set_sale_favorite.php";
                this.http.post(url, body, options)
                .map(res => res.json())
                .subscribe(data => { 
                    if (data.result != 'ok') {
                      let alert = this.alertCtrl.create({
                        title: '알림',
                        subTitle: this.msgService.getMessage(data.result),
                        buttons: ['확인']
                      });
                      alert.present();
                    } else {
                      let alert = this.alertCtrl.create({
                        title: '알림',
                        subTitle: '선택하신 차량이 찜 목록에서 삭제되었습니다.',
                        buttons: ['확인']
                      });
                      alert.present();
                      this.pickCars[i].is_favorite = '0';
                      let set_key = selectedCar.key;
                      let set_type = '3'; // 1=조회, 2=찜등록, 3=찜취소, 4=신고, 5=문자, 6=전화, 7=메일
                      this.setAnalytics.setAnalytics(set_key, set_type);
                    }
                });              
            }
          }
        }
    }
  }


  doInfinite(infiniteScroll) {
    if(this.doInfiniteFlag == true) {
        //this.queryParams = 'pick='+this.selectedPick;
        if(this.login_flag == true) {
          this.carDetailParam = this.queryParams+ '&email=' +this.userInfoProvider.userProfile.u_email;
        } else {
          this.carDetailParam = this.queryParams;
        }
        let body   : string	 = this.carDetailParam+'&key=' +this.queryLength,
            type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
            headers: any		 = new Headers({ 'Content-Type': type}),
            options: any 		 = new RequestOptions({ headers: headers }),
            url 	 : any		 = this.boardURI + "get_board_pick_detail_list.php";
        this.http.post(url, body, options)
        .map(res => res.json())
        .subscribe(data => {
          if (data.result != 'ok') {
            this.doInfiniteFlag = false;
          } else {
            for (let i = 0; i < Object.keys(data.pick_detail_list).length; i++) {
              data.pick_detail_list[i].maker = this.makerModelProvider.getMakerName(data.pick_detail_list[i].maker);
              data.pick_detail_list[i].model = this.makerModelProvider.getModelName(data.pick_detail_list[i].model);

              // for 거래중 date_blind:"2017-08-31 00:00:00" >= this.todayDate
              if(data.pick_detail_list[i].date_blind.substr(0, 10) >= this.todayDate) {
                data.pick_detail_list[i].state = '거래중';
              }

              // for find my favorite cars
              for (let j = 0; j < this.myFavoriteCars.length; j++) {
                if(data.pick_detail_list[i].key == this.myFavoriteCars[j].key) {
                  data.pick_detail_list[i].is_favorite = '1';
                }
              }

              this.pickCars.push(data.pick_detail_list[i]);
            }
            if( Object.keys(data.pick_detail_list).length < 20) {
              this.doInfiniteFlag = false;
            }
            this.queryLength = this.queryLength + Object.keys(data.pick_detail_list).length;
          }
          //console.log('Next operation has ended');
          infiniteScroll.complete();
        });
      }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
