import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';


@IonicPage()
@Component({
  selector: 'page-terms-use',
  templateUrl: 'terms-use.html',
})
export class TermsUsePage {

  termUseContent: any = '';
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private ga: GoogleAnalytics,) {
      this.termUseContent = navParams.get("termUseContent");
  }
  
  ionViewDidLoad() {
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    console.log('ionViewDidLoad TermsUsePage');
  }
  ionViewDidEnter() {
    this.ga.trackView('이용약관');  //trackView for Google Analytics
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }

}
