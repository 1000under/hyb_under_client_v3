import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistAgreePage } from './regist-agree';

@NgModule({
  declarations: [
    RegistAgreePage,
  ],
  imports: [
    IonicPageModule.forChild(RegistAgreePage),
  ],
  exports: [
    RegistAgreePage
  ]
})
export class RegistAgreePageModule {}
