import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@IonicPage()
@Component({
  selector: 'page-regist-agree',
  templateUrl: 'regist-agree.html',
})
export class RegistAgreePage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    private ga: GoogleAnalytics,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistAgreePage');
  }

  ionViewDidEnter() {
    this.ga.trackView('내차등록동의');  //trackView for Google Analytics
  }

  agree() {
    this.ga.trackEvent('내차팔기', '동의하기');  //trackEvent for Google Analytics
    let AddMyCarPage_modal = this.modalCtrl.create('AddMyCarPage');
    AddMyCarPage_modal.onDidDismiss(data => {
      this.viewCtrl.dismiss(data);
    });
    AddMyCarPage_modal.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
