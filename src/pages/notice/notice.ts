import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { DeviceProvider } from '../../providers/device/device';

@IonicPage()
@Component({
  selector: 'page-notice',
  templateUrl: 'notice.html',
})
export class NoticePage {

  notice: any =[];  //parameter from SubHomePage
  deviceType: any;

  constructor( 
    public viewCtrl         : ViewController, 
    public navCtrl          : NavController, 
    public navParams        : NavParams,
    public deviceProvider   : DeviceProvider,
    private ga              : GoogleAnalytics,) 
    {
      this.notice = navParams.get("notice_param");
    }

  ionViewDidLoad() {
    this.deviceType = this.deviceProvider.deviceInfo.d_Type; // '1': Android, '2': iOS
  }

  ionViewDidEnter() {
    this.ga.trackView('공지사항');  //trackView for Google Analytics
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}