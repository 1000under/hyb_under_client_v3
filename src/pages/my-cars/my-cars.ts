import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertController, ViewController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import { MakerModelProvider } from '../../providers/maker-model/maker-model';

@IonicPage()
@Component({
  selector: 'page-my-cars',
  templateUrl: 'my-cars.html',
})
export class MyCarsPage {

  showNothing:boolean = false;
  carDetailTabFlag: boolean = false;
  updateMyCarTabFlag: boolean = false;
  //-------------------------------------------------> for data query
  cars: any =[];
  queryParams: any;
  queryLength: number = 0;
  doInfiniteFlag:boolean = true;
  private marketURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/market/url/";

  constructor(
    public viewCtrl           : ViewController, 
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public modalCtrl          : ModalController,
    public loadingCtrl        : LoadingController,
    public alertCtrl          : AlertController,
    public http               : Http,
    private zone              : NgZone,
    private ga                : GoogleAnalytics,
    public makerModelProvider : MakerModelProvider,
    public userInfoProvider   : UserInfoProvider,
    public msgService         : ResponseMsgServiceProvider) {

    }

  ionViewDidLoad() {
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    this.getSaleMyCar();
    console.log('ionViewDidLoad MyCarsPage');
  }

  ionViewDidEnter() {
    this.ga.trackView('내등록차량');  //trackView for Google Analytics
  }

  reflashPage() {
    this.zone.run(() => {
      this.getSaleMyCar();
      console.log('Force update ======> MyCarsPage');
    });
  }

  getSaleMyCar() {
    this.cars = [];
    this.queryParams = 'email=' +this.userInfoProvider.userProfile.u_email;  
    let body   : string	 = this.queryParams, 
        type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
        headers: any		 = new Headers({ 'Content-Type': type}),
        options: any 		 = new RequestOptions({ headers: headers }),
        url 	 : any		 = this.marketURI + "get_sale_me.php";
    this.http.post(url, body, options)
    .map(res => res.json())
    .subscribe(data => { 
      if (data.result !== 'ok') {
        this.showNothing = true;
      } else {
        this.showNothing = false;
        //data.sale_list[0].state = "완료";
        for (let i = 0; i < Object.keys(data.sale_list).length; i++) {
          data.sale_list[i].maker = this.makerModelProvider.getMakerName(data.sale_list[i].maker);
          data.sale_list[i].model = this.makerModelProvider.getModelName(data.sale_list[i].model);
          let strArray = data.sale_list[i].address.split(' ');
          data.sale_list[i].address = strArray[2];
        }
        this.cars = data.sale_list;
        this.queryLength = Object.keys(data.sale_list).length;
        console.log('this.queryLength: ' +this.queryLength);
      }
    });
  }

  naviCarDetailPage(selectedCar) {
    if(!this.carDetailTabFlag) {
      this.carDetailTabFlag = true;
      this.queryParams = 'sale=' +selectedCar;
      let body   : string	 = this.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.marketURI + "get_sale_detail.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result != 'ok') {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: this.msgService.getMessage(data.result),
              buttons: ['확인']
            });
            alert.present();
          } else {
            let CarDetailPage_modal = this.modalCtrl.create('CarDetailPage',{
              carParam: data.sale_detail
            });
            CarDetailPage_modal.onDidDismiss(data => {
              this.carDetailTabFlag = false;
            });
            CarDetailPage_modal.present();
          }
      });
    }
  }


  updateMyCar(selectedCar) {
    if(!this.updateMyCarTabFlag) {
      this.updateMyCarTabFlag = true;
      this.queryParams = 'sale=' +selectedCar;
      let body   : string	 = this.queryParams,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.marketURI + "get_sale_detail.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
          if (data.result != 'ok') {
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: this.msgService.getMessage(data.result),
              buttons: ['확인']
            });
            alert.present();
          } else {
            let UpdateMyCarPage_modal = this.modalCtrl.create('UpdateMyCarPage', {
              carParam: data.sale_detail
            });
            UpdateMyCarPage_modal.onDidDismiss(data => {
              this.updateMyCarTabFlag = false;
              if(data == true) {
                  this.reflashPage();
              } 
            });
            UpdateMyCarPage_modal.present();
          }
      });
    }
  }

  soldMyCar(carKey) {

    let confirm = this.alertCtrl.create({
      title: '판매가 완료되었습니까?',
      message: '"확인"를 선택하시면 판매완료로 처리됩니다.',
      buttons: [
        {
          text: '취소',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: '확인',
          handler: () => {

            this.queryParams = 'sale=' +carKey+
                              '&email=' +this.userInfoProvider.userProfile.u_email+
                              '&pw=' +this.userInfoProvider.userProfile.u_pw+
                              '&state=' +'2'; // 1:판매중, 2:완료, 3:삭제, 4:신고, 5:검수
            console.log('this.queryParams:'+this.queryParams);
            let body   : string	 = this.queryParams, 
                type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                headers: any		 = new Headers({ 'Content-Type': type}),
                options: any 		 = new RequestOptions({ headers: headers }),
                url 	 : any		 = this.marketURI + "set_sale_state.php";
                this.http.post(url, body, options)
                .map(res => res.json())
                .subscribe(data => { 
                    if (data.result != 'ok') {
                      let alert = this.alertCtrl.create({
                        title: '알림',
                        subTitle: this.msgService.getMessage(data.result),
                        buttons: ['확인']
                      });
                      alert.present();
                    } else {
                        let alert = this.alertCtrl.create({
                          title: '알림',
                          subTitle: '정상 처리되었습니다.',
                          buttons: ['확인']
                        });
                        alert.present();
                        this.reflashPage();
                        //this.navCtrl.setRoot(this.navCtrl.getActive().component);
                    } 
                }); //====> (subscribe(data => {)

          } //====> '동의' handler:
        }
      ] //====> buttons:
    });
    confirm.present();

  }

  deleteMyCar(carKey) {

    let confirm = this.alertCtrl.create({
      title: '삭제하시겠습니까?',
      message: '"동의"를 선택하시면 차량이 삭제됩니다.',
      buttons: [
        {
          text: '취소',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: '동의',
          handler: () => {

            this.queryParams = 'sale=' +carKey+
                              '&email=' +this.userInfoProvider.userProfile.u_email+
                              '&pw=' +this.userInfoProvider.userProfile.u_pw;

            let body   : string	 = this.queryParams, 
                type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
                headers: any		 = new Headers({ 'Content-Type': type}),
                options: any 		 = new RequestOptions({ headers: headers }),
                url 	 : any		 = this.marketURI + "set_sale_delete.php";
            this.http.post(url, body, options)
            .map(res => res.json())
            .subscribe(data => {
              console.log('=======> deleteMyCar server response: '+data.result);
              if (data.result != 'ok') {
                let alert = this.alertCtrl.create({
                  title: '알림',
                  subTitle: this.msgService.getMessage(data.result),
                  buttons: ['확인']
                });
                alert.present();
              } else {
                let alert = this.alertCtrl.create({
                  title: '알림',
                  subTitle: '정상 처리되었습니다.',
                  buttons: ['확인']
                });
                alert.present();
                this.reflashPage();
                //this.navCtrl.setRoot(this.navCtrl.getActive().component);
              } 
            });
          } //====> '동의' handler:
        }
      ] //====> buttons:
    });
    confirm.present();

  }

  addMyCar(){
    this.ga.trackEvent('내등록차량', '내차팔기');  //trackEvent for Google Analytics
    let RegistAgreePage_modal = this.modalCtrl.create('RegistAgreePage');
    RegistAgreePage_modal.onDidDismiss(data => {
      if(data == true) {
          this.reflashPage();
      } 
    });
    RegistAgreePage_modal.present();
  }
   
  doInfinite(infiniteScroll) {
    if(this.doInfiniteFlag == true) {
      let body   : string	 = this.queryParams +'&key=' +this.queryLength,
          type 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
          headers: any		 = new Headers({ 'Content-Type': type}),
          options: any 		 = new RequestOptions({ headers: headers }),
          url 	 : any		 = this.marketURI + "get_sale_me.php";
      this.http.post(url, body, options)
      .map(res => res.json())
      .subscribe(data => { 
        if (data.result != 'ok') {
          this.doInfiniteFlag = false;
        } else {
          for (let i = 0; i < Object.keys(data.sale_list).length; i++) {
              data.sale_list[i].maker = this.makerModelProvider.getMakerName(data.sale_list[i].maker);
              data.sale_list[i].model = this.makerModelProvider.getModelName(data.sale_list[i].model);
              let strArray = data.sale_list[i].address.split(' ');
              data.sale_list[i].address = strArray[2];
              this.cars.push(data.sale_list[i]);
          }
          if( Object.keys(data.pick_detail_list).length < 20) {
            this.doInfiniteFlag = false;
          }
          this.queryLength = this.queryLength + Object.keys(data.sale_list).length;
          console.log('this.queryLength: ' +this.queryLength);
        }
      });
      infiniteScroll.complete();
    }
  }

  backToProfile() {
    this.navCtrl.pop();
    //this.viewCtrl.dismiss();
  }

}
