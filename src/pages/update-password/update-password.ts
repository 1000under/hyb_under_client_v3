import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

//-----> Providers
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';


@IonicPage()
@Component({
  selector: 'page-update-password',
  templateUrl: 'update-password.html',
})
export class UpdatePasswordPage {

  //-------------------------------------------------> for data query
  queryParams: any;
  private memberURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/member/url/";

  //-------------------------------------------------> loginParams for userInfoProvider
  loginParams: any = [
    { loginApp:       '' },
    { u_name:         '' },
    { u_phone_num:    '' },
    { u_email:        '' },
    { u_pw:           '' },
    { u_photo:        '' },
    { u_type:         '' },
    { cu_flag:        false},
    { fb_flag:        false},
    { kk_flag:        false},
    { gg_flag:        false},
    { login_flag:     false},
    { isMember_flag:   false},
  ];

  userProfile_pw: any ='';
  currentPassword: any ='';
  newPassword: any = '';
  confirmNewPassword: any = '';
  
  isSuccess: boolean = false;

  constructor( 
    public navCtrl            : NavController, 
    public navParams          : NavParams,
    public viewCtrl           : ViewController,
    public alertCtrl          : AlertController,
    public modalCtrl          : ModalController,
    public http               : Http,
    public userInfoProvider   : UserInfoProvider,
    public msgService         : ResponseMsgServiceProvider,
    private ga                : GoogleAnalytics,) {
      this.userProfile_pw = navParams.get("userProfile_pw");
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdatePasswordPage');
  }
  ionViewDidEnter() {
    this.ga.trackView('비밀번호변경');  //trackView for Google Analytics
  }
  submitUpdate() {
      let chkPwdResualt = chkPwd(this.newPassword);
      if(this.currentPassword == '') {
          let alert = this.alertCtrl.create({
            title: '알림!',
            subTitle: '현재 비밀번호를 입력 해 주세요.',
            buttons: ['확인']
          });
          alert.present();
      } else if(this.newPassword == '') {
        let alert = this.alertCtrl.create({
          title: '알림!',
          subTitle: '새 비밀번호를 입력 해 주세요.',
          buttons: ['확인']
        });
        alert.present();
      } else if(this.confirmNewPassword == '') {
        let alert = this.alertCtrl.create({
          title: '알림!',
          subTitle: '새 비밀번호 확인을 입력 해 주세요.',
          buttons: ['확인']
        });
        alert.present();
      } else if(this.currentPassword != this.userProfile_pw) {
          let alert = this.alertCtrl.create({
            title: '알림!',
            subTitle: '"현재 비밀번호"가 올바르지 않습니다.',
            buttons: ['확인']
          });
          alert.present();
      } else if(this.newPassword != this.confirmNewPassword) {
          let alert = this.alertCtrl.create({
            title: '알림!',
            subTitle: '"비밀번호 확인"이 올바르지 않습니다.',
            buttons: ['확인']
          });
          alert.present();
      } else if(chkPwdResualt != 'good'){
              let alert = this.alertCtrl.create({
                title: '알림!',
                subTitle: chkPwdResualt,
                buttons: ['확인']
              });
              alert.present();
      } else {

        console.log('<------------------ Input Items ---------------->');          
        console.log('pw: ' +this.userProfile_pw);

        this.queryParams = 'email='+this.userInfoProvider.userProfile.u_email+
                           '&pw='+this.userInfoProvider.userProfile.u_pw+
                           '&rpw='+this.confirmNewPassword;
        //----------------------------------------------------------------> get Similar cars
        let body_s   : string	 = this.queryParams,
            type_s 	 : string	 = 'application/x-www-form-urlencoded; charset=UTF-8',
            headers_s: any		 = new Headers({ 'Content-Type': type_s}),
            options_s: any 		 = new RequestOptions({ headers: headers_s }),
            url_s 	 : any		 = this.memberURI + "set_member_modify.php";
        this.http.post(url_s, body_s, options_s)
        .map(res => res.json())
        .subscribe(data => { 
            if (data.result != 'ok') {
              let alert = this.alertCtrl.create({
                title: '알림',
                subTitle: this.msgService.getMessage(data.result),
                buttons: ['확인']
              });
              alert.present();
            } else {
              this.loginParams.loginApp = '1000Under';
              this.loginParams.u_name = this.userInfoProvider.userProfile.u_name;
              this.loginParams.u_phone_num = this.userInfoProvider.userProfile.u_phone_num;
              this.loginParams.u_email = this.userInfoProvider.userProfile.u_email;
              this.loginParams.u_pw = this.confirmNewPassword;
              this.loginParams.u_type = '1';            //가입경로 1: 1000Under, 2: Facebook, 3: Kakao, 4: Google
              this.loginParams.cu_flag = true;
              this.loginParams.login_flag = true;
              this.loginParams.isMember_flag = true;
              this.userInfoProvider.setProfile(this.loginParams);
              let alert = this.alertCtrl.create({
                title: '알림',
                subTitle: '비밀번호 변경이 완료되었습니다.',
                buttons: ['확인']
              });
              alert.present();
              this.isSuccess = true;
              this.viewCtrl.dismiss(this.isSuccess);
            }
        });

      }

      function chkPwd(str){
        let pw = str;
        let num = pw.search(/[0-9]/g);
        let eng = pw.search(/[a-z]/ig);
        //let spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

        if(pw.length < 6 || pw.length > 15){
          return '영.숫자 포함 6자리 ~ 15자리 이내로 입력해주세요.';
        }
        if(pw.search(/₩s/) != -1){
          return '비밀번호는 공백없이 입력해주세요.';
        } 
        if(num < 0 || eng < 0){
          return '영문.숫자를 혼합하여 입력해주세요.';
        }
        return 'good';
      };
  }

  dismiss() {
    this.viewCtrl.dismiss(this.loginParams);
  }

}
