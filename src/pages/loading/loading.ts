import { Component } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

//-----> Providers
import { DeviceProvider } from '../../providers/device/device';
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import { OuterLinkProvider } from '../../providers/outer-link/outer-link';
import { MakerModelProvider } from '../../providers/maker-model/maker-model';

@IonicPage()
@Component({
  selector: 'page-loading',
  templateUrl: 'loading.html',
})
export class LoadingPage {

  public options;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    private storage: Storage,
    public deviceProvider: DeviceProvider,
    public userInfoProvider: UserInfoProvider,
    public msgService: ResponseMsgServiceProvider,
    public outerLinkProvider: OuterLinkProvider,
    public makerModelProvider: MakerModelProvider,) {
      this.platform.ready().then(() => { 
        this.deviceProvider.setDeviceInfo();
        this.userInfoProvider.initLogin();
        this.outerLinkProvider.getOuterLink();
        this.outerLinkProvider.getPolicy();
        this.makerModelProvider.getModelList();
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  ionViewDidEnter() {
    this.makerModelProvider.getModelList(); // second request for get to MakerName & ModelName 
    this.storage.get('showIntro').then((val) => {
      console.log('showIntro value: '+val);
      if (val == 0) {
        //this.navCtrl.pop();
        //this.navCtrl.push('TabsPage');
        this.navCtrl.setRoot('TabsPage');
      } else {
        setTimeout(() => {
          this.navCtrl.setRoot('IntroPage');
        }, 100);
        //this.navCtrl.push('IntroPage');
      }
    }, () => {
      setTimeout(() => {
        this.navCtrl.setRoot('IntroPage');
      }, 100);
      //this.navCtrl.push('IntroPage');
    });
  }
}
