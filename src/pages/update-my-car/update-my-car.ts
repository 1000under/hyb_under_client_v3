import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, Platform, ViewController, NavController, NavParams, ModalController, LoadingController, AlertController, } from 'ionic-angular';
import { Http } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { ImagePicker } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import numeral from 'numeral';

//-----> Providers
import { UserInfoProvider } from '../../providers/user-info/user-info';
import { ResponseMsgServiceProvider } from '../../providers/response-msg-service/response-msg-service';
import { MakerModelProvider } from '../../providers/maker-model/maker-model';

declare var google;

@IonicPage()
@Component({
  selector: 'page-update-my-car',
  templateUrl: 'update-my-car.html',
  providers: [ImagePicker]
})
export class UpdateMyCarPage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('places') places: ElementRef;
  map: any;

  //-------> for input focus
  @ViewChild('f_owner_name') f_owner_name;
  @ViewChild('f_phone') f_phone;
  @ViewChild('f_str_price') f_str_price;
  @ViewChild('f_model_level') f_model_level;
  @ViewChild('f_car_number') f_car_number;
  @ViewChild('f_str_mileage') f_str_mileage;
  @ViewChild('f_introduce') f_introduce;

  public selectedCar: any;

  //-------> connect to server .php
  categorys: any =[];
  private marketURI: string  = "http://cjsekfvm.cafe24.com/test_server/app/market/url/"; 

  searchBrand: any;
  showModelFlag: boolean = false;
  gpsFlag: boolean;
  navigationFlag: boolean = false;
  sunLoopFlag: boolean = false;
  blackBoxFlag: boolean = false;
  warrantyFlag: boolean = false;
  rearSensorFlag: boolean = false;
  startButtonFlag: boolean = false;
  rearCameraFlag: boolean = false;
  heatingSeatFlag: boolean = false;
  
  //images;
  rep: any = '';
  registDoc: any = '';
  once: any = '';
  byOne: any = '';
  navigation_on: any = '';
  navigation_off: any = '';
  sunLoop_on: any = '';
  sunLoop_off: any = '';
  blackBox_on: any = '';
  blackBox_off: any = '';
  warranty_on: any = '';
  warranty_off: any = '';
  rearSensor_on: any = '';
  rearSensor_off: any = '';
  startButton_on: any = '';
  startButton_off: any = '';
  rearCamera_on: any = '';
  rearCamera_off: any = '';
  heatingSeat_on: any = '';
  heatingSeat_off: any = '';
  
  getRepImgUrl: any = '';             //--> 대표 이미지 url
  repImgFileName: any = 'noChange';      //--> 대표 이미지
  getRegistImgUrl: any = '';          //--> 등록증 이미지 url
  registImgFileName: any = 'noChange';   //--> 등록증 이미지
  getOtherImgCnt: number = 0;         //--> 추가 이미지 수량
  getOtherImgUrl: any = ['noImg', 'noImg', 'noImg', 'noImg', 'noImg', 'noImg', 'noImg'];          //--> 추가 이미지 url
  otherImgFileName: any = ['noChange', 'noChange', 'noChange', 'noChange', 'noChange', 'noChange', 'noChange'];        //--> 추가 이미지

  mainImgFlag: any = [false, false];
  subImgFlag: any = [false, false, false, false, false, false];
  delFlag: any = [false, false, false, false, false, false];

  photo_main: any=[];                 //--> 메인 이미지 for Blob file
  photo_main_order: any=[];          //--> 메인 순서
  photo_sub: any=[];                  //--> 서브 이미지 for Blob file
  photo_sub_order: any=[];           //--> 서브 순서
  photo_delete: any=[];               //--> 서브 사진 삭제 번호(1,4)

  sale: any = '';                     //--> 매물 key
  maker: string = '';                 //--> 제조사명
  maker_key: string = '';
  model: string = '';                 //--> 모델명
  model_key: string = '';
  model_sub: string = '';             //--> 세부모델명
  model_sub_key: string = '';
  model_level: string = '';           //--> 모델레벨
  email: string = '';                 //--> 맴버 이메일
  phone: string = '';                 //--> 맴버 전화번호
  puer: string = '';                  //--> 연료
  car_number: string = '';            //--> 차량 등록 번호
  yearMonth: string = '';             
  year: string = '';                  //--> 최초등록년도
  month: string = '';                 //--> 최초등록월
  price: number = 0;                  //--> 차량 가격
  str_price: string = '';
  mileage: number = 0;                //--> 주행거리
  str_mileage: string = '';
  introduce: string = '';             //--> 내차 소개
  address: string = '';               //--> 차량 소재지 주소
  address_lat: number = 0;            //--> 위도
  address_lon: number = 0;            //--> 경도
  owner_name: string = '';            //--> 소유주 이름
  color: string = '';                 //--> 색상
  gear: string = '';                  //--> 변속기

  location: string = 'initial';       //--> 차량 위치 for UI
  searchPlace: string = '';           //--> 위치 검색 for UI
  model_type: string = '';            //--> 모델 타입 no Used
  additory_options: string = '';      //--> 추가옵션
  repair: string = '';                //--> 정비이력
  replacement: string = '';           //--> 교환이력
  options: any = [];                  //--> 주요옵션 [1:네비게이션, 2:선루프, 3:블랙박스, 4:제조사보증, 5:후방센서, 6:START버튼, 7:후방카메라, 8:열선시트]

  puers = [
    { puer: '디젤'},
    { puer: '가솔린'},
    { puer: '하이브리드'},
    { puer: '전기'},
    { puer: '가스'},
    { puer: '기타'}
  ];
  gears = [
    { gear: '자동'},
    { gear: '수동'},
    { gear: '기타'}
  ];
  colors = [
    { c_color: '검정색' },
    { c_color: '흰색' },
    { c_color: '은색' },
    { c_color: '은회색' },
    { c_color: '쥐색' },
    { c_color: '청색' },
    { c_color: '하늘색' },
    { c_color: '갈대색' },
    { c_color: '갈색' },
    { c_color: '자주색' },
    { c_color: '빨간색' }
  ];

  constructor(
    public viewCtrl         : ViewController, 
    public navCtrl          : NavController, 
    public navParams        : NavParams,
    public modalCtrl        : ModalController,
    public alertCtrl        : AlertController,
    public loadingCtrl      : LoadingController,
    public http             : Http,
    public platform         : Platform,
    public userInfoProvider : UserInfoProvider,
    public makerModelProvider : MakerModelProvider,
    public msgService       : ResponseMsgServiceProvider,
    private imagePicker     : ImagePicker,
    private transfer        : Transfer,
    private file            : File, 
    private filePath        : FilePath,
    private geolocation     : Geolocation,
    private ga              : GoogleAnalytics,){
      this.selectedCar = navParams.get("carParam");
      this.rep = "img/icon/regist/rep.png";
      this.registDoc = "img/icon/regist/registDoc.png";
      this.once = "img/icon/regist/once.png";
      this.byOne = "img/icon/regist/byOne.png";
      this.navigation_on = "img/option/navigation_on.png";
      this.navigation_off = "img/option/navigation_off.png";
      this.sunLoop_on = "img/option/sunLoop_on.png";
      this.sunLoop_off = "img/option/sunLoop_off.png";
      this.blackBox_on = "img/option/blackBox_on.png";
      this.blackBox_off = "img/option/blackBox_off.png";
      this.warranty_on = "img/option/warranty_on.png";
      this.warranty_off = "img/option/warranty_off.png";
      this.rearSensor_on = "img/option/rearSensor_on.png";
      this.rearSensor_off = "img/option/rearSensor_off.png";
      this.startButton_on = "img/option/startButton_on.png";
      this.startButton_off = "img/option/startButton_off.png";
      this.rearCamera_on = "img/option/rearCamera_on.png";
      this.rearCamera_off = "img/option/rearCamera_off.png";
      this.heatingSeat_on = "img/option/heatingSeat_on.png";
      this.heatingSeat_off = "img/option/heatingSeat_off.png";
  }

  ionViewDidLoad() {  //-----------------------------------------------------------------> ionViewDidLoad()
    this.viewCtrl.setBackButtonText(' '); //remove "Back" text on ion-navbar
    console.log('ionViewDidLoad UpdateMyCarPage');
  }

  ionViewDidEnter() {
    this.ga.trackView('내차량정보변경');  //trackView for Google Analytics
    this.showModelFlag = true;
    this.sale = this.selectedCar.key;                            //--> 매물
    this.maker_key = this.selectedCar.maker;
    this.model_key = this.selectedCar.model;
    this.model_sub_key = this.selectedCar.model_sub;
    this.phone = this.selectedCar.phone;                          //--> phone
    this.gear = this.selectedCar.gear;  
    this.puer = this.selectedCar.puer;                            //--> 연료
    this.car_number = this.selectedCar.car_number;                //--> 차량 등록 번호

    this.year = this.selectedCar.year;                            //--> 최초 등록 연도
    this.month = this.selectedCar.month;                          //--> 최초 등록 월
    if(this.month.indexOf("0") == -1) {
      this.month = "0"+this.month;
    }
    this.yearMonth = this.year+'-'+this.month;

    this.price = this.selectedCar.price;                          //--> 차량 가격
    //this.str_price = this.selectedCar.price.toString();
    this.str_price = numeral(this.price).format('0,0');

    this.mileage = this.selectedCar.mileage;                      //--> 주행거리
    //this.str_mileage = this.selectedCar.mileage.toString();
    this.str_mileage = numeral(this.mileage).format('0,0');

    if(this.selectedCar.options != null) {
      this.options = this.selectedCar.options.split(","); //--> 주요옵션 [1:네비게이션, 2:선루프, 3:블랙박스, 4:제조사보증, 5:후방센서, 6:START버튼, 7:후방카메라, 8:열선시트]
      //let optionsArray = this.selectedCar.options.split(",");
      for(let i=0; i<=this.options.length; i++) {
        switch(this.options[i]) { 
          case "1": { this.navigationFlag = true; break; }
          case "2": { this.sunLoopFlag = true; break; } 
          case "3": { this.blackBoxFlag = true; break; }
          case "4": { this.warrantyFlag = true; break; }
          case "5": { this.rearSensorFlag = true; break; }
          case "6": { this.startButtonFlag = true; break; } 
          case "7": { this.rearCameraFlag = true; break; }
          case "8": { this.heatingSeatFlag = true; break; } 
          default: { break; } 
        } 
      }
    }
    this.introduce = this.selectedCar.memo;                       //--> 내차 소개
    this.introduce = this.introduce.replace(/\n/g,''); //개행 제거
    this.introduce = this.introduce.replace(/\r/g,''); //엔터 제거
    this.model_level = this.selectedCar.model_level;              //--> 모델 레벨
    this.additory_options = this.selectedCar.additory_options;    //--> 추가옵션
    this.repair = this.selectedCar.repair;                        //--> 수리내역
    this.replacement = this.selectedCar.replacement;              //--> 교체내역

    let j = 0;
    for (let i = 0; i < this.selectedCar.photo_list.length; i++) {
      if (i == 0) {
        this.getRepImgUrl = this.selectedCar.photo_list[0].path;            //--> 대표 이미지 url
      } else if(i == 1) {
        this.getRegistImgUrl = this.selectedCar.photo_list[1].path;         //--> 등록증 이미지 url
      } else {    
        if(this.selectedCar.photo_list[i].type != '9' && this.selectedCar.photo_list[i].type != '10') { //photo_list.type: 9(차량원부), 10(사고이력) 제외
            this.getOtherImgUrl[j] = this.selectedCar.photo_list[i].path;       //--> 추가 이미지 url
            j++;  //--> 추가 이미지 수량
        }
      }
    }
    
    this.getOtherImgCnt = j + 1;

    this.address = this.selectedCar.address;                      //--> 차량 소재지 주소
    this.searchPlace= this.selectedCar.address;
    this.address_lat = this.selectedCar.address_lat;              //--> 위도
    this.address_lon = this.selectedCar.address_lon;              //--> 경도
    this.owner_name = this.selectedCar.owner_name;                //--> 소유주 이름
    this.color = this.selectedCar.color;                          //--> 색상

    this.maker = this.makerModelProvider.getMakerName(this.selectedCar.maker);  //--> 제조사명
    this.model = this.makerModelProvider.getModelName(this.selectedCar.model);  //--> 모델명
    this.searchBrand = this.maker+'-'+this.model;
    this.model_sub = this.makerModelProvider.getModelSubName(this.selectedCar.model_sub);  //--> 세부모델명  
    this.loadMap();
  }

  loadMap(){  //-------------------------------------------------------------------------> loadMap()
    this.geolocation.getCurrentPosition().then((position) => {
      
      let latLng = new google.maps.LatLng(this.address_lat, this.address_lon);
      let mapOptions = {
        center: latLng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        fullscreenControl: false
      }
      
      const element: HTMLElement = document.getElementById('umap');
      this.map = new google.maps.Map(element, mapOptions);
	    let input_from = /** @type {HTMLInputElement} */ (document.getElementById('place_input'));
      let options = {
          types: [],
          componentRestrictions: {country: 'kr'}
      };

      // create the two autocompletes on the place_input fields
      let autocomplete = new google.maps.places.Autocomplete(input_from, options);
      autocomplete.bindTo('bounds', this.map);
      // we need to save a reference to this as we lose it in the callbacks
      let self = this;

      //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      let infowindow = new google.maps.InfoWindow();
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: this.map.getCenter()
      });

      // add the first listener
      autocomplete.addListener('place_changed', function() {
        infowindow.close();
        let place = autocomplete.getPlace();
        if (!place.geometry) {
          return;
        } else {
          self.location = place.name;
          self.address = place.formatted_address;
          self.address_lat = place.geometry.location.lat();
          self.address_lon = place.geometry.location.lng();
        }
        // Set the position of the marker using the place ID and location.
        marker.setPlace( /** @type {!google.maps.Place} */ ({
            placeId: place.place_id,
            location: place.geometry.location
        }));
        marker.setVisible(true);
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + '<br>' + place.formatted_address);
        infowindow.open(this.map, marker);
        self.f_introduce.setFocus();
      });

    });
  } 

  updateRepImage() {
    let options = {
        maximumImagesCount: 1, width: 800, height: 800, quality: 100
    };
    this.imagePicker.getPictures(options).then((results) => {
      if(results[0] != undefined) {
        this.getRepImgUrl = results[0];
        console.log('this.getRepImgUrl: '+this.getRepImgUrl);
        if (this.platform.is('android')) {
          this.filePath.resolveNativePath(this.getRepImgUrl)
          .then(filePath => {
            let d = new Date();
            let n = d.getTime();
            this.repImgFileName =  this.userInfoProvider.userProfile.u_email+ "_rep_" +n+ ".jpg";
            this.CreateMainImage(filePath,0);
          });
        } else {
          let d = new Date();
          let n = d.getTime();
          this.repImgFileName =  this.userInfoProvider.userProfile.u_email+ "_rep_" +n+ ".jpg";
          this.CreateMainImage(this.getRepImgUrl,0);
        }
        if(this.mainImgFlag[0] == false) {
          this.mainImgFlag[0] = true;
          this.photo_main_order.push('1');
          console.log('repImg order: '+this.photo_main_order);
        }
      }
    }, (err) => {
      console.log('imagePicker.getPictures ERROR: '+err);
    });
  }

  updateRegistImage() {
    let options = {
        maximumImagesCount: 1, width: 800, height: 800, quality: 100
    };
    this.imagePicker.getPictures(options).then((results) => {
      if(results[0] != undefined) {
        this.getRegistImgUrl = results[0];
        console.log('this.getRegistImgUrl: '+this.getRegistImgUrl);
        if (this.platform.is('android')) {
          this.filePath.resolveNativePath(this.getRegistImgUrl)
          .then(filePath => {
            //Create a new name for the Image
            let d = new Date();
            let n = d.getTime();
            this.registImgFileName =  this.userInfoProvider.userProfile.u_email+ "_regist_" +n+ ".jpg";
            this.CreateMainImage(filePath,1);
          });
        } else {
          //Create a new name for the Image
          let d = new Date();
          let n = d.getTime();
          this.registImgFileName =  this.userInfoProvider.userProfile.u_email+ "_regist_" +n+ ".jpg";
          this.CreateMainImage(this.getRegistImgUrl,1);
        }
        if(this.mainImgFlag[1] == false) {
          this.mainImgFlag[1] = true;
          this.photo_main_order.push('2');
          console.log('registImg order: '+this.photo_main_order);
        }
      }
    }, (err) => {
      console.log('imagePicker.getPictures ERROR: '+err);
    });
  }

  selectedOtherImage(selectedImgUrl) {
    let subImgIndex = 0;
    console.log('getOtherImgUrl[0]' +selectedImgUrl);
    for (let i = 0; i < this.getOtherImgUrl.length ; i++) {
      if (this.getOtherImgUrl[i] == selectedImgUrl) {
        subImgIndex = i; //--> select subImage subImgIndex
      }
    }
    let alert = this.alertCtrl.create();
    alert.setTitle('이미지 수정/삭제');

    alert.addInput({
      type: 'checkbox',
      label: '수정',
      value: 'replace'
    });

    alert.addInput({
      type: 'checkbox',
      label: '삭제',
      value: 'delete'
    });

    alert.addButton('취소');
    alert.addButton({
      text: '선택',
      handler: data => {
        console.log('Checkbox data:'+data);
        if(data == 'replace') {
          this.updateOtherImage(subImgIndex);
        } else {
          this.photo_delete.push((subImgIndex+1).toString());
          //this.getOtherImgUrl[subImgIndex] = 'noImg';
          this.delFlag[subImgIndex] = true;
          this.subImgFlag[subImgIndex] = false;
        }
      }
    });
    alert.present();
  }

  addOtherImage(selectedImgUrl) {
    let subImgIndex = 0;
    for (let i = 0; i < this.getOtherImgUrl.length ; i++) {
      if (this.getOtherImgUrl[i] == selectedImgUrl) {
        subImgIndex = i; //--> select subImage subImgIndex
        console.log('selectedImgUrl: noImg: ['+subImgIndex+ ']-'+this.getOtherImgUrl[i]);
        break;
      }
    }
    this.getOtherImgCnt = this.getOtherImgCnt + 1;
    this.updateOtherImage(subImgIndex);
  }

  updateOtherImage(subImgIndex) {
    this.subImgFlag[subImgIndex] = true;
    let options = {
        maximumImagesCount: 1, width: 800, height: 800, quality: 100
    };

    this.imagePicker.getPictures(options).then((results) => {
      if(results[0] != undefined) {
        this.getOtherImgUrl[subImgIndex] = results[0];
        console.log('this.getOtherImgUrl: '+this.getOtherImgUrl[subImgIndex]);       
        if (this.platform.is('android')) {
          this.filePath.resolveNativePath(this.getOtherImgUrl[subImgIndex])
          .then(filePath => {
            //Create a new name for the Image
            let d = new Date();
            let n = d.getTime();
            this.otherImgFileName[subImgIndex] =  this.userInfoProvider.userProfile.u_email+ "_other+"+[subImgIndex]+"_"+n+ ".jpg";
            this.CreateSubImage(filePath, subImgIndex);
          });
        } else {
          //Create a new name for the Image
          let d = new Date();
          let n = d.getTime();
          this.otherImgFileName[subImgIndex] =  this.userInfoProvider.userProfile.u_email+ "_other+"+[subImgIndex]+"_"+n+ ".jpg";  
          this.CreateSubImage(this.getOtherImgUrl[subImgIndex], subImgIndex);
        }
      }
    }, (err) => {
      console.log('imagePicker.getPictures ERROR: '+err);
    });
  }

  //Main Image파일 생성
  public CreateMainImage(fileName,index)
  {
      this.file.resolveLocalFilesystemUrl(fileName).then((fileEntry : any) => {

        fileEntry.file((resFile) => {
          var reader = new FileReader();
          reader.onloadend = (evt: any) => {
            var imgBlob: any = new Blob([evt.target.result], { type: 'image/jpeg' });
            imgBlob.name = fileName;
            this.photo_main[index] = imgBlob;
            //console.log('Create MainImage: ' +fileName);
          };
          reader.onerror = (e) => {
            console.log("Failed file read: " + e.toString());
          };
          reader.readAsArrayBuffer(resFile);
        });

      }, (err) => {
        console.log(err);
        alert(JSON.stringify(err))
      });
  }
  
  //Sub Image파일 생성
  public CreateSubImage(fileName,index)
  {
      this.file.resolveLocalFilesystemUrl(fileName).then((fileEntry : any) => {

        fileEntry.file((resFile) => {
          var reader = new FileReader();
          reader.onloadend = (evt: any) => {
            var imgBlob: any = new Blob([evt.target.result], { type: 'image/jpeg' });
            imgBlob.name = fileName;
            this.photo_sub[index] = imgBlob;
            this.photo_sub_order[index] = (index+1).toString();
            //console.log('Create SubImage: ' +fileName);
          };
          reader.onerror = (e) => {
            console.log("Failed file read: " + e.toString());
          };
          reader.readAsArrayBuffer(resFile);
        });

      }, (err) => {
        console.log(err);
        alert(JSON.stringify(err))
      });
  }

  sameOwner() {
    this.owner_name = this.userInfoProvider.userProfile.u_name;
    if(this.userInfoProvider.userProfile.u_phone_num == 'google' || 
       this.userInfoProvider.userProfile.u_phone_num == 'facebook' ||
       this.userInfoProvider.userProfile.u_phone_num == 'kakao'){
      this.phone = '';
    } else {
      this.phone = this.userInfoProvider.userProfile.u_phone_num;
    }
  }
  
  SearchBrand(){ //Modal ------------------------------------------------------->  SearchBrand()
    this.model_sub_key = '0';
    this.model_sub = '';
    let searchBrandPage_modal = this.modalCtrl.create('SearchBrandPage',{fromPage: 'updateMyCarPage'});
    searchBrandPage_modal.onDidDismiss(data => {
      this.searchBrand = '';        
      if (data.maker_name == '전체' || data.model_name == '전체') {
        this.showModelFlag = false;
      } else {
        //{maker_key: "1", maker_name: "현대", model_key: "9", model_name: "i30"}
        this.searchBrand = data.maker_name+'-'+data.model_name;
        this.maker_key = data.maker_key;
        this.maker = data.maker_name;
        this.model_key = data.model_key;
        this.model = data.model_name;
        this.model_sub_key = data.level_key;
        this.model_sub = data.level_name;
        this.showModelFlag = true;
      }
      this.f_model_level.setFocus();
    });
    searchBrandPage_modal.present();
  }
  //-------------------------------------------------> for next input focus
  f_owner_name_eventHandler(e) {
    if(e.keyCode == 13) {
      this.f_phone.setFocus();
    }
  }
  f_phone_eventHandler(e) {
    if(e.keyCode == 13) {
      if(!this.chk_phoneNumber(this.phone)) {
        let alert = this.alertCtrl.create({
          title: '알림!',
          subTitle: '잘못된 전화번호입니다. 숫자만 입력해주세요',
          buttons: ['확인']
        });
        alert.present();
        this.phone = '';
      } else {
        this.f_str_price.setFocus();
      }
    }    
  }

  onChangePrice(evt) {
    this.str_price = evt.replace(/[^0-9.]/g, "");
    if (this.str_price) {
      let myNumeral = numeral(this.str_price);
      let value = myNumeral.value();
      this.price = Number(value);
      if(this.price > 0) {
        this.str_price = numeral(this.price).format('0,0');
      } else {
        let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: '차량가격을 입력해주세요',
            buttons: ['확인']
          });
        alert.present(); 
      }
    } else {
      this.str_price = '0'; this.price = 0;
    }
  }

  f_model_level_eventHandler(e) {
    if(e.keyCode == 13) {
      this.f_car_number.setFocus();
    }    
  }
  f_car_number_eventHandler(e) {
    if(e.keyCode == 13) {
      this.f_str_mileage.setFocus();
    } 
  }

  onChangeMileage(evt) {
    this.str_mileage = evt.replace(/[^0-9.]/g, "");
    if (this.str_mileage) {
      let myNumeral = numeral(this.str_mileage);
      let value = myNumeral.value();
      this.mileage = Number(value);
      if(this.mileage > 0) {
        this.str_mileage = numeral(value).format('0,0');        
      } else {
        let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: '주행거리를 입력해주세요',
            buttons: ['확인']
          });
        alert.present(); 
      }
    } else {
      this.str_mileage = '0'; this.mileage = 0;
    }
  }  
  //-------------------------------------------------> for next input focus

  chk_phoneNumber(phoneNumber) {   
    console.log('check this.phoneNumber :' +phoneNumber);
    var regExp = /^[0-9]+$/;
    if ( !regExp.test(phoneNumber) ) {
      return false;
    } else {
      return true;
    }
  } 
   
  selectedColor(e) {  //------------------------------------------------------------------> selectedColor()
    console.log('color:' +this.color);
  }  
  selectedPuer(e) {  //-------------------------------------------------------------------> selectedpuer()
    console.log('puer:' +this.puer);
  }
  selectedGear(e) {  //-----------------------------------------------------------> selectedselectedTransmission()
    console.log('gear:' +this.gear);
  }

  // [1:네비게이션, 2:선루프, 3:블랙박스, 4:제조사보증, 5:후방센서, 6:START버튼, 7:후방카메라, 8:열선시트]
  navigationOnOff() { 
    if( this.navigationFlag == false ) { 
      this.navigationFlag = true; 
      this.options.push('1');
    } else { 
      this.navigationFlag = false;
      let index = this.options.indexOf('1');
      if(index > -1){
         this.options.splice(index, 1);
      }
    }
    //console.log('this.options : '+this.options);
  }
  sunLoopOnOff() { 
    if( this.sunLoopFlag == false ) { 
      this.sunLoopFlag = true; 
      this.options.push('2');
    } else { 
      this.sunLoopFlag = false; 
      let index = this.options.indexOf('2');
      if(index > -1){
         this.options.splice(index, 1);
      }
    }
    //console.log('this.options : '+this.options);
  }
  blackBoxOnOff() { 
    if( this.blackBoxFlag == false ) { 
      this.blackBoxFlag = true;
      this.options.push('3'); 
    } else {
      this.blackBoxFlag = false;
      let index = this.options.indexOf('3');
      if(index > -1){
         this.options.splice(index, 1);
      } 
    }
    //console.log('this.options : '+this.options);
  }
  warrantyOnOff() { 
    if( this.warrantyFlag == false ) { 
      this.warrantyFlag = true; 
      this.options.push('4');
    } else { 
      this.warrantyFlag = false; 
      let index = this.options.indexOf('4');
      if(index > -1){
         this.options.splice(index, 1);
      }
    }
    //console.log('this.options : '+this.options);
  }
  rearSensorOnOff() { 
    if( this.rearSensorFlag == false ) { 
      this.rearSensorFlag = true; 
      this.options.push('5');
    } else { 
      this.rearSensorFlag = false; 
      let index = this.options.indexOf('5');
      if(index > -1){
         this.options.splice(index, 1);
      }
    }
    //console.log('this.options : '+this.options);
  }
  startButtonOnOff() { 
    if( this.startButtonFlag == false ) { 
      this.startButtonFlag = true; 
      this.options.push('6');
    } else { 
      this.startButtonFlag = false; 
      let index = this.options.indexOf('6');
      if(index > -1){
         this.options.splice(index, 1);
      }
    }
    //console.log('this.options : '+this.options);
  }
  rearCameraOnOff() { 
    if( this.rearCameraFlag == false ) { 
      this.rearCameraFlag = true; 
      this.options.push('7');
    } else { 
      this.rearCameraFlag = false; 
      let index = this.options.indexOf('7');
      if(index > -1){
         this.options.splice(index, 1);
      }
    }
    //console.log('this.options : '+this.options);
  }
  heatingSeatOnOff() { 
    if( this.heatingSeatFlag == false ) { 
      this.heatingSeatFlag = true;
      this.options.push('8'); 
    } else { 
      this.heatingSeatFlag = false;
      let index = this.options.indexOf('8');
      if(index > -1){
         this.options.splice(index, 1);
      } 
    }
    //console.log('this.options : '+this.options);
  }

  submitRegiste(){  //------------------------------------------------------------------------->  submitRegiste()

    if(this.owner_name == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량 소유주 이름을 입력해주세요',
          buttons: ['확인']
        });
      alert.present();      
    } else if(!this.chk_phoneNumber(this.phone)) {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '잘못된 전화번호입니다. 차량 소유주 연락처를 입력해주세요',
          buttons: ['확인']
        });
      alert.present();      
      this.phone = '';
    } else if(this.price == 0) {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량가격을 입력해주세요.',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.model_key == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량 모델을 선택해주세요',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.model_level == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량 세부등급을 입력해주세요',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.car_number == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량등록번호를 입력해주세요',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.yearMonth == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '차량연식을 선택해주세요',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.mileage == 0) {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '주행거리를 입력해주세요.',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.puer == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '연료타입을 선택해주세요.',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.gear == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '변속기 타입을 선택해주세요.',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.color == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '색상을 선택해주세요.',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.address == '') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '휴대폰의 "위치서비스"를 켜고 차량위치를 입력해주세요.',
          buttons: ['확인']
        });
      alert.present();      
    } else if(this.repImgFileName === 'noImg' || this.registImgFileName === 'noImg') {
      let alert = this.alertCtrl.create({
          title: '알림',
          subTitle: '대표/등록증 이미지를 선택해주세요.',
          buttons: ['확인']
        });
      alert.present();
    } else {
        this.year = this.yearMonth.substr(0, 4);
        this.month = this.yearMonth.substr(5, 2);
      /*
      console.log('sale: ' +this.sale );
      console.log('maker: ' +this.maker );
      console.log('maker_key: ' +this.maker_key );
      console.log('model: ' +this.model );
      console.log('model_key: ' +this.model_key );
      console.log('model_sub_key: ' +this.model_sub_key);
      console.log('model_sub: ' +this.model_sub);
      console.log('email: ' +this.email);
      console.log('pw: ' +this.userInfoProvider.userProfile.u_pw);
      console.log('phone: ' +this.phone);
      console.log('gear: ' +this.gear );
      console.log('puer: ' +this.puer );
      console.log('car_number: ' +this.car_number );
      console.log('year: ' +this.year );
      console.log('month: ' +this.month );
      console.log('mileage: ' +this.mileage );
      console.log('price: ' +this.price );
      console.log('options: ' +this.options.toString() );      
      console.log('introduce: ' +this.introduce );
      console.log('additory_options: ' +this.additory_options );
      console.log('repair: ' +this.repair );
      console.log('replacement: ' +this.replacement );
      console.log('model_level: ' +this.model_level);
      console.log('address: ' +this.address );
      console.log('address_lat: ' +this.address_lat );
      console.log('address_lon: ' +this.address_lon );
      console.log('owner_name: ' +this.owner_name);
      console.log('color: ' +this.color );
      console.log('photo_main_order[]: ' +this.photo_main_order);
      console.log('photo_sub_order[]: ' +this.photo_sub_order);
      for (let i = 0; i < this.photo_sub.length ; ++i)
      {
        console.log('photo_sub['+i+']: '+this.photo_sub[i]+', otherImgFileName['+i+']: '+this.otherImgFileName[i]);
        console.log('photo_sub_order['+i+']: '+this.photo_sub_order[i]);
      }
      console.log('photo_delete: ' +this.photo_delete.toString());
      */
      let loading = this.loadingCtrl.create({
          spinner: 'bubbles',
          content: '차량 등록 중 입니다.'
      });
      loading.present();

      let body = new FormData();
      let xhr = new XMLHttpRequest();
      let env = this;

      ////////////////////////////////////////////////////////////////////////////////////
      body.append('sale', this.sale );
      body.append('model', this.model_key);
      body.append('model_sub', this.model_sub_key);
      body.append('email', this.userInfoProvider.userProfile.u_email);
      body.append('pw', this.userInfoProvider.userProfile.u_pw);
      body.append('phone', this.phone);
      body.append('gear', this.gear);
      body.append('puer', this.puer);
      body.append('car_number', this.car_number);
      body.append('year', this.year);
      body.append('month', this.month);
      body.append('mileage', String(this.mileage));
      body.append('price', String(this.price));
      body.append('options', this.options.toString());
      body.append('introduce', this.introduce);
      body.append('additory_options', this.additory_options );
      body.append('repair', this.repair );
      body.append('replacement', this.replacement );
      body.append('model_level', this.model_level);
      body.append('address', this.address);
      body.append('address_lat', String(this.address_lat));
      body.append('address_lon', String(this.address_lon));
      body.append('owner_name', this.owner_name);
      body.append('color', this.color );
      //////////////////////////////////////////////////////////////////////////////////////

      //메인 이미지
      if(this.repImgFileName != 'noChange') {
        body.append('photo_main[]',this.photo_main[0],this.repImgFileName);
        body.append('photo_main_order[]', '1');
      }
      if(this.registImgFileName != 'noChange') {
        body.append('photo_main[]',this.photo_main[1],this.registImgFileName);
        body.append('photo_main_order[]', '2');
      }

      //기타 이미지
      for (let i = 0; i < this.photo_sub.length ; ++i) 
      {
        //if(this.otherImgFileName[i] != 'noChange' && this.subImgFlag[i] == true ) {
        if(this.subImgFlag[i] == true ) {
          body.append('photo_sub[]',this.photo_sub[i],this.otherImgFileName[i]);
          body.append('photo_sub_order[]', this.photo_sub_order[i]);
        }
      }
      body.append('photo_delete', this.photo_delete.toString());
      let url_s: any = this.marketURI + "set_sale_modify.php";

      xhr.open("POST",url_s,true);
      xhr.send(body);

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {  
            loading.dismiss();
            //alert(xhr.responseText); //테스트를 위해 alert으로 바꿨습니다.
            let response = JSON.parse(xhr.responseText);  //xhr.responseText{"result":"error_no_parameter"}
            if(response.result == 'ok') {
                let alert = env.alertCtrl.create({
                  title: '알림',
                  subTitle: '차량정보 수정이 완료되었습니다.',
                  buttons: ['확인']
                });
                alert.present();
                let result = true;
                env.viewCtrl.dismiss(result);
              } else {
                let alert = env.alertCtrl.create({
                  title: '알림',
                  subTitle: env.msgService.getMessage(response.result),
                  buttons: ['확인']
                });
                alert.present();
              }

          } else {
            alert(xhr.readyState);
          }
        } // end of if (xhr.readyState == 4)
      } // end of xhr.onreadystatechange = function ()
      
    } // else of check image
  } // end of submitRegiste()
  
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
