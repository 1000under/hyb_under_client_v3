import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateMyCarPage } from './update-my-car';

@NgModule({
  declarations: [
    UpdateMyCarPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateMyCarPage),
  ],
  exports: [
    UpdateMyCarPage
  ]
})
export class UpdateMyCarPageModule {}
